package com.calkasoftware.rs5.plugin.rs4;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.calkasoftware.rs5.plugin.rs4.model.Sample;
import com.calkasoftware.rs5.plugin.rs4.model.Sensor;

import rs4.core.sensors.SensorsCriteria;
import rs4.database.dao.RSAbstractDao;
import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;
import rs4.utils.Logger;

@SuppressWarnings("unchecked")
public class SensorDao extends RSAbstractDao {

	private static SensorDao instance;

	static Logger logger = Logger.getLogger(SensorDao.class.getName());

	public static final SensorDao getInstance() {
		if (instance == null) {
			instance = new SensorDao("com/calkasoftware/rs5/plugin/rs4/db.cfg.xml");
		}
		return instance;
	}

	private SensorDao(final String config) {
		super(config);
	}

	public List<Sample> getSamplesByCriteria(SensorsCriteria queryCriteria) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(Sample.class);
		String deviceId = queryCriteria.getDeviceId();
		if (!StringUtils.isEmpty(deviceId) && (!deviceId.equals(DisplayUtils.KEYWORD_ALL))) {
			criteria.add(Restrictions.eq("deviceId", deviceId));
		}
		String dateFrom = queryCriteria.getDateFrom();
		if (!StringUtils.isEmpty(dateFrom)) {
			criteria.add(Restrictions.ge("published", DateUtils.getDateFromTimestamp(dateFrom)));
		}
		String dateTo = queryCriteria.getDateTo();
		if (!StringUtils.isEmpty(dateTo)) {
			criteria.add(Restrictions.le("published", DateUtils.getDateToTimestamp(dateTo)));
		}
		criteria.addOrder(Order.desc("published"));
		List<Sample> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public List<Sensor> getSensorsByAccountId(String accountId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(Sensor.class);
		criteria.add(Restrictions.eq("accountId", accountId));
		criteria.addOrder(Order.asc("name"));
		List<Sensor> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

}
