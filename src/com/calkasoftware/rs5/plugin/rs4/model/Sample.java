package com.calkasoftware.rs5.plugin.rs4.model;

import java.sql.Timestamp;
import java.util.Date;

import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;

public class Sample {

	private String id;
	private String deviceId;
	private Double temperature;
	private Double humidity;
	private Double voltage;
	private Double signalStrength;
	private Timestamp published;
	
	private String temperatureDisplay;
	private String humidityDisplay;
	private String voltageDisplay;
	private String signalStrengthDisplay;
	private String publishedDisplay;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public Double getTemperature() {
		return temperature;
	}
	public void setTemperature(Double temperature) {
		setTemperatureDisplay(DisplayUtils.formatTemperatureZero(Double.valueOf(temperature).floatValue(), true, true));
		this.temperature = temperature;
	}
	public String getTemperatureDisplay() {
		return temperatureDisplay;
	}

	public void setTemperatureDisplay(String temperatureDisplay) {
		this.temperatureDisplay = temperatureDisplay;
	}
	public Double getHumidity() {
		return humidity;
	}
	public void setHumidity(Double humidity) {
		setHumidityDisplay(DisplayUtils.formatPercentageZero(Double.valueOf(humidity).floatValue(), true, true));
		this.humidity = humidity;
	}
	public Double getVoltage() {
		return voltage;
	}
	public void setVoltage(Double voltage) {
		setVoltageDisplay(DisplayUtils.formatVoltageZero(Double.valueOf(voltage).floatValue(), true, true));
		this.voltage = voltage;
	}
	public Double getSignalStrength() {
		return signalStrength;
	}
	public void setSignalStrength(Double signalStrength) {
		setSignalStrengthDisplay(DisplayUtils.formatPercentageZero(Double.valueOf(signalStrength).floatValue(), true, true));
		this.signalStrength = signalStrength;
	}
	public String getHumidityDisplay() {
		return humidityDisplay;
	}
	public void setHumidityDisplay(String humidityDisplay) {
		this.humidityDisplay = humidityDisplay;
	}
	public String getVoltageDisplay() {
		return voltageDisplay;
	}
	public void setVoltageDisplay(String voltageDisplay) {
		this.voltageDisplay = voltageDisplay;
	}
	public String getSignalStrengthDisplay() {
		return signalStrengthDisplay;
	}
	public void setSignalStrengthDisplay(String signalStrengthDisplay) {
		this.signalStrengthDisplay = signalStrengthDisplay;
	}
	public Timestamp getPublished() {
		return published;
	}
	public void setPublished(Timestamp published) {
		Date date = new Date(published.getTime());
		this.setPublishedDisplay(DateUtils.formatDateTime(date));
		this.published = published;
	}
	public String getPublishedDisplay() {
		return publishedDisplay;
	}
	public void setPublishedDisplay(String publishedDisplay) {
		this.publishedDisplay = publishedDisplay;
	}

}
