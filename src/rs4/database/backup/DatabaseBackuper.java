package rs4.database.backup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

import org.springframework.stereotype.Service;

import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;
import rs4.utils.Logger;

@Service("DatabaseBackuper")
public class DatabaseBackuper {

	public static final String BACKUP_SCRIPT = "C:\\rs4\\backup\\backup.bat";
	
	public static final String DB_SNAPSHOTS_DIR = "C:\\rs4\\backup\\snapshots\\";
	
	static Logger logger = Logger.getLogger(DatabaseBackuper.class.getName());

	public DatabaseBackuper() {
	}

	public synchronized void executeBackup() {
		long before = System.currentTimeMillis();
		try {
			String actionId = DisplayUtils.getActionId();
			logger.info("Starting database backup action @", actionId);
			runCloudBackup(buildBackupFile());
			long after = System.currentTimeMillis();
			logger.info("Database backup action @ took @ millis", actionId, after - before);
		} catch (Exception e) {
			logger.error("Database backup problem: @", e.getMessage());
			e.printStackTrace();
		}
	}

	private String buildBackupFile() throws IOException, InterruptedException {
		String backupFileName = buildBackupFileName(); 
		ProcessBuilder processBuilder = new ProcessBuilder(BACKUP_SCRIPT, backupFileName);
		final Process process = processBuilder.start();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String outputLine = new String();
		while ((outputLine = bufferedReader.readLine()) != null) {
			logger.info("SHELL> @", outputLine);
		}
		return backupFileName;
	}

	private void runCloudBackup(final String backupFile) {
		DropboxUploader dropboxUploader = new DropboxUploader();
		dropboxUploader.upload(DB_SNAPSHOTS_DIR, backupFile);
	}
	
	private String buildBackupFileName() {
		return (new StringBuilder())
				.append("rs4db_")
				.append(DateUtils.formatBackupDate(new Date()))
				.append(".sql")
				.toString();
	}
	
}
