package rs4.database.backup;

import java.io.FileInputStream;
import java.io.InputStream;

import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;

import rs4.utils.DisplayUtils;
import rs4.utils.Logger;

public class DropboxUploader {

	public static final String APPLICATION_ID = "rs4-webapp";
	
	public static final String DB_SNAPSHOTS_DIR = "/snapshots/";
	
	public static final String ACCESS_TOKEN = "IBDuzb-xUh8AAAAAAAAfD7aQpTD_gA3zkEGgFkVCG9ggFOpSbhDF0Ej8BYnwA6ka";
	
	static Logger logger = Logger.getLogger(DropboxUploader.class.getName());

	public DropboxUploader() {
	}

	public synchronized void upload(final String path, final String file) {
		long before = System.currentTimeMillis();
		try {
			String actionId = DisplayUtils.getActionId();
			logger.info("Starting Dropbox upload action @ with file: @", actionId, file);
			executeUpload(path, file);
			long after = System.currentTimeMillis();
			logger.info("Dropbox upload action @ took @ millis", actionId, after - before);
		} catch (Exception e) {
			logger.error("Dropbox upload problem: @", e.getMessage());
			e.printStackTrace();
		}
	}

	private void executeUpload(final String path, final String file) throws Exception {
		DbxRequestConfig config = DbxRequestConfig.newBuilder(APPLICATION_ID).build();
		DbxClientV2 client = new DbxClientV2(config, ACCESS_TOKEN);
		try (InputStream in = new FileInputStream(path + file)) {
			client.files().uploadBuilder(DB_SNAPSHOTS_DIR + file).uploadAndFinish(in);
		}
	}

}
