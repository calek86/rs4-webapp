package rs4.database.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.util.CollectionUtils;

import rs4.database.dao.model.RSDestination;
import rs4.database.dao.model.RSTransaction;
import rs4.utils.Logger;

@SuppressWarnings("unchecked")
public class RSDestinationDao extends RSAbstractDao {

	private static RSDestinationDao instance;

	static Logger logger = Logger.getLogger(RSDestinationDao.class.getName());

	public static final RSDestinationDao getInstance() {
		if (instance == null) {
			instance = new RSDestinationDao("rs4.mysql.cfg.xml");
		}
		return instance;
	}

	private RSDestinationDao(final String config) {
		super(config);
	}

	public List<RSDestination> getRSDestinations() {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSDestination.class);
		criteria.addOrder(Order.asc("code"));
		List<RSDestination> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public List<RSDestination> getRSDestinationsFromStockYear(int stockYearId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria transactionCriteria = session.createCriteria(RSTransaction.class);
		transactionCriteria.add(Restrictions.eq("stockYearId", stockYearId));
		transactionCriteria.addOrder(Order.asc("destinationId"));
		transactionCriteria.setProjection(Projections.distinct(Projections.property("destinationId")));
		List<Integer> destinationIds = transactionCriteria.list();
		if(CollectionUtils.isEmpty(destinationIds)) {
			return new ArrayList<RSDestination>();
		}
		Criteria destinationCriteria = session.createCriteria(RSDestination.class);
		destinationCriteria.add(Restrictions.in("id", destinationIds));
		destinationCriteria.addOrder(Order.asc("code"));
		List<RSDestination> result = destinationCriteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	public RSDestination getRSDestinationByCode(String code) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSDestination.class);
		criteria.add(Restrictions.eq("code", code));
		RSDestination result = (RSDestination) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public void insertRSDestinationFromParams(String weighmanId, String code, String description) {
		RSDestination rsDestination = new RSDestination();
		rsDestination.setWeighmanId(weighmanId);
		rsDestination.setCode(code);
		rsDestination.setDescription(description);
		insert(rsDestination);
	}
	
}
