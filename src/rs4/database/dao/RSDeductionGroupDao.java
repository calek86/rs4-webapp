package rs4.database.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import rs4.database.dao.model.RSDeductionGroup;
import rs4.utils.Logger;

@SuppressWarnings("unchecked")
public class RSDeductionGroupDao extends RSAbstractDao {

	private static RSDeductionGroupDao instance;

	static Logger logger = Logger.getLogger(RSDeductionGroupDao.class.getName());

	public static final RSDeductionGroupDao getInstance() {
		if (instance == null) {
			instance = new RSDeductionGroupDao("rs4.mysql.cfg.xml");
		}
		return instance;
	}

	private RSDeductionGroupDao(final String config) {
		super(config);
	}

	public List<RSDeductionGroup> getRSDeductionGroups() {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSDeductionGroup.class);
		List<RSDeductionGroup> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public RSDeductionGroup getRSDeductionGroupById(int id) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSDeductionGroup.class);
		criteria.add(Restrictions.eq("id", id));
		RSDeductionGroup result = (RSDeductionGroup) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public RSDeductionGroup getRSDeductionGroupByCode(String deductionGroupCode) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSDeductionGroup.class);
		criteria.add(Restrictions.eq("code", deductionGroupCode));
		RSDeductionGroup result = (RSDeductionGroup) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
}
