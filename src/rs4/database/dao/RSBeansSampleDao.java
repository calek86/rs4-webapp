package rs4.database.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import rs4.core.beans.BeansCriteria;
import rs4.database.dao.model.RSBeansSample;
import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;
import rs4.utils.Logger;

@SuppressWarnings("unchecked")
public class RSBeansSampleDao extends RSAbstractDao {

	private static RSBeansSampleDao instance;

	static Logger logger = Logger.getLogger(RSBeansSampleDao.class.getName());

	public static final RSBeansSampleDao getInstance() {
		if (instance == null) {
			instance = new RSBeansSampleDao("rs4.mysql.cfg.xml");
		}
		return instance;
	}

	private RSBeansSampleDao(final String config) {
		super(config);
	}

	public List<RSBeansSample> getRSBeansSamples() {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSBeansSample.class);
		//criteria.addOrder(Order.asc("code"));
		List<RSBeansSample> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public List<RSBeansSample> getRSBeansSampleFromStockYear(int stockYearId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSBeansSample.class);
		criteria.add(Restrictions.eq("stockYearId", stockYearId));
		List<RSBeansSample> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public List<RSBeansSample> getRSSBeansSampleByCriteria(BeansCriteria queryCriteria) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSBeansSample.class);
		String transactionsNo = queryCriteria.getTransactionsNo();
		if (!StringUtils.isEmpty(transactionsNo)) {
			String[] tokens = transactionsNo.split("[ ]+");
			Disjunction disjunction = Restrictions.disjunction();
			Criterion criterion;
			for (int i = 0; i < tokens.length; i++) {
				try {
					criterion = Restrictions.eq("id", Integer.parseInt(tokens[i]));
					disjunction.add(criterion);
				}
				catch(NumberFormatException nfe) {
					logger.error("Cannot read beans sample no: @", tokens[i]);
				}
			}
			criteria.add(disjunction);
		} else {
			String customerCode = queryCriteria.getCustomerCode();
			if (!StringUtils.isEmpty(customerCode) && (!customerCode.equals(DisplayUtils.KEYWORD_ALL))) {
				criteria.add(Restrictions.eq("customer", customerCode));
			}
			String productCode = queryCriteria.getProductCode();
			if (!StringUtils.isEmpty(productCode) && (!productCode.equals(DisplayUtils.KEYWORD_ALL))) {
				criteria.add(Restrictions.eq("product", productCode));
			}
			criteria.add(Restrictions.like("product", "beans", MatchMode.ANYWHERE));
			int stockYearId = queryCriteria.getStockYearId();
			if ((stockYearId != -1)) {
				criteria.add(Restrictions.eq("stockYearId", stockYearId));
			}
			String dateFrom = queryCriteria.getDateFrom();
			if (!StringUtils.isEmpty(dateFrom)) {
				criteria.add(Restrictions.ge("sampleDate", DateUtils.getDateFromTimestamp(dateFrom)));
			}
			String dateTo = queryCriteria.getDateTo();
			if (!StringUtils.isEmpty(dateTo)) {
				criteria.add(Restrictions.le("sampleDate", DateUtils.getDateToTimestamp(dateTo)));
			}	
		}
		criteria.addOrder(Order.asc("sampleDate"));
		List<RSBeansSample> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	public RSBeansSample getRSBeansSampleById(int id) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSBeansSample.class);
		criteria.add(Restrictions.eq("id", id));
		RSBeansSample result = (RSBeansSample) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	public void deleteBeansSampleById(int id) {
		RSBeansSample rsBeansSample = getRSBeansSampleById(id);
		if(rsBeansSample != null) {
			delete(rsBeansSample);
		}
	}

}
