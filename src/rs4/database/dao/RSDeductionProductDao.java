package rs4.database.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.util.CollectionUtils;

import rs4.database.dao.model.RSDeductionProduct;
import rs4.utils.Logger;

@SuppressWarnings("unchecked")
public class RSDeductionProductDao extends RSAbstractDao {

	private static RSDeductionProductDao instance;

	static Logger logger = Logger.getLogger(RSDeductionProductDao.class.getName());

	public static final RSDeductionProductDao getInstance() {
		if (instance == null) {
			instance = new RSDeductionProductDao("rs4.mysql.cfg.xml");
		}
		return instance;
	}

	private RSDeductionProductDao(final String config) {
		super(config);
	}

	public List<RSDeductionProduct> getRSDeductionProducts() {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSDeductionProduct.class);
		List<RSDeductionProduct> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public List<RSDeductionProduct> getRSDeductionProductsByGroupId(int groupId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSDeductionProduct.class);
		criteria.add(Restrictions.eq("groupId", groupId));
		List<RSDeductionProduct> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	public RSDeductionProduct getRSDeductionProductById(int id) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSDeductionProduct.class);
		criteria.add(Restrictions.eq("id", id));
		RSDeductionProduct result = (RSDeductionProduct) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public RSDeductionProduct getRSDeductionProductByGroupIdAndProductId(int groupId, int productId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSDeductionProduct.class);
		criteria.add(Restrictions.eq("groupId", groupId));
		criteria.add(Restrictions.eq("productId", productId));
		RSDeductionProduct result = (RSDeductionProduct) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public int getRSDeductionGroupIdByProductId(int productId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSDeductionProduct.class);
		criteria.add(Restrictions.eq("productId", productId));
		List<RSDeductionProduct> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		if(!CollectionUtils.isEmpty(result)) {
			return result.get(0).getGroupId();
		}
		return -1;
	}
	
	public void deleteRSDeductionProductByGroupIdAndProductId(int groupId, int productId) {
		RSDeductionProduct rsDeductionProduct = getRSDeductionProductByGroupIdAndProductId(groupId, productId);
		if (rsDeductionProduct != null) {
			delete(rsDeductionProduct);
		}
	}
	
}
