package rs4.database.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.util.CollectionUtils;

import rs4.database.dao.model.RSCustomer;
import rs4.database.dao.model.RSTransaction;
import rs4.utils.Logger;

@SuppressWarnings("unchecked")
public class RSCustomerDao extends RSAbstractDao {

	private static RSCustomerDao instance;

	static Logger logger = Logger.getLogger(RSCustomerDao.class.getName());

	public static final RSCustomerDao getInstance() {
		if (instance == null) {
			instance = new RSCustomerDao("rs4.mysql.cfg.xml");
		}
		return instance;
	}

	private RSCustomerDao(final String config) {
		super(config);
	}

	public List<RSCustomer> getRSCustomers() {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSCustomer.class);
		criteria.addOrder(Order.asc("code"));
		List<RSCustomer> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public List<RSCustomer> getRSCustomersFromStockYear(int stockYearId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria transactionCriteria = session.createCriteria(RSTransaction.class);
		transactionCriteria.add(Restrictions.eq("stockYearId", stockYearId));
		transactionCriteria.addOrder(Order.asc("customerId"));
		transactionCriteria.setProjection(Projections.distinct(Projections.property("customerId")));
		List<Integer> customerIds = transactionCriteria.list();
		if(CollectionUtils.isEmpty(customerIds)) {
			return new ArrayList<RSCustomer>();
		}
		Criteria customerCriteria = session.createCriteria(RSCustomer.class);
		customerCriteria.add(Restrictions.in("id", customerIds));
		customerCriteria.addOrder(Order.asc("code"));
		List<RSCustomer> result = customerCriteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public RSCustomer getRSCustomerById(int customerId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSCustomer.class);
		criteria.add(Restrictions.eq("id", customerId));
		RSCustomer result = (RSCustomer) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	public RSCustomer getRSCustomerByCode(String code) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSCustomer.class);
		criteria.add(Restrictions.eq("code", code));
		RSCustomer result = (RSCustomer) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public void insertRSCustomerFromParams(String weighmanId, String code, String description) {
		RSCustomer rsCustomer = new RSCustomer();
		rsCustomer.setWeighmanId(weighmanId);
		rsCustomer.setCode(code);
		rsCustomer.setDescription(description);
		insert(rsCustomer);
	}

}
