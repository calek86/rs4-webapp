package rs4.database.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import rs4.database.dao.model.RSTransactionTrash;
import rs4.utils.Logger;

@SuppressWarnings("unchecked")
public class RSTransactionTrashDao extends RSAbstractDao {

	private static RSTransactionTrashDao instance;

	static Logger logger = Logger.getLogger(RSTransactionTrashDao.class.getName());

	public static final RSTransactionTrashDao getInstance() {
		if (instance == null) {
			instance = new RSTransactionTrashDao("rs4.mysql.cfg.xml");
		}
		return instance;
	}

	private RSTransactionTrashDao(final String config) {
		super(config);
	}

	public List<RSTransactionTrash> getRSTransactionTrashes() {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSTransactionTrash.class);
		List<RSTransactionTrash> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public RSTransactionTrash getRSTransactionTrashByTicketId(String ticketId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSTransactionTrash.class);
		criteria.add(Restrictions.eq("ticketId", ticketId));
		RSTransactionTrash result = (RSTransactionTrash) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
}
