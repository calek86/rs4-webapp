package rs4.database.dao;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistryBuilder;

public abstract class RSAbstractDao {

	private String config;
	
	private static Map<String, SessionFactory> sessionFactories = new HashMap<>();

	protected RSAbstractDao(final String config) {
		this.config = config;
		configureSessionFactory();
	}

	public Session getSession() {
		return sessionFactories.get(config).openSession();
	}

	public void insert(Object rsObject) {
		Session session = getSession();
		session.beginTransaction();
		session.save(rsObject);
		session.getTransaction().commit();
		session.close();
	}

	public void update(Object rsObject) {
		Session session = getSession();
		session.beginTransaction();
		session.update(rsObject);
		session.getTransaction().commit();
		session.close();
	}

	public void insertOrUpdate(Object rsObject) {
		Session session = getSession();
		session.beginTransaction();
		session.saveOrUpdate(rsObject);
		session.getTransaction().commit();
		session.close();
	}

	public void delete(Object rsObject) {
		Session session = getSession();
		session.beginTransaction();
		session.delete(rsObject);
		session.getTransaction().commit();
		session.close();
	}

	private void configureSessionFactory() throws HibernateException {
		Configuration configuration = new Configuration();
		configuration.configure(config);
		sessionFactories.put(config, configuration.buildSessionFactory(
				new ServiceRegistryBuilder().applySettings(
						configuration.getProperties()).buildServiceRegistry()));
	}

}
