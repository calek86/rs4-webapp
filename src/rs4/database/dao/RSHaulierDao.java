package rs4.database.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.util.CollectionUtils;

import rs4.database.dao.model.RSHaulier;
import rs4.database.dao.model.RSTransaction;
import rs4.utils.Logger;

@SuppressWarnings("unchecked")
public class RSHaulierDao extends RSAbstractDao {

	private static RSHaulierDao instance;

	static Logger logger = Logger.getLogger(RSHaulierDao.class.getName());

	public static final RSHaulierDao getInstance() {
		if (instance == null) {
			instance = new RSHaulierDao("rs4.mysql.cfg.xml");
		}
		return instance;
	}

	private RSHaulierDao(final String config) {
		super(config);
	}

	public List<RSHaulier> getRSHauliers() {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSHaulier.class);
		criteria.addOrder(Order.asc("code"));
		List<RSHaulier> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public List<RSHaulier> getRSHauliersFromStockYear(int stockYearId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria transactionCriteria = session.createCriteria(RSTransaction.class);
		transactionCriteria.add(Restrictions.eq("stockYearId", stockYearId));
		transactionCriteria.addOrder(Order.asc("haulierId"));
		transactionCriteria.setProjection(Projections.distinct(Projections.property("haulierId")));
		List<Integer> haulierIds = transactionCriteria.list();
		if(CollectionUtils.isEmpty(haulierIds)) {
			return new ArrayList<RSHaulier>();
		}
		Criteria haulierCriteria = session.createCriteria(RSHaulier.class);
		haulierCriteria.add(Restrictions.in("id", haulierIds));
		haulierCriteria.addOrder(Order.asc("code"));
		List<RSHaulier> result = haulierCriteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public RSHaulier getRSHaulierByCode(String code) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSHaulier.class);
		criteria.add(Restrictions.eq("code", code));
		RSHaulier result = (RSHaulier) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public void insertRSHaulierFromParams(String weighmanId, String code, String description) {
		RSHaulier rsHaulier = new RSHaulier();
		rsHaulier.setWeighmanId(weighmanId);
		rsHaulier.setCode(code);
		rsHaulier.setDescription(description);
		insert(rsHaulier);
	}

}
