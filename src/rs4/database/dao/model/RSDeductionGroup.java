package rs4.database.dao.model;

import java.util.List;

public class RSDeductionGroup {

	private int id;
	private String code;
	private String description;
	private String regex;
	
	private List<RSDeduction> deductions;
	private List<RSProduct> products;

	public RSDeductionGroup() {
	}

	public RSDeductionGroup(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}

	public List<RSDeduction> getDeductions() {
		return deductions;
	}

	public void setDeductions(List<RSDeduction> deductions) {
		this.deductions = deductions;
	}

	public List<RSProduct> getProducts() {
		return products;
	}

	public void setProducts(List<RSProduct> products) {
		this.products = products;
	}

}
