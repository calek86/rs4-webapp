package rs4.database.dao.model;

import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import rs4.core.beans.form.BeansInForm;
import rs4.core.beans.form.BeansOutForm;
import rs4.core.search.TransactionType;
import rs4.core.search.form.TransactionForm;
import rs4.utils.DataUtils;
import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;

public class RSTransaction {

	private int id;
	private String weighmanId;
	private String ticketId;
	private String transactionType;

	private String registration;
	private Timestamp transactionDate;
	private Timestamp modificationDate;
	private String transactionDateDisplay;

	private int customerId;
	private String customer;
	private int haulierId;
	private String haulier;
	private int productId;
	private String product;
	private int destinationId;
	private String destination;
	private int sourceId;
	private String source;
	private int stockYearId;
	private String stockYear;

	private float firstWeight;
	private String firstWeightDisplay;
	private float secondWeight;
	private String secondWeightDisplay;
	private float netWeight;
	private String netWeightDisplay;
	private float entWeight;
	private String entWeightDisplay;
	private float entWeightBeans;
	private String entWeightBeansDisplay;
	private float percentLoss;
	private String percentLossDisplay;
	private float percentLossBeans;
	private String percentLossBeansDisplay;

	private float moisture;
	private String moistureDisplay;
	private float admix;
	private String admixDisplay;
	private float spWt;
	private String spWtDisplay;
	private float temperature;
	private String temperatureDisplay;

	private String screen1;
	private String screen2;
	private String orderNo;
	private String exRef;
	private String n2CP;
	private String hagberg;
	private String firstPrevLoad;
	private String secondPrevLoad;
	private String thirdPrevLoad;
	private String trailerNo;
	private String inspected;
	private String aCCSNo;
	private String holed;
	private String blind;
	private String stains;
	private String comments;

	public String getSecondWeightDisplay() {
		return secondWeightDisplay;
	}

	public void setSecondWeightDisplay(String secondWeightDisplay) {
		this.secondWeightDisplay = secondWeightDisplay;
	}

	public String getNetWeightDisplay() {
		return netWeightDisplay;
	}

	public void setNetWeightDisplay(String netWeightDisplay) {
		this.netWeightDisplay = netWeightDisplay;
	}

	public String getEntWeightDisplay() {
		return entWeightDisplay;
	}

	public void setEntWeightDisplay(String entWeightDisplay) {
		this.entWeightDisplay = entWeightDisplay;
	}

	public String getEntWeightBeansDisplay() {
		return entWeightBeansDisplay;
	}

	public void setEntWeightBeansDisplay(String entWeightBeansDisplay) {
		this.entWeightBeansDisplay = entWeightBeansDisplay;
	}

	public String getPercentLossDisplay() {
		return percentLossDisplay;
	}

	public void setPercentLossDisplay(String percentLossDisplay) {
		this.percentLossDisplay = percentLossDisplay;
	}

	public String getPercentLossBeansDisplay() {
		return percentLossBeansDisplay;
	}

	public void setPercentLossBeansDisplay(String percentLossBeansDisplay) {
		this.percentLossBeansDisplay = percentLossBeansDisplay;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getWeighmanId() {
		return weighmanId;
	}

	public void setWeighmanId(String weighmanId) {
		this.weighmanId = weighmanId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Timestamp getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Timestamp transactionDate) {
		Date date = new Date(transactionDate.getTime());
		this.setTransactionDateDisplay(DateUtils.formatDateTime(date));
		this.transactionDate = transactionDate;
	}

	public Timestamp getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Timestamp modificationDate) {
		this.modificationDate = modificationDate;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getHaulier() {
		return haulier;
	}

	public void setHaulier(String haulier) {
		this.haulier = haulier;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public float getFirstWeight() {
		return firstWeight;
	}

	public void setFirstWeight(float firstWeight) {
		setFirstWeightDisplay(DisplayUtils.formatWeight(firstWeight, true));
		this.firstWeight = firstWeight;
	}

	public float getSecondWeight() {
		return secondWeight;
	}

	public void setSecondWeight(float secondWeight) {
		setSecondWeightDisplay(DisplayUtils.formatWeight(secondWeight, true));
		this.secondWeight = secondWeight;
	}

	public String getMoistureDisplay() {
		return moistureDisplay;
	}

	public void setMoistureDisplay(String moistureDisplay) {
		this.moistureDisplay = moistureDisplay;
	}

	public String getAdmixDisplay() {
		return admixDisplay;
	}

	public void setAdmixDisplay(String admixDisplay) {
		this.admixDisplay = admixDisplay;
	}

	public String getSpWtDisplay() {
		return spWtDisplay;
	}

	public void setSpWtDisplay(String spWtDisplay) {
		this.spWtDisplay = spWtDisplay;
	}

	public String getTemperatureDisplay() {
		return temperatureDisplay;
	}

	public void setTemperatureDisplay(String temperatureDisplay) {
		this.temperatureDisplay = temperatureDisplay;
	}

	public float getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(float netWeight) {
		setNetWeightDisplay(DisplayUtils.formatWeight(netWeight, true));
		this.netWeight = netWeight;
	}

	public float getEntWeight() {
		return entWeight;
	}

	public void setEntWeight(float entWeight) {
		setEntWeightDisplay(DisplayUtils.formatWeight(entWeight, true));
		this.entWeight = entWeight;
	}

	public float getPercentLoss() {
		return percentLoss;
	}

	public void setPercentLoss(float percentLoss) {
		setPercentLossDisplay(DisplayUtils.formatPercentage(percentLoss, true));
		this.percentLoss = percentLoss;
	}

	public float getMoisture() {
		return moisture;
	}

	public void setMoisture(float moisture) {
		setMoistureDisplay(DisplayUtils.formatPercentage(moisture, true));
		this.moisture = moisture;
	}

	public float getAdmix() {
		return admix;
	}

	public void setAdmix(float admix) {
		setAdmixDisplay(DisplayUtils.formatPercentage(admix, true));
		this.admix = admix;
	}

	public float getSpWt() {
		return spWt;
	}

	public void setSpWt(float spWt) {
		setSpWtDisplay(DisplayUtils.formatSpecificWeight(spWt, true));
		this.spWt = spWt;
	}

	public float getTemperature() {
		return temperature;
	}

	public void setTemperature(float temperature) {
		setTemperatureDisplay(DisplayUtils.formatTemperature(temperature, true));
		this.temperature = temperature;
	}

	public String getScreen1() {
		return screen1;
	}

	public void setScreen1(String screen1) {
		this.screen1 = DisplayUtils.formatTextNumber(screen1);
	}

	public String getScreen2() {
		return screen2;
	}

	public void setScreen2(String screen2) {
		this.screen2 = DisplayUtils.formatTextNumber(screen2);
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getExRef() {
		return exRef;
	}

	public void setExRef(String exRef) {
		this.exRef = exRef;
	}

	public String getN2CP() {
		return n2CP;
	}

	public void setN2CP(String n2cp) {
		this.n2CP = DisplayUtils.formatTextNumber(n2cp);
	}

	public String getHagberg() {
		return hagberg;
	}

	public void setHagberg(String hagberg) {
		this.hagberg = DisplayUtils.formatTextNumber(hagberg);
	}

	public String getFirstPrevLoad() {
		return firstPrevLoad;
	}

	public void setFirstPrevLoad(String firstPrevLoad) {
		this.firstPrevLoad = firstPrevLoad;
	}

	public String getSecondPrevLoad() {
		return secondPrevLoad;
	}

	public void setSecondPrevLoad(String secondPrevLoad) {
		this.secondPrevLoad = secondPrevLoad;
	}

	public String getThirdPrevLoad() {
		return thirdPrevLoad;
	}

	public void setThirdPrevLoad(String thirdPrevLoad) {
		this.thirdPrevLoad = thirdPrevLoad;
	}

	public String getTrailerNo() {
		return trailerNo;
	}

	public void setTrailerNo(String trailerNo) {
		this.trailerNo = trailerNo;
	}

	public String getInspected() {
		return inspected;
	}

	public void setInspected(String inspected) {
		this.inspected = inspected;
	}

	public String getaCCSNo() {
		return aCCSNo;
	}

	public void setaCCSNo(String aCCSNo) {
		this.aCCSNo = aCCSNo;
	}

	public String getHoled() {
		return holed;
	}

	public void setHoled(String holed) {
		this.holed = DisplayUtils.formatTextNumber(holed);
	}

	public String getBlind() {
		return blind;
	}

	public void setBlind(String blind) {
		this.blind = DisplayUtils.formatTextNumber(blind);
	}

	public String getStains() {
		return stains;
	}

	public void setStains(String stains) {
		this.stains = DisplayUtils.formatTextNumber(stains);
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getHaulierId() {
		return haulierId;
	}

	public void setHaulierId(int haulierId) {
		this.haulierId = haulierId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(int destinationId) {
		this.destinationId = destinationId;
	}

	public int getSourceId() {
		return sourceId;
	}

	public void setSourceId(int sourceId) {
		this.sourceId = sourceId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public int getStockYearId() {
		return stockYearId;
	}

	public void setStockYearId(int stockYearId) {
		this.stockYearId = stockYearId;
	}

	public String getStockYear() {
		return stockYear;
	}

	public void setStockYear(String stockYear) {
		this.stockYear = stockYear;
	}

	public float getEntWeightBeans() {
		return entWeightBeans;
	}

	public void setEntWeightBeans(float entWeightBeans) {
		if (DataUtils.isBeans(getProduct()) && equalsType(TransactionType.IN)) {
			setEntWeightBeansDisplay(DisplayUtils.formatWeight(entWeightBeans, true));
		}
		this.entWeightBeans = entWeightBeans;
	}

	public float getPercentLossBeans() {
		return percentLossBeans;
	}

	public void setPercentLossBeans(float percentLossBeans) {
		if (DataUtils.isBeans(getProduct()) && equalsType(TransactionType.IN)) {
			setPercentLossBeansDisplay(DisplayUtils.formatPercentage(percentLossBeans, true));
		}
		this.percentLossBeans = percentLossBeans;
	}

	public String getTransactionDateDisplay() {
		return transactionDateDisplay;
	}

	public void setTransactionDateDisplay(String transactionDateDisplay) {
		this.transactionDateDisplay = transactionDateDisplay;
	}

	public String getFirstWeightDisplay() {
		return firstWeightDisplay;
	}

	public void setFirstWeightDisplay(String firstWeightDisplay) {
		this.firstWeightDisplay = firstWeightDisplay;
	}

	public boolean equalsType(TransactionType type) {
		if (StringUtils.isEmpty(transactionType) || type == null) {
			return false;
		}
		return transactionType.equalsIgnoreCase(type.toString());
	}

	public boolean isModified() {
		return !transactionDate.equals(modificationDate);
	}
	
	public TransactionForm toTransactionForm(boolean units) {
		TransactionForm form = new TransactionForm();
		form.setaCCSNo(this.getaCCSNo());
		form.setBlind(this.getBlind());
		form.setComments(this.getComments());
		form.setCurrentStockYear(this.getStockYear());
		form.setCustomer(this.getCustomer());
		form.setDestination(this.getDestination());
		form.setSource(this.getSource());		
		form.setExRef(this.getExRef());
		form.setFirstPrevLoad(this.getFirstPrevLoad());
		form.setHagberg(this.getHagberg());
		form.setHaulier(this.getHaulier());
		form.setHoled(this.getHoled());
		form.setInspected(this.getInspected());
		form.setN2CP(this.getN2CP());
		form.setOrderNo(this.getOrderNo());
		form.setProduct(this.getProduct());
		form.setRegistration(this.getRegistration());
		form.setScreen1(this.getScreen1());
		form.setScreen2(this.getScreen2());
		form.setSecondPrevLoad(this.getSecondPrevLoad());
		form.setStains(this.getStains());
		form.setThirdPrevLoad(this.getThirdPrevLoad());
		form.setTicketId(this.getTicketId());
		form.setTrailerNo(this.getTrailerNo());
		form.setTransactionType(this.getTransactionType());
		form.setWeighmanId(this.getWeighmanId());
		form.setId(this.getId());
		form.setTransactionDate(DateUtils.formatDateTime(this.getTransactionDate()));
		form.setTemperature(DisplayUtils.formatTemperature(this.getTemperature(), units));
		form.setSecondWeight(DisplayUtils.formatWeight(this.getSecondWeight(), units));
		form.setSpWt(DisplayUtils.formatSpecificWeight(this.getSpWt(), units));
		form.setPercentLoss(DisplayUtils.formatPercentage(this.getPercentLoss(), units));
		form.setPercentLossBeans(DisplayUtils.formatPercentage(this.getPercentLossBeans(), units));
		form.setNetWeight(DisplayUtils.formatWeight(this.getNetWeight(), units));
		form.setEntWeight(DisplayUtils.formatWeight(this.getEntWeight(), units));
		form.setEntWeightBeans(DisplayUtils.formatWeight(this.getEntWeightBeans(), units));
		form.setFirstWeight(DisplayUtils.formatWeight(this.getFirstWeight(), units));
		form.setMoisture(DisplayUtils.formatPercentage(this.getMoisture(), units));
		form.setAdmix(DisplayUtils.formatPercentage(this.getAdmix(), units));
		return form;
	}

	public BeansInForm toBeansInForm() {
		BeansInForm beansInForm = new BeansInForm();
		beansInForm.setTransaction(this);
		beansInForm.setDeduction(DataUtils.getTotalBeansDeductionFromTransaction(this));
		beansInForm.setFeed((beansInForm.getDeduction())*this.getEntWeight());
		beansInForm.setHead(this.getEntWeight()-beansInForm.getFeed());
		return beansInForm;
	}

	public BeansOutForm toBeansOutForm() {
		BeansOutForm beansOutForm = new BeansOutForm();
		beansOutForm.setTransaction(this);
		beansOutForm.setCulmTotal(this.getSecondWeight() + this.getMoisture());
		beansOutForm.setContainerTare(this.getMoisture());
		return beansOutForm;
	}
	
	public String toValidationString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(this.getWeighmanId());
		stringBuilder.append(this.getTicketId());
		stringBuilder.append(this.getTransactionType());
		stringBuilder.append(this.getTransactionDate());
		stringBuilder.append(this.getRegistration());
		stringBuilder.append(this.getFirstWeight());
		stringBuilder.append(this.getSecondWeight());
		stringBuilder.append(this.getNetWeight());
		stringBuilder.append(this.getOrderNo());
		stringBuilder.append(this.getExRef());
		stringBuilder.append(this.getFirstPrevLoad());
		stringBuilder.append(this.getSecondPrevLoad());
		stringBuilder.append(this.getThirdPrevLoad());
		stringBuilder.append(this.getTrailerNo());
		stringBuilder.append(this.getInspected());
		stringBuilder.append(this.getaCCSNo());
		stringBuilder.append(this.getComments());
		stringBuilder.append(this.getMoisture());
		stringBuilder.append(this.getAdmix());
		stringBuilder.append(this.getSpWt());
		stringBuilder.append(this.getTemperature());
		stringBuilder.append(this.getScreen1());
		stringBuilder.append(this.getScreen2());
		stringBuilder.append(this.getN2CP());
		stringBuilder.append(this.getHagberg());
		stringBuilder.append(this.getHoled());
		stringBuilder.append(this.getBlind());
		stringBuilder.append(this.getStains());
		return stringBuilder.toString();
	}
	
	@Override
	public String toString() {
		return DisplayUtils.buildToSting(Integer.toString(id), weighmanId, ticketId, transactionType, registration,
				DateUtils.formatDate(transactionDate), Integer.toString(stockYearId), Integer.toString(customerId),
				Integer.toString(haulierId), Integer.toString(productId), Integer.toString(destinationId),
				Float.toString(firstWeight), Float.toString(secondWeight), Float.toString(netWeight),
				Float.toString(entWeight), Float.toString(entWeightBeans), Float.toString(percentLoss),
				Float.toString(percentLossBeans), Float.toString(moisture), Float.toString(admix), Float.toString(spWt),
				Float.toString(temperature));
	}

	public RSTransactionTrash toTrash() {
		RSTransactionTrash trash = new RSTransactionTrash();
		trash.setTicketId(this.getTicketId());
		trash.setTransactionDate(this.getTransactionDate());
		trash.setTransactionType(this.getTransactionType());
		trash.setWeighmanId(this.getWeighmanId());
		return trash;
	}

}
