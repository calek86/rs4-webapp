package rs4.database.dao.model;

import java.sql.Timestamp;
import java.util.Date;

import rs4.core.beans.form.BeansSampleForm;
import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;

public class RSBeansSample {

	private int id;
	private Timestamp sampleDate;
	private String sampleDateDisplay;
	private int customerId;
	private String customer;
	private String referenceNo;
	private int stockYearId;
	private String stockYear;
	private int productId;
	private String product;
	private float tonnage;
	private String tonnageDisplay;
	private String processable;
	private float moisture;
	private String moistureDisplay;
	private float admix;
	private String admixDisplay;
	private String kgHl;
	private float temperature;
	private String temperatureDisplay;
	private String screen1;
	private String screen2;
	private String holed;
	private String blind;
	private String stains;
	private String comments;

	public Timestamp getSampleDate() {
		return sampleDate;
	}

	public void setSampleDate(Timestamp sampleDate) {
		Date date = new Date(sampleDate.getTime());
		this.setSampleDateDisplay(DateUtils.formatDateTime(date));
		this.sampleDate = sampleDate;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public float getTonnage() {
		return tonnage;
	}

	public void setTonnage(float tonnage) {
		setTonnageDisplay(DisplayUtils.formatWeight(tonnage, true));
		this.tonnage = tonnage;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getMoistureDisplay() {
		return moistureDisplay;
	}

	public void setMoistureDisplay(String moistureDisplay) {
		this.moistureDisplay = moistureDisplay;
	}

	public String getAdmixDisplay() {
		return admixDisplay;
	}

	public void setAdmixDisplay(String admixDisplay) {
		this.admixDisplay = admixDisplay;
	}

	public String getTemperatureDisplay() {
		return temperatureDisplay;
	}

	public void setTemperatureDisplay(String temperatureDisplay) {
		this.temperatureDisplay = temperatureDisplay;
	}

	public float getMoisture() {
		return moisture;
	}

	public void setMoisture(float moisture) {
		setMoistureDisplay(DisplayUtils.formatPercentage(moisture, true));
		this.moisture = moisture;
	}

	public float getAdmix() {
		return admix;
	}

	public void setAdmix(float admix) {
		setAdmixDisplay(DisplayUtils.formatPercentage(admix, true));
		this.admix = admix;
	}

	public float getTemperature() {
		return temperature;
	}

	public void setTemperature(float temperature) {
		setTemperatureDisplay(DisplayUtils.formatTemperature(temperature, true));
		this.temperature = temperature;
	}

	public String getScreen1() {
		return screen1;
	}

	public void setScreen1(String screen1) {
		this.screen1 = DisplayUtils.formatTextNumber(screen1);
	}

	public String getScreen2() {
		return screen2;
	}

	public void setScreen2(String screen2) {
		this.screen2 = DisplayUtils.formatTextNumber(screen2);
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getKgHl() {
		return kgHl;
	}

	public void setKgHl(String kgHl) {
		this.kgHl = kgHl;
	}

	public String getProcessable() {
		return processable;
	}

	public void setProcessable(String processable) {
		this.processable = processable;
	}

	public String getHoled() {
		return holed;
	}

	public void setHoled(String holed) {
		this.holed = DisplayUtils.formatTextNumber(holed);
	}

	public String getBlind() {
		return blind;
	}

	public void setBlind(String blind) {
		this.blind = DisplayUtils.formatTextNumber(blind);
	}

	public String getStains() {
		return stains;
	}

	public void setStains(String stains) {
		this.stains = DisplayUtils.formatTextNumber(stains);
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getStockYearId() {
		return stockYearId;
	}

	public void setStockYearId(int stockYearId) {
		this.stockYearId = stockYearId;
	}

	public String getStockYear() {
		return stockYear;
	}

	public void setStockYear(String stockYear) {
		this.stockYear = stockYear;
	}

	public String getSampleDateDisplay() {
		return sampleDateDisplay;
	}

	public void setSampleDateDisplay(String sampleDateDisplay) {
		this.sampleDateDisplay = sampleDateDisplay;
	}

	public String getTonnageDisplay() {
		return tonnageDisplay;
	}

	public void setTonnageDisplay(String tonnageDisplay) {
		this.tonnageDisplay = tonnageDisplay;
	}

	public BeansSampleForm toBeansSampleForm(boolean units) {
		BeansSampleForm form = new BeansSampleForm();
		form.setId(this.getId());
		form.setCurrentStockYear(this.getStockYear());
		form.setProduct(this.getProduct());
		form.setCustomer(this.getCustomer());
		form.setTonnage(DisplayUtils.formatWeight(this.getTonnage(), units));
		form.setSampleDate(DateUtils.formatDateTime(this.getSampleDate()));
		form.setTemperature(DisplayUtils.formatTemperature(this.getTemperature(), units));
		form.setMoisture(DisplayUtils.formatPercentage(this.getMoisture(), units));
		form.setAdmix(DisplayUtils.formatPercentage(this.getAdmix(), units));
		form.setReferenceNo(this.getReferenceNo());
		form.setProcessable(this.getProcessable());
		form.setKgHl(this.getKgHl());
		form.setScreen1(this.getScreen1());
		form.setScreen2(this.getScreen2());
		form.setHoled(this.getHoled());
		form.setBlind(this.getBlind());
		form.setStains(this.getStains());
		form.setComments(this.getComments());
		return form;
	}
	
	@Override
	public String toString() {
		return DisplayUtils.buildToSting(Integer.toString(id), DateUtils.formatDate(sampleDate),
				Integer.toString(stockYearId), Integer.toString(customerId), Integer.toString(productId),
				Float.toString(tonnage), Float.toString(moisture), Float.toString(admix), Float.toString(temperature));
	}

}
