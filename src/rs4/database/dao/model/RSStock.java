package rs4.database.dao.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import rs4.utils.DisplayUtils;

public class RSStock {

	private int id;
	private int customerId;
	private String customerCode;
	private int productId;
	private String productCode;
	private int stockYearId;
	private String stockYearDescription;
	private float totalValue;
	private String totalValueDisplay;

	public RSStock() {
	}

	public RSStock(int customerId, String customerCode, int productId, String productCode, int stockYearId,
			String stockYearDescription, float totalValue) {
		this.setCustomerId(customerId);
		this.setCustomerCode(customerCode);
		this.setProductId(productId);
		this.setProductCode(productCode);
		this.setStockYearId(stockYearId);
		this.setStockYearDescription(stockYearDescription);
		this.setTotalValue(totalValue);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getStockYearId() {
		return stockYearId;
	}

	public void setStockYearId(int stockYearId) {
		this.stockYearId = stockYearId;
	}

	public float getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(float totalValue) {
		setTotalValueDisplay(DisplayUtils.formatWeightZero(totalValue, true, true));
		this.totalValue = totalValue;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getStockYearDescription() {
		return stockYearDescription;
	}

	public void setStockYearDescription(String stockYearDescription) {
		this.stockYearDescription = stockYearDescription;
	}

	public String getTotalValueDisplay() {
		return totalValueDisplay;
	}

	public void setTotalValueDisplay(String totalValueDisplay) {
		this.totalValueDisplay = totalValueDisplay;
	}

	public boolean isDenne() {
		if (getCustomerCode().contains("Denne")) {
			return true;
		}
		return false;
	}

	public static RSStock fromRSTransaction(RSTransaction rsTransaction) {
		RSStock rsStock = new RSStock();
		rsStock.setCustomerId(rsTransaction.getCustomerId());
		rsStock.setCustomerCode(rsTransaction.getCustomer());
		rsStock.setProductId(rsTransaction.getProductId());
		rsStock.setProductCode(rsTransaction.getProduct());
		rsStock.setStockYearId(rsTransaction.getStockYearId());
		rsStock.setStockYearDescription(rsTransaction.getStockYear());
		return rsStock;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
