package rs4.database.dao.model;

public class RSDeductionProduct {

	private int id;
	private int groupId;
	private int productId;

	public RSDeductionProduct() {
	}

	public RSDeductionProduct(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

}
