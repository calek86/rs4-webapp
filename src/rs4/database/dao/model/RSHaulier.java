package rs4.database.dao.model;

public class RSHaulier {

	private int id;
	private String weighmanId;
	private String code;
	private String description;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getWeighmanId() {
		return weighmanId;
	}

	public void setWeighmanId(String weighmanId) {
		this.weighmanId = weighmanId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
