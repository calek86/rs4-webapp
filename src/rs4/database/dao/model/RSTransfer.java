package rs4.database.dao.model;

import java.sql.Timestamp;

import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;

public class RSTransfer {

	private int id;
	private String ticketId;
	private int fromStockId;
	private int toStockId;
	private int fromStockYearId;
	private int toStockYearId;
	private Timestamp transferDate;
	private String referenceNumber;
	private String transferDateDisplay;
	private float transferValue;
	private String transferValueDisplay;

	public RSTransfer() {
	}

	public RSTransfer(String ticketId, int fromStockId, int toStockId, int fromStockYearId, int toStockYearId,
			Timestamp transferDate, String referenceNumber, float transferValue) {
		this.setTicketId(ticketId);
		this.setFromStockId(fromStockId);
		this.setToStockId(toStockId);
		this.setFromStockYearId(fromStockYearId);
		this.setToStockYearId(toStockYearId);
		this.setReferenceNumber(referenceNumber);
		this.setTransferDate(transferDate);
		this.setTransferValue(transferValue);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public int getFromStockId() {
		return fromStockId;
	}

	public void setFromStockId(int fromStockId) {
		this.fromStockId = fromStockId;
	}

	public int getToStockId() {
		return toStockId;
	}

	public void setToStockId(int toStockId) {
		this.toStockId = toStockId;
	}

	public Timestamp getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Timestamp transferDate) {
		setTransferDateDisplay(DateUtils.formatDateTime(transferDate));
		this.transferDate = transferDate;
	}

	public float getTransferValue() {
		return transferValue;
	}

	public void setTransferValue(float transferValue) {
		setTransferValueDisplay(DisplayUtils.formatWeight(transferValue, true));
		this.transferValue = transferValue;
	}

	public int getFromStockYearId() {
		return fromStockYearId;
	}

	public void setFromStockYearId(int fromStockYearId) {
		this.fromStockYearId = fromStockYearId;
	}

	public int getToStockYearId() {
		return toStockYearId;
	}

	public void setToStockYearId(int toStockYearId) {
		this.toStockYearId = toStockYearId;
	}

	public String getTransferDateDisplay() {
		return transferDateDisplay;
	}

	public void setTransferDateDisplay(String transferDateDisplay) {
		this.transferDateDisplay = transferDateDisplay;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getTransferValueDisplay() {
		return transferValueDisplay;
	}

	public void setTransferValueDisplay(String transferValueDisplay) {
		this.transferValueDisplay = transferValueDisplay;
	}

}
