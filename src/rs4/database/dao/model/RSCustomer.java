package rs4.database.dao.model;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class RSCustomer implements Serializable {

	private static final long serialVersionUID = -3775289087892580818L;
	private int id;
	private String weighmanId;
	private String code;
	private String description;
	private String address;

	public RSCustomer() {
	}

	public RSCustomer(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getWeighmanId() {
		return weighmanId;
	}

	public void setWeighmanId(String weighmanId) {
		this.weighmanId = weighmanId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
