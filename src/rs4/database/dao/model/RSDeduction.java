package rs4.database.dao.model;

import rs4.utils.DisplayUtils;

public class RSDeduction implements Comparable<RSDeduction> {

	private int id;
	private int groupId;
	private float moistureFrom;
	private float moistureTo;
	private float moistureDeduction;
	private float intakeCost;
	private float dryingCost;
	private String groupCode;

	private String moistureFromDisplay;
	private String moistureToDisplay;
	private String moistureDeductionDisplay;
	private String intakeCostDisplay;
	private String dryingCostDisplay;
	
	public RSDeduction() {
	}

	public RSDeduction(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getMoistureFrom() {
		return moistureFrom;
	}

	public void setMoistureFrom(float moistureFrom) {
		this.moistureFrom = moistureFrom;
		setMoistureFromDisplay(DisplayUtils.formatPercentageDecimalsZero(moistureFrom, 2, true, true));
	}

	public float getMoistureTo() {
		return moistureTo;
	}

	public void setMoistureTo(float moistureTo) {
		this.moistureTo = moistureTo;
		setMoistureToDisplay(DisplayUtils.formatPercentageDecimalsZero(moistureTo, 2, true, true));
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public float getMoistureDeduction() {
		return moistureDeduction;
	}

	public void setMoistureDeduction(float moistureDeduction) {
		this.moistureDeduction = moistureDeduction;
		setMoistureDeductionDisplay(DisplayUtils.formatPercentageDecimalsZero(moistureDeduction, 2, true, true));
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public float getIntakeCost() {
		return intakeCost;
	}

	public void setIntakeCost(float intakeCost) {
		this.intakeCost = intakeCost;
		setIntakeCostDisplay(DisplayUtils.formatCurrencyZero(intakeCost, true, true));
	}

	public float getDryingCost() {
		return dryingCost;
	}

	public void setDryingCost(float dryingCost) {
		this.dryingCost = dryingCost;
		setDryingCostDisplay(DisplayUtils.formatCurrencyZero(dryingCost, true, true));
	}

	public String getMoistureFromDisplay() {
		return moistureFromDisplay;
	}

	public void setMoistureFromDisplay(String moistureFromDisplay) {
		this.moistureFromDisplay = moistureFromDisplay;
	}

	public String getMoistureToDisplay() {
		return moistureToDisplay;
	}

	public void setMoistureToDisplay(String moistureToDisplay) {
		this.moistureToDisplay = moistureToDisplay;
	}

	public String getMoistureDeductionDisplay() {
		return moistureDeductionDisplay;
	}

	public void setMoistureDeductionDisplay(String moistureDeductionDisplay) {
		this.moistureDeductionDisplay = moistureDeductionDisplay;
	}

	public String getIntakeCostDisplay() {
		return intakeCostDisplay;
	}

	public void setIntakeCostDisplay(String intakeCostDisplay) {
		this.intakeCostDisplay = intakeCostDisplay;
	}

	public String getDryingCostDisplay() {
		return dryingCostDisplay;
	}

	public void setDryingCostDisplay(String dryingCostDisplay) {
		this.dryingCostDisplay = dryingCostDisplay;
	}

	@Override
	public int compareTo(RSDeduction otherDeduction) {
		if (this.getGroupCode() == null || otherDeduction == null || otherDeduction.getGroupCode() == null) {
			return 0;
		}
		int result = this.getGroupCode().compareTo(otherDeduction.getGroupCode());
		if(result == 0) {
			return this.getMoistureFrom() < otherDeduction.getMoistureFrom() ? -1 : 1;
		}
		return result;
	}

}
