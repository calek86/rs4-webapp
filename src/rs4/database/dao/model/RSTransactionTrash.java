package rs4.database.dao.model;

import java.sql.Timestamp;

public class RSTransactionTrash {

	private int id;
	private String weighmanId;
	private String ticketId;
	private String transactionType;
	private Timestamp transactionDate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getWeighmanId() {
		return weighmanId;
	}
	public void setWeighmanId(String weighmanId) {
		this.weighmanId = weighmanId;
	}
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public Timestamp getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Timestamp transactionDate) {
		this.transactionDate = transactionDate;
	}

}
