package rs4.database.dao.model;

public class RSProduct {

	private int id;
	private String weighmanId;
	private String code;
	private String description;
	private float admixDeduction;
	private int beans;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getWeighmanId() {
		return weighmanId;
	}

	public void setWeighmanId(String weighmanId) {
		this.weighmanId = weighmanId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getAdmixDeduction() {
		return admixDeduction;
	}

	public void setAdmixDeduction(float admixDeduction) {
		this.admixDeduction = admixDeduction;
	}

	public int getBeans() {
		return beans;
	}

	public void setBeans(int beans) {
		this.beans = beans;
	}
}
