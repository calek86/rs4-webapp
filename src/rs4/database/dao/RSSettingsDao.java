package rs4.database.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import rs4.database.dao.model.RSSettings;
import rs4.utils.Logger;

public class RSSettingsDao extends RSAbstractDao {

	public static final String DEFAULT_STOCK_YEAR_ID = "DefaultStockYearId";
	public static final String EXPORT_EXCEL_PATH = "ExportExcelPath";
	public static final String STORING_RATE = "StoringRate";
	public static final String MIN_MOISTURE_DEDUCTION = "MinMoistureDeduction";
	public static final String MIN_INTAKE_COST = "MinIntakeCost";
	public static final String MIN_DRYING_COST = "MinDryingCost";
	public static final String MIN_ACCEPTABLE_ADMIX = "MinAcceptableAdmix";
	public static final String BEANS_PRODUCT_REGEX = "BeansProductRegex";
	
	private static RSSettingsDao instance;
	static Logger logger = Logger.getLogger(RSSettingsDao.class.getName());

	public static final RSSettingsDao getInstance() {
		if (instance == null) {
			instance = new RSSettingsDao("rs4.mysql.cfg.xml");
		}
		return instance;
	}

	private RSSettingsDao(final String config) {
		super(config);
	}

	public int getIntSettingValueByKey(String key) {
		RSSettings rsSetting = getRSSettingByKey(key);
		if(rsSetting != null) {
			return Integer.parseInt(rsSetting.getSettingValue());
		}
		return 0;
	}
	
	public String getStringSettingValueByKey(String key) {
		RSSettings rsSetting = getRSSettingByKey(key);
		if(rsSetting != null) {
			return rsSetting.getSettingValue(); 
		}
		return new String();
	}
	
	public float getFloatSettingValueByKey(String key) {
		RSSettings rsSetting = getRSSettingByKey(key);
		if(rsSetting != null) {
			return Float.parseFloat(rsSetting.getSettingValue()); 
		}
		return 0.0F;
	}

	public void updateIntSettingValueByKey(String key, int value) {
		RSSettings rsSetting = getRSSettingByKey(key);
		if(rsSetting != null) {
			rsSetting.setSettingValue(Integer.toString(value));
			update(rsSetting);
		}
	}

	public void updateStringSettingValueByKey(String key, String value) {
		RSSettings rsSetting = getRSSettingByKey(key);
		if(rsSetting != null) {
			rsSetting.setSettingValue(value);
			update(rsSetting);
		}
	}

	public void updateFloatSettingValueByKey(String key, Float value) {
		RSSettings rsSetting = getRSSettingByKey(key);
		if(rsSetting != null) {
			rsSetting.setSettingValue(Float.toString(value));
			update(rsSetting);
		}
	}

	private RSSettings getRSSettingByKey(String key) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSSettings.class);
		criteria.add(Restrictions.eq("settingKey", key));
		RSSettings result = (RSSettings) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
}
