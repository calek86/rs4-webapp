package rs4.database.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import rs4.database.dao.model.RSTransfer;
import rs4.utils.DateUtils;
import rs4.utils.Logger;

@SuppressWarnings("unchecked")
public class RSTransferDao extends RSAbstractDao {

	private static RSTransferDao instance;

	static Logger logger = Logger.getLogger(RSTransferDao.class.getName());

	public static final RSTransferDao getInstance() {
		if (instance == null) {
			instance = new RSTransferDao("rs4.mysql.cfg.xml");
		}
		return instance;
	}

	private RSTransferDao(final String config) {
		super(config);
	}

	public List<RSTransfer> getRSTransfers() {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSTransfer.class);
		List<RSTransfer> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public List<RSTransfer> getRSTransfersByStockId(int stockId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSTransfer.class);
		Disjunction disjunction = Restrictions.disjunction();
		Criterion fromStockCriterion = Restrictions.eq("fromStockId", stockId);
		Criterion toStockCriterion = Restrictions.eq("toStockId", stockId);
		disjunction.add(fromStockCriterion);
		disjunction.add(toStockCriterion);
		criteria.add(disjunction);
		criteria.addOrder(Order.desc("transferDate"));
		List<RSTransfer> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public List<RSTransfer> getRSTransfersByStockYearId(int stockYearId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSTransfer.class);
		Disjunction disjunction = Restrictions.disjunction();
		Criterion fromStockCriterion = Restrictions.eq("fromStockYearId", stockYearId);
		Criterion toStockCriterion = Restrictions.eq("toStockYearId", stockYearId);
		disjunction.add(fromStockCriterion);
		disjunction.add(toStockCriterion);
		criteria.add(disjunction);
		criteria.addOrder(Order.desc("transferDate"));
		List<RSTransfer> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public List<RSTransfer> getRSTransfersByStockIdAndDateRange(int stockId, String dateFrom, String dateTo) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSTransfer.class);
		Disjunction disjunction = Restrictions.disjunction();
		Criterion fromStockCriterion = Restrictions.eq("fromStockId", stockId);
		Criterion toStockCriterion = Restrictions.eq("toStockId", stockId);
		disjunction.add(fromStockCriterion);
		disjunction.add(toStockCriterion);
		criteria.add(disjunction);
		if (!StringUtils.isEmpty(dateFrom)) {
			criteria.add(Restrictions.ge("transferDate", DateUtils.getDateFromTimestamp(dateFrom)));
		}
		if (!StringUtils.isEmpty(dateTo)) {
			criteria.add(Restrictions.le("transferDate", DateUtils.getDateToTimestamp(dateTo)));
		}
		criteria.addOrder(Order.desc("transferDate"));
		List<RSTransfer> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public RSTransfer getRSTransferById(int id) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSTransfer.class);
		criteria.add(Restrictions.eq("id", id));
		RSTransfer result = (RSTransfer) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public RSTransfer getLastRSTransferFromStockYear(int fromStockYearId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSTransfer.class);
		criteria.add(Restrictions.eq("fromStockYearId", fromStockYearId));
		criteria.addOrder(Order.desc("id"));
		criteria.setMaxResults(1);
		RSTransfer result = (RSTransfer) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}

}
