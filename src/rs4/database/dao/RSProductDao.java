package rs4.database.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.util.CollectionUtils;

import rs4.core.search.TransactionType;
import rs4.database.dao.model.RSProduct;
import rs4.database.dao.model.RSTransaction;
import rs4.utils.DataUtils;
import rs4.utils.Logger;

@SuppressWarnings("unchecked")
public class RSProductDao extends RSAbstractDao {

	private static RSProductDao instance;

	static Logger logger = Logger.getLogger(RSProductDao.class.getName());

	public static final RSProductDao getInstance() {
		if (instance == null) {
			instance = new RSProductDao("rs4.mysql.cfg.xml");
		}
		return instance;
	}

	private RSProductDao(final String config) {
		super(config);
	}

	public List<RSProduct> getRSProducts() {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSProduct.class);
		criteria.addOrder(Order.asc("code"));
		List<RSProduct> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public List<RSProduct> getRSProductsFromTransactionTypes(TransactionType... transactionTypes) {
		Session session = getSession();
		session.beginTransaction();
		Criteria transactionCriteria = session.createCriteria(RSTransaction.class);
		Disjunction disjunction = Restrictions.disjunction();
		Criterion criterion;
		for (TransactionType transactionType : transactionTypes) {
			criterion = Restrictions.eq("transactionType", transactionType.toString());
			disjunction.add(criterion);
		}
		transactionCriteria.add(disjunction);
		transactionCriteria.addOrder(Order.asc("productId"));
		transactionCriteria.setProjection(Projections.distinct(Projections.property("productId")));
		List<Integer> productIds = transactionCriteria.list();
		if(CollectionUtils.isEmpty(productIds)) {
			return new ArrayList<RSProduct>();
		}
		Criteria productCriteria = session.createCriteria(RSProduct.class);
		productCriteria.add(Restrictions.in("id", productIds));
		productCriteria.addOrder(Order.asc("code"));
		List<RSProduct> result = productCriteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public List<RSProduct> getRSProductsFromStockYear(int stockYearId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria transactionCriteria = session.createCriteria(RSTransaction.class);
		transactionCriteria.add(Restrictions.eq("stockYearId", stockYearId));
		transactionCriteria.addOrder(Order.asc("productId"));
		transactionCriteria.setProjection(Projections.distinct(Projections.property("productId")));
		List<Integer> productIds = transactionCriteria.list();
		if(CollectionUtils.isEmpty(productIds)) {
			return new ArrayList<RSProduct>();
		}
		Criteria productCriteria = session.createCriteria(RSProduct.class);
		productCriteria.add(Restrictions.in("id", productIds));
		productCriteria.addOrder(Order.asc("code"));
		List<RSProduct> result = productCriteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	public List<RSProduct> getRSProductsBeansFromStockYear(int stockYearId) {
		List<RSProduct> beans = new ArrayList<RSProduct>();
		for (RSProduct rsProduct : getRSProductsFromStockYear(stockYearId)) {
			if(DataUtils.isBeans(rsProduct.getCode())) {
				beans.add(rsProduct);
			}
		}
		return beans;
	}

	public RSProduct getRSProductById(int id) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSProduct.class);
		criteria.add(Restrictions.eq("id", id));
		RSProduct result = (RSProduct) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public List<RSProduct> getRSProductsByIds(List<Integer> productIds) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSProduct.class);
		if(CollectionUtils.isEmpty(productIds)) {
			return new ArrayList<RSProduct>();
		}
		criteria.add(Restrictions.in("id", productIds));
		List<RSProduct> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	public RSProduct getRSProductByCode(String code) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSProduct.class);
		criteria.add(Restrictions.eq("code", code));
		RSProduct result = (RSProduct) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public void insertRSProductFromParams(String weighmanId, String code, String description, Integer beans) {
		RSProduct rsProduct = new RSProduct();
		rsProduct.setWeighmanId(weighmanId);
		rsProduct.setCode(code);
		rsProduct.setDescription(description);
		rsProduct.setBeans(beans);
		insert(rsProduct);
	}
	
}
