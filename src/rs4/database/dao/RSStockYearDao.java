package rs4.database.dao;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import rs4.database.dao.model.RSStockYear;
import rs4.utils.Logger;

@SuppressWarnings("unchecked")
public class RSStockYearDao extends RSAbstractDao {

	private static RSStockYearDao instance;

	static Logger logger = Logger.getLogger(RSStockYearDao.class.getName());

	public static final RSStockYearDao getInstance() {
		if (instance == null) {
			instance = new RSStockYearDao("rs4.mysql.cfg.xml");
		}
		return instance;
	}

	private RSStockYearDao(final String config) {
		super(config);
	}

	public List<RSStockYear> getRSStockYears() {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSStockYear.class);
		List<RSStockYear> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public RSStockYear getRSStockYearById(int id) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSStockYear.class);
		criteria.add(Restrictions.eq("id", id));
		RSStockYear result = (RSStockYear) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	public RSStockYear getRSStockYearByDescription(String description) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSStockYear.class);
		criteria.add(Restrictions.eq("description", description));
		RSStockYear result = (RSStockYear) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public RSStockYear getRSStockYearByDate(Timestamp date) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSStockYear.class);
		criteria.add(Restrictions.le("dateFrom", date));
		criteria.add(Restrictions.ge("dateTo", date));
		RSStockYear result = (RSStockYear) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
}
