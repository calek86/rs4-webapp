package rs4.database.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import rs4.database.dao.model.RSUser;
import rs4.utils.Logger;

public class RSUserDao extends RSAbstractDao {

	private static RSUserDao instance;

	static Logger logger = Logger.getLogger(RSUserDao.class.getName());

	public static final RSUserDao getInstance() {
		if (instance == null) {
			instance = new RSUserDao("rs4.mysql.cfg.xml");
		}
		return instance;
	}

	private RSUserDao(final String config) {
		super(config);
	}

	public RSUser getRSAdminUser() {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSUser.class);
		criteria.add(Restrictions.eq("id", 1));
		RSUser result = (RSUser) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	public RSUser getRSUserByEmail(String email) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSUser.class);
		criteria.add(Restrictions.eq("email", email));
		RSUser result = (RSUser) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public RSUser getRSUserByToken(String token) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSUser.class);
		criteria.add(Restrictions.eq("token", token));
		RSUser result = (RSUser) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}

}
