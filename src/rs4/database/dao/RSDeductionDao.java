package rs4.database.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import rs4.database.dao.model.RSDeduction;
import rs4.utils.Logger;

@SuppressWarnings("unchecked")
public class RSDeductionDao extends RSAbstractDao {

	private static RSDeductionDao instance;

	static Logger logger = Logger.getLogger(RSDeductionDao.class.getName());

	public static final RSDeductionDao getInstance() {
		if (instance == null) {
			instance = new RSDeductionDao("rs4.mysql.cfg.xml");
		}
		return instance;
	}

	private RSDeductionDao(final String config) {
		super(config);
	}

	public List<RSDeduction> getRSDeductions() {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSDeduction.class);
		List<RSDeduction> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public RSDeduction getRSDeductionById(int id) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSDeduction.class);
		criteria.add(Restrictions.eq("id", id));
		RSDeduction result = (RSDeduction) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	public List<RSDeduction> getRSDeductionByGroupId(int groupId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSDeduction.class);
		criteria.add(Restrictions.eq("groupId", groupId));
		criteria.addOrder(Order.asc("moistureFrom"));
		List<RSDeduction> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	public void insertDeductionFromParams(float moistureFrom, float moistureTo, 
			int groupId, float moistureDeduction) {
		RSDeduction rsMoisture = new RSDeduction();
		rsMoisture.setMoistureFrom(moistureFrom);
		rsMoisture.setMoistureTo(moistureTo);
		rsMoisture.setGroupId(groupId);
		rsMoisture.setMoistureDeduction(moistureDeduction);
		insert(rsMoisture);
	}

	public void deleteMoistureDeductionById(int id) {
		RSDeduction rsMoistureDeduction = getRSDeductionById(id);
		if (rsMoistureDeduction != null) {
			delete(rsMoistureDeduction);
		}
	}

}
