package rs4.database.dao;

import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import rs4.core.stock.StockCriteria;
import rs4.core.stock.StockCriteria.StockGroupBy;
import rs4.database.dao.model.RSStock;
import rs4.utils.DisplayUtils;
import rs4.utils.Logger;

@SuppressWarnings("unchecked")
public class RSStockDao extends RSAbstractDao {

	private static RSStockDao instance;

	static Logger logger = Logger.getLogger(RSStockDao.class.getName());

	public static final RSStockDao getInstance() {
		if (instance == null) {
			instance = new RSStockDao("rs4.mysql.cfg.xml");
		}
		return instance;
	}

	private RSStockDao(final String config) {
		super(config);
	}

	public List<RSStock> getRSStocks() {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSStock.class);
		criteria.addOrder(Order.desc("stockYearDescription"));
		criteria.addOrder(Order.asc("customerCode"));
		List<RSStock> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public List<RSStock> getRSStocksByStockYearId(int stockYearId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSStock.class);
		criteria.add(Restrictions.eq("stockYearId", stockYearId));
		criteria.addOrder(Order.asc("customerCode"));
		List<RSStock> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public List<RSStock> getRSStocksByStockYearId(Collection<Integer> stockYearIds) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSStock.class);
		criteria.add(Restrictions.in("stockYearId", stockYearIds));
		criteria.addOrder(Order.asc("customerCode"));
		List<RSStock> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	public List<RSStock> getRSStocksByCriteria(StockCriteria stockCriteria) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSStock.class);
		String customerCode = stockCriteria.getCustomerCode();
		if ((customerCode != null) && (!customerCode.equals(DisplayUtils.KEYWORD_ALL))) {
			criteria.add(Restrictions.eq("customerCode", customerCode));
		}
		String productCode = stockCriteria.getProductCode();
		if ((productCode != null) && (!productCode.equals(DisplayUtils.KEYWORD_ALL))) {
			criteria.add(Restrictions.eq("productCode", productCode));
		}
		int stockYearId = stockCriteria.getStockYearId();
		if ((stockYearId != -1)) {
			criteria.add(Restrictions.eq("stockYearId", stockYearId));
		}
		if (stockCriteria.getGroupBy().equals(StockGroupBy.Product)) {
			criteria.addOrder(Order.asc("productCode"));
			criteria.addOrder(Order.asc("customerCode"));
		} else if (stockCriteria.getGroupBy().equals(StockGroupBy.Customer)) {
			criteria.addOrder(Order.asc("customerCode"));
			criteria.addOrder(Order.asc("productCode"));
		}
		List<RSStock> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public RSStock getRSStockRecordByParams(int customerId, int productId, int stockYearId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSStock.class);
		criteria.add(Restrictions.eq("customerId", customerId));
		criteria.add(Restrictions.eq("productId", productId));
		criteria.add(Restrictions.eq("stockYearId", stockYearId));
		RSStock result = (RSStock) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public RSStock getRSStockById(int id) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSStock.class);
		criteria.add(Restrictions.eq("id", id));
		RSStock result = (RSStock) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}

}
