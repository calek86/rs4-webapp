package rs4.database.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import rs4.core.beans.BeansCriteria;
import rs4.core.bills.BillsCriteria;
import rs4.core.search.SearchCriteria;
import rs4.database.dao.model.RSTransaction;
import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;
import rs4.utils.Logger;

@SuppressWarnings("unchecked")
public class RSTransactionDao extends RSAbstractDao {

	private static RSTransactionDao instance;

	static Logger logger = Logger.getLogger(RSTransactionDao.class.getName());

	public static final RSTransactionDao getInstance() {
		if (instance == null) {
			instance = new RSTransactionDao("rs4.mysql.cfg.xml");
		}
		return instance;
	}

	private RSTransactionDao(final String config) {
		super(config);
	}

	public List<RSTransaction> getRSTransactions() {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSTransaction.class);
		List<RSTransaction> result = criteria.list();
		criteria.addOrder(Order.asc("transactionDate"));
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	public List<RSTransaction> getRSTransactionsByCriteria(SearchCriteria queryCriteria) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSTransaction.class);
		String transactionsNo = queryCriteria.getTransactionsNo();
		if (!StringUtils.isEmpty(transactionsNo)) {
			String[] tokens = transactionsNo.split("[ ]+");
			Disjunction disjunction = Restrictions.disjunction();
			Criterion criterion;
			for (int i = 0; i < tokens.length; i++) {
				criterion = Restrictions.eq("ticketId", tokens[i]);
				disjunction.add(criterion);
			}
			criteria.add(disjunction);
		} else {
			String customerCode = queryCriteria.getCustomerCode();
			if (!StringUtils.isEmpty(customerCode) && (!customerCode.equals(DisplayUtils.KEYWORD_ALL))) {
				criteria.add(Restrictions.eq("customer", customerCode));
			}
			String productCode = queryCriteria.getProductCode();
			if (!StringUtils.isEmpty(productCode) && (!productCode.equals(DisplayUtils.KEYWORD_ALL))) {
				criteria.add(Restrictions.eq("product", productCode));
			}
			String haulierCode = queryCriteria.getHaulierCode();
			if (!StringUtils.isEmpty(haulierCode) && (!haulierCode.equals(DisplayUtils.KEYWORD_ALL))) {
				criteria.add(Restrictions.eq("haulier", haulierCode));
			}
			String destinationCode = queryCriteria.getDestinationCode();
			if (!StringUtils.isEmpty(destinationCode) && (!destinationCode.equals(DisplayUtils.KEYWORD_ALL))) {
				criteria.add(Restrictions.eq("destination", destinationCode));
			}
			String sourceCode = queryCriteria.getSourceCode();
			if (!StringUtils.isEmpty(sourceCode) && (!sourceCode.equals(DisplayUtils.KEYWORD_ALL))) {
				criteria.add(Restrictions.eq("source", sourceCode));
			}
			int stockYearId = queryCriteria.getStockYearId();
			if ((stockYearId != -1)) {
				criteria.add(Restrictions.eq("stockYearId", stockYearId));
			}
			String transactionType = queryCriteria.getTransactionType().toString();
			if (!StringUtils.isEmpty(transactionType) && (!transactionType.equals(DisplayUtils.KEYWORD_ALL))) {
				criteria.add(Restrictions.eq("transactionType", transactionType));
			}
			String registration = queryCriteria.getRegistration();
			if (!StringUtils.isEmpty(registration)) {
				criteria.add(Restrictions.eq("registration", registration));
			}
			String dateFrom = queryCriteria.getDateFrom();
			if (!StringUtils.isEmpty(dateFrom)) {
				criteria.add(Restrictions.ge("transactionDate", DateUtils.getDateFromTimestamp(dateFrom)));
			}
			String dateTo = queryCriteria.getDateTo();
			if (!StringUtils.isEmpty(dateTo)) {
				criteria.add(Restrictions.le("transactionDate", DateUtils.getDateToTimestamp(dateTo)));
			}
		}
		criteria.addOrder(Order.asc("transactionDate"));
		List<RSTransaction> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	public List<RSTransaction> getRSTransactionsByCriteria(BeansCriteria queryCriteria) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSTransaction.class);
		String transactionsNo = queryCriteria.getTransactionsNo();
		if (!StringUtils.isEmpty(transactionsNo)) {
			String[] tokens = transactionsNo.split("[ ]+");
			Disjunction disjunction = Restrictions.disjunction();
			Criterion criterion;
			for (int i = 0; i < tokens.length; i++) {
				criterion = Restrictions.eq("ticketId", tokens[i]);
				disjunction.add(criterion);
			}
			criteria.add(disjunction);
		} else {
			String customerCode = queryCriteria.getCustomerCode();
			if (!StringUtils.isEmpty(customerCode) && (!customerCode.equals(DisplayUtils.KEYWORD_ALL))) {
				criteria.add(Restrictions.eq("customer", customerCode));
			}
			String productCode = queryCriteria.getProductCode();
			if (!StringUtils.isEmpty(productCode) && (!productCode.equals(DisplayUtils.KEYWORD_ALL))) {
				criteria.add(Restrictions.eq("product", productCode));
			}
			criteria.add(Restrictions.like("product", "beans", MatchMode.ANYWHERE));
			String haulierCode = queryCriteria.getHaulierCode();
			if (!StringUtils.isEmpty(haulierCode) && (!haulierCode.equals(DisplayUtils.KEYWORD_ALL))) {
				criteria.add(Restrictions.eq("haulier", haulierCode));
			}
			String destinationCode = queryCriteria.getDestinationCode();
			if (!StringUtils.isEmpty(destinationCode) && (!destinationCode.equals(DisplayUtils.KEYWORD_ALL))) {
				criteria.add(Restrictions.eq("destination", destinationCode));
			}
			int stockYearId = queryCriteria.getStockYearId();
			if ((stockYearId != -1)) {
				criteria.add(Restrictions.eq("stockYearId", stockYearId));
			}
			String transactionType = queryCriteria.getTransactionType().toString();
			if (!StringUtils.isEmpty(transactionType)) {
				criteria.add(Restrictions.eq("transactionType", transactionType));
			}
			String dateFrom = queryCriteria.getDateFrom();
			if (!StringUtils.isEmpty(dateFrom)) {
				criteria.add(Restrictions.ge("transactionDate", DateUtils.getDateFromTimestamp(dateFrom)));
			}
			String dateTo = queryCriteria.getDateTo();
			if (!StringUtils.isEmpty(dateTo)) {
				criteria.add(Restrictions.le("transactionDate", DateUtils.getDateToTimestamp(dateTo)));
			}	
		}
		criteria.addOrder(Order.asc("transactionDate"));
		List<RSTransaction> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	public List<RSTransaction> getRSTransactionsByCriteria(BillsCriteria queryCriteria) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSTransaction.class);
		String transactionsNo = queryCriteria.getTransactionsNo();
		if (!StringUtils.isEmpty(transactionsNo)) {
			String[] tokens = transactionsNo.split("[ ]+");
			Disjunction disjunction = Restrictions.disjunction();
			Criterion criterion;
			for (int i = 0; i < tokens.length; i++) {
				criterion = Restrictions.eq("ticketId", tokens[i]);
				disjunction.add(criterion);
			}
			criteria.add(disjunction);
		} else {
			String customerCode = queryCriteria.getCustomerCode();
			if (!StringUtils.isEmpty(customerCode) && (!customerCode.equals(DisplayUtils.KEYWORD_ALL))) {
				criteria.add(Restrictions.eq("customer", customerCode));
			}
			String productCode = queryCriteria.getProductCode();
			if (!StringUtils.isEmpty(productCode) && (!productCode.equals(DisplayUtils.KEYWORD_ALL))) {
				criteria.add(Restrictions.eq("product", productCode));
			}
			int stockYearId = queryCriteria.getStockYearId();
			if ((stockYearId != -1)) {
				criteria.add(Restrictions.eq("stockYearId", stockYearId));
			}
			String[] transactionTypes = queryCriteria.getTransactionTypes();
			if(transactionTypes != null && transactionTypes.length > 0) {
				boolean includeTypes = true;
				for (int i = 0; i < transactionTypes.length; i++) {
					if(!StringUtils.isEmpty(transactionTypes[i])) {
						if(transactionTypes[i].equalsIgnoreCase(DisplayUtils.KEYWORD_ALL)) {
							includeTypes = false;
						}
					}
				}
				if(includeTypes) {
					criteria.add(Restrictions.in("transactionType", transactionTypes));	
				}
			}
			String dateFrom = queryCriteria.getDateFrom();
			if (!StringUtils.isEmpty(dateFrom)) {
				criteria.add(Restrictions.ge("transactionDate", DateUtils.getDateFromTimestamp(dateFrom)));
			}
			String dateTo = queryCriteria.getDateTo();
			if (!StringUtils.isEmpty(dateTo)) {
				criteria.add(Restrictions.le("transactionDate", DateUtils.getDateToTimestamp(dateTo)));
			}
		}
		criteria.addOrder(Order.asc("transactionDate"));
		List<RSTransaction> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	public List<RSTransaction> getRSTransactionsByStockYearId(int stockYearId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSTransaction.class);
		criteria.add(Restrictions.eq("stockYearId", stockYearId));
		criteria.addOrder(Order.asc("transactionDate"));
		List<RSTransaction> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	public RSTransaction getRSTransactionById(int id) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSTransaction.class);
		criteria.add(Restrictions.eq("id", id));
		RSTransaction result = (RSTransaction) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public RSTransaction getRSTransactionByTicketId(String ticketId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSTransaction.class);
		criteria.add(Restrictions.eq("ticketId", ticketId));
		RSTransaction result = (RSTransaction) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public List<RSTransaction> getRSTransactionsByProductId(int productId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSTransaction.class);
		criteria.add(Restrictions.eq("productId", productId));
		List<RSTransaction> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	public RSTransaction getRSTransactionLast() {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSTransaction.class);
		criteria.addOrder(Order.desc("id"));
		criteria.setMaxResults(1);
		RSTransaction result = (RSTransaction) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public void deleteTransactionById(int id) {
		RSTransaction rsTransaction = getRSTransactionById(id);
		if(rsTransaction != null) {
			delete(rsTransaction);
		}
	}

}
