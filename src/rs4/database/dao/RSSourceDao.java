package rs4.database.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.util.CollectionUtils;

import rs4.database.dao.model.RSSource;
import rs4.database.dao.model.RSTransaction;
import rs4.utils.Logger;

@SuppressWarnings("unchecked")
public class RSSourceDao extends RSAbstractDao {

	private static RSSourceDao instance;

	static Logger logger = Logger.getLogger(RSSourceDao.class.getName());

	public static final RSSourceDao getInstance() {
		if (instance == null) {
			instance = new RSSourceDao("rs4.mysql.cfg.xml");
		}
		return instance;
	}

	private RSSourceDao(final String config) {
		super(config);
	}

	public List<RSSource> getRSSources() {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSSource.class);
		criteria.addOrder(Order.asc("code"));
		List<RSSource> result = criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public List<RSSource> getRSSourcesFromStockYear(int stockYearId) {
		Session session = getSession();
		session.beginTransaction();
		Criteria transactionCriteria = session.createCriteria(RSTransaction.class);
		transactionCriteria.add(Restrictions.eq("stockYearId", stockYearId));
		transactionCriteria.addOrder(Order.asc("sourceId"));
		transactionCriteria.setProjection(Projections.distinct(Projections.property("sourceId")));
		List<Integer> sourceIds = transactionCriteria.list();
		if(CollectionUtils.isEmpty(sourceIds)) {
			return new ArrayList<RSSource>();
		}
		Criteria sourceCriteria = session.createCriteria(RSSource.class);
		sourceCriteria.add(Restrictions.in("id", sourceIds));
		sourceCriteria.addOrder(Order.asc("code"));
		List<RSSource> result = sourceCriteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	public RSSource getRSSourceByCode(String code) {
		Session session = getSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(RSSource.class);
		criteria.add(Restrictions.eq("code", code));
		RSSource result = (RSSource) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return result;
	}

	public void insertRSSourceFromParams(String weighmanId, String code, String description) {
		RSSource rsSource = new RSSource();
		rsSource.setWeighmanId(weighmanId);
		rsSource.setCode(code);
		rsSource.setDescription(description);
		insert(rsSource);
	}
	
}
