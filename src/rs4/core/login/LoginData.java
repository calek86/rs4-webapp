package rs4.core.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import rs4.database.dao.RSUserDao;
import rs4.database.dao.model.RSUser;
import rs4.utils.CookieUtils;
import rs4.utils.DataUtils;
import rs4.utils.DisplayUtils;
import rs4.utils.Logger;

public class LoginData {

	private RSUserDao rsUserDao;
	private LoginCriteria loginCriteria;
	private String errorMessage;

	static Logger logger = Logger.getLogger(LoginData.class.getName());

	public LoginData() {
		rsUserDao = RSUserDao.getInstance();
		loginCriteria = new LoginCriteria();
		setErrorMessage("0");
	}

	public boolean login(LoginCriteria loginCriteria, HttpServletRequest request, HttpServletResponse response) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Login attempt @ with username @", actionId, loginCriteria.getUserName());
		if (StringUtils.isEmpty(loginCriteria.getUserName()) || StringUtils.isEmpty(loginCriteria.getPassword())) {
			logger.error("Empty user name or password");
			return loginFailed(response, actionId);
		}
		RSUser user = rsUserDao.getRSUserByEmail(loginCriteria.getUserName());
		if (user == null || StringUtils.isEmpty(user.getToken())) {
			logger.error("Missing user or empty token for login attempt @", actionId);
			return loginFailed(response, actionId);
		}
		if (!validatePassword(user, loginCriteria.getPassword())) {
			logger.error("Invalid password @ for login attemp @", loginCriteria.getPassword(), actionId);
			return loginFailed(response, actionId);
		}
		request.setAttribute(DataUtils.REQUEST_USER_ATTRIBUTE, user);
		return loginSuccess(user, response, actionId);
	}

	public boolean autoLogin(final String cookieToken, HttpServletRequest request, HttpServletResponse response) {
		RSUser user = rsUserDao.getRSUserByToken(cookieToken);
		if (!DataUtils.validateToken(user, cookieToken)) {
			CookieUtils.deleteUserToken(response);
			logger.error("Automatic login failed for token @", cookieToken);
			return false;
		}
		request.setAttribute(DataUtils.REQUEST_USER_ATTRIBUTE, user);
		return true;
	}

	public void logout(RSUser user, HttpServletResponse response) {
		logger.error("Logout user @", DisplayUtils.logUser(user));
		setErrorMessage("0");
		CookieUtils.deleteUserToken(response);
	}

	private boolean validatePassword(RSUser user, String password) {
		if(StringUtils.isEmpty(user.getPassword()) || !user.getPassword().equals(password)) {
			return false;
		}
		return true;
	}

	private boolean loginSuccess(RSUser user, HttpServletResponse response, String actionId) {
		CookieUtils.saveUserToken(response, user.getToken());
		setErrorMessage("0");
		logger.info("Login @ successful", actionId);
		return true;
	}

	private boolean loginFailed(HttpServletResponse response, String actionId) {
		CookieUtils.deleteUserToken(response);
		logger.error("Login @ failed", actionId);
		setErrorMessage("Login error. Check your username and password.");
		return false;
	}

	public LoginCriteria getLoginCriteria() {
		return loginCriteria;
	}

	public void setLoginCriteria(LoginCriteria loginCriteria) {
		this.loginCriteria = loginCriteria;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
