package rs4.core.login;

import rs4.utils.DisplayUtils;

public class LoginCriteria {

	private String userName;
	private String password;

	public LoginCriteria() {
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return DisplayUtils.buildToSting(userName, password);
	}
	
}
