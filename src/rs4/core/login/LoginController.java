package rs4.core.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import rs4.core.search.SearchData;
import rs4.database.dao.model.RSUser;
import rs4.utils.DataUtils;

@Controller
@SessionAttributes
public class LoginController {

	@RequestMapping(value = "/DoLogin", method = RequestMethod.POST)
	public ModelAndView doLogin(HttpServletRequest request, HttpServletResponse response) {
		LoginData loginData = new LoginData();
		loginData.getLoginCriteria().setUserName(request.getParameter("loginCriteria.userName"));
		loginData.getLoginCriteria().setPassword(request.getParameter("loginCriteria.password"));
		if (loginData.login(loginData.getLoginCriteria(), request, response)) {
			RSUser user = DataUtils.requestUser(request);
			SearchData searchData = SearchData.getInstance(user);
			searchData.searchTransactions(user);
			return new ModelAndView("search", "command", searchData);
		} else {
			return new ModelAndView("login", "command", loginData);
		}
	}

	@RequestMapping(value = "/DoLogout", method = RequestMethod.POST)
	public ModelAndView doLogout(HttpServletRequest request, HttpServletResponse response) {
		RSUser user = DataUtils.requestUser(request);
		LoginData loginData = new LoginData();
		loginData.logout(user, response);
		return new ModelAndView("login", "command", loginData);
	}

}