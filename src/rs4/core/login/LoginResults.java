package rs4.core.login;

public class LoginResults {

	private String sessionId;
	private String errorMessage;

	public LoginResults() {
		setSessionId(new String());
		setErrorMessage("0");
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
}
