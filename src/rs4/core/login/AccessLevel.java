package rs4.core.login;

import rs4.core.search.SearchData;
import rs4.utils.Logger;

public enum AccessLevel {
	
	READ, WRITE;
	
	static Logger logger = Logger.getLogger(SearchData.class.getName());
	
	public static AccessLevel fromString(String access) {
		try {
			if(AccessLevel.valueOf(access).equals(WRITE)) {
				return WRITE;
			}
			return READ;
		} catch (Exception e) {
			logger.error("Invalid access level @", access);
			return READ;
		}
	}
}
