package rs4.core.transfer;

import java.util.List;

import rs4.database.dao.model.RSCustomer;
import rs4.database.dao.model.RSStock;
import rs4.database.dao.model.RSStockYear;
import rs4.utils.DataUtils;
import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;

public class TransferCriteria {

	private String transferDate;
	private int fromStockId;
	private int toCustomerId;
	private int toStockYearId;
	private String toStockYearDescription;
	private float transferValue;
	private int transferEverything;
	private String referenceNumber;
	private List<RSStock> fromStockList;
	private List<RSStock> toStockList;
	private List<RSCustomer> toCustomerList;
	private List<RSStockYear> stockYears;

	public TransferCriteria() {
		RSStockYear rsStockYear = DataUtils.getDefaultStockYear();
		setToStockYearId(rsStockYear.getId());
		setToStockYearDescription(rsStockYear.getDescription());
		setDefaultDates();
	}

	public void setDefaultDates() {
		setTransferDate(DateUtils.getDateTimeToday());
	}

	public List<RSStockYear> getStockYears() {
		return stockYears;
	}

	public void setStockYears(List<RSStockYear> stockYear) {
		this.stockYears = stockYear;
	}

	public int getFromStockId() {
		return fromStockId;
	}

	public void setFromStockId(int fromStockId) {
		this.fromStockId = fromStockId;
	}

	public int getToCustomerId() {
		return toCustomerId;
	}

	public void setToCustomerId(int toCustomerId) {
		this.toCustomerId = toCustomerId;
	}

	public String getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public float getTransferValue() {
		return transferValue;
	}

	public void setTransferValue(float transferValue) {
		this.transferValue = transferValue;
	}

	public int getTransferEverything() {
		return transferEverything;
	}

	public void setTransferEverything(int transferEverything) {
		this.transferEverything = transferEverything;
	}

	public List<RSStock> getFromStockList() {
		return fromStockList;
	}

	public List<RSStock> getToStockList() {
		return toStockList;
	}

	public void setToStockList(List<RSStock> toStockList) {
		this.toStockList = toStockList;
	}

	public void setFromStockList(List<RSStock> fromStockList) {
		this.fromStockList = fromStockList;
	}

	public List<RSCustomer> getToCustomerList() {
		return toCustomerList;
	}

	public void setToCustomerList(List<RSCustomer> toCustomerList) {
		this.toCustomerList = toCustomerList;
	}

	public int getToStockYearId() {
		return toStockYearId;
	}

	public void setToStockYearId(int toStockYearId) {
		this.toStockYearId = toStockYearId;
	}

	public String getToStockYearDescription() {
		return toStockYearDescription;
	}

	public void setToStockYearDescription(String toStockYearDescription) {
		this.toStockYearDescription = toStockYearDescription;
	}

	@Override
	public String toString() {
		return DisplayUtils.buildToSting(Integer.toString(toStockYearId), Integer.toString(fromStockId),
				Integer.toString(toCustomerId), transferDate, Float.toString(transferValue),
				Integer.toString(transferEverything));
	}

}
