package rs4.core.transfer;

import java.util.List;

import rs4.core.login.AccessLevel;
import rs4.database.dao.model.RSTransfer;

public class TransferResults {

	private String accessLevel;
	private List<RSTransfer> resultTransfers;
	private String errorMessage;

	public TransferResults() {
		accessLevel = AccessLevel.READ.toString();
		setErrorMessage("0");
	}

	public String getAccessLevel() {
		return accessLevel;
	}

	public void setAccessLevel(String accessLevel) {
		this.accessLevel = accessLevel;
	}

	public List<RSTransfer> getResultTransfers() {
		return resultTransfers;
	}

	public void setResultTransfers(List<RSTransfer> resultTransfers) {
		this.resultTransfers = resultTransfers;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
}
