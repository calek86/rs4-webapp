package rs4.core.transfer;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import rs4.database.dao.RSCustomerDao;
import rs4.database.dao.RSProductDao;
import rs4.database.dao.RSStockDao;
import rs4.database.dao.RSStockYearDao;
import rs4.database.dao.RSTransferDao;
import rs4.database.dao.model.RSCustomer;
import rs4.database.dao.model.RSProduct;
import rs4.database.dao.model.RSStock;
import rs4.database.dao.model.RSStockYear;
import rs4.database.dao.model.RSTransfer;
import rs4.database.dao.model.RSUser;
import rs4.utils.DataUtils;
import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;
import rs4.utils.Logger;

public class TransferData {

	private RSTransferDao rsTransferDao;
	private RSStockDao rsStockDao;
	private RSCustomerDao rsCustomerDao;
	private RSProductDao rsProductDao;
	private RSStockYearDao rsStockYearDao;

	private TransferCriteria transferCriteria;
	private TransferResults transferResults;

	private static Map<Integer, TransferData> instances;
	private static Logger logger = Logger.getLogger(TransferData.class.getName());

	public static final TransferData getInstance(RSUser user) {
		if (user == null) {
			logger.error("Requesting data object without user");
			return new TransferData();
		}
		if (instances == null) {
			instances = new HashMap<>();
		}
		if (!instances.containsKey(user.getId())) {
			instances.put(user.getId(), new TransferData());
		}
		return instances.get(user.getId());
	}

	public static void reloadInstance() {
		instances = new HashMap<>();
	}

	private TransferData() {
		rsTransferDao = RSTransferDao.getInstance();
		rsStockDao = RSStockDao.getInstance();
		rsCustomerDao = RSCustomerDao.getInstance();
		rsProductDao = RSProductDao.getInstance();
		rsStockYearDao = RSStockYearDao.getInstance();
		transferCriteria = new TransferCriteria();
		transferCriteria.setStockYears(rsStockYearDao.getRSStockYears());
		List<RSStock> stockList = rsStockDao.getRSStocks();
		List<RSCustomer> customerList = rsCustomerDao.getRSCustomers();
		transferCriteria.setFromStockList(stockList);
		transferCriteria.setToStockList(stockList);
		transferCriteria.setToCustomerList(customerList);
		transferResults = new TransferResults();
	}

	public void searchTransfers(RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Search transfers @ by @ with criteria @", actionId, DisplayUtils.logUser(user),
				transferCriteria.toString());
		transferResults.setAccessLevel(DataUtils.requestAccess(user).toString());
		List<RSTransfer> rsTransfers = rsTransferDao.getRSTransfers();
		transferResults.setErrorMessage("0");
		transferResults.setResultTransfers(rsTransfers);
		reloadCriteria();
	}

	public void transfer(RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("New transfer @ by @ with criteria @", actionId, DisplayUtils.logUser(user),
				transferCriteria.toString());
		try {
			RSTransfer rsTransfer = prepareTransfer(transferCriteria);
			rsTransferDao.insert(rsTransfer);
			transferResults.setErrorMessage("0");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Transfer @ problem", actionId);
			transferResults.setErrorMessage("Transfer not possible. " + e.getMessage());
			return;
		}

		logger.info("Successfully executed transfer @", actionId);
		searchTransfers(user);
	}

	private RSTransfer prepareTransfer(TransferCriteria transferCriteria) throws Exception {
		RSStock fromStock = rsStockDao.getRSStockById(transferCriteria.getFromStockId());
		RSCustomer customer = rsCustomerDao.getRSCustomerById(transferCriteria.getToCustomerId());
		RSProduct product = rsProductDao.getRSProductById(fromStock.getProductId());
		RSStockYear toStockYear = rsStockYearDao.getRSStockYearById(transferCriteria.getToStockYearId());
		RSStock toStock = rsStockDao.getRSStockRecordByParams(customer.getId(), product.getId(),
				transferCriteria.getToStockYearId());
		if (toStock == null) {
			RSStock rsStock = new RSStock(customer.getId(), customer.getCode(), product.getId(), product.getCode(),
					toStockYear.getId(), toStockYear.getDescription(), 0.0F);
			rsStockDao.insert(rsStock);
			toStock = rsStockDao.getRSStockRecordByParams(customer.getId(), product.getId(), toStockYear.getId());
		}
		float transferValue = transferCriteria.getTransferValue();
		if (transferCriteria.getTransferEverything() == 1) {
			transferValue = fromStock.getTotalValue();
		}
		String transferError = checkTransferError(fromStock, toStock, transferValue);
		if (!StringUtils.isEmpty(transferError)) {
			throw new Exception(transferError);
		}
		String transferTicketId = DataUtils.generateTransferTicketId(toStockYear);
		Timestamp transferDate = DateUtils.parseDateTimeToTimestamp(transferCriteria.getTransferDate());
		return new RSTransfer(transferTicketId, fromStock.getId(), toStock.getId(), fromStock.getStockYearId(),
				toStock.getStockYearId(), transferDate, transferCriteria.getReferenceNumber(), transferValue);
	}

	public void deleteTransferById(int transferId, RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Delete transfer @ by @ with criteria @", actionId, DisplayUtils.logUser(user),
				transferCriteria.toString());
		transferResults.setAccessLevel(DataUtils.requestAccess(user).toString());
		RSTransfer rsTransfer = rsTransferDao.getRSTransferById(transferId);
		if (rsTransfer == null) {
			logger.error("Cannot find transfer @ to delete", transferId);
			return;
		}
		RSStock fromStock = rsStockDao.getRSStockById(rsTransfer.getFromStockId());
		RSStock toStock = rsStockDao.getRSStockById(rsTransfer.getToStockId());
		float transferValue = rsTransfer.getTransferValue();
		String transferError = checkTransferError(fromStock, toStock, transferValue);
		if (!StringUtils.isEmpty(transferError)) {
			logger.error("Delete transfer @ error: @", rsTransfer.getId(), transferError);
			return;
		}
		fromStock.setTotalValue(fromStock.getTotalValue() + transferValue);
		toStock.setTotalValue(toStock.getTotalValue() - transferValue);
		try {
			rsStockDao.update(fromStock);
			rsStockDao.update(toStock);
			rsTransferDao.delete(rsTransfer);
			transferResults.setErrorMessage("0");
		} catch (Exception e) {
			logger.error("Problem deleting transfer @", rsTransfer.getId());
		}
		searchTransfers(user);
	}

	public void reloadCriteria() {
		transferCriteria.setDefaultDates();
		transferCriteria.setTransferEverything(0);
		transferCriteria.setReferenceNumber(new String());
		List<RSStock> stockList = rsStockDao.getRSStocks();
		List<RSCustomer> customerList = rsCustomerDao.getRSCustomers();
		transferCriteria.setFromStockList(stockList);
		transferCriteria.setToStockList(stockList);
		transferCriteria.setToCustomerList(customerList);
	}

	private String checkTransferError(RSStock fromStock, RSStock toStock, float transferValue) {
		if (fromStock == null || toStock == null) {
			return "Cannot transfer because stock(s) not present";
		}
		if (fromStock.getId() == toStock.getId()) {
			return "Cannot transfer to the same customer";
		}
		if (fromStock.getProductId() != toStock.getProductId()) {
			return "Cannot transfer different products";
		}
		if (transferValue <= 0.0) {
			return "Must declare transfer value bigger then zero";
		}
		return new String();
	}

	public TransferCriteria getTransferCriteria() {
		return transferCriteria;
	}

	public void setTransferCriteria(TransferCriteria transferCriteria) {
		this.transferCriteria = transferCriteria;
	}

	public TransferResults getTransferResults() {
		return transferResults;
	}

	public void setTransferResults(TransferResults transferResults) {
		this.transferResults = transferResults;
	}

}
