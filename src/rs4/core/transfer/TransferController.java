package rs4.core.transfer;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import rs4.core.excel.AbstractExcelBuilder;
import rs4.core.excel.TransferExcelBuilder;
import rs4.database.dao.model.RSTransfer;
import rs4.database.dao.model.RSUser;
import rs4.utils.DataUtils;

@Controller
@SessionAttributes
public class TransferController {

	@RequestMapping(value = "/TransferStock", method = RequestMethod.POST)
	public ModelAndView transferStock(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		TransferData transferData = TransferData.getInstance(user);
		transferData.getTransferCriteria().setTransferDate(request.getParameter("transferCriteria.transferDate"));
		transferData.getTransferCriteria().setFromStockId(Integer.parseInt(request.getParameter("transferCriteria.fromStockList")));
		transferData.getTransferCriteria().setToCustomerId(Integer.parseInt(request.getParameter("transferCriteria.toCustomerList")));
		transferData.getTransferCriteria().setToStockYearId(Integer.parseInt(request.getParameter("transferCriteria.stockYears")));
		transferData.getTransferCriteria().setReferenceNumber(request.getParameter("transferCriteria.referenceNumber"));
		transferData.getTransferCriteria().setTransferValue(Float.parseFloat(request.getParameter("transferCriteria.transferValue")));
		String transferEverything = request.getParameter("transferCriteria.transferEverything");
		if(!StringUtils.isEmpty(transferEverything) && transferEverything.equals("1")) {
			transferData.getTransferCriteria().setTransferEverything(1);
		}
		else {
			transferData.getTransferCriteria().setTransferEverything(0);
		}
		transferData.transfer(user);
		return new ModelAndView("transfer", "command", transferData);
	}

	@RequestMapping(value = "/DeleteOneTransfer", method = RequestMethod.POST)
	public ModelAndView deleteOneMoistureDeduction(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		TransferData transferData = TransferData.getInstance(user);
		int idToDelete = Integer.valueOf(request.getParameter("idToDelete"));
		transferData.deleteTransferById(idToDelete, user);
		return new ModelAndView("transfer", "command", transferData);
	}
	
	@RequestMapping(value = "/TransferStock/transferList", method = RequestMethod.GET)
	public ModelAndView downloadStockControlExcel(HttpServletRequest request) {
		TransferData transferData = TransferData.getInstance(DataUtils.requestUser(request));
		List<RSTransfer> transferList = transferData.getTransferResults().getResultTransfers();
		ModelAndView model = new ModelAndView("transferExcelView");
		model.addObject(AbstractExcelBuilder.EXCEL_TYPE, TransferExcelBuilder.EXCEL_TYPE_TRANSFER_LIST);
		model.addObject(AbstractExcelBuilder.EXCEL_DATA, transferList);
		model.addObject(AbstractExcelBuilder.EXCEL_FILENAME, "transferList" + AbstractExcelBuilder.getFileNameDatePrefix());
		return model;
	}
	
}