package rs4.core.search;

public class SearchResultsSummary {
	
	private String totalNetWt;	
	private String totalEntWt;
	private String totalEntWtBeans;
	private String avgMoisture;
	private String avgAdmix;
	private String avgTemperature;
	private String avgSpWt;
	private String avgPercentLoss;
	private String avgPercentLossBeans;
	
	public String getTotalNetWt() {
		return totalNetWt;
	}
	public void setTotalNetWt(String totalNetWt) {
		this.totalNetWt = totalNetWt;
	}
	public String getTotalEntWt() {
		return totalEntWt;
	}
	public void setTotalEntWt(String totalEntWt) {
		this.totalEntWt = totalEntWt;
	}
	public String getTotalEntWtBeans() {
		return totalEntWtBeans;
	}
	public void setTotalEntWtBeans(String totalEntWtBeans) {
		this.totalEntWtBeans = totalEntWtBeans;
	}
	public String getAvgMoisture() {
		return avgMoisture;
	}
	public void setAvgMoisture(String avgMoisture) {
		this.avgMoisture = avgMoisture;
	}
	public String getAvgAdmix() {
		return avgAdmix;
	}
	public void setAvgAdmix(String avgAdmix) {
		this.avgAdmix = avgAdmix;
	}
	public String getAvgTemperature() {
		return avgTemperature;
	}
	public void setAvgTemperature(String avgTemperature) {
		this.avgTemperature = avgTemperature;
	}
	public String getAvgSpWt() {
		return avgSpWt;
	}
	public void setAvgSpWt(String avgSpWt) {
		this.avgSpWt = avgSpWt;
	}
	public String getAvgPercentLoss() {
		return avgPercentLoss;
	}
	public void setAvgPercentLoss(String avgPercentLoss) {
		this.avgPercentLoss = avgPercentLoss;
	}
	public String getAvgPercentLossBeans() {
		return avgPercentLossBeans;
	}
	public void setAvgPercentLossBeans(String avgPercentLossBeans) {
		this.avgPercentLossBeans = avgPercentLossBeans;
	}
	
}
