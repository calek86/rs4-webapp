package rs4.core.search;

import rs4.utils.Logger;

public enum TransactionType {
	
	All, IN, OUT, WO;
	
	static Logger logger = Logger.getLogger(SearchData.class.getName());
	
	public static TransactionType fromString(String transactionType) {
		try {
			return TransactionType.valueOf(transactionType);
		} catch (Exception e) {
			logger.error("Invalid transaction type @", transactionType);
		}
		return null;
	}
}
