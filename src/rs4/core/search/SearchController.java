package rs4.core.search;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import rs4.core.excel.AbstractExcelBuilder;
import rs4.core.excel.SearchExcelBuilder;
import rs4.core.search.form.TransactionForm;
import rs4.core.search.form.TransactionFormValidator;
import rs4.database.dao.model.RSTransaction;
import rs4.database.dao.model.RSUser;
import rs4.utils.DataUtils;
import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;

@Controller
@SessionAttributes("transactionForm")
public class SearchController {

	private TransactionFormValidator transactionValidator;

	public SearchController() {
		transactionValidator = new TransactionFormValidator();
	}

	@RequestMapping(value = "/SearchTransactions", params = "Search", method = RequestMethod.POST)
	public ModelAndView searchTransactions(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		SearchData searchData = SearchData.getInstance(user);
		searchData.getSearchCriteria().setReportType(request.getParameter("searchCriteria.reportType"));
		searchData.getSearchCriteria().setTransactionsNo(request.getParameter("searchCriteria.transactionsNo"));
		searchData.getSearchCriteria().setRegistration(request.getParameter("searchCriteria.registration"));
		searchData.getSearchCriteria().setTransactionType(request.getParameter("searchCriteria.transactionTypes"));
		searchData.getSearchCriteria().setDateFrom(request.getParameter("searchCriteria.dateFrom"));
		searchData.getSearchCriteria().setDateTo(request.getParameter("searchCriteria.dateTo"));
		searchData.getSearchCriteria().setCustomerCode(request.getParameter("searchCriteria.customerCode"));
		searchData.getSearchCriteria().setProductCode(request.getParameter("searchCriteria.products"));
		searchData.getSearchCriteria().setHaulierCode(request.getParameter("searchCriteria.hauliers"));
		searchData.getSearchCriteria().setDestinationCode(request.getParameter("searchCriteria.destinations"));
		searchData.getSearchCriteria().setSourceCode(request.getParameter("searchCriteria.sources"));
		searchData.searchTransactions(user);
		return new ModelAndView("search", "command", searchData);
	}

	@RequestMapping(value = "/SearchTransactions", params = "Add", method = RequestMethod.POST)
	public ModelAndView gotoAddTransaction(HttpServletRequest request, ModelMap model) {
		SearchData searchData = SearchData.getInstance(DataUtils.requestUser(request));
		TransactionForm transactionForm = searchData.getTransactionForm();
		transactionForm.setTransactionDate(DateUtils.getDateTimeToday());
		model.addAttribute("transactionForm", transactionForm);
		return new ModelAndView("transaction");
	}

	@RequestMapping(value = "/SearchTransactions", params = "Save", method = RequestMethod.POST)
	public ModelAndView saveTransaction(@ModelAttribute("transactionForm") TransactionForm transactionForm,
			BindingResult result, ModelMap model, HttpServletRequest request) {
		transactionValidator.validate(transactionForm, result);
		if (result.hasErrors()) {
			return new ModelAndView("transaction");
		}
		RSUser user = DataUtils.requestUser(request);
		SearchData searchData = SearchData.getInstance(user);
		RSTransaction rst = TransactionForm.formToRSTransaction(transactionForm);
		searchData.insertTransaction(rst, user);
		model.addAttribute("addingTransactionsOK", "Succesfully saved new transaction");
		searchData.searchTransactions(user);
		return new ModelAndView("search", "command", searchData);
	}
	
	@RequestMapping(value = "/EditTransaction", method = RequestMethod.POST)
	public ModelAndView gotoEditTransaction(HttpServletRequest request, ModelMap model) {
		String idToEdit = request.getParameter("idToEdit");
		SearchData searchData = SearchData.getInstance(DataUtils.requestUser(request));
		RSTransaction rsTransaction = searchData.getTransactionById(Integer.parseInt(idToEdit));
		if (rsTransaction != null) {
			TransactionForm form = searchData.getTransactionForm(rsTransaction);
			form.setFirstWeight(DisplayUtils.formatPlainNumber(rsTransaction.getFirstWeight(), 0));
			form.setSecondWeight(DisplayUtils.formatPlainNumber(rsTransaction.getSecondWeight(), 0));
			form.setNetWeight(DisplayUtils.formatPlainNumber(rsTransaction.getNetWeight(), 0));
			form.setEntWeight(DisplayUtils.formatPlainNumber(rsTransaction.getEntWeight(), 0));
			form.setEntWeightBeans(DisplayUtils.formatPlainNumber(rsTransaction.getEntWeightBeans(), 0));
			model.addAttribute("transactionForm", form);
			return new ModelAndView("transaction");
		} else {
			return new ModelAndView("search", "command", searchData);
		}
	}

	@RequestMapping(value = "/EditTransaction", params = "Save", method = RequestMethod.POST)
	public ModelAndView editTransaction(@ModelAttribute("transactionForm") TransactionForm transactionForm,
			BindingResult result, ModelMap model, HttpServletRequest request) {
		transactionValidator.validate(transactionForm, result);
		if (result.hasErrors()) {
			return new ModelAndView("transaction");
		}
		RSUser user = DataUtils.requestUser(request);
		SearchData searchData = SearchData.getInstance(user);
		RSTransaction rst = TransactionForm.formToRSTransaction(transactionForm);
		searchData.updateTransaction(rst, user);
		model.addAttribute("editTransactionsOK", "Succesfully updated transaction");
		searchData.searchTransactions(user);
		return new ModelAndView("search", "command", searchData);
	}

	@RequestMapping(value = "/EditTransaction", params = "Delete", method = RequestMethod.POST)
	public ModelAndView deleteTransaction(@ModelAttribute("transactionForm") TransactionForm transactionForm,
			BindingResult result, ModelMap model, HttpServletRequest request) {
		transactionValidator.validate(transactionForm, result);
		if (result.hasErrors()) {
			return new ModelAndView("transaction");
		}
		RSUser user = DataUtils.requestUser(request);
		SearchData searchData = SearchData.getInstance(user);
		RSTransaction rst = TransactionForm.formToRSTransaction(transactionForm);
		searchData.deleteTransaction(rst, user);
		model.addAttribute("deleteTransactionsOK", "Succesfully deleted transaction");
		searchData.searchTransactions(user);
		return new ModelAndView("search", "command", searchData);
	}

	@RequestMapping(value = "/SearchTransactions", params = "Cancel", method = RequestMethod.POST)
	public ModelAndView cancelSavingTransaction(@ModelAttribute("transactionForm") TransactionForm transactionForm,
			BindingResult result, HttpServletRequest request) {
		transactionForm = new TransactionForm();
		RSUser user = DataUtils.requestUser(request);
		SearchData searchData = SearchData.getInstance(user);
		searchData.searchTransactions(user);
		return new ModelAndView("search", "command", searchData);
	}

	@RequestMapping(value = "/EditTransaction", params = "Cancel", method = RequestMethod.POST)
	public ModelAndView cancelUpdatingTransaction(@ModelAttribute("transactionForm") TransactionForm transactionForm,
			BindingResult result, HttpServletRequest request) {
		transactionForm = new TransactionForm();
		RSUser user = DataUtils.requestUser(request);
		SearchData searchData = SearchData.getInstance(user);
		searchData.searchTransactions(user);
		return new ModelAndView("search", "command", searchData);
	}

	@RequestMapping(value = "/SearchTransactions/simpleExcel", method = RequestMethod.GET)
	public ModelAndView downloadSimpleSearchExcel(HttpServletRequest request) {
		SearchData searchData = SearchData.getInstance(DataUtils.requestUser(request));
		List<RSTransaction> transactionList = searchData.getSearchResults().getResultTransactions();
		ModelAndView model = new ModelAndView("searchExcelView");
		model.addObject(AbstractExcelBuilder.EXCEL_TYPE, SearchExcelBuilder.EXCEL_TYPE_TRANSACTIONS_SIMPLE);
		model.addObject(AbstractExcelBuilder.EXCEL_DATA, transactionList);
		model.addObject(AbstractExcelBuilder.EXCEL_FILENAME,
				"transactionsSimple" + AbstractExcelBuilder.getFileNameDatePrefix());
		model.addObject(AbstractExcelBuilder.EXCEL_SUMMARY_DATA, searchData.getSearchResults().getResultsSummary());
		return model;
	}

	@RequestMapping(value = "/SearchTransactions/detailedExcel", method = RequestMethod.GET)
	public ModelAndView downloadDetailedSearchdExcel(HttpServletRequest request) {
		SearchData searchData = SearchData.getInstance(DataUtils.requestUser(request));
		List<RSTransaction> transactionList = searchData.getSearchResults().getResultTransactions();
		ModelAndView model = new ModelAndView("searchExcelView");
		model.addObject(AbstractExcelBuilder.EXCEL_TYPE, SearchExcelBuilder.EXCEL_TYPE_TRANSACTIONS_DETAILED);
		model.addObject(AbstractExcelBuilder.EXCEL_DATA, transactionList);
		model.addObject(AbstractExcelBuilder.EXCEL_FILENAME,
				"transactionsDetailed" + AbstractExcelBuilder.getFileNameDatePrefix());
		model.addObject(AbstractExcelBuilder.EXCEL_SUMMARY_DATA, searchData.getSearchResults().getResultsSummary());
		return model;
	}

}