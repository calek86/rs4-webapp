package rs4.core.search.form;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import rs4.core.search.TransactionType;
import rs4.utils.DateUtils;

public class TransactionFormValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return TransactionForm.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		TransactionForm atf = (TransactionForm) target;
		if (StringUtils.isBlank(atf.getTransactionDate())) {
			errors.rejectValue("transactionDate", "empty");
		} else {
			try {
				DateUtils.parseDateTime(atf.getTransactionDate());
			} catch (Exception e) {
				errors.rejectValue("transactionDate", "notDate");
			}
		}
		if (StringUtils.isBlank(atf.getFirstWeight())) {
			errors.rejectValue("firstWeight", "empty");
		} else {
			try {
				Float f = Float.parseFloat(atf.getFirstWeight());
				if (f < 0) {
					errors.rejectValue("firstWeight", "notPositiveNumber");
				}
			} catch (NumberFormatException e) {
				errors.rejectValue("firstWeight", "notNumber");
			}
		}
		if (StringUtils.isBlank(atf.getSecondWeight())) {
			errors.rejectValue("secondWeight", "empty");
		} else {
			try {
				Float f = Float.parseFloat(atf.getSecondWeight());
				if (f < 0) {
					errors.rejectValue("secondWeight", "notPositiveNumber");
				}
			} catch (NumberFormatException e) {
				errors.rejectValue("secondWeight", "notNumber");
			}
		}
		if (StringUtils.isBlank(atf.getTransactionType())) {
			errors.rejectValue("transactionType", "empty");
		} else {
			try {
				Float first = Float.parseFloat(atf.getFirstWeight());
				Float second = Float.parseFloat(atf.getSecondWeight());
				if(atf.getTransactionType().equals(TransactionType.IN.toString())) {
					if(second > first) {
						errors.rejectValue("secondWeight", "biggerThenFirstWeight");
					}
				}
				else if(atf.getTransactionType().equals(TransactionType.OUT.toString())) {
					if(first > second) {
						errors.rejectValue("firstWeight", "biggerThenSecondWeight");	
					}
				}
			} catch (NumberFormatException e) {
			}
		}
		if (!StringUtils.isBlank(atf.getMoisture())) {
			try {
				Float f = Float.parseFloat(atf.getMoisture());
				if (f < 0) {
					errors.rejectValue("moisture", "notPositiveNumber");
				}
			} catch (NumberFormatException e) {
				errors.rejectValue("moisture", "notNumber");
			}
		}
		if (!StringUtils.isBlank(atf.getAdmix())) {
			try {
				Float f = Float.parseFloat(atf.getAdmix());
				if (f < 0) {
					errors.rejectValue("admix", "notPositiveNumber");
				}
			} catch (NumberFormatException e) {
				errors.rejectValue("admix", "notNumber");
			}
		}
		if (!StringUtils.isBlank(atf.getSpWt())) {
			try {
				Float f = Float.parseFloat(atf.getSpWt());
				if (f < 0) {
					errors.rejectValue("spWt", "notPositiveNumber");
				}
			} catch (NumberFormatException e) {
				errors.rejectValue("spWt", "notNumber");
			}
		}
		if (!StringUtils.isBlank(atf.getTemperature())) {
			try {
				Float f = Float.parseFloat(atf.getTemperature());
				if (f < 0) {
					errors.rejectValue("temperature", "notPositiveNumber");
				}
			} catch (NumberFormatException e) {
				errors.rejectValue("temperature", "notNumber");
			}
		}
		if (!StringUtils.isBlank(atf.getScreen1()) && atf.getScreen1().length() > 15) {
			errors.rejectValue("screen1", "addTransaction.screen1.tooLong");
		} 
		if (!StringUtils.isBlank(atf.getScreen2()) && atf.getScreen2().length() > 15) {
			errors.rejectValue("screen2", "addTransaction.screen2.tooLong");
		}
		if (!StringUtils.isBlank(atf.getOrderNo()) && atf.getOrderNo().length() > 50) {
			errors.rejectValue("orderNo", "addTransaction.orderNo.tooLong");
		}
		if (!StringUtils.isBlank(atf.getExRef()) && atf.getExRef().length() > 50) {
			errors.rejectValue("exRef", "addTransaction.exRef.tooLong");
		}
		if (!StringUtils.isBlank(atf.getN2CP()) && atf.getN2CP().length() > 50) {
			errors.rejectValue("n2CP", "addTransaction.n2CP.tooLong");
		}
		if (!StringUtils.isBlank(atf.getHagberg()) && atf.getHagberg().length() > 50) {
			errors.rejectValue("hagberg", "addTransaction.hagberg.tooLong");
		}
		if (!StringUtils.isBlank(atf.getFirstPrevLoad()) && atf.getFirstPrevLoad().length() > 50) {
			errors.rejectValue("firstPrevLoad", "addTransaction.firstPrevLoad.tooLong");
		}
		if (!StringUtils.isBlank(atf.getSecondPrevLoad()) && atf.getSecondPrevLoad().length() > 50) {
			errors.rejectValue("secondPrevLoad", "addTransaction.secondPrevLoad.tooLong");
		}
		if (!StringUtils.isBlank(atf.getThirdPrevLoad()) && atf.getThirdPrevLoad().length() > 50) {
			errors.rejectValue("thirdPrevLoad", "addTransaction.thirdPrevLoad.tooLong");
		}
		if (!StringUtils.isBlank(atf.getTrailerNo()) && atf.getTrailerNo().length() > 50) {
			errors.rejectValue("trailerNo", "addTransaction.trailerNo.tooLong");
		}
		if (!StringUtils.isBlank(atf.getaCCSNo()) && atf.getaCCSNo().length() > 50) {
			errors.rejectValue("aCCSNo", "addTransaction.aCCSNo.tooLong");
		}
		if (!StringUtils.isBlank(atf.getHoled()) && atf.getHoled().length() > 50) {
			errors.rejectValue("holen", "addTransaction.holen.tooLong");
		}
		if (!StringUtils.isBlank(atf.getBlind()) && atf.getBlind().length() > 50) {
			errors.rejectValue("blind", "addTransaction.blind.tooLong");
		}
		if (!StringUtils.isBlank(atf.getStains()) && atf.getStains().length() > 50) {
			errors.rejectValue("stains", "addTransaction.stains.tooLong");
		}
		if (!StringUtils.isBlank(atf.getComments()) && atf.getComments().length() > 255) {
			errors.rejectValue("stains", "addTransaction.comments.tooLong");
		}
	}

}
