package rs4.core.search.form;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

import rs4.database.dao.model.RSCustomer;
import rs4.database.dao.model.RSDestination;
import rs4.database.dao.model.RSHaulier;
import rs4.database.dao.model.RSProduct;
import rs4.database.dao.model.RSSource;
import rs4.database.dao.model.RSStockYear;
import rs4.database.dao.model.RSTransaction;
import rs4.utils.DataUtils;
import rs4.utils.DateUtils;

public class TransactionForm implements Comparable<TransactionForm> {

	private Integer id;
	private String weighmanId;
	private String ticketId;
	private String transactionType;
	private String transactionDate;
	private String customer;
	private List<RSCustomer> customers;
	private String haulier;
	private List<RSHaulier> hauliers;
	private String product;
	private List<RSProduct> products;
	private String destination;
	private List<RSDestination> destinations;
	private String source;
	private List<RSSource> sources;	
	private String currentStockYear;
	private String stockYearWithId;
	private List<RSStockYear> stockYears;
	private String registration;
	private String firstWeight;
	private String secondWeight;
	private String netWeight;
	private String entWeight;
	private String entWeightBeans;
	private String percentLoss;
	private String percentLossBeans;
	private String moisture;
	private String admix;
	private String spWt;
	private String temperature;
	private String screen1;
	private String screen2;
	private String orderNo;
	private String exRef;
	private String n2CP;
	private String hagberg;
	private String firstPrevLoad;
	private String secondPrevLoad;
	private String thirdPrevLoad;
	private String trailerNo;
	private String inspected;
	private String aCCSNo;
	private String holed;
	private String blind;
	private String stains;
	private String comments;

	public static RSTransaction formToRSTransaction(TransactionForm form) {
		RSTransaction rst = new RSTransaction();
		if (form.getId() != null) {
			rst.setId(form.getId());
		}
		rst.setWeighmanId(UUID.randomUUID().toString());

		if (form.getTicketId() == null) {
			rst.setTicketId(DataUtils.generateNextTransactionTicketId());
		} else {
			rst.setTicketId(form.getTicketId());
		}

		rst.setTransactionType(form.getTransactionType());
		try {
			rst.setTransactionDate(DateUtils.parseDateTimeToTimestamp(form.getTransactionDate()));
		} catch (Exception e) {
			rst.setTransactionDate(new Timestamp((new Date()).getTime()));
		}

		rst.setModificationDate(new Timestamp((new Date()).getTime()));
		
		String[] splitCustomer = form.getCustomer().split("\\|");
		Integer customerId = Integer.parseInt(splitCustomer[0]);
		String customerCode = splitCustomer[1];
		rst.setCustomerId(customerId);
		rst.setCustomer(customerCode);

		String[] splitHaulier = form.getHaulier().split("\\|");
		Integer haulierId = Integer.parseInt(splitHaulier[0]);
		String haulierCode = splitHaulier[1];
		rst.setHaulierId(haulierId);
		rst.setHaulier(haulierCode);

		String[] splitProduct = form.getProduct().split("\\|");
		Integer productId = Integer.parseInt(splitProduct[0]);
		String productCode = splitProduct[1];
		rst.setProductId(productId);
		rst.setProduct(productCode);

		String[] splitDestination = form.getDestination().split("\\|");
		Integer destinationId = Integer.parseInt(splitDestination[0]);
		String destinationDescription = splitDestination[1];
		rst.setDestination(destinationDescription);
		rst.setDestinationId(destinationId);

		String[] splitSource = form.getSource().split("\\|");
		Integer sourceId = Integer.parseInt(splitSource[0]);
		String sourceDescription = splitSource[1];
		rst.setSource(sourceDescription);
		rst.setSourceId(sourceId);
		
		String[] splitStockYear = form.getStockYearWithId().split("\\|");
		Integer stockYearId = Integer.parseInt(splitStockYear[0]);
		String stockYear = splitStockYear[1];
		rst.setStockYearId(stockYearId);
		rst.setStockYear(stockYear);

		if (!StringUtils.isBlank(form.getRegistration())) {
			rst.setRegistration(form.getRegistration());
		}

		float firstWeight = Float.parseFloat(form.getFirstWeight()); 
		rst.setFirstWeight(firstWeight);
		float secondWeight = Float.parseFloat(form.getSecondWeight());
		rst.setSecondWeight(secondWeight);
		rst.setNetWeight(Math.abs(firstWeight-secondWeight));

		if (!StringUtils.isBlank(form.getMoisture())) {
			rst.setMoisture(Float.parseFloat(form.getMoisture()));
		}

		if (!StringUtils.isBlank(form.getAdmix())) {
			rst.setAdmix(Float.parseFloat(form.getAdmix()));
		}

		if (!StringUtils.isBlank(form.getSpWt())) {
			rst.setSpWt(Float.parseFloat(form.getSpWt()));
		}

		if (!StringUtils.isBlank(form.getTemperature())) {
			rst.setTemperature(Float.parseFloat(form.getTemperature()));
		}

		rst.setScreen1(form.getScreen1());
		rst.setScreen2(form.getScreen2());
		rst.setOrderNo(form.getOrderNo());
		rst.setExRef(form.getExRef());
		rst.setHagberg(form.getHagberg());
		rst.setFirstPrevLoad(form.getFirstPrevLoad());
		rst.setSecondPrevLoad(form.getSecondPrevLoad());
		rst.setThirdPrevLoad(form.getThirdPrevLoad());
		rst.setTrailerNo(form.getTrailerNo());
		rst.setInspected(form.getInspected());
		rst.setN2CP(form.getN2CP());
		rst.setaCCSNo(form.getaCCSNo());
		rst.setHoled(form.getHoled());
		rst.setBlind(form.getBlind());
		rst.setStains(form.getStains());
		rst.setComments(form.getComments());
		
		DataUtils.putStockEntWt(rst);
		
		return rst;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getWeighmanId() {
		return weighmanId;
	}

	public void setWeighmanId(String weighmanId) {
		this.weighmanId = weighmanId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getHaulier() {
		return haulier;
	}

	public void setHaulier(String haulier) {
		this.haulier = haulier;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getScreen1() {
		return screen1;
	}

	public void setScreen1(String screen1) {
		this.screen1 = screen1;
	}

	public String getScreen2() {
		return screen2;
	}

	public void setScreen2(String screen2) {
		this.screen2 = screen2;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getExRef() {
		return exRef;
	}

	public void setExRef(String exRef) {
		this.exRef = exRef;
	}

	public String getN2CP() {
		return n2CP;
	}

	public void setN2CP(String n2cp) {
		n2CP = n2cp;
	}

	public String getHagberg() {
		return hagberg;
	}

	public void setHagberg(String hagberg) {
		this.hagberg = hagberg;
	}

	public String getFirstPrevLoad() {
		return firstPrevLoad;
	}

	public void setFirstPrevLoad(String firstPrevLoad) {
		this.firstPrevLoad = firstPrevLoad;
	}

	public String getSecondPrevLoad() {
		return secondPrevLoad;
	}

	public void setSecondPrevLoad(String secondPrevLoad) {
		this.secondPrevLoad = secondPrevLoad;
	}

	public String getThirdPrevLoad() {
		return thirdPrevLoad;
	}

	public void setThirdPrevLoad(String thirdPrevLoad) {
		this.thirdPrevLoad = thirdPrevLoad;
	}

	public String getTrailerNo() {
		return trailerNo;
	}

	public void setTrailerNo(String trailerNo) {
		this.trailerNo = trailerNo;
	}

	public String getInspected() {
		return inspected;
	}

	public void setInspected(String inspected) {
		this.inspected = inspected;
	}

	public String getaCCSNo() {
		return aCCSNo;
	}

	public void setaCCSNo(String aCCSNo) {
		this.aCCSNo = aCCSNo;
	}

	public String getHoled() {
		return holed;
	}

	public void setHoled(String holed) {
		this.holed = holed;
	}

	public String getBlind() {
		return blind;
	}

	public void setBlind(String blind) {
		this.blind = blind;
	}

	public String getStains() {
		return stains;
	}

	public void setStains(String stains) {
		this.stains = stains;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public List<RSCustomer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<RSCustomer> customers) {
		this.customers = customers;
	}

	public List<RSHaulier> getHauliers() {
		return hauliers;
	}

	public void setHauliers(List<RSHaulier> hauliers) {
		this.hauliers = hauliers;
	}

	public List<RSProduct> getProducts() {
		return products;
	}

	public void setProducts(List<RSProduct> products) {
		this.products = products;
	}

	public List<RSDestination> getDestinations() {
		return destinations;
	}

	public void setDestinations(List<RSDestination> destinations) {
		this.destinations = destinations;
	}

	public List<RSSource> getSources() {
		return sources;
	}

	public void setSources(List<RSSource> sources) {
		this.sources = sources;
	}

	public List<RSStockYear> getStockYears() {
		return stockYears;
	}

	public void setStockYears(List<RSStockYear> stockYears) {
		this.stockYears = stockYears;
	}

	public String getFirstWeight() {
		return firstWeight;
	}

	public void setFirstWeight(String firstWeight) {
		this.firstWeight = firstWeight;
	}

	public String getSecondWeight() {
		return secondWeight;
	}

	public void setSecondWeight(String secondWeight) {
		this.secondWeight = secondWeight;
	}

	public String getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(String netWeight) {
		this.netWeight = netWeight;
	}

	public String getEntWeight() {
		return entWeight;
	}

	public void setEntWeight(String entWeight) {
		this.entWeight = entWeight;
	}

	public String getPercentLoss() {
		return percentLoss;
	}

	public void setPercentLoss(String percentLoss) {
		this.percentLoss = percentLoss;
	}

	public String getPercentLossBeans() {
		return percentLossBeans;
	}

	public void setPercentLossBeans(String percentLossBeans) {
		this.percentLossBeans = percentLossBeans;
	}

	public String getMoisture() {
		return moisture;
	}

	public void setMoisture(String moisture) {
		this.moisture = moisture;
	}

	public String getAdmix() {
		return admix;
	}

	public void setAdmix(String admix) {
		this.admix = admix;
	}

	public String getSpWt() {
		return spWt;
	}

	public void setSpWt(String spWt) {
		this.spWt = spWt;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getCurrentStockYear() {
		return currentStockYear;
	}

	public void setCurrentStockYear(String currentStockYear) {
		this.currentStockYear = currentStockYear;
	}

	public String getStockYearWithId() {
		return stockYearWithId;
	}

	public void setStockYearWithId(String stockYearWithId) {
		this.stockYearWithId = stockYearWithId;
	}

	public String getEntWeightBeans() {
		return entWeightBeans;
	}

	public void setEntWeightBeans(String entWeightBeans) {
		this.entWeightBeans = entWeightBeans;
	}

	@Override
	public int compareTo(TransactionForm other) {
		try {
			Date thisDate = DateUtils.parseDateTime(this.getTransactionDate());
			Date otherDate = DateUtils.parseDateTime(other.getTransactionDate());
			return thisDate.compareTo(otherDate);
		} catch (Exception e) {
		}
		return 0;
	}

}
