package rs4.core.search;

import java.util.List;

import rs4.core.login.AccessLevel;
import rs4.database.dao.model.RSTransaction;

public class SearchResults {

	private String accessLevel;
	private String reportType;
	private String reportDate;
	private SearchResultsSummary resultsSummary;
	private List<RSTransaction> resultTransactions;

	public SearchResults() {
		reportType = "Simple";
		accessLevel = AccessLevel.READ.toString();
		resultsSummary = new SearchResultsSummary();
	}
	
	public String getAccessLevel() {
		return accessLevel;
	}

	public void setAccessLevel(String accessLevel) {
		this.accessLevel = accessLevel;
	}

	public List<RSTransaction> getResultTransactions() {
		return resultTransactions;
	}

	public void setResultTransactions(List<RSTransaction> resultTransactions) {
		this.resultTransactions = resultTransactions;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public SearchResultsSummary getResultsSummary() {
		return resultsSummary;
	}

	public void setResultsSummary(SearchResultsSummary resultsSummary) {
		this.resultsSummary = resultsSummary;
	}
}
