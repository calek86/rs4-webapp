package rs4.core.search;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rs4.core.search.form.TransactionForm;
import rs4.database.dao.RSCustomerDao;
import rs4.database.dao.RSDestinationDao;
import rs4.database.dao.RSHaulierDao;
import rs4.database.dao.RSProductDao;
import rs4.database.dao.RSSourceDao;
import rs4.database.dao.RSStockYearDao;
import rs4.database.dao.RSTransactionDao;
import rs4.database.dao.RSTransactionTrashDao;
import rs4.database.dao.model.RSCustomer;
import rs4.database.dao.model.RSDestination;
import rs4.database.dao.model.RSHaulier;
import rs4.database.dao.model.RSProduct;
import rs4.database.dao.model.RSSource;
import rs4.database.dao.model.RSStockYear;
import rs4.database.dao.model.RSTransaction;
import rs4.database.dao.model.RSUser;
import rs4.utils.DataUtils;
import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;
import rs4.utils.Logger;

public class SearchData {

	private RSCustomerDao rsCustomerDao;
	private RSProductDao rsProductDao;
	private RSHaulierDao rsHaulierDao;
	private RSSourceDao rsSourceDao;
	private RSDestinationDao rsDestinationDao;
	private RSStockYearDao rsStockYearDao;
	private RSTransactionDao rsTransactionDao;
	private RSTransactionTrashDao rsTransactionTrashDao;

	private SearchCriteria searchCriteria;
	private SearchResults searchResults;

	private static Map<Integer, SearchData> instances;
	private static Logger logger = Logger.getLogger(SearchData.class.getName());

	public static SearchData getInstance(RSUser user) {
		if (user == null) {
			logger.error("Requesting data object without user");
			return new SearchData();
		}
		if (instances == null) {
			instances = new HashMap<>();
		}
		if (!instances.containsKey(user.getId())) {
			instances.put(user.getId(), new SearchData());
		}
		return instances.get(user.getId());
	}

	public static void reloadInstance() {
		instances = new HashMap<>();
	}

	private SearchData() {
		rsCustomerDao = RSCustomerDao.getInstance();
		rsProductDao = RSProductDao.getInstance();
		rsHaulierDao = RSHaulierDao.getInstance();
		rsDestinationDao = RSDestinationDao.getInstance();
		rsStockYearDao = RSStockYearDao.getInstance();
		rsTransactionDao = RSTransactionDao.getInstance();
		rsSourceDao = RSSourceDao.getInstance();
		rsTransactionTrashDao = RSTransactionTrashDao.getInstance();
		searchCriteria = new SearchCriteria();
		searchResults = new SearchResults();
		reloadCriteria();
	}

	private void reloadCriteria() {
		searchCriteria.setCustomers(rsCustomerDao.getRSCustomersFromStockYear(searchCriteria.getStockYearId()));
		searchCriteria.setProducts(rsProductDao.getRSProductsFromStockYear(searchCriteria.getStockYearId()));
		searchCriteria.setHauliers(rsHaulierDao.getRSHauliersFromStockYear(searchCriteria.getStockYearId()));
		searchCriteria.setDestinations(rsDestinationDao.getRSDestinationsFromStockYear(searchCriteria.getStockYearId()));
		searchCriteria.setSources(rsSourceDao.getRSSourcesFromStockYear(searchCriteria.getStockYearId()));
		searchCriteria.setStockYears(rsStockYearDao.getRSStockYears());
	}

	public void searchTransactions(RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Search transactions @ by @ with criteria @", actionId, DisplayUtils.logUser(user),
				searchCriteria.toString());
		List<RSTransaction> rsTransactions = rsTransactionDao.getRSTransactionsByCriteria(searchCriteria);
		searchResults.setAccessLevel(DataUtils.requestAccess(user).toString());
		searchResults.setReportType(searchCriteria.getReportType().toString());
		searchResults.setReportDate(DateUtils.formatDateTime(new Date()));
		searchResults.setResultsSummary(getSearchResultsSummary(rsTransactions));
		searchResults.setResultTransactions(rsTransactions);
		reloadCriteria();
		logger.info("Search @ returned @ results", actionId, DisplayUtils.logList(rsTransactions));
	}

	public void reloadDates() {
		RSStockYear rsStockYear = DataUtils.getDefaultStockYear();
		searchCriteria.setDefaultDates(rsStockYear);
	}

	public TransactionForm getTransactionForm(RSTransaction transaction) {
		TransactionForm form = transaction.toTransactionForm(false);
		fillTransactionForm(form);
		return form;
	}

	public TransactionForm getTransactionForm() {
		TransactionForm form = new TransactionForm();
		fillTransactionForm(form);
		return form;
	}

	private void fillTransactionForm(TransactionForm form) {
		List<RSCustomer> customers = rsCustomerDao.getRSCustomers();
		List<RSHaulier> hauliers = rsHaulierDao.getRSHauliers();
		List<RSProduct> products = rsProductDao.getRSProducts();
		List<RSDestination> destinations = rsDestinationDao.getRSDestinations();
		List<RSSource> sources = rsSourceDao.getRSSources();
		List<RSStockYear> stockYears = rsStockYearDao.getRSStockYears();
		form.setCustomers(customers);
		form.setDestinations(destinations);
		form.setHauliers(hauliers);
		form.setProducts(products);
		form.setStockYears(stockYears);
		form.setSources(sources);
		form.setCurrentStockYear(searchCriteria.getStockYearDescription());
	}

	public static SearchResultsSummary getSearchResultsSummary(List<RSTransaction> rsTransactions) {
		SearchResultsSummary resultsSummary = new SearchResultsSummary();
		Float totalNetWt = 0.0F;
		Float totalEntWt = 0.0F;
		Float totalEntWtBeans = 0.0F;
		Float avgMoisture = 0.0F;
		int avgMoistureCount = 0;
		Float avgAdmix = 0.0F;
		int avgAdmixCount = 0;
		Float avgTemperature = 0.0F;
		int avgTemperatureCount = 0;
		Float avgSpWt = 0.0F;
		int avgSpWtCount = 0;
		Float avgPercentLoss = 0.0F;
		int avgPercentLossCount = 0;
		Float avgPercentLossBeans = 0.0F;
		int avgPercentLossBeansCount = 0;
		for (RSTransaction rsTransaction : rsTransactions) {
			totalNetWt += rsTransaction.getNetWeight();
			totalEntWt += rsTransaction.getEntWeight();
			totalEntWtBeans += rsTransaction.getEntWeightBeans();
			if (rsTransaction.getMoisture() > 0) {
				avgMoisture += rsTransaction.getMoisture();
				avgMoistureCount++;
			}
			if (rsTransaction.getAdmix() > 0) {
				avgAdmix += rsTransaction.getAdmix();
				avgAdmixCount++;
			}
			if (rsTransaction.getTemperature() > 0) {
				avgTemperature += rsTransaction.getTemperature();
				avgTemperatureCount++;
			}
			if (rsTransaction.getSpWt() > 0) {
				avgSpWt += rsTransaction.getSpWt();
				avgSpWtCount++;
			}
			if (rsTransaction.getPercentLoss() > 0) {
				avgPercentLoss += rsTransaction.getPercentLoss();
				avgPercentLossCount++;
			}
			if (rsTransaction.getPercentLossBeans() > 0) {
				avgPercentLossBeans += rsTransaction.getPercentLossBeans();
				avgPercentLossBeansCount++;
			}
		}
		if (avgMoisture > 0) {
			avgMoisture /= avgMoistureCount;
		}
		if (avgAdmix > 0) {
			avgAdmix /= avgAdmixCount;
		}
		if (avgTemperature > 0) {
			avgTemperature /= avgTemperatureCount;
		}
		if (avgSpWt > 0) {
			avgSpWt /= avgSpWtCount;
		}
		if (avgPercentLoss > 0) {
			avgPercentLoss /= avgPercentLossCount;
		}
		if (avgPercentLossBeans > 0) {
			avgPercentLossBeans /= avgPercentLossBeansCount;
		}
		resultsSummary.setTotalNetWt(DisplayUtils.formatWeight(totalNetWt, true));
		resultsSummary.setTotalEntWt(DisplayUtils.formatWeight(totalEntWt, true));
		resultsSummary.setTotalEntWtBeans(DisplayUtils.formatWeight(totalEntWtBeans, true));
		resultsSummary.setAvgMoisture(DisplayUtils.formatPercentage(avgMoisture, true));
		resultsSummary.setAvgAdmix(DisplayUtils.formatPercentage(avgAdmix, true));
		resultsSummary.setAvgTemperature(DisplayUtils.formatTemperature(avgTemperature, true));
		resultsSummary.setAvgSpWt(DisplayUtils.formatSpecificWeight(avgSpWt, true));
		resultsSummary.setAvgPercentLoss(DisplayUtils.formatPercentage(avgPercentLoss, true));
		resultsSummary.setAvgPercentLossBeans(DisplayUtils.formatPercentage(avgPercentLossBeans, true));
		return resultsSummary;
	}

	public SearchResults getSearchResults() {
		return searchResults;
	}

	public void setSearchResults(SearchResults searchResults) {
		this.searchResults = searchResults;
	}

	public SearchCriteria getSearchCriteria() {
		return searchCriteria;
	}

	public void setSearchCriteria(SearchCriteria searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

	public RSTransaction getTransactionById(int id) {
		return rsTransactionDao.getRSTransactionById(id);
	}

	public void insertTransaction(RSTransaction transaction, RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Add transaction @ by @ with data @", actionId, DisplayUtils.logUser(user), transaction.toString());
		rsTransactionDao.insert(transaction);
		logger.info("Add transaction @ successful", actionId);
	}

	public void updateTransaction(RSTransaction transaction, RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Update transaction @ by @ with data @", actionId, DisplayUtils.logUser(user),
				transaction.toString());
		rsTransactionDao.update(transaction);
		logger.info("Update transaction @  successful", actionId);
	}

	public void deleteTransaction(RSTransaction transaction, RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Delete transaction @ by @ with data @", actionId, DisplayUtils.logUser(user),
				transaction.toString());
		rsTransactionTrashDao.insert(transaction.toTrash());
		rsTransactionDao.deleteTransactionById(transaction.getId());
		logger.info("Delete transaction @ successful", actionId);
	}

}
