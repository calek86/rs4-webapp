package rs4.core.search;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rs4.database.dao.model.RSCustomer;
import rs4.database.dao.model.RSDestination;
import rs4.database.dao.model.RSHaulier;
import rs4.database.dao.model.RSProduct;
import rs4.database.dao.model.RSSource;
import rs4.database.dao.model.RSStockYear;
import rs4.utils.DataUtils;
import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;

public class SearchCriteria {

	private int stockYearId;

	private SearchReportType reportType;
	private TransactionType transactionType;
	private String transactionsNo;
	private String registration;
	private String customerCode;
	private String productCode;
	private String destinationCode;
	private String haulierCode;
	private String sourceCode;
	private String stockYearDescription;
	private String dateFrom;
	private String dateTo;

	private List<SearchReportType> reportTypes;
	private List<TransactionType> transactionTypes;
	private List<RSStockYear> stockYears;
	private List<RSCustomer> customers;
	private List<RSProduct> products;
	private List<RSDestination> destinations;
	private List<RSHaulier> hauliers;
	private List<RSSource> sources;

	public enum SearchReportType {
		Simple, Detailed
	}

	public SearchCriteria() {
		RSStockYear rsStockYear = DataUtils.getDefaultStockYear();
		setStockYearId(rsStockYear.getId());
		setStockYearDescription(rsStockYear.getDescription());
		setReportType(SearchReportType.Simple);
		setTransactionType(DisplayUtils.KEYWORD_ALL);
		setCustomerCode(DisplayUtils.KEYWORD_ALL);
		setDestinationCode(DisplayUtils.KEYWORD_ALL);
		setHaulierCode(DisplayUtils.KEYWORD_ALL);
		setProductCode(DisplayUtils.KEYWORD_ALL);
		setSourceCode(DisplayUtils.KEYWORD_ALL);
		setDefaultDates(rsStockYear);
		setTransactionTypes();
		setReportTypes();
	}

	private void setReportTypes() {
		reportTypes = new ArrayList<SearchReportType>();
		for (SearchReportType type : SearchReportType.values()) {
			reportTypes.add(type);
		}
	}

	private void setTransactionTypes() {
		transactionTypes = new ArrayList<TransactionType>();
		for (TransactionType transactionType : TransactionType.values()) {
			transactionTypes.add(transactionType);
		}
	}

	public void setDefaultDates(RSStockYear rsStockYear) {
		Date stockDateFrom = rsStockYear.getDateFrom();
		setDateFrom(DateUtils.formatDate(stockDateFrom));
		Date stockDateTo = rsStockYear.getDateTo();
		Date dateToday = new Date();
		if(dateToday.after(stockDateTo)) {
			setDateTo(DateUtils.formatDate(stockDateTo));	
		}
		else {
			setDateTo(DateUtils.getDateToday());
		}
	}

	public List<RSCustomer> getCustomers() {
		return customers;
	}

	public List<RSProduct> getProducts() {
		return products;
	}

	public List<RSDestination> getDestinations() {
		return destinations;
	}

	public List<RSHaulier> getHauliers() {
		return hauliers;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customer) {
		this.customerCode = customer;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String product) {
		this.productCode = product;
	}

	public String getDestinationCode() {
		return destinationCode;
	}

	public void setDestinationCode(String destination) {
		this.destinationCode = destination;
	}

	public String getHaulierCode() {
		return haulierCode;
	}

	public void setHaulierCode(String haulier) {
		this.haulierCode = haulier;
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public int getStockYearId() {
		return stockYearId;
	}

	public void setStockYearId(int stockYearId) {
		this.stockYearId = stockYearId;
	}

	public List<RSStockYear> getStockYears() {
		return stockYears;
	}

	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public void setTransactionType(String transactionType) {
		TransactionType type = TransactionType.fromString(transactionType);
		if(type != null) {
			 this.transactionType = type;	
		}
		else {
			 this.transactionType = TransactionType.All;
		}
	}
	
	public List<TransactionType> getTransactionTypes() {
		return transactionTypes;
	}

	public void setTransactionTypes(List<TransactionType> transactionTypes) {
		this.transactionTypes = transactionTypes;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public String getTransactionsNo() {
		return transactionsNo;
	}

	public void setTransactionsNo(String transactionsNo) {
		this.transactionsNo = transactionsNo;
	}

	public SearchReportType getReportType() {
		return reportType;
	}

	public void setReportType(SearchReportType reportType) {
		this.reportType = reportType;
	}

	public void setReportType(String reportType) {
		if (reportType.equals(SearchReportType.Simple.toString())) {
			this.reportType = SearchReportType.Simple;
		}
		if (reportType.equals(SearchReportType.Detailed.toString())) {
			this.reportType = SearchReportType.Detailed;
		}
	}

	public String getStockYearDescription() {
		return stockYearDescription;
	}

	public void setStockYearDescription(String stockYearDescription) {
		this.stockYearDescription = stockYearDescription;
	}

	public void setStockYears(List<RSStockYear> stockYears) {
		this.stockYears = stockYears;
	}

	public void setCustomers(List<RSCustomer> customers) {
		this.customers = customers;
	}

	public void setProducts(List<RSProduct> products) {
		this.products = products;
	}

	public void setDestinations(List<RSDestination> destinations) {
		this.destinations = destinations;
	}

	public void setHauliers(List<RSHaulier> hauliers) {
		this.hauliers = hauliers;
	}

	public List<RSSource> getSources() {
		return sources;
	}

	public void setSources(List<RSSource> sources) {
		this.sources = sources;
	}

	public List<SearchReportType> getReportTypes() {
		return reportTypes;
	}

	public void setReportTypes(List<SearchReportType> reportTypes) {
		this.reportTypes = reportTypes;
	}

	@Override
	public String toString() {
		return DisplayUtils.buildToSting(Integer.toString(stockYearId), reportType.toString(), transactionType.toString(),
				transactionsNo, registration, customerCode, productCode, destinationCode, haulierCode, dateFrom, dateTo);
	}

}
