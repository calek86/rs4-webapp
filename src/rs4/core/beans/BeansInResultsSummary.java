package rs4.core.beans;

import rs4.core.search.SearchResultsSummary;

public class BeansInResultsSummary {
	
	private SearchResultsSummary searchResultSummary;

	private String avgScreen;
	private String avgHoled;
	private String avgBlind;
	private String avgStains;
	private String avgDeduction;
	private String avgFeed;
	private String avgHead;
	
	public SearchResultsSummary getSearchResultSummary() {
		return searchResultSummary;
	}
	public void setSearchResultSummary(SearchResultsSummary searchResultSummary) {
		this.searchResultSummary = searchResultSummary;
	}
	public String getAvgScreen() {
		return avgScreen;
	}
	public void setAvgScreen(String avgScreen) {
		this.avgScreen = avgScreen;
	}
	public String getAvgHoled() {
		return avgHoled;
	}
	public void setAvgHoled(String avgHoled) {
		this.avgHoled = avgHoled;
	}
	public String getAvgBlind() {
		return avgBlind;
	}
	public void setAvgBlind(String avgBlind) {
		this.avgBlind = avgBlind;
	}
	public String getAvgStains() {
		return avgStains;
	}
	public void setAvgStains(String avgStains) {
		this.avgStains = avgStains;
	}
	public String getAvgDeduction() {
		return avgDeduction;
	}
	public void setAvgDeduction(String avgDeduction) {
		this.avgDeduction = avgDeduction;
	}
	public String getAvgFeed() {
		return avgFeed;
	}
	public void setAvgFeed(String avgFeed) {
		this.avgFeed = avgFeed;
	}
	public String getAvgHead() {
		return avgHead;
	}
	public void setAvgHead(String avgHead) {
		this.avgHead = avgHead;
	}

}
