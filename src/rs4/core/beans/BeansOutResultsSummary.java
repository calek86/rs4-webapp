package rs4.core.beans;

public class BeansOutResultsSummary {
	
	private String fromDate;
	private String toDate;
	private String customer;
	private String totalNetWeight;
	
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getTotalNetWeight() {
		return totalNetWeight;
	}
	public void setTotalNetWeight(String totalNetWeight) {
		this.totalNetWeight = totalNetWeight;
	}
	
}
