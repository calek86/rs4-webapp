package rs4.core.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rs4.core.search.TransactionType;
import rs4.database.dao.model.RSCustomer;
import rs4.database.dao.model.RSDestination;
import rs4.database.dao.model.RSHaulier;
import rs4.database.dao.model.RSProduct;
import rs4.database.dao.model.RSStockYear;
import rs4.utils.DataUtils;
import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;

public class BeansCriteria {

	private int stockYearId;

	private TransactionType transactionType;
	private BeansReportType reportType;
	private String transactionsNo;	
	private String customerCode;
	private String productCode;
	private String destinationCode;
	private String haulierCode;
	private String dateFrom;
	private String dateTo;

	private List<TransactionType> transactionTypes;
	private List<BeansReportType> reportTypes;
	private List<RSStockYear> stockYears;
	private List<RSCustomer> customers;
	private List<RSProduct> products;
	private List<RSDestination> destinations;
	private String stockYearDescription;
	private List<RSHaulier> hauliers;

	public enum BeansReportType {
		BeansIn, BeansOut, BeansSample
	}

	public BeansCriteria() {
		RSStockYear rsStockYear = DataUtils.getDefaultStockYear();
		setStockYearId(rsStockYear.getId());
		setStockYearDescription(rsStockYear.getDescription());
		setReportType(BeansReportType.BeansIn);
		setTransactionType(TransactionType.IN);
		setCustomerCode(DisplayUtils.KEYWORD_ALL);
		setProductCode(DisplayUtils.KEYWORD_ALL);
		setDestinationCode(DisplayUtils.KEYWORD_ALL);
		setHaulierCode(DisplayUtils.KEYWORD_ALL);
		setTransactionTypes();
		setDefaultDates(rsStockYear);
		setReportTypes();
	}

	private void setTransactionTypes() {
		transactionTypes = new ArrayList<TransactionType>();
		transactionTypes.add(TransactionType.IN);
		transactionTypes.add(TransactionType.OUT);
	}

	public void setDefaultDates(RSStockYear rsStockYear) {
		Date stockDateFrom = rsStockYear.getDateFrom();
		setDateFrom(DateUtils.formatDate(stockDateFrom));
		Date stockDateTo = rsStockYear.getDateTo();
		Date dateToday = new Date();
		if (dateToday.after(stockDateTo)) {
			setDateTo(DateUtils.formatDate(stockDateTo));
		} else {
			setDateTo(DateUtils.getDateToday());
		}
	}

	private void setReportTypes() {
		reportTypes = new ArrayList<BeansReportType>();
		for (BeansReportType s : BeansReportType.values()) {
			reportTypes.add(s);
		}
	}

	public List<RSCustomer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<RSCustomer> customers) {
		this.customers = customers;
	}

	public List<RSProduct> getProducts() {
		return products;
	}

	public void setProducts(List<RSProduct> products) {
		this.products = products;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customer) {
		this.customerCode = customer;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String product) {
		this.productCode = product;
	}

	public int getStockYearId() {
		return stockYearId;
	}

	public void setStockYearId(int stockYearId) {
		this.stockYearId = stockYearId;
	}

	public List<RSStockYear> getStockYears() {
		return stockYears;
	}

	public void setStockYears(List<RSStockYear> stockYear) {
		this.stockYears = stockYear;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public BeansReportType getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		if (reportType.equals(BeansReportType.BeansIn.toString())) {
			this.reportType = BeansReportType.BeansIn;
			this.transactionType = TransactionType.IN;
		}
		if (reportType.equals(BeansReportType.BeansOut.toString())) {
			this.reportType = BeansReportType.BeansOut;
			this.transactionType = TransactionType.OUT;
		}
		if (reportType.equals(BeansReportType.BeansSample.toString())) {
			this.reportType = BeansReportType.BeansSample;
		}		
	}

	public String getStockYearDescription() {
		return stockYearDescription;
	}

	public void setStockYearDescription(String stockYearDescription) {
		this.stockYearDescription = stockYearDescription;
	}
	
	public List<BeansReportType> getReportTypes() {
		return reportTypes;
	}

	public void setReportTypes(List<BeansReportType> reportTypes) {
		this.reportTypes = reportTypes;
	}

	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public void setTransactionType(String transactionType) {
		TransactionType type = TransactionType.fromString(transactionType);
		if (type != null) {
			this.transactionType = type;
		} else {
			this.transactionType = TransactionType.All;
		}
	}

	public List<TransactionType> getTransactionTypes() {
		return transactionTypes;
	}

	public void setTransactionTypes(List<TransactionType> transactionTypes) {
		this.transactionTypes = transactionTypes;
	}

	public void setReportType(BeansReportType reportType) {
		this.reportType = reportType;
	}

	public String getTransactionsNo() {
		return transactionsNo;
	}

	public void setTransactionsNo(String transactionsNo) {
		this.transactionsNo = transactionsNo;
	}
	
	public String getDestinationCode() {
		return destinationCode;
	}

	public void setDestinationCode(String destinationCode) {
		this.destinationCode = destinationCode;
	}

	public String getHaulierCode() {
		return haulierCode;
	}

	public void setHaulierCode(String haulierCode) {
		this.haulierCode = haulierCode;
	}

	public List<RSDestination> getDestinations() {
		return destinations;
	}

	public void setDestinations(List<RSDestination> destinations) {
		this.destinations = destinations;
	}

	public List<RSHaulier> getHauliers() {
		return hauliers;
	}

	public void setHauliers(List<RSHaulier> hauliers) {
		this.hauliers = hauliers;
	}
	
	@Override
	public String toString() {
		return DisplayUtils.buildToSting(Integer.toString(stockYearId), transactionType.toString(),
				reportType.toString(), customerCode, productCode, destinationCode, haulierCode, dateFrom, dateTo);
	}

}
