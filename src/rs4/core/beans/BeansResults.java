package rs4.core.beans;

import java.util.List;

import rs4.core.beans.form.BeansInForm;
import rs4.core.beans.form.BeansOutForm;
import rs4.core.login.AccessLevel;
import rs4.database.dao.model.RSBeansSample;
import rs4.database.dao.model.RSTransaction;

public class BeansResults {

	private String accessLevel;
	private String reportType;
	private String transactionType;
	private String reportDate;
	private List<RSTransaction> resultTransactions;
	private List<RSBeansSample> resultBeansSamples;
	private List<BeansInForm> resultBeansInTransactions;
	private List<BeansOutForm> resultBeansOutTransactions;
	private BeansInResultsSummary beansInResultsSummary;
	private BeansOutResultsSummary beansOutResultsSummary;
	
	public BeansResults() {
		reportType = "BeansIn";
		accessLevel = AccessLevel.READ.toString();
		beansInResultsSummary = new BeansInResultsSummary();
		beansOutResultsSummary = new BeansOutResultsSummary();
	}
	
	public String getAccessLevel() {
		return accessLevel;
	}

	public void setAccessLevel(String accessLevel) {
		this.accessLevel = accessLevel;
	}
	
	public List<RSTransaction> getResultTransactions() {
		return resultTransactions;
	}

	public void setResultTransactions(List<RSTransaction> resultTransactions) {
		this.resultTransactions = resultTransactions;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public BeansInResultsSummary getBeansInResultsSummary() {
		return beansInResultsSummary;
	}

	public void setBeansInResultsSummary(BeansInResultsSummary beansInResultsSummary) {
		this.beansInResultsSummary = beansInResultsSummary;
	}

	public BeansOutResultsSummary getBeansOutResultsSummary() {
		return beansOutResultsSummary;
	}

	public void setBeansOutResultsSummary(BeansOutResultsSummary beansOutResultsSummary) {
		this.beansOutResultsSummary = beansOutResultsSummary;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public List<BeansInForm> getResultBeansInTransactions() {
		return resultBeansInTransactions;
	}

	public void setResultBeansInTransactions(List<BeansInForm> resultBeansInTransactions) {
		this.resultBeansInTransactions = resultBeansInTransactions;
	}

	public List<BeansOutForm> getResultBeansOutTransactions() {
		return resultBeansOutTransactions;
	}

	public void setResultBeansOutTransactions(List<BeansOutForm> resultBeansOutTransactions) {
		this.resultBeansOutTransactions = resultBeansOutTransactions;
	}

	public List<RSBeansSample> getResultBeansSamples() {
		return resultBeansSamples;
	}

	public void setResultBeansSamples(List<RSBeansSample> resultBeansSamples) {
		this.resultBeansSamples = resultBeansSamples;
	}

}
