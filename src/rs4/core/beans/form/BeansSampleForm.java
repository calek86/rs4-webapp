package rs4.core.beans.form;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import rs4.database.dao.model.RSBeansSample;
import rs4.database.dao.model.RSCustomer;
import rs4.database.dao.model.RSProduct;
import rs4.database.dao.model.RSStockYear;
import rs4.utils.DateUtils;

public class BeansSampleForm implements Comparable<BeansSampleForm> {

	private Integer id;
	private String sampleDate;
	private String customer;
	private List<RSCustomer> customers;
	private String referenceNo;
	private String currentStockYear;
	private String stockYearWithId;
	private List<RSStockYear> stockYears;
	private String product;
	private List<RSProduct> products;
	private String tonnage;
	private String processable;
	private String moisture;
	private String admix;
	private String kgHl;
	private String temperature;
	private String screen1;
	private String screen2;
	private String holed;
	private String blind;
	private String stains;
	private String comments;

	public static RSBeansSample formToRSBeansSample(BeansSampleForm form) {
		RSBeansSample rsbs = new RSBeansSample();
		if (form.getId() != null) {
			rsbs.setId(form.getId());
		}
		try {
			rsbs.setSampleDate(DateUtils.parseDateTimeToTimestamp(form.getSampleDate()));
		} catch (Exception e) {
			rsbs.setSampleDate(new Timestamp((new Date()).getTime()));
		}
		String[] splitCustomer = form.getCustomer().split("\\|");
		Integer customerId = Integer.parseInt(splitCustomer[0]);
		String customerCode = splitCustomer[1];
		rsbs.setCustomerId(customerId);
		rsbs.setCustomer(customerCode);
		rsbs.setReferenceNo(form.getReferenceNo());
	
		String[] splitProduct = form.getProduct().split("\\|");
		Integer productId = Integer.parseInt(splitProduct[0]);
		String productCode = splitProduct[1];
		rsbs.setProductId(productId);
		rsbs.setProduct(productCode);

		String[] splitStockYear = form.getStockYearWithId().split("\\|");
		Integer stockYearId = Integer.parseInt(splitStockYear[0]);
		String stockYear = splitStockYear[1];
		rsbs.setStockYearId(stockYearId);
		rsbs.setStockYear(stockYear);

		if (!StringUtils.isBlank(form.getTonnage())) {
			rsbs.setTonnage(Float.parseFloat(form.getTonnage()));
		}
		if (!StringUtils.isBlank(form.getMoisture())) {
			rsbs.setMoisture(Float.parseFloat(form.getMoisture()));
		}
		if (!StringUtils.isBlank(form.getAdmix())) {
			rsbs.setAdmix(Float.parseFloat(form.getAdmix()));
		}
		if (!StringUtils.isBlank(form.getTemperature())) {
			rsbs.setTemperature(Float.parseFloat(form.getTemperature()));
		}
		rsbs.setScreen1(form.getScreen1());
		rsbs.setScreen2(form.getScreen2());
		rsbs.setHoled(form.getHoled());
		rsbs.setBlind(form.getBlind());
		rsbs.setStains(form.getStains());
		rsbs.setComments(form.getComments());
		rsbs.setKgHl(form.getKgHl());
		rsbs.setProcessable(form.getProcessable());
		return rsbs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSampleDate() {
		return sampleDate;
	}

	public void setSampleDate(String sampleDate) {
		this.sampleDate = sampleDate;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public List<RSCustomer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<RSCustomer> customers) {
		this.customers = customers;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getCurrentStockYear() {
		return currentStockYear;
	}

	public void setCurrentStockYear(String currentStockYear) {
		this.currentStockYear = currentStockYear;
	}

	public String getStockYearWithId() {
		return stockYearWithId;
	}

	public void setStockYearWithId(String stockYearWithId) {
		this.stockYearWithId = stockYearWithId;
	}

	public List<RSStockYear> getStockYears() {
		return stockYears;
	}

	public void setStockYears(List<RSStockYear> stockYears) {
		this.stockYears = stockYears;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public List<RSProduct> getProducts() {
		return products;
	}

	public void setProducts(List<RSProduct> products) {
		this.products = products;
	}

	public String getTonnage() {
		return tonnage;
	}

	public void setTonnage(String tonnage) {
		this.tonnage = tonnage;
	}

	public String getProcessable() {
		return processable;
	}

	public void setProcessable(String processable) {
		this.processable = processable;
	}

	public String getMoisture() {
		return moisture;
	}

	public void setMoisture(String moisture) {
		this.moisture = moisture;
	}

	public String getAdmix() {
		return admix;
	}

	public void setAdmix(String admix) {
		this.admix = admix;
	}

	public String getKgHl() {
		return kgHl;
	}

	public void setKgHl(String kgHl) {
		this.kgHl = kgHl;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getScreen1() {
		return screen1;
	}

	public void setScreen1(String screen1) {
		this.screen1 = screen1;
	}

	public String getScreen2() {
		return screen2;
	}

	public void setScreen2(String screen2) {
		this.screen2 = screen2;
	}

	public String getHoled() {
		return holed;
	}

	public void setHoled(String holed) {
		this.holed = holed;
	}

	public String getBlind() {
		return blind;
	}

	public void setBlind(String blind) {
		this.blind = blind;
	}

	public String getStains() {
		return stains;
	}

	public void setStains(String stains) {
		this.stains = stains;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public int compareTo(BeansSampleForm other) {
		try {
			Date thisDate = DateUtils.parseDateTime(this.getSampleDate());
			Date otherDate = DateUtils.parseDateTime(other.getSampleDate());
			return thisDate.compareTo(otherDate);
		} catch (Exception e) {
		}
		return 0;
	}

}
