package rs4.core.beans.form;

import rs4.database.dao.model.RSTransaction;
import rs4.utils.DisplayUtils;

public class BeansOutForm {

	private RSTransaction transaction;
	
	private float culmTotal;
	private float containerTare;
	
	private String culmTotalDisplay;
	private String containerTareDisplay;
	
	public RSTransaction getTransaction() {
		return transaction;
	}
	public void setTransaction(RSTransaction transaction) {
		this.transaction = transaction;
	}
	public float getCulmTotal() {
		return culmTotal;
	}
	public void setCulmTotal(float culmTotal) {
		setCulmTotalDisplay(DisplayUtils.formatWeightZero(culmTotal, true, true));
		this.culmTotal = culmTotal;
	}
	public String getCulmTotalDisplay() {
		return culmTotalDisplay;
	}
	public void setCulmTotalDisplay(String culmTotalDisplay) {
		this.culmTotalDisplay = culmTotalDisplay;
	}
	public float getContainerTare() {
		return containerTare;
	}
	public void setContainerTare(float containerTare) {
		setContainerTareDisplay(DisplayUtils.formatWeightZero(containerTare, true, true));		
		this.containerTare = containerTare;
	}
	public String getContainerTareDisplay() {
		return containerTareDisplay;
	}
	public void setContainerTareDisplay(String containerTareDisplay) {
		this.containerTareDisplay = containerTareDisplay;
	}
	
}
