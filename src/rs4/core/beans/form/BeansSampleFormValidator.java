package rs4.core.beans.form;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import rs4.utils.DateUtils;

public class BeansSampleFormValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return BeansSampleForm.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		BeansSampleForm absf = (BeansSampleForm) target;
		if (StringUtils.isBlank(absf.getSampleDate())) {
			errors.rejectValue("sampleDate", "empty");
		} else {
			try {
				DateUtils.parseDateTime(absf.getSampleDate());
			} catch (Exception e) {
				errors.rejectValue("sampleDate", "notDate");
			}
		}
		if (!StringUtils.isBlank(absf.getMoisture())) {
			try {
				Float f = Float.parseFloat(absf.getMoisture());
				if (f < 0) {
					errors.rejectValue("moisture", "notPositiveNumber");
				}
			} catch (NumberFormatException e) {
				errors.rejectValue("moisture", "notNumber");
			}
		}
		if (!StringUtils.isBlank(absf.getAdmix())) {
			try {
				Float f = Float.parseFloat(absf.getAdmix());
				if (f < 0) {
					errors.rejectValue("admix", "notPositiveNumber");
				}
			} catch (NumberFormatException e) {
				errors.rejectValue("admix", "notNumber");
			}
		}
		if (!StringUtils.isBlank(absf.getTemperature())) {
			try {
				Float f = Float.parseFloat(absf.getTemperature());
				if (f < 0) {
					errors.rejectValue("temperature", "notPositiveNumber");
				}
			} catch (NumberFormatException e) {
				errors.rejectValue("temperature", "notNumber");
			}
		}
		if (!StringUtils.isBlank(absf.getScreen1()) && absf.getScreen1().length() > 15) {
			errors.rejectValue("screen1", "addBeansSample.screen1.tooLong");
		} 
		if (!StringUtils.isBlank(absf.getScreen2()) && absf.getScreen2().length() > 15) {
			errors.rejectValue("screen2", "addBeansSample.screen2.tooLong");
		}
		if (!StringUtils.isBlank(absf.getReferenceNo()) && absf.getReferenceNo().length() > 50) {
			errors.rejectValue("exRef", "addBeansSample.exRef.tooLong");
		}
		if (!StringUtils.isBlank(absf.getHoled()) && absf.getHoled().length() > 50) {
			errors.rejectValue("holen", "addBeansSample.holen.tooLong");
		}
		if (!StringUtils.isBlank(absf.getBlind()) && absf.getBlind().length() > 50) {
			errors.rejectValue("blind", "addBeansSample.blind.tooLong");
		}
		if (!StringUtils.isBlank(absf.getStains()) && absf.getStains().length() > 50) {
			errors.rejectValue("stains", "addBeansSample.stains.tooLong");
		}
		if (!StringUtils.isBlank(absf.getComments()) && absf.getComments().length() > 255) {
			errors.rejectValue("stains", "addBeansSample.comments.tooLong");
		}
	}

}
