package rs4.core.beans.form;

import rs4.database.dao.model.RSTransaction;
import rs4.utils.DisplayUtils;

public class BeansInForm {
	
	private RSTransaction transaction;
	
	private float deduction;
	private float feed;
	private float head;
	
	private String deductionDisplay;
	private String feedDisplay;
	private String headDisplay;
	
	public RSTransaction getTransaction() {
		return transaction;
	}
	public void setTransaction(RSTransaction transaction) {
		this.transaction = transaction;
	}
	public float getDeduction() {
		return deduction;
	}
	public void setDeduction(float deduction) {
		setDeductionDisplay(DisplayUtils.formatPercentageZero(deduction*100, true, true));
		this.deduction = deduction;
	}
	public float getFeed() {
		return feed;
	}
	public void setFeed(float feed) {
		setFeedDisplay(DisplayUtils.formatWeightZero(feed, true, true));
		this.feed = feed;
	}
	public float getHead() {
		return head;
	}
	public void setHead(float head) {
		setHeadDisplay(DisplayUtils.formatWeightZero(head, true, true));
		this.head = head;
	}
	public String getDeductionDisplay() {
		return deductionDisplay;
	}
	public void setDeductionDisplay(String deductionDisplay) {
		this.deductionDisplay = deductionDisplay;
	}
	public String getFeedDisplay() {
		return feedDisplay;
	}
	public void setFeedDisplay(String feedDisplay) {
		this.feedDisplay = feedDisplay;
	}
	public String getHeadDisplay() {
		return headDisplay;
	}
	public void setHeadDisplay(String headDisplay) {
		this.headDisplay = headDisplay;
	}

}

