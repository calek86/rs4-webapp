package rs4.core.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rs4.core.beans.BeansCriteria.BeansReportType;
import rs4.core.beans.form.BeansInForm;
import rs4.core.beans.form.BeansOutForm;
import rs4.core.beans.form.BeansSampleForm;
import rs4.core.search.SearchData;
import rs4.database.dao.RSBeansSampleDao;
import rs4.database.dao.RSCustomerDao;
import rs4.database.dao.RSDestinationDao;
import rs4.database.dao.RSHaulierDao;
import rs4.database.dao.RSProductDao;
import rs4.database.dao.RSStockYearDao;
import rs4.database.dao.RSTransactionDao;
import rs4.database.dao.model.RSBeansSample;
import rs4.database.dao.model.RSCustomer;
import rs4.database.dao.model.RSProduct;
import rs4.database.dao.model.RSStockYear;
import rs4.database.dao.model.RSTransaction;
import rs4.database.dao.model.RSUser;
import rs4.utils.DataUtils;
import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;
import rs4.utils.Logger;

public class BeansData {

	private RSCustomerDao rsCustomerDao;
	private RSProductDao rsProductDao;
	private RSTransactionDao rsTransactionDao;
	private RSBeansSampleDao rsBeansSampleDao;
	private RSHaulierDao rsHaulierDao;
	private RSDestinationDao rsDestinationDao;
	private RSStockYearDao rsStockYearDao;

	private BeansCriteria beansCriteria;
	private BeansResults beansResults;

	private static Map<Integer, BeansData> instances;
	private static Logger logger = Logger.getLogger(BeansData.class.getName());

	public static final BeansData getInstance(RSUser user) {
		if (user == null) {
			logger.error("Requesting data object without user");
			return new BeansData();
		}
		if (instances == null) {
			instances = new HashMap<>();
		}
		if (!instances.containsKey(user.getId())) {
			instances.put(user.getId(), new BeansData());
		}
		return instances.get(user.getId());
	}

	public static void reloadInstance() {
		instances = new HashMap<>();
	}

	private BeansData() {
		rsCustomerDao = RSCustomerDao.getInstance();
		rsProductDao = RSProductDao.getInstance();
		rsTransactionDao = RSTransactionDao.getInstance();
		rsHaulierDao = RSHaulierDao.getInstance();
		rsDestinationDao = RSDestinationDao.getInstance();
		rsBeansSampleDao = RSBeansSampleDao.getInstance();
		rsStockYearDao = RSStockYearDao.getInstance();
		beansCriteria = new BeansCriteria();
		beansResults = new BeansResults();
		reloadCriteria();
	}

	public void insertBeansSample(RSBeansSample beansSample, RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Add beans sample @ by @ with data @", actionId, DisplayUtils.logUser(user), beansSample.toString());
		rsBeansSampleDao.insert(beansSample);
		logger.info("Add beans sample @ successful", actionId);
	}
	
	public void updateBeansSample(RSBeansSample beansSample, RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Update beans sample @ by @ with data @", actionId, DisplayUtils.logUser(user),
				beansSample.toString());
		rsBeansSampleDao.update(beansSample);
		logger.info("Update beans sample @  successful", actionId);
	}
	
	public void deleteBeansSample(RSBeansSample beansSample, RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Delete beans sample @ by @ with data @", actionId, DisplayUtils.logUser(user),
				beansSample.toString());
		rsBeansSampleDao.deleteBeansSampleById(beansSample.getId());
		logger.info("Delete beans sample @ successful", actionId);
	}
	
	private void reloadCriteria() {
		beansCriteria.setCustomers(rsCustomerDao.getRSCustomersFromStockYear(beansCriteria.getStockYearId()));
		beansCriteria.setProducts(rsProductDao.getRSProductsBeansFromStockYear(beansCriteria.getStockYearId()));
		beansCriteria.setHauliers(rsHaulierDao.getRSHauliersFromStockYear(beansCriteria.getStockYearId()));
		beansCriteria.setDestinations(rsDestinationDao.getRSDestinationsFromStockYear(beansCriteria.getStockYearId()));
		beansCriteria.setStockYears(rsStockYearDao.getRSStockYears());
	}

	public void searchBeans(RSUser user) {
		String actionId = DisplayUtils.getActionId();		
		logger.info("Search beans @ by @ with criteria @", actionId, DisplayUtils.logUser(user),
				beansCriteria.toString());
		beansResults.setAccessLevel(DataUtils.requestAccess(user).toString());
		beansResults.setReportType(beansCriteria.getReportType().name());
		beansResults.setReportDate(DateUtils.getDateTimeToday());
		beansResults.setTransactionType(beansCriteria.getTransactionType().toString());
		Integer records = 0;		
		if (BeansReportType.BeansIn.equals(beansCriteria.getReportType())) {
			List<RSTransaction> rsTransactions = rsTransactionDao.getRSTransactionsByCriteria(beansCriteria);
			beansResults.setResultTransactions(rsTransactions);
			beansResults.setResultBeansInTransactions(getBeansInTransactions(rsTransactions));
			beansResults.setBeansInResultsSummary(
					getBeansInResultsSummary(beansResults.getResultBeansInTransactions(), rsTransactions));
			records = rsTransactions.size();
		} else if (BeansReportType.BeansOut.equals(beansCriteria.getReportType())) {
			List<RSTransaction> rsTransactions = rsTransactionDao.getRSTransactionsByCriteria(beansCriteria);
			beansResults.setResultTransactions(rsTransactions);
			beansResults.setResultBeansOutTransactions(getBeansOutTransactions(rsTransactions));
			beansResults
					.setBeansOutResultsSummary(getBeansOutResultsSummary(beansResults.getResultBeansOutTransactions()));
			records = rsTransactions.size();
		} else if (BeansReportType.BeansSample.equals(beansCriteria.getReportType())) {
			List<RSBeansSample> rsBeansSamples = rsBeansSampleDao.getRSSBeansSampleByCriteria(beansCriteria); 
			beansResults.setResultBeansSamples(rsBeansSamples);
			records = rsBeansSamples.size();
		}
		reloadCriteria();
		logger.info("Search beans @ returned @ records", actionId, records);
	}

	private List<BeansInForm> getBeansInTransactions(List<RSTransaction> rsTransactions) {
		List<BeansInForm> beansInTransactions = new ArrayList<BeansInForm>();
		for (RSTransaction transaction : rsTransactions) {
			beansInTransactions.add(transaction.toBeansInForm());
		}
		return beansInTransactions;
	}

	private List<BeansOutForm> getBeansOutTransactions(List<RSTransaction> rsTransactions) {
		List<BeansOutForm> beansOutTransactions = new ArrayList<BeansOutForm>();
		for (RSTransaction transaction : rsTransactions) {
			beansOutTransactions.add(transaction.toBeansOutForm());
		}
		return beansOutTransactions;
	}

	public RSBeansSample getBeansSampleById(int id) {
		return rsBeansSampleDao.getRSBeansSampleById(id);
	}
	
	private BeansInResultsSummary getBeansInResultsSummary(List<BeansInForm> resultBeansInTransactions,
			List<RSTransaction> rsTransactions) {
		BeansInResultsSummary beansInResultsSummary = new BeansInResultsSummary();
		beansInResultsSummary.setSearchResultSummary(SearchData.getSearchResultsSummary(rsTransactions));
		int avgScreenCount = 0;
		Float avgScreen = 0.0F;
		int avgHoledCount = 0;
		Float avgHoled = 0.0F;
		int avgBlindCount = 0;
		Float avgBlind = 0.0F;
		int avgStainsCount = 0;
		Float avgStains = 0.0F;
		int avgDeductionCount = 0;
		Float avgDeduction = 0.0F;
		int avgFeedCount = 0;
		Float avgFeed = 0.0F;
		int avgHeadCount = 0;
		Float avgHead = 0.0F;
		for (BeansInForm beansInForm : resultBeansInTransactions) {
			float screen = DataUtils.getScreening(beansInForm.getTransaction());
			if (screen > 0) {
				avgScreen += screen;
				avgScreenCount++;
			}
			float holed = DataUtils.getHoled(beansInForm.getTransaction());
			if (holed > 0) {
				avgHoled += holed;
				avgHoledCount++;
			}
			float blind = DataUtils.getBlind(beansInForm.getTransaction());
			if (blind > 0) {
				avgBlind += blind;
				avgBlindCount++;
			}
			float stains = DataUtils.getStains(beansInForm.getTransaction());
			if (stains > 0) {
				avgStains += stains;
				avgStainsCount++;
			}
			if (beansInForm.getDeduction() > 0) {
				avgDeduction += beansInForm.getDeduction();
				avgDeductionCount++;
			}
			if (beansInForm.getFeed() > 0) {
				avgFeed += beansInForm.getFeed();
				avgFeedCount++;
			}
			if (beansInForm.getHead() > 0) {
				avgHead += beansInForm.getHead();
				avgHeadCount++;
			}
		}
		if (avgScreen > 0) {
			avgScreen /= avgScreenCount;
		}
		if (avgHoled > 0) {
			avgHoled /= avgHoledCount;
		}
		if (avgBlind > 0) {
			avgBlind /= avgBlindCount;
		}
		if (avgStains > 0) {
			avgStains /= avgStainsCount;
		}
		if (avgDeduction > 0) {
			avgDeduction /= avgDeductionCount;
		}
		if (avgFeed > 0) {
			avgFeed /= avgFeedCount;
		}
		if (avgHead > 0) {
			avgHead /= avgHeadCount;
		}
		beansInResultsSummary.setAvgScreen(DisplayUtils.formatPercentage(avgScreen, true));
		beansInResultsSummary.setAvgHoled(DisplayUtils.formatPercentage(avgHoled, true));
		beansInResultsSummary.setAvgBlind(DisplayUtils.formatPercentage(avgBlind, true));
		beansInResultsSummary.setAvgStains(DisplayUtils.formatPercentage(avgStains, true));
		beansInResultsSummary.setAvgDeduction(DisplayUtils.formatPercentage(avgDeduction * 100, true));
		beansInResultsSummary.setAvgFeed(DisplayUtils.formatWeight(avgFeed, true));
		beansInResultsSummary.setAvgHead(DisplayUtils.formatWeight(avgHead, true));
		return beansInResultsSummary;
	}

	private BeansOutResultsSummary getBeansOutResultsSummary(List<BeansOutForm> resultBeansOutTransactions) {
		BeansOutResultsSummary beansOutResultsSummary = new BeansOutResultsSummary();
		beansOutResultsSummary.setCustomer(beansCriteria.getCustomerCode());
		beansOutResultsSummary.setFromDate(beansCriteria.getDateFrom());
		beansOutResultsSummary.setToDate(beansCriteria.getDateTo());
		Float totalNetWt = 0.0F;
		for (BeansOutForm resultBeansOutTransaction : resultBeansOutTransactions) {
			totalNetWt += resultBeansOutTransaction.getTransaction().getEntWeight();
		}
		beansOutResultsSummary.setTotalNetWeight(DisplayUtils.formatWeight(totalNetWt, true));
		return beansOutResultsSummary;
	}

	public BeansSampleForm getBeansSampleForm() {
		BeansSampleForm form = new BeansSampleForm();
		fillBeansSampleForm(form);
		return form;
	}

	public BeansSampleForm getBeansSampleForm(RSBeansSample beansSample) {
		BeansSampleForm form = beansSample.toBeansSampleForm(false);
		fillBeansSampleForm(form);
		return form;
	}
	
	private void fillBeansSampleForm(BeansSampleForm form) {
		List<RSCustomer> customers = rsCustomerDao.getRSCustomers();
		List<RSProduct> products = rsProductDao.getRSProductsBeansFromStockYear(beansCriteria.getStockYearId());
		List<RSStockYear> stockYears = rsStockYearDao.getRSStockYears();
		form.setCustomers(customers);
		form.setProducts(products);
		form.setStockYears(stockYears);
		form.setCurrentStockYear(beansCriteria.getStockYearDescription());
	}

	public void reloadDates() {
		RSStockYear rsStockYear = DataUtils.getDefaultStockYear();
		beansCriteria.setDefaultDates(rsStockYear);
	}

	public BeansCriteria getBeansCriteria() {
		return beansCriteria;
	}

	public void setBeansCriteria(BeansCriteria beansCriteria) {
		this.beansCriteria = beansCriteria;
	}

	public BeansResults getBeansResults() {
		return beansResults;
	}

	public void setBeansResults(BeansResults beansResults) {
		this.beansResults = beansResults;
	}

}
