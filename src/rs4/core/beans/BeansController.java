package rs4.core.beans;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import rs4.core.beans.form.BeansInForm;
import rs4.core.beans.form.BeansOutForm;
import rs4.core.beans.form.BeansSampleForm;
import rs4.core.beans.form.BeansSampleFormValidator;
import rs4.core.excel.AbstractExcelBuilder;
import rs4.core.excel.BeansExcelBuilder;
import rs4.database.dao.model.RSBeansSample;
import rs4.database.dao.model.RSUser;
import rs4.utils.DataUtils;
import rs4.utils.DateUtils;

@Controller
@SessionAttributes("beansSampleForm")
public class BeansController {

	private BeansSampleFormValidator beansSampleValidator;
	
	public BeansController() {
		beansSampleValidator = new BeansSampleFormValidator();
	}
	
	@RequestMapping(value = "/SearchBeans", method = RequestMethod.POST)
	public ModelAndView searchBeans(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		BeansData beansData = BeansData.getInstance(user);
		beansData.getBeansCriteria().setReportType(request.getParameter("beansCriteria.reportType"));
		beansData.getBeansCriteria().setTransactionsNo(request.getParameter("beansCriteria.transactionsNo"));		
		beansData.getBeansCriteria().setDateFrom(request.getParameter("beansCriteria.dateFrom"));
		beansData.getBeansCriteria().setDateTo(request.getParameter("beansCriteria.dateTo"));
		beansData.getBeansCriteria().setCustomerCode(request.getParameter("beansCriteria.customers"));
		beansData.getBeansCriteria().setProductCode(request.getParameter("beansCriteria.products"));
		beansData.getBeansCriteria().setHaulierCode(request.getParameter("beansCriteria.hauliers"));
		beansData.getBeansCriteria().setDestinationCode(request.getParameter("beansCriteria.destinations"));
		beansData.searchBeans(user);
		return new ModelAndView("beans", "command", beansData);
	}
	
	@RequestMapping(value = "/SearchBeans", params = "Add", method = RequestMethod.POST)
	public ModelAndView gotoAddBeansSample(HttpServletRequest request, ModelMap model) {
		BeansData beansData = BeansData.getInstance(DataUtils.requestUser(request));	
		BeansSampleForm beansSampleForm = beansData.getBeansSampleForm();
		beansSampleForm.setSampleDate(DateUtils.getDateTimeToday());
		model.addAttribute("beansSampleForm", beansSampleForm);
		return new ModelAndView("sample");
	}
	
	@RequestMapping(value = "/SearchBeans", params = "Save", method = RequestMethod.POST)
	public ModelAndView saveBeansSample(@ModelAttribute("beansSampleForm") BeansSampleForm beansSampleForm,
			BindingResult result, ModelMap model, HttpServletRequest request) {
		beansSampleValidator.validate(beansSampleForm, result);
		if (result.hasErrors()) {
			return new ModelAndView("sample");
		}
		RSUser user = DataUtils.requestUser(request);
		BeansData beansData = BeansData.getInstance(user);
		RSBeansSample rsbs = BeansSampleForm.formToRSBeansSample(beansSampleForm);
		beansData.insertBeansSample(rsbs, user);
		model.addAttribute("addingBeansSampleOK", "Succesfully saved new beans sample");
		beansData.searchBeans(user);
		return new ModelAndView("beans", "command", beansData);
	}	
	
	@RequestMapping(value = "/EditBeansSample", method = RequestMethod.POST)
	public ModelAndView gotoEditBeansSample(HttpServletRequest request, ModelMap model) {
		String idToEdit = request.getParameter("idToEdit");
		BeansData beansData = BeansData.getInstance(DataUtils.requestUser(request));
		RSBeansSample rsBeansSample = beansData.getBeansSampleById(Integer.parseInt(idToEdit));
		if (rsBeansSample != null) {
			BeansSampleForm form = beansData.getBeansSampleForm(rsBeansSample);
			model.addAttribute("beansSampleForm", form);
			return new ModelAndView("sample");
		} else {
			return new ModelAndView("beans", "command", beansData);
		}
	}
	
	@RequestMapping(value = "/EditBeansSample", params = "Save", method = RequestMethod.POST)
	public ModelAndView editBeansSample(@ModelAttribute("beansSampleForm") BeansSampleForm beansSampleForm,
			BindingResult result, ModelMap model, HttpServletRequest request) {
		beansSampleValidator.validate(beansSampleForm, result);
		if (result.hasErrors()) {
			return new ModelAndView("sample");
		}
		RSUser user = DataUtils.requestUser(request);
		BeansData beansData = BeansData.getInstance(user);
		RSBeansSample rsbs = BeansSampleForm.formToRSBeansSample(beansSampleForm);
		beansData.updateBeansSample(rsbs, user);
		model.addAttribute("editBeansSampleOK", "Succesfully updated beans sample");
		beansData.searchBeans(user);
		return new ModelAndView("beans", "command", beansData);
	}
	
	@RequestMapping(value = "/EditBeansSample", params = "Delete", method = RequestMethod.POST)
	public ModelAndView deleteBeansSample(@ModelAttribute("beansSampleForm") BeansSampleForm beansSampleForm,
			BindingResult result, ModelMap model, HttpServletRequest request) {
		beansSampleValidator.validate(beansSampleForm, result);
		if (result.hasErrors()) {
			return new ModelAndView("sample");
		}
		RSUser user = DataUtils.requestUser(request);
		BeansData beansData = BeansData.getInstance(user);
		RSBeansSample rsbs = BeansSampleForm.formToRSBeansSample(beansSampleForm);
		beansData.deleteBeansSample(rsbs, user);
		model.addAttribute("deleteBeansSampleOK", "Succesfully deleted beans sample");
		beansData.searchBeans(user);
		return new ModelAndView("beans", "command", beansData);
	}
	
	@RequestMapping(value = "/SearchBeans", params = "Cancel", method = RequestMethod.POST)
	public ModelAndView cancelSavingBeansSample(@ModelAttribute("beansSampleForm") BeansSampleForm beansSampleForm,
			BindingResult result, HttpServletRequest request) {
		beansSampleForm = new BeansSampleForm();
		RSUser user = DataUtils.requestUser(request);
		BeansData beansData = BeansData.getInstance(user);
		beansData.searchBeans(user);
		return new ModelAndView("beans", "command", beansData);
	}

	@RequestMapping(value = "/EditBeansSample", params = "Cancel", method = RequestMethod.POST)
	public ModelAndView cancelUpdatingTransaction(@ModelAttribute("beansSampleForm") BeansSampleForm beansSampleForm,
			BindingResult result, HttpServletRequest request) {
		beansSampleForm = new BeansSampleForm();
		RSUser user = DataUtils.requestUser(request);
		BeansData beansData = BeansData.getInstance(user);
		beansData.searchBeans(user);
		return new ModelAndView("beans", "command", beansData);
	}

	@RequestMapping(value = "/SearchBeans/beansInExcel", method = RequestMethod.GET)
	public ModelAndView downloadHarvestBeansExcel(HttpServletRequest request) {
		BeansData beansData = BeansData.getInstance(DataUtils.requestUser(request));
		List<BeansInForm> transactionList = beansData.getBeansResults().getResultBeansInTransactions();
		ModelAndView model = new ModelAndView("beansExcelView");
		model.addObject(AbstractExcelBuilder.EXCEL_TYPE, BeansExcelBuilder.EXCEL_TYPE_BEANS_IN);
		model.addObject(AbstractExcelBuilder.EXCEL_DATA, transactionList);
		model.addObject(AbstractExcelBuilder.EXCEL_FILENAME, "beansIn" + AbstractExcelBuilder.getFileNameDatePrefix());
		model.addObject(AbstractExcelBuilder.EXCEL_SUMMARY_DATA, beansData.getBeansResults().getBeansInResultsSummary());
		return model;
	}

	@RequestMapping(value = "/SearchBeans/beansOutExcel", method = RequestMethod.GET)
	public ModelAndView downloadStockBeansExcel(HttpServletRequest request) {
		BeansData beansData = BeansData.getInstance(DataUtils.requestUser(request));
		List<BeansOutForm> transactionList = beansData.getBeansResults().getResultBeansOutTransactions();
		ModelAndView model = new ModelAndView("beansExcelView");
		model.addObject(AbstractExcelBuilder.EXCEL_TYPE, BeansExcelBuilder.EXCEL_TYPE_BEANS_OUT);
		model.addObject(AbstractExcelBuilder.EXCEL_DATA, transactionList);
		model.addObject(AbstractExcelBuilder.EXCEL_FILENAME, "beansOut" + AbstractExcelBuilder.getFileNameDatePrefix());
		model.addObject(AbstractExcelBuilder.EXCEL_SUMMARY_DATA, beansData.getBeansResults().getBeansOutResultsSummary());
		return model;
	}

}