package rs4.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import rs4.core.beans.BeansData;
import rs4.core.bills.BillsData;
import rs4.core.login.LoginData;
import rs4.core.search.SearchData;
import rs4.core.sensors.SensorsData;
import rs4.core.settings.SettingsData;
import rs4.core.stock.StockData;
import rs4.core.transfer.TransferData;
import rs4.database.dao.model.RSUser;
import rs4.utils.CookieUtils;
import rs4.utils.DataUtils;

@Controller
@SessionAttributes
public class MenuController {

	@RequestMapping("/login")
	public ModelAndView login(HttpServletRequest request, HttpServletResponse response) {
		LoginData loginData = new LoginData();
		String cookieToken = CookieUtils.extractUserToken(request);
		if (loginData.autoLogin(cookieToken, request, response)) {
			return search(request);
		} else {
			return new ModelAndView("login", "command", loginData);
		}
	}

	@RequestMapping("/search")
	public ModelAndView search(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		SearchData searchData = SearchData.getInstance(user);
		searchData.reloadDates();
		searchData.searchTransactions(user);
		return new ModelAndView("search", "command", searchData);
	}

	@RequestMapping("/beans")
	public ModelAndView beans(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		BeansData beansData = BeansData.getInstance(user);
		beansData.reloadDates();
		beansData.searchBeans(user);
		return new ModelAndView("beans", "command", beansData);
	}

	@RequestMapping("/stock")
	public ModelAndView stock(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		StockData stockData = StockData.getInstance(user);
		stockData.searchStock(user);
		return new ModelAndView("stock", "command", stockData);
	}

	@RequestMapping("/bills")
	public ModelAndView bills(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		BillsData billsData = BillsData.getInstance(user);
		billsData.reloadDates();
		billsData.generateBill(user);
		return new ModelAndView("bills", "command", billsData);
	}

	@RequestMapping("/transfer")
	public ModelAndView transfers(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		TransferData transferData = TransferData.getInstance(user);
		transferData.reloadCriteria();
		transferData.searchTransfers(user);
		return new ModelAndView("transfer", "command", transferData);
	}

	@RequestMapping("/sensors")
	public ModelAndView sensors(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		SensorsData sensorsData = SensorsData.getInstance(user);
		sensorsData.reloadDates();
		sensorsData.searchSensors(user);
		return new ModelAndView("sensors", "command", sensorsData);
	}
	
	@RequestMapping("/settings")
	public ModelAndView settings(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		SettingsData settingsData = SettingsData.getInstance(user);
		settingsData.searchSettings(user);
		return new ModelAndView("settings", "command", settingsData);
	}

}