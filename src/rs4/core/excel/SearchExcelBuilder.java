package rs4.core.excel;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;

import rs4.core.search.SearchResultsSummary;
import rs4.database.dao.model.RSTransaction;
import rs4.utils.DateUtils;

public class SearchExcelBuilder extends AbstractExcelBuilder {

	public static final String EXCEL_TYPE_TRANSACTIONS_SIMPLE = "excel_type_transactions_simple";
	public static final String EXCEL_TYPE_TRANSACTIONS_DETAILED = "excel_type_transactions_detailed";

	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String excelType = (String) model.get(EXCEL_TYPE);
		if (EXCEL_TYPE_TRANSACTIONS_SIMPLE.equals(excelType)) {
			buildSearchSimpleReport(model, workbook);
		}
		if (EXCEL_TYPE_TRANSACTIONS_DETAILED.equals(excelType)) {
			buildSearchDetailedReport(model, workbook);
		}
		writeExcelFile(model, workbook, response);
	}

	@SuppressWarnings("unchecked")
	private void buildSearchSimpleReport(Map<String, Object> model, HSSFWorkbook workbook) {
		List<RSTransaction> transactionList = (List<RSTransaction>) model.get(EXCEL_DATA);
		SearchResultsSummary summary = (SearchResultsSummary) model.get(EXCEL_SUMMARY_DATA);

		// arbitrary define here
		int rowCount = 1, sheetWidth = 14;

		HSSFSheet sheet = workbook.createSheet("transactions");
		HSSFCellStyle headerStyle = createHeaderStyle(workbook);
		HSSFCellStyle headerDataStyle = createHeaderDataStyle(workbook);
		HSSFCellStyle columnStyle = createColumnStyle(workbook);
		HSSFCellStyle lastRowStyle = createLastRowStyle(workbook);
		insertTitleHeader(workbook, sheet, sheetWidth, 6, "RS4 Transactions Report Simple");

		HSSFRow summaryHeader = sheet.createRow(rowCount++);
		summaryHeader.setHeight(HEADER_HEIGHT);
		summaryHeader.createCell(0).setCellValue("Report Date");
		summaryHeader.createCell(1).setCellValue("Total Net.Wt.");
		summaryHeader.createCell(2).setCellValue("Total Ent.Wt.");
		summaryHeader.createCell(3).setCellValue("Avg. Moisture ");
		summaryHeader.createCell(4).setCellValue("Avg. Admix");
		summaryHeader.createCell(5).setCellValue("Avg. Temperature");
		summaryHeader.createCell(6).setCellValue("Avg. Sp.Wt.");
		for (int i = 7; i < sheetWidth+1; i++) {
			summaryHeader.createCell(i);
		}
		sheet.addMergedRegion(new CellRangeAddress(1, 1, 7, sheetWidth));
		for (int i = 0; i < summaryHeader.getLastCellNum(); i++) {
			summaryHeader.getCell(i).setCellStyle(headerStyle);
		}

		HSSFRow summaryRow = sheet.createRow(rowCount++);
		summaryRow.setHeight(HEADER_HEIGHT);
		summaryRow.createCell(0).setCellValue(DateUtils.getDateTimeToday());
		summaryRow.createCell(1).setCellValue(summary.getTotalNetWt());
		summaryRow.createCell(2).setCellValue(summary.getTotalEntWt());
		summaryRow.createCell(3).setCellValue(summary.getAvgMoisture());
		summaryRow.createCell(4).setCellValue(summary.getAvgAdmix());
		summaryRow.createCell(5).setCellValue(summary.getAvgTemperature());
		summaryRow.createCell(6).setCellValue(summary.getAvgSpWt());
		for (int i = 7; i < sheetWidth+1; i++) {
			summaryRow.createCell(i);
		}
		sheet.addMergedRegion(new CellRangeAddress(2, 2, 7, sheetWidth));
		for (int i = 0; i < summaryRow.getLastCellNum(); i++) {
			summaryRow.getCell(i).setCellStyle(headerDataStyle);
		}

		HSSFRow dataHeaderRow = sheet.createRow(rowCount++);
		dataHeaderRow.setHeight(HEADER_HEIGHT);
		dataHeaderRow.createCell(0).setCellValue("No.");
		dataHeaderRow.createCell(1).setCellValue("Type");
		dataHeaderRow.createCell(2).setCellValue("Date");
		dataHeaderRow.createCell(3).setCellValue("Customer");
		dataHeaderRow.createCell(4).setCellValue("Haulier");
		dataHeaderRow.createCell(5).setCellValue("Product");
		dataHeaderRow.createCell(6).setCellValue("Destination");
		dataHeaderRow.createCell(7).setCellValue("Reg.No.");
		dataHeaderRow.createCell(8).setCellValue("Net.Wt.");
		dataHeaderRow.createCell(9).setCellValue("Ent.Wt.");
		dataHeaderRow.createCell(10).setCellValue("% Loss");
		dataHeaderRow.createCell(11).setCellValue("Moisture");
		dataHeaderRow.createCell(12).setCellValue("Admix");
		dataHeaderRow.createCell(13).setCellValue("Temp.");
		dataHeaderRow.createCell(14).setCellValue("Sp.Wt.");
		for (int i = 0; i < dataHeaderRow.getLastCellNum(); i++) {
			dataHeaderRow.getCell(i).setCellStyle(headerStyle);
		}

		for (RSTransaction transaction : transactionList) {
			HSSFRow transactionRow = sheet.createRow(rowCount++);
			transactionRow.createCell(0).setCellValue(transaction.getTicketId());
			transactionRow.createCell(1).setCellValue(transaction.getTransactionType());
			transactionRow.createCell(2).setCellValue(transaction.getTransactionDateDisplay());
			transactionRow.createCell(3).setCellValue(transaction.getCustomer());
			transactionRow.createCell(4).setCellValue(transaction.getHaulier());
			transactionRow.createCell(5).setCellValue(transaction.getProduct());
			transactionRow.createCell(6).setCellValue(transaction.getDestination());
			transactionRow.createCell(7).setCellValue(transaction.getRegistration());
			transactionRow.createCell(8).setCellValue(transaction.getNetWeightDisplay());
			transactionRow.createCell(9).setCellValue(transaction.getEntWeightDisplay());
			transactionRow.createCell(10).setCellValue(transaction.getPercentLossDisplay());
			transactionRow.createCell(11).setCellValue(transaction.getMoistureDisplay());
			transactionRow.createCell(12).setCellValue(transaction.getAdmixDisplay());
			transactionRow.createCell(13).setCellValue(transaction.getTemperatureDisplay());
			transactionRow.createCell(14).setCellValue(transaction.getSpWtDisplay());
			for (int i = 0; i < transactionRow.getLastCellNum(); i++) {
				if (rowCount == transactionList.size() + 4) {
					transactionRow.getCell(i).setCellStyle(lastRowStyle);
				} else {
					transactionRow.getCell(i).setCellStyle(columnStyle);
				}
			}
		}
		for (int i = 0; i < dataHeaderRow.getLastCellNum(); i++) {
			sheet.autoSizeColumn(i);
		}
	}

	@SuppressWarnings("unchecked")
	private void buildSearchDetailedReport(Map<String, Object> model, HSSFWorkbook workbook) {
		List<RSTransaction> transactionList = (List<RSTransaction>) model.get(EXCEL_DATA);
		SearchResultsSummary summary = (SearchResultsSummary) model.get(EXCEL_SUMMARY_DATA);

		// arbitrary define here
		int rowCount = 1, sheetWidth = 35;
		
		HSSFSheet sheet = workbook.createSheet("transactions");
		HSSFCellStyle headerStyle = createHeaderStyle(workbook);
		HSSFCellStyle headerDataStyle = createHeaderDataStyle(workbook);
		HSSFCellStyle columnStyle = createColumnStyle(workbook);
		HSSFCellStyle lastRowStyle = createLastRowStyle(workbook);
		insertTitleHeader(workbook, sheet, sheetWidth, 15, "RS4 Transactions Report Detailed");

		HSSFRow summaryHeader = sheet.createRow(rowCount++);
		summaryHeader.setHeight(HEADER_HEIGHT);
		summaryHeader.createCell(0).setCellValue("Report Date");
		summaryHeader.createCell(1).setCellValue("Total Net.Wt.");
		summaryHeader.createCell(2).setCellValue("Total Ent.Wt.");
		summaryHeader.createCell(3).setCellValue("Avg. Moisture ");
		summaryHeader.createCell(4).setCellValue("Avg. Admix");
		summaryHeader.createCell(5).setCellValue("Avg. Temperature");
		summaryHeader.createCell(6).setCellValue("Avg. Sp.Wt.");
		for (int i = 7; i < sheetWidth+1; i++) {
			summaryHeader.createCell(i);
		}
		sheet.addMergedRegion(new CellRangeAddress(1, 1, 7, sheetWidth));
		for (int i = 0; i < summaryHeader.getLastCellNum(); i++) {
			summaryHeader.getCell(i).setCellStyle(headerStyle);
		}

		HSSFRow summaryRow = sheet.createRow(rowCount++);
		summaryRow.setHeight(HEADER_HEIGHT);
		summaryRow.createCell(0).setCellValue(DateUtils.getDateTimeToday());
		summaryRow.createCell(1).setCellValue(summary.getTotalNetWt());
		summaryRow.createCell(2).setCellValue(summary.getTotalEntWt());
		summaryRow.createCell(3).setCellValue(summary.getAvgMoisture());
		summaryRow.createCell(4).setCellValue(summary.getAvgAdmix());
		summaryRow.createCell(5).setCellValue(summary.getAvgTemperature());
		summaryRow.createCell(6).setCellValue(summary.getAvgSpWt());
		for (int i = 7; i < sheetWidth+1; i++) {
			summaryRow.createCell(i);
		}
		sheet.addMergedRegion(new CellRangeAddress(2, 2, 7, sheetWidth));
		for (int i = 0; i < summaryRow.getLastCellNum(); i++) {
			summaryRow.getCell(i).setCellStyle(headerDataStyle);
		}

		HSSFRow dataHeaderRow = sheet.createRow(rowCount++);
		dataHeaderRow.setHeight(HEADER_HEIGHT);
		dataHeaderRow.createCell(0).setCellValue("No.");
		dataHeaderRow.createCell(1).setCellValue("Type");
		dataHeaderRow.createCell(2).setCellValue("Date");
		dataHeaderRow.createCell(3).setCellValue("Customer");
		dataHeaderRow.createCell(4).setCellValue("Haulier");
		dataHeaderRow.createCell(5).setCellValue("Product");
		dataHeaderRow.createCell(6).setCellValue("Destination");
		dataHeaderRow.createCell(7).setCellValue("StockYear");
		dataHeaderRow.createCell(8).setCellValue("Reg.No.");
		dataHeaderRow.createCell(9).setCellValue("1st Wt.");
		dataHeaderRow.createCell(10).setCellValue("2nd Wt.");
		dataHeaderRow.createCell(11).setCellValue("Net.Wt.");
		dataHeaderRow.createCell(12).setCellValue("Ent.Wt.");
		dataHeaderRow.createCell(13).setCellValue("Ent.Wt.Beans");
		dataHeaderRow.createCell(14).setCellValue("% Loss");
		dataHeaderRow.createCell(15).setCellValue("% Loss Beans");
		dataHeaderRow.createCell(16).setCellValue("Moisture");
		dataHeaderRow.createCell(17).setCellValue("Admix");
		dataHeaderRow.createCell(18).setCellValue("SpWt");
		dataHeaderRow.createCell(19).setCellValue("Temp.");
		dataHeaderRow.createCell(20).setCellValue("Screen1");
		dataHeaderRow.createCell(21).setCellValue("Screen2");
		dataHeaderRow.createCell(22).setCellValue("OrderNo");
		dataHeaderRow.createCell(23).setCellValue("EexRef");
		dataHeaderRow.createCell(24).setCellValue("N2CP");
		dataHeaderRow.createCell(25).setCellValue("Hagberg");
		dataHeaderRow.createCell(26).setCellValue("1st PrevLoad");
		dataHeaderRow.createCell(27).setCellValue("2nd PrevLoad");
		dataHeaderRow.createCell(28).setCellValue("3rd PrevLoad");
		dataHeaderRow.createCell(29).setCellValue("TrailerNo");
		dataHeaderRow.createCell(30).setCellValue("Inspected");
		dataHeaderRow.createCell(31).setCellValue("ACCSNo");
		dataHeaderRow.createCell(32).setCellValue("Holed");
		dataHeaderRow.createCell(33).setCellValue("Blind");
		dataHeaderRow.createCell(34).setCellValue("Stains");
		dataHeaderRow.createCell(35).setCellValue("Comments");
		for (int i = 0; i < dataHeaderRow.getLastCellNum(); i++) {
			dataHeaderRow.getCell(i).setCellStyle(headerStyle);
		}

		for (RSTransaction transaction : transactionList) {
			HSSFRow transactionRow = sheet.createRow(rowCount++);
			transactionRow.createCell(0).setCellValue(transaction.getTicketId());
			transactionRow.createCell(1).setCellValue(transaction.getTransactionType());
			transactionRow.createCell(2).setCellValue(transaction.getTransactionDateDisplay());
			transactionRow.createCell(3).setCellValue(transaction.getCustomer());
			transactionRow.createCell(4).setCellValue(transaction.getHaulier());
			transactionRow.createCell(5).setCellValue(transaction.getProduct());
			transactionRow.createCell(6).setCellValue(transaction.getDestination());
			transactionRow.createCell(7).setCellValue(transaction.getStockYear());
			transactionRow.createCell(8).setCellValue(transaction.getRegistration());
			transactionRow.createCell(9).setCellValue(transaction.getFirstWeightDisplay());
			transactionRow.createCell(10).setCellValue(transaction.getSecondWeightDisplay());
			transactionRow.createCell(11).setCellValue(transaction.getNetWeightDisplay());
			transactionRow.createCell(12).setCellValue(transaction.getEntWeightDisplay());
			transactionRow.createCell(13).setCellValue(transaction.getEntWeightBeansDisplay());
			transactionRow.createCell(14).setCellValue(transaction.getPercentLossDisplay());
			transactionRow.createCell(15).setCellValue(transaction.getPercentLossBeansDisplay());
			transactionRow.createCell(16).setCellValue(transaction.getMoistureDisplay());
			transactionRow.createCell(17).setCellValue(transaction.getAdmixDisplay());
			transactionRow.createCell(18).setCellValue(transaction.getSpWtDisplay());
			transactionRow.createCell(19).setCellValue(transaction.getTemperatureDisplay());
			transactionRow.createCell(20).setCellValue(transaction.getScreen1());
			transactionRow.createCell(21).setCellValue(transaction.getScreen2());
			transactionRow.createCell(22).setCellValue(transaction.getOrderNo());
			transactionRow.createCell(23).setCellValue(transaction.getExRef());
			transactionRow.createCell(24).setCellValue(transaction.getN2CP());
			transactionRow.createCell(25).setCellValue(transaction.getHagberg());
			transactionRow.createCell(26).setCellValue(transaction.getFirstPrevLoad());
			transactionRow.createCell(27).setCellValue(transaction.getSecondPrevLoad());
			transactionRow.createCell(28).setCellValue(transaction.getThirdPrevLoad());
			transactionRow.createCell(29).setCellValue(transaction.getTrailerNo());
			transactionRow.createCell(30).setCellType(HSSFCell.CELL_TYPE_BOOLEAN);
			transactionRow.getCell(30).setCellValue(transaction.getInspected().equals("Y") ? true : false);
			transactionRow.createCell(31).setCellValue(transaction.getaCCSNo());
			transactionRow.createCell(32).setCellValue(transaction.getHoled());
			transactionRow.createCell(33).setCellValue(transaction.getBlind());
			transactionRow.createCell(34).setCellValue(transaction.getStains());
			transactionRow.createCell(35).setCellValue(transaction.getComments());
			for (int i = 0; i < transactionRow.getLastCellNum(); i++) {
				if (rowCount == transactionList.size() + 4) {
					transactionRow.getCell(i).setCellStyle(lastRowStyle);
				} else {
					transactionRow.getCell(i).setCellStyle(columnStyle);
				}
			}
		}
		for (int i = 0; i < dataHeaderRow.getLastCellNum(); i++) {
			sheet.autoSizeColumn(i);
		}
	}
}
