package rs4.core.excel;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;

import rs4.core.beans.BeansInResultsSummary;
import rs4.core.beans.BeansOutResultsSummary;
import rs4.core.beans.form.BeansInForm;
import rs4.core.beans.form.BeansOutForm;
import rs4.utils.DateUtils;

public class BeansExcelBuilder extends AbstractExcelBuilder {

	public static final String EXCEL_TYPE_BEANS_IN = "excel_type_beans_in";
	public static final String EXCEL_TYPE_BEANS_OUT = "excel_type_beans_out";
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String excelType = (String) model.get(EXCEL_TYPE);
		if (EXCEL_TYPE_BEANS_IN.equals(excelType)) {
			buildBeansInReport(model, workbook);
		}
		if (EXCEL_TYPE_BEANS_OUT.equals(excelType)) {
			buildBeansOutReport(model, workbook);
		}
		writeExcelFile(model, workbook, response);
	}

	@SuppressWarnings("unchecked")
	private void buildBeansInReport(Map<String, Object> model, HSSFWorkbook workbook) {
		List<BeansInForm> transactionList = (List<BeansInForm>) model.get(EXCEL_DATA);
		BeansInResultsSummary summary = (BeansInResultsSummary) model.get(EXCEL_SUMMARY_DATA);

		// arbitrary define here
		int rowCount = 1, sheetWidth = 17;
		
		HSSFSheet sheet = workbook.createSheet("beans in");
		HSSFCellStyle headerStyle = createHeaderStyle(workbook);
		HSSFCellStyle headerDataStyle = createHeaderDataStyle(workbook);
		HSSFCellStyle columnStyle = createColumnStyle(workbook);
		HSSFCellStyle lastRowStyle = createLastRowStyle(workbook);
		insertTitleHeader(workbook, sheet, sheetWidth, 7, "RS4 Beans In Report");
		
		HSSFRow summaryHeaderRow = sheet.createRow(rowCount++);
		summaryHeaderRow.setHeight(HEADER_HEIGHT);
		summaryHeaderRow.createCell(0).setCellValue("Report Date");
		summaryHeaderRow.createCell(1).setCellValue("Total Net.Wt.");
		summaryHeaderRow.createCell(2).setCellValue("Total Ent.Wt.");
		summaryHeaderRow.createCell(3).setCellValue("Avg. % Loss");
		summaryHeaderRow.createCell(4).setCellValue("Avg. Moisture");
		summaryHeaderRow.createCell(5).setCellValue("Avg. Admix");
		summaryHeaderRow.createCell(6).setCellValue("Avg. Screen");
		summaryHeaderRow.createCell(7).setCellValue("Avg. Holed");
		summaryHeaderRow.createCell(8).setCellValue("Avg. Blind");
		summaryHeaderRow.createCell(9).setCellValue("Avg. Stains");
		summaryHeaderRow.createCell(10).setCellValue("Avg. Deduction");
		summaryHeaderRow.createCell(11).setCellValue("Avg. Feed");
		summaryHeaderRow.createCell(12).setCellValue("Avg. Head");
		for (int i = 13; i < sheetWidth+1; i++) {
			summaryHeaderRow.createCell(i);
		}
		sheet.addMergedRegion(new CellRangeAddress(1, 1, 13, sheetWidth));
		for (int i = 0; i < summaryHeaderRow.getLastCellNum(); i++) {
			summaryHeaderRow.getCell(i).setCellStyle(headerStyle);
		}

		HSSFRow summaryDataRow = sheet.createRow(rowCount++);
		summaryDataRow.setHeight(HEADER_HEIGHT);
		summaryDataRow.createCell(0).setCellValue(DateUtils.getDateTimeToday());
		summaryDataRow.createCell(1).setCellValue(summary.getSearchResultSummary().getTotalNetWt());
		summaryDataRow.createCell(2).setCellValue(summary.getSearchResultSummary().getTotalEntWt());
		summaryDataRow.createCell(3).setCellValue(summary.getSearchResultSummary().getAvgPercentLoss());
		summaryDataRow.createCell(4).setCellValue(summary.getSearchResultSummary().getAvgMoisture());
		summaryDataRow.createCell(5).setCellValue(summary.getSearchResultSummary().getAvgAdmix());
		summaryDataRow.createCell(6).setCellValue(summary.getAvgScreen());
		summaryDataRow.createCell(7).setCellValue(summary.getAvgHoled());
		summaryDataRow.createCell(8).setCellValue(summary.getAvgBlind());
		summaryDataRow.createCell(9).setCellValue(summary.getAvgStains());
		summaryDataRow.createCell(10).setCellValue(summary.getAvgDeduction());
		summaryDataRow.createCell(11).setCellValue(summary.getAvgFeed());
		summaryDataRow.createCell(12).setCellValue(summary.getAvgHead());
		for (int i = 13; i < sheetWidth+1; i++) {
			summaryDataRow.createCell(i);
		}
		sheet.addMergedRegion(new CellRangeAddress(2, 2, 13, sheetWidth));
		for (int i = 0; i < summaryDataRow.getLastCellNum(); i++) {
			summaryDataRow.getCell(i).setCellStyle(headerDataStyle);
		}

		HSSFRow headerRow = sheet.createRow(rowCount++);
		headerRow.setHeight(HEADER_HEIGHT);
		headerRow.createCell(0).setCellValue("No.");
		headerRow.createCell(1).setCellValue("Date");
		headerRow.createCell(2).setCellValue("Customer");
		headerRow.createCell(3).setCellValue("Product");
		headerRow.createCell(4).setCellValue("Destination");
		headerRow.createCell(5).setCellValue("Haulier");
		headerRow.createCell(6).setCellValue("Net.Wt.");
		headerRow.createCell(7).setCellValue("Ent.Wt.");
		headerRow.createCell(8).setCellValue("% Loss");
		headerRow.createCell(9).setCellValue("Moisture");
		headerRow.createCell(10).setCellValue("Admix");
		headerRow.createCell(11).setCellValue("Screen");
		headerRow.createCell(12).setCellValue("Holed");
		headerRow.createCell(13).setCellValue("Blind");
		headerRow.createCell(14).setCellValue("Stains");
		headerRow.createCell(15).setCellValue("Deduction");
		headerRow.createCell(16).setCellValue("Feed");
		headerRow.createCell(17).setCellValue("Head");
		for (int i = 0; i < headerRow.getLastCellNum(); i++) {
			headerRow.getCell(i).setCellStyle(headerStyle);
		}

		for (BeansInForm beansInTransaction: transactionList) {
			HSSFRow transactionRow = sheet.createRow(rowCount++);
			transactionRow.createCell(0).setCellValue(beansInTransaction.getTransaction().getTicketId());
			transactionRow.createCell(1).setCellValue(beansInTransaction.getTransaction().getTransactionDateDisplay());
			transactionRow.createCell(2).setCellValue(beansInTransaction.getTransaction().getCustomer());
			transactionRow.createCell(3).setCellValue(beansInTransaction.getTransaction().getProduct());
			transactionRow.createCell(4).setCellValue(beansInTransaction.getTransaction().getDestination());
			transactionRow.createCell(5).setCellValue(beansInTransaction.getTransaction().getHaulier());
			transactionRow.createCell(6).setCellValue(beansInTransaction.getTransaction().getNetWeightDisplay());
			transactionRow.createCell(7).setCellValue(beansInTransaction.getTransaction().getEntWeightDisplay());
			transactionRow.createCell(8).setCellValue(beansInTransaction.getTransaction().getPercentLossDisplay());
			transactionRow.createCell(9).setCellValue(beansInTransaction.getTransaction().getMoistureDisplay());
			transactionRow.createCell(10).setCellValue(beansInTransaction.getTransaction().getAdmixDisplay());
			transactionRow.createCell(11).setCellValue(beansInTransaction.getTransaction().getScreen2());
			transactionRow.createCell(12).setCellValue(beansInTransaction.getTransaction().getHoled());
			transactionRow.createCell(13).setCellValue(beansInTransaction.getTransaction().getBlind());
			transactionRow.createCell(14).setCellValue(beansInTransaction.getTransaction().getStains());
			transactionRow.createCell(15).setCellValue(beansInTransaction.getDeductionDisplay());
			transactionRow.createCell(16).setCellValue(beansInTransaction.getFeedDisplay());
			transactionRow.createCell(17).setCellValue(beansInTransaction.getHeadDisplay());
			for (int i = 0; i < transactionRow.getLastCellNum(); i++) {
				if (rowCount == transactionList.size() + 4) {
					transactionRow.getCell(i).setCellStyle(lastRowStyle);
				} else {
					transactionRow.getCell(i).setCellStyle(columnStyle);
				}
			}
		}
		for (int i = 0; i < headerRow.getLastCellNum(); i++) {
			sheet.autoSizeColumn(i);
			headerRow.getCell(i).setCellStyle(headerStyle);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void buildBeansOutReport(Map<String, Object> model, HSSFWorkbook workbook) {
		List<BeansOutForm> transactionList = (List<BeansOutForm>) model.get(EXCEL_DATA);
		BeansOutResultsSummary summary = (BeansOutResultsSummary) model.get(EXCEL_SUMMARY_DATA);

		// arbitrary define here
		int rowCount = 1, sheetWidth = 17;
		
		HSSFSheet sheet = workbook.createSheet("beans out");
		HSSFCellStyle headerStyle = createHeaderStyle(workbook);
		HSSFCellStyle headerDataStyle = createHeaderDataStyle(workbook);
		HSSFCellStyle columnStyle = createColumnStyle(workbook);
		HSSFCellStyle lastRowStyle = createLastRowStyle(workbook);
		insertTitleHeader(workbook, sheet, sheetWidth, 8, "RS4 Beans Out Report");
		
		HSSFRow summaryHeaderRow = sheet.createRow(rowCount++);
		summaryHeaderRow.setHeight(HEADER_HEIGHT);
		summaryHeaderRow.createCell(0).setCellValue("Report Date");
		summaryHeaderRow.createCell(1).setCellValue("From Date");
		summaryHeaderRow.createCell(2).setCellValue("To Date");
		summaryHeaderRow.createCell(3).setCellValue("Customer Name");
		summaryHeaderRow.createCell(4).setCellValue("Total Net Weight");
		for (int i = 5; i < sheetWidth+1; i++) {
			summaryHeaderRow.createCell(i);
		}
		sheet.addMergedRegion(new CellRangeAddress(1, 1, 5, sheetWidth));
		for (int i = 0; i < summaryHeaderRow.getLastCellNum(); i++) {
			summaryHeaderRow.getCell(i).setCellStyle(headerStyle);
		}

		HSSFRow summaryDataRow = sheet.createRow(rowCount++);
		summaryDataRow.setHeight(HEADER_HEIGHT);
		summaryDataRow.createCell(0).setCellValue(DateUtils.getDateTimeToday());
		summaryDataRow.createCell(1).setCellValue(summary.getFromDate());
		summaryDataRow.createCell(2).setCellValue(summary.getToDate());
		summaryDataRow.createCell(3).setCellValue(summary.getCustomer());
		summaryDataRow.createCell(4).setCellValue(summary.getTotalNetWeight());
		for (int i = 5; i < sheetWidth+1; i++) {
			summaryDataRow.createCell(i);
		}
		sheet.addMergedRegion(new CellRangeAddress(2, 2, 5, sheetWidth));
		for (int i = 0; i < summaryDataRow.getLastCellNum(); i++) {
			summaryDataRow.getCell(i).setCellStyle(headerDataStyle);
		}

		HSSFRow headerRow = sheet.createRow(rowCount++);
		headerRow.setHeight(HEADER_HEIGHT);
		headerRow.createCell(0).setCellValue("No.");
		headerRow.createCell(1).setCellValue("Date");
		headerRow.createCell(2).setCellValue("Customer");
		headerRow.createCell(3).setCellValue("Product");
		headerRow.createCell(4).setCellValue("Destination");
		headerRow.createCell(5).setCellValue("Haulier");
		headerRow.createCell(6).setCellValue("Tare");
		headerRow.createCell(7).setCellValue("Gross");
		headerRow.createCell(8).setCellValue("Net.Wt.");
		headerRow.createCell(9).setCellValue("Container Tare");
		headerRow.createCell(10).setCellValue("Haulier");
		headerRow.createCell(11).setCellValue("Registration");
		headerRow.createCell(12).setCellValue("Seal");
		headerRow.createCell(13).setCellValue("Container");
		headerRow.createCell(14).setCellValue("Release");
		headerRow.createCell(15).setCellValue("Contract");
		headerRow.createCell(16).setCellValue("LPL");
		headerRow.createCell(17).setCellValue("Inspected");
		for (int i = 0; i < headerRow.getLastCellNum(); i++) {
			headerRow.getCell(i).setCellStyle(headerStyle);
		}

		for (BeansOutForm beansOutTransaction : transactionList) {
			HSSFRow transactionRow = sheet.createRow(rowCount++);
			transactionRow.createCell(0).setCellValue(beansOutTransaction.getTransaction().getTicketId());
			transactionRow.createCell(1).setCellValue(beansOutTransaction.getTransaction().getTransactionDateDisplay());
			transactionRow.createCell(2).setCellValue(beansOutTransaction.getTransaction().getCustomer());
			transactionRow.createCell(3).setCellValue(beansOutTransaction.getTransaction().getProduct());
			transactionRow.createCell(4).setCellValue(beansOutTransaction.getTransaction().getDestination());
			transactionRow.createCell(5).setCellValue(beansOutTransaction.getTransaction().getHaulier());
			transactionRow.createCell(6).setCellValue(beansOutTransaction.getTransaction().getFirstWeightDisplay());
			transactionRow.createCell(7).setCellValue(beansOutTransaction.getTransaction().getSecondWeightDisplay());
			transactionRow.createCell(8).setCellValue(beansOutTransaction.getTransaction().getNetWeightDisplay());
			transactionRow.createCell(9).setCellValue(beansOutTransaction.getContainerTareDisplay());
			transactionRow.createCell(10).setCellValue(beansOutTransaction.getTransaction().getHaulier());
			transactionRow.createCell(11).setCellValue(beansOutTransaction.getTransaction().getRegistration());
			transactionRow.createCell(12).setCellValue(beansOutTransaction.getTransaction().getTrailerNo());
			transactionRow.createCell(13).setCellValue(beansOutTransaction.getTransaction().getaCCSNo());
			transactionRow.createCell(14).setCellValue(beansOutTransaction.getTransaction().getOrderNo());
			transactionRow.createCell(15).setCellValue(beansOutTransaction.getTransaction().getExRef());
			transactionRow.createCell(16).setCellValue(beansOutTransaction.getTransaction().getComments());
			transactionRow.createCell(17).setCellValue(beansOutTransaction.getTransaction().getInspected());
			for (int i = 0; i < transactionRow.getLastCellNum(); i++) {
				if (rowCount == transactionList.size() + 4) {
					transactionRow.getCell(i).setCellStyle(lastRowStyle);
				} else {
					transactionRow.getCell(i).setCellStyle(columnStyle);
				}
			}
		}
		for (int i = 0; i < headerRow.getLastCellNum(); i++) {
			sheet.autoSizeColumn(i);
			headerRow.getCell(i).setCellStyle(headerStyle);
		}
	}

}
