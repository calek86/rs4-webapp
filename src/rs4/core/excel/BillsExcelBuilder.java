package rs4.core.excel;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.util.CollectionUtils;

import rs4.core.bills.BillsSummary;
import rs4.core.bills.form.StandardBillForm;
import rs4.core.bills.form.StorageBillForm;
import rs4.core.bills.form.StorageProductBillForm;
import rs4.utils.DateUtils;

public class BillsExcelBuilder extends AbstractExcelBuilder {

	public static final String EXCEL_TYPE_BILLS_STANDARD = "excel_type_bills_standard";
	public static final String EXCEL_TYPE_BILLS_STORAGE = "excel_type_bills_storage";
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String excelType = (String) model.get(EXCEL_TYPE);
		if (EXCEL_TYPE_BILLS_STANDARD.equals(excelType)) {
			buildBillsStandardReport(model, workbook);
		}
		if (EXCEL_TYPE_BILLS_STORAGE.equals(excelType)) {
			buildBillsStorageReport(model, workbook);
		}
		writeExcelFile(model, workbook, response);
	}

	@SuppressWarnings("unchecked")
	private void buildBillsStandardReport(Map<String, Object> model, HSSFWorkbook workbook) {
		List<StandardBillForm> billsList = (List<StandardBillForm>) model.get(EXCEL_DATA);
		BillsSummary summary = (BillsSummary) model.get(EXCEL_SUMMARY_DATA);

		// arbitrary define here
		int rowCount = 1, sheetWidth = 10;
		
		HSSFSheet sheet = workbook.createSheet("standard bill");
		HSSFCellStyle headerStyle = createHeaderStyle(workbook);
		HSSFCellStyle headerDataStyle = createHeaderDataStyle(workbook);
		HSSFCellStyle columnStyle = createColumnStyle(workbook);
		HSSFCellStyle lastRowStyle = createLastRowStyle(workbook);
		insertTitleHeader(workbook, sheet, sheetWidth, 5, "RS4 Standard Bill");

		HSSFRow summaryHeaderRow = sheet.createRow(rowCount++);
		summaryHeaderRow.setHeight(HEADER_HEIGHT);
		summaryHeaderRow.createCell(0).setCellValue("Bill Date");
		summaryHeaderRow.createCell(1).setCellValue("Customer");
		summaryHeaderRow.createCell(2).setCellValue("Product");
		summaryHeaderRow.createCell(3).setCellValue("Avg. Moisture");
		summaryHeaderRow.createCell(4).setCellValue("Avg. Admix");
		summaryHeaderRow.createCell(5).setCellValue("Avg. Temperature");
		summaryHeaderRow.createCell(6).setCellValue("Total Net.Wt.");
		summaryHeaderRow.createCell(7).setCellValue("Total Cost");
		for (int i = 8; i < sheetWidth+1; i++) {
			summaryHeaderRow.createCell(i);
		}
		sheet.addMergedRegion(new CellRangeAddress(1, 1, 8, sheetWidth));
		for (int i = 0; i < summaryHeaderRow.getLastCellNum(); i++) {
			summaryHeaderRow.getCell(i).setCellStyle(headerStyle);
		}

		HSSFRow summaryDataRow = sheet.createRow(rowCount++);
		summaryDataRow.setHeight(HEADER_HEIGHT);
		summaryDataRow.createCell(0).setCellValue(DateUtils.getDateToday());
		summaryDataRow.createCell(1).setCellValue(summary.getCustomer());
		summaryDataRow.createCell(2).setCellValue(summary.getProduct());
		summaryDataRow.createCell(3).setCellValue(summary.getAvgMoistureDisplay());
		summaryDataRow.createCell(4).setCellValue(summary.getAvgAdmixDisplay());
		summaryDataRow.createCell(5).setCellValue(summary.getAvgTemperatureDisplay());
		summaryDataRow.createCell(6).setCellValue(summary.getTotalNetWtDisplay());
		summaryDataRow.createCell(7).setCellValue(summary.getTotalCostDisplay());
		for (int i = 8; i < sheetWidth+1; i++) {
			summaryDataRow.createCell(i);
		}
		sheet.addMergedRegion(new CellRangeAddress(2, 2, 8, sheetWidth));
		for (int i = 0; i < summaryDataRow.getLastCellNum(); i++) {
			summaryDataRow.getCell(i).setCellStyle(headerDataStyle);
		}

		HSSFRow headerRow = sheet.createRow(rowCount++);
		headerRow.setHeight(HEADER_HEIGHT);
		headerRow.createCell(0).setCellValue("No.");
		headerRow.createCell(1).setCellValue("Date");
		headerRow.createCell(2).setCellValue("Customer");
		headerRow.createCell(3).setCellValue("Product");
		headerRow.createCell(4).setCellValue("Net.Wt.");
		headerRow.createCell(5).setCellValue("Moisture");
		headerRow.createCell(6).setCellValue("Admix");
		headerRow.createCell(7).setCellValue("Temperature");
		headerRow.createCell(8).setCellValue("Intake Cost");
		headerRow.createCell(9).setCellValue("Drying Cost");
		headerRow.createCell(10).setCellValue("Total Cost");
		for (int i = 0; i < headerRow.getLastCellNum(); i++) {
			headerRow.getCell(i).setCellStyle(headerStyle);
		}

		for (StandardBillForm bill : billsList) {
			HSSFRow billRow = sheet.createRow(rowCount++);
			billRow.createCell(0).setCellValue(bill.getTransaction().getTicketId());
			billRow.createCell(1).setCellValue(bill.getTransaction().getTransactionDateDisplay());
			billRow.createCell(2).setCellValue(bill.getTransaction().getCustomer());
			billRow.createCell(3).setCellValue(bill.getTransaction().getProduct());
			billRow.createCell(4).setCellValue(bill.getTransaction().getNetWeightDisplay());
			billRow.createCell(5).setCellValue(bill.getTransaction().getMoistureDisplay());
			billRow.createCell(6).setCellValue(bill.getTransaction().getAdmixDisplay());
			billRow.createCell(7).setCellValue(bill.getTransaction().getTemperatureDisplay());
			billRow.createCell(8).setCellValue(bill.getIntakeCostDisplay());
			billRow.createCell(9).setCellValue(bill.getDryingCostDisplay());
			billRow.createCell(10).setCellValue(bill.getTotalCostDisplay());
			for (int i = 0; i < billRow.getLastCellNum(); i++) {
				if (rowCount == billsList.size() + 4) {
					billRow.getCell(i).setCellStyle(lastRowStyle);
				} else {
					billRow.getCell(i).setCellStyle(columnStyle);
				}
			}
		}
		for (int i = 0; i < headerRow.getLastCellNum(); i++) {
			sheet.autoSizeColumn(i);
			headerRow.getCell(i).setCellStyle(headerStyle);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void buildBillsStorageReport(Map<String, Object> model, HSSFWorkbook workbook) {
		List<StorageProductBillForm> productBillsList = (List<StorageProductBillForm>) model.get(EXCEL_DATA);
		BillsSummary summary = (BillsSummary) model.get(EXCEL_SUMMARY_DATA);

		// arbitrary define here
		int rowCount = 1, sheetWidth = 5;
		
		HSSFSheet sheet = workbook.createSheet("storage bill");
		HSSFCellStyle headerStyle = createHeaderStyle(workbook);
		HSSFCellStyle headerDataStyle = createHeaderDataStyle(workbook);
		HSSFCellStyle columnStyle = createColumnStyle(workbook);
		HSSFCellStyle lastRowStyle = createLastRowStyle(workbook);
		insertTitleHeader(workbook, sheet, sheetWidth, 3, "RS4 Storage Bill");

		HSSFRow summaryHeaderRow = sheet.createRow(rowCount++);
		summaryHeaderRow.setHeight(HEADER_HEIGHT);
		summaryHeaderRow.createCell(0).setCellValue("Bill Date");
		summaryHeaderRow.createCell(1).setCellValue("Customer");
		summaryHeaderRow.createCell(2).setCellValue("Date From");
		summaryHeaderRow.createCell(3).setCellValue("Date To");
		for (int i = 4; i < sheetWidth+1; i++) {
			summaryHeaderRow.createCell(i);
		}
		sheet.addMergedRegion(new CellRangeAddress(1, 1, 4, sheetWidth));
		for (int i = 0; i < summaryHeaderRow.getLastCellNum(); i++) {
			summaryHeaderRow.getCell(i).setCellStyle(headerStyle);
		}

		HSSFRow summaryDataRow = sheet.createRow(rowCount++);
		summaryDataRow.setHeight(HEADER_HEIGHT);
		summaryDataRow.createCell(0).setCellValue(DateUtils.getDateToday());
		summaryDataRow.createCell(1).setCellValue(summary.getCustomer());
		summaryDataRow.createCell(2).setCellValue(summary.getFromDateDisplay());
		summaryDataRow.createCell(3).setCellValue(summary.getToDateDisplay());
		for (int i = 4; i < sheetWidth+1; i++) {
			summaryDataRow.createCell(i);
		}
		sheet.addMergedRegion(new CellRangeAddress(2, 2, 4, sheetWidth));
		for (int i = 0; i < summaryDataRow.getLastCellNum(); i++) {
			summaryDataRow.getCell(i).setCellStyle(headerDataStyle);
		}
		
		HSSFRow summaryHeaderRow2 = sheet.createRow(rowCount++);
		summaryHeaderRow2.setHeight(HEADER_HEIGHT);
		summaryHeaderRow2.createCell(0).setCellValue("Total Days");
		summaryHeaderRow2.createCell(1).setCellValue("Total Day Tonnage");
		summaryHeaderRow2.createCell(2).setCellValue("Tonne/Day Rate");
		summaryHeaderRow2.createCell(3).setCellValue("Total Cost");
		for (int i = 4; i < sheetWidth+1; i++) {
			summaryHeaderRow2.createCell(i);
		}
		sheet.addMergedRegion(new CellRangeAddress(3, 3, 4, sheetWidth));
		for (int i = 0; i < summaryHeaderRow2.getLastCellNum(); i++) {
			summaryHeaderRow2.getCell(i).setCellStyle(headerStyle);
		}
		
		HSSFRow summaryDataRow2 = sheet.createRow(rowCount++);
		summaryDataRow2.setHeight(HEADER_HEIGHT);
		summaryDataRow2.createCell(0).setCellValue(summary.getTotalDays());
		summaryDataRow2.createCell(1).setCellValue(summary.getTotalDayTonnageDisplay());
		summaryDataRow2.createCell(2).setCellValue(summary.getStoringRateDisplay());
		summaryDataRow2.createCell(3).setCellValue(summary.getTotalCostDisplay());
		for (int i = 4; i < sheetWidth+1; i++) {
			summaryDataRow2.createCell(i);
		}
		sheet.addMergedRegion(new CellRangeAddress(4, 4, 4, sheetWidth));
		for (int i = 0; i < summaryDataRow2.getLastCellNum(); i++) {
			summaryDataRow2.getCell(i).setCellStyle(headerDataStyle);
		}
	
		for (StorageProductBillForm productBill : productBillsList) {
			if(CollectionUtils.isEmpty(productBill.getResultStorageBills())) {
				continue;
			}
			HSSFRow productHeaderRow = sheet.createRow(rowCount++);
			productHeaderRow.setHeight(HEADER_HEIGHT);
			productHeaderRow.createCell(0).setCellValue("No.");
			productHeaderRow.createCell(1).setCellValue("Date");
			productHeaderRow.createCell(2).setCellValue("Ent.Wt. Change");
			productHeaderRow.createCell(3).setCellValue("Product: "+productBill.getProduct());
			productHeaderRow.createCell(4).setCellValue("Total DT: "+productBill.getTotalDayTonnageDisplay());
			productHeaderRow.createCell(5).setCellValue("Total Cost: "+productBill.getTotalCostDisplay());			
			int innerRowCount = 0;
			for (StorageBillForm storageBill : productBill.getResultStorageBills()) {
				HSSFRow billRow = sheet.createRow(rowCount++);
				billRow.createCell(0).setCellValue(storageBill.getDayCountDisplay());
				billRow.createCell(1).setCellValue(storageBill.getDateDisplay());
				billRow.createCell(2).setCellValue(storageBill.getEntWtChangeDisplay());
				billRow.createCell(3).setCellValue(storageBill.getEntWtStockDisplay());
				billRow.createCell(4).setCellValue(storageBill.getDayTonnageDisplay());
				billRow.createCell(5).setCellValue(storageBill.getDayCostDisplay());
				for (int i = 0; i < billRow.getLastCellNum(); i++) {
					if (innerRowCount == productBill.getResultStorageBills().size() - 1) {
						billRow.getCell(i).setCellStyle(lastRowStyle);
					} else {
						billRow.getCell(i).setCellStyle(columnStyle);
					}
				}
				innerRowCount++;
			}
			for (int i = 0; i < productHeaderRow.getLastCellNum(); i++) {
				sheet.autoSizeColumn(i);
				productHeaderRow.getCell(i).setCellStyle(headerStyle);
			}
		}
	}

}
