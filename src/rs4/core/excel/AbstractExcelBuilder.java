package rs4.core.excel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import rs4.database.dao.RSSettingsDao;

@Service
public abstract class AbstractExcelBuilder extends AbstractExcelView {

	public static final String EXCEL_SUMMARY_DATA = "excel_summary_data";
	public static final String EXCEL_FILENAME = "excelFileName";
	public static final String EXCEL_TYPE = "excel_type";
	public static final String EXCEL_DATA = "excel_data";

	protected static final String RIGHT_TEXT_TITLE = "T.Denne & Sons Ltd.";

	protected static final short FONT_HEIGHT = (short) 10;
	protected static final short FONT_HEIGHT_HEADER = (short) 11;
	protected static final short HEADER_HEIGHT = (short) 350;

	public static String getFileNameDatePrefix() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdfExcel = new SimpleDateFormat("YYYYMMdd_HHmmss");
		return sdfExcel.format(cal.getTime());
	}

	@Override
	protected abstract void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook,
			HttpServletRequest request, HttpServletResponse response) throws Exception;

	protected void insertTitleHeader(HSSFWorkbook workbook, HSSFSheet sheet, int maxColumns, int splitColumn,
			String textLeft) {
		insertTitleHeader(workbook, sheet, maxColumns, splitColumn, textLeft, RIGHT_TEXT_TITLE);
	}

	protected void insertTitleHeader(HSSFWorkbook workbook, HSSFSheet sheet, int maxColumns, int splitColumn,
			String textLeft, String textRight) {
		HSSFRow titleHeaderRow = sheet.createRow(0);
		titleHeaderRow.setHeight(HEADER_HEIGHT);
		for (int i = 0; i < maxColumns + 1; i++) {
			titleHeaderRow.createCell(i);
		}
		titleHeaderRow.getCell(0).setCellValue(textLeft);
		titleHeaderRow.getCell(splitColumn + 1).setCellValue(textRight);
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, splitColumn));
		sheet.addMergedRegion(new CellRangeAddress(0, 0, splitColumn + 1, maxColumns));
		HSSFCellStyle headerLeftStyle = createHeaderTitleStyle(workbook, CellStyle.ALIGN_LEFT);
		HSSFCellStyle headerRightStyle = createHeaderTitleStyle(workbook, CellStyle.ALIGN_RIGHT);
		for (int i = 0; i < splitColumn + 1; i++) {
			titleHeaderRow.getCell(i).setCellStyle(headerLeftStyle);
		}
		for (int i = splitColumn + 1; i < maxColumns + 1; i++) {
			titleHeaderRow.getCell(i).setCellStyle(headerRightStyle);
		}
	}

	protected HSSFCellStyle createHeaderStyle(HSSFWorkbook workbook) {
		HSSFCellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		short headerColor = getHeaderColor(workbook);
		font.setFontName("Arial");
		font.setFontHeightInPoints(FONT_HEIGHT_HEADER);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		font.setColor(HSSFColor.WHITE.index);
		style.setFillForegroundColor(headerColor);
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFont(font);
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		style.setBottomBorderColor(headerColor);
		style.setTopBorderColor(headerColor);
		style.setLeftBorderColor(headerColor);
		style.setRightBorderColor(headerColor);
		return style;
	}

	protected HSSFCellStyle createHeaderTitleStyle(HSSFWorkbook workbook, short align) {
		HSSFCellStyle style = createHeaderStyle(workbook);
		Font font = workbook.createFont();
		font.setFontName("Arial");
		font.setFontHeightInPoints(FONT_HEIGHT_HEADER);
		font.setColor(HSSFColor.WHITE.index);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		style.setFont(font);
		style.setAlignment(align);
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style.setBottomBorderColor(HSSFColor.WHITE.index);
		return style;
	}

	protected HSSFCellStyle createHeaderDataStyle(HSSFWorkbook workbook) {
		HSSFCellStyle style = createHeaderStyle(workbook);
		Font font = workbook.createFont();
		font.setFontName("Arial");
		font.setFontHeightInPoints(FONT_HEIGHT_HEADER);
		font.setColor(HSSFColor.BLACK.index);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
		style.setFillForegroundColor(getHeaderDataColor(workbook));
		style.setFont(font);
		return style;
	}

	protected HSSFCellStyle createColumnStyle(HSSFWorkbook workbook) {
		HSSFCellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		short headerColor = getHeaderColor(workbook);
		font.setFontName("Arial");
		font.setFontHeightInPoints(FONT_HEIGHT);
		font.setColor(HSSFColor.BLACK.index);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
		style.setFont(font);
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		style.setLeftBorderColor(headerColor);
		style.setRightBorderColor(headerColor);
		return style;
	}

	protected HSSFCellStyle createLastRowStyle(HSSFWorkbook workbook) {
		HSSFCellStyle style = createColumnStyle(workbook);
		short headerColor = getHeaderColor(workbook);
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style.setBottomBorderColor(headerColor);
		return style;
	}

	protected short getHeaderColor(HSSFWorkbook workbook) {
		HSSFPalette palette = workbook.getCustomPalette();
		palette.setColorAtIndex(new Byte((byte) 41), new Byte((byte) 27), new Byte((byte) 142), new Byte((byte) 224));
		return palette.getColor(41).getIndex();
	}

	protected short getHeaderDataColor(HSSFWorkbook workbook) {
		HSSFPalette palette = workbook.getCustomPalette();
		palette.setColorAtIndex(new Byte((byte) 42), new Byte((byte) 227), new Byte((byte) 248), new Byte((byte) 255));
		return palette.getColor(42).getIndex();
	}

	protected void writeExcelFile(Map<String, Object> model, HSSFWorkbook workbook, HttpServletResponse response)
			throws IOException {
		String excelFileName = (String) model.get(EXCEL_FILENAME);
		response.setHeader("Content-disposition", "attachment; filename=" + excelFileName + ".xls");
		response.setContentType("application/vnd.ms-excel");
		FileOutputStream fos = null;
		try {
			String excelPathStr = RSSettingsDao.getInstance().getStringSettingValueByKey(RSSettingsDao.EXPORT_EXCEL_PATH);
			File excelPath = new File(excelPathStr, "");
			if (!excelPath.exists()) {
				excelPath.mkdirs();
			}
			File excelFile = new File(excelPathStr, excelFileName + ".xls");
			fos = new FileOutputStream(excelFile, false);
			workbook.write(fos);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fos != null) {
				fos.flush();
				fos.close();
			}
			if (workbook != null) {
				workbook.close();
			}
		}
	}

}
