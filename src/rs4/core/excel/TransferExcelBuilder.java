package rs4.core.excel;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import rs4.database.dao.RSStockDao;
import rs4.database.dao.model.RSStock;
import rs4.database.dao.model.RSTransfer;

public class TransferExcelBuilder extends AbstractExcelBuilder {

	public static final String EXCEL_TYPE_TRANSFER_LIST = "excel_type_transfer_list";

	private static RSStockDao rsStockDao = RSStockDao.getInstance();
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String excelType = (String) model.get(EXCEL_TYPE);
		if (EXCEL_TYPE_TRANSFER_LIST.equals(excelType)) {
			buildTransferListReport(model, workbook);
		}
		writeExcelFile(model, workbook, response);
	}

	@SuppressWarnings("unchecked")
	private void buildTransferListReport(Map<String, Object> model, HSSFWorkbook workbook) {

		// arbitrary define here
		int rowCount = 1, sheetWidth = 4;
		
		List<RSTransfer> transferList = (List<RSTransfer>) model.get(EXCEL_DATA);
		HSSFSheet sheet = workbook.createSheet("transfer list");
		HSSFCellStyle headerStyle = createHeaderStyle(workbook);
		HSSFCellStyle columnStyle = createColumnStyle(workbook);
		HSSFCellStyle lastRowStyle = createLastRowStyle(workbook);
		insertTitleHeader(workbook, sheet, sheetWidth, 2, "RS4 Transfer List");

		HSSFRow headerRow = sheet.createRow(rowCount++);
		headerRow.setHeight(HEADER_HEIGHT);
		headerRow.createCell(0).setCellValue("No");
		headerRow.createCell(1).setCellValue("Transfer Date");
		headerRow.createCell(2).setCellValue("From Stock");
		headerRow.createCell(3).setCellValue("To Stock");
		headerRow.createCell(4).setCellValue("Transfer Value");
		for (int i = 0; i < headerRow.getLastCellNum(); i++) {
			headerRow.getCell(i).setCellStyle(headerStyle);
		}
		for (RSTransfer transfer : transferList) {
			RSStock fromStock = rsStockDao.getRSStockById(transfer.getFromStockId());
			RSStock toStock = rsStockDao.getRSStockById(transfer.getToStockId());
			if(fromStock != null && toStock != null) {
				HSSFRow transferRow = sheet.createRow(rowCount++);
				transferRow.createCell(0).setCellValue(transfer.getTicketId());
				transferRow.createCell(1).setCellValue(transfer.getTransferDateDisplay());
				transferRow.createCell(2).setCellValue(fromStock.getCustomerCode() + " - " + fromStock.getProductCode());
				transferRow.createCell(3).setCellValue(toStock.getCustomerCode() + " - " + toStock.getProductCode());
				transferRow.createCell(4).setCellValue(transfer.getTransferValueDisplay());
				for (int i = 0; i < transferRow.getLastCellNum(); i++) {
					if (rowCount == transferList.size() + 2) {
						transferRow.getCell(i).setCellStyle(lastRowStyle);
					} else {
						transferRow.getCell(i).setCellStyle(columnStyle);
					}
				}
			}
		}
		for (int i = 0; i < headerRow.getLastCellNum(); i++) {
			sheet.autoSizeColumn(i);
			headerRow.getCell(i).setCellStyle(headerStyle);
		}
	}

}
