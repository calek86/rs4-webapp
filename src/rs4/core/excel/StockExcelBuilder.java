package rs4.core.excel;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.util.CollectionUtils;

import rs4.core.search.SearchResultsSummary;
import rs4.core.search.form.TransactionForm;
import rs4.core.stock.form.AnnualReportForm;
import rs4.core.stock.form.AnnualReportStockForm;
import rs4.core.stock.form.DetailedReportForm;
import rs4.database.dao.model.RSStock;
import rs4.utils.DateUtils;

public class StockExcelBuilder extends AbstractExcelBuilder {

	public static final String EXCEL_TYPE_STOCK_CONTROL = "excel_type_stock_control";
	public static final String EXCEL_TYPE_STOCK_DETAILED = "excel_type_stock_detailed";
	public static final String EXCEL_TYPE_STOCK_ANNUAL = "excel_type_stock_annual";

	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String excelType = (String) model.get(EXCEL_TYPE);
		if (EXCEL_TYPE_STOCK_CONTROL.equals(excelType)) {
			buildStockControlReport(model, workbook);
		}
		if (EXCEL_TYPE_STOCK_DETAILED.equals(excelType)) {
			buildStockDetailedReport(model, workbook);
		}
		if (EXCEL_TYPE_STOCK_ANNUAL.equals(excelType)) {
			buildStockAnnualReport(model, workbook);
		}
		writeExcelFile(model, workbook, response);
	}

	@SuppressWarnings("unchecked")
	private void buildStockControlReport(Map<String, Object> model, HSSFWorkbook workbook) {
		List<RSStock> stockList = (List<RSStock>) model.get(EXCEL_DATA);
		SearchResultsSummary summary = (SearchResultsSummary) model.get(EXCEL_SUMMARY_DATA);

		// arbitrary define here
		int rowCount = 1, sheetWidth = 2;
		
		HSSFSheet sheet = workbook.createSheet("stock control");
		HSSFCellStyle headerStyle = createHeaderStyle(workbook);
		HSSFCellStyle headerDataStyle = createHeaderDataStyle(workbook);
		HSSFCellStyle columnStyle = createColumnStyle(workbook);
		HSSFCellStyle lastRowStyle = createLastRowStyle(workbook);
		insertTitleHeader(workbook, sheet, sheetWidth, 1, "RS4 Stock Control Report", new String());

		HSSFRow summaryHeader = sheet.createRow(rowCount++);
		summaryHeader.setHeight(HEADER_HEIGHT);
		summaryHeader.createCell(0).setCellValue("Report Date");
		summaryHeader.createCell(1).setCellValue("Stock Year");
		summaryHeader.createCell(2).setCellValue("Total Stock");
		for (int i = 0; i < summaryHeader.getLastCellNum(); i++) {
			summaryHeader.getCell(i).setCellStyle(headerStyle);
		}

		HSSFRow summaryRow = sheet.createRow(rowCount++);
		summaryRow.setHeight(HEADER_HEIGHT);
		summaryRow.createCell(0).setCellValue(DateUtils.getDateTimeToday());
		if (!CollectionUtils.isEmpty(stockList)) {
			summaryRow.createCell(1).setCellValue(stockList.get(0).getStockYearDescription());
		}
		summaryRow.createCell(2).setCellValue(summary.getTotalEntWt());
		for (int i = 0; i < summaryRow.getLastCellNum(); i++) {
			summaryRow.getCell(i).setCellStyle(headerDataStyle);
		}

		HSSFRow headerRow = sheet.createRow(rowCount++);
		headerRow.createCell(0).setCellValue("Customer");
		headerRow.createCell(1).setCellValue("Product");
		headerRow.createCell(2).setCellValue("Total");
		for (int i = 0; i < headerRow.getLastCellNum(); i++) {
			headerRow.getCell(i).setCellStyle(headerStyle);
		}
		for (RSStock stock : stockList) {
			if (stock.getId() != -1) {
				HSSFRow valueRow = sheet.createRow(rowCount++);
				valueRow.createCell(0).setCellValue(stock.getCustomerCode());
				valueRow.createCell(1).setCellValue(stock.getProductCode());
				valueRow.createCell(2).setCellValue(stock.getTotalValueDisplay());
				for (int i = 0; i < valueRow.getLastCellNum(); i++) {
					valueRow.getCell(i).setCellStyle(columnStyle);
				}
			}
			else {
				HSSFRow valueRow = sheet.createRow(rowCount++);
				valueRow.createCell(0).setCellValue("");
				valueRow.createCell(1).setCellValue("");
				valueRow.createCell(2).setCellValue(stock.getTotalValueDisplay());
				for (int i = 0; i < valueRow.getLastCellNum(); i++) {
					valueRow.getCell(i).setCellStyle(lastRowStyle);
				}
			}
		}
		for (int i = 0; i < headerRow.getLastCellNum(); i++) {
			sheet.autoSizeColumn(i);
		}
	}

	@SuppressWarnings("unchecked")
	private void buildStockDetailedReport(Map<String, Object> model, HSSFWorkbook workbook) {
		List<DetailedReportForm> stockFormList = (List<DetailedReportForm>) model.get(EXCEL_DATA);
		SearchResultsSummary summary = (SearchResultsSummary) model.get(EXCEL_SUMMARY_DATA);

		// arbitrary define here
		int rowCount = 1, sheetWidth = 11;
		
		HSSFSheet sheet = workbook.createSheet("stock detailed");
		HSSFCellStyle headerStyle = createHeaderStyle(workbook);
		HSSFCellStyle headerDataStyle = createHeaderDataStyle(workbook);
		HSSFCellStyle columnStyle = createColumnStyle(workbook);
		HSSFCellStyle lastRowStyle = createLastRowStyle(workbook);
		HSSFCellStyle mediumHeaderStyle = createHeaderTitleStyle(workbook, CellStyle.ALIGN_CENTER);
		insertTitleHeader(workbook, sheet, sheetWidth, 5, "RS4 Stock Detailed Report");

		HSSFRow summaryHeaderRow = sheet.createRow(rowCount++);
		summaryHeaderRow.setHeight(HEADER_HEIGHT);
		summaryHeaderRow.createCell(0).setCellValue("Report Date");
		summaryHeaderRow.createCell(1).setCellValue("Avg. Moisture");
		summaryHeaderRow.createCell(2).setCellValue("Avg. Admix");
		summaryHeaderRow.createCell(3).setCellValue("Avg. Temperature");
		summaryHeaderRow.createCell(4).setCellValue("Avg. Sp.Wt.");
		summaryHeaderRow.createCell(5).setCellValue("Total Stock");
		for (int i = 6; i < sheetWidth+1; i++) {
			summaryHeaderRow.createCell(i);
		}
		sheet.addMergedRegion(new CellRangeAddress(1, 1, 6, sheetWidth));
		for (int i = 0; i < summaryHeaderRow.getLastCellNum(); i++) {
			summaryHeaderRow.getCell(i).setCellStyle(headerStyle);
		}

		HSSFRow summaryDataRow = sheet.createRow(rowCount++);
		summaryDataRow.setHeight(HEADER_HEIGHT);
		summaryDataRow.createCell(0).setCellValue(DateUtils.getDateTimeToday());
		summaryDataRow.createCell(1).setCellValue(summary.getAvgMoisture());
		summaryDataRow.createCell(2).setCellValue(summary.getAvgAdmix());
		summaryDataRow.createCell(3).setCellValue(summary.getAvgTemperature());
		summaryDataRow.createCell(4).setCellValue(summary.getAvgSpWt());
		summaryDataRow.createCell(5).setCellValue(summary.getTotalEntWt());
		for (int i = 6; i < sheetWidth+1; i++) {
			summaryDataRow.createCell(i);
		}
		sheet.addMergedRegion(new CellRangeAddress(2, 2, 6, sheetWidth));
		for (int i = 0; i < summaryDataRow.getLastCellNum(); i++) {
			summaryDataRow.getCell(i).setCellStyle(headerDataStyle);
		}

		HSSFRow dataHeaderRow = sheet.createRow(rowCount++);
		dataHeaderRow.setHeight(HEADER_HEIGHT);
		dataHeaderRow.createCell(0).setCellValue("Customer");
		dataHeaderRow.createCell(1).setCellValue("Product");
		dataHeaderRow.createCell(2).setCellValue("Total");
		for (int i = 3; i < sheetWidth+1; i++) {
			dataHeaderRow.createCell(i);
		}
		sheet.addMergedRegion(new CellRangeAddress(3, 3, 3, sheetWidth));
		for (int i = 0; i < dataHeaderRow.getLastCellNum(); i++) {
			dataHeaderRow.getCell(i).setCellStyle(headerStyle);
		}

		for (DetailedReportForm stockForm : stockFormList) {
			if (stockForm.getId() > 0) {
				HSSFRow dataRow = sheet.createRow(rowCount++);
				dataRow.setHeight(HEADER_HEIGHT);
				dataRow.createCell(0).setCellValue(stockForm.getCustomer());
				dataRow.createCell(1).setCellValue(stockForm.getProduct());
				dataRow.createCell(2).setCellValue(stockForm.getTotalValue());
				for (int i = 3; i < sheetWidth+1; i++) {
					dataRow.createCell(i);
				}
				for (int i = 0; i < dataRow.getLastCellNum(); i++) {
					dataRow.getCell(i).setCellStyle(mediumHeaderStyle);
				}
				if (stockForm.getTransactions() != null && stockForm.getTransactions().size() > 0) {
					HSSFRow transactionHeaderRow = sheet.createRow(rowCount++);
					transactionHeaderRow.setHeight(HEADER_HEIGHT);
					transactionHeaderRow.createCell(0).setCellValue("No.");
					transactionHeaderRow.createCell(1).setCellValue("Type");
					transactionHeaderRow.createCell(2).setCellValue("Date");
					transactionHeaderRow.createCell(3).setCellValue("Haulier");
					transactionHeaderRow.createCell(4).setCellValue("Destination");
					transactionHeaderRow.createCell(5).setCellValue("Reg.No.");
					transactionHeaderRow.createCell(6).setCellValue("Net.Wt.");
					transactionHeaderRow.createCell(7).setCellValue("Ent.Wt.");
					transactionHeaderRow.createCell(8).setCellValue("1st Weight");
					transactionHeaderRow.createCell(9).setCellValue("2nd Weight");
					transactionHeaderRow.createCell(10).setCellValue("Order No.");
					transactionHeaderRow.createCell(11).setCellValue("Ex Ref.");
					for (int i = 0; i < transactionHeaderRow.getLastCellNum(); i++) {
						transactionHeaderRow.getCell(i).setCellStyle(headerStyle);
					}
					HSSFRow lastRow = null;
					HSSFRow transactionDataRow = null;
					for (TransactionForm t : stockForm.getTransactions()) {
						transactionDataRow = sheet.createRow(rowCount++);
						transactionDataRow.createCell(0).setCellValue(t.getTicketId());
						transactionDataRow.createCell(1).setCellValue(t.getTransactionType());
						transactionDataRow.createCell(2).setCellValue(t.getTransactionDate());
						transactionDataRow.createCell(3).setCellValue(t.getHaulier());
						transactionDataRow.createCell(4).setCellValue(t.getDestination());
						transactionDataRow.createCell(5).setCellValue(t.getRegistration());
						transactionDataRow.createCell(6).setCellValue(t.getNetWeight());
						if(t.getEntWeight().equals(t.getEntWeightBeans())) {
							transactionDataRow.createCell(7).setCellValue(t.getEntWeight());	
						}
						else {
							transactionDataRow.createCell(7).setCellValue(t.getEntWeightBeans());
						}
						transactionDataRow.createCell(8).setCellValue(t.getFirstWeight());
						transactionDataRow.createCell(9).setCellValue(t.getSecondWeight());
						transactionDataRow.createCell(10).setCellValue(t.getOrderNo());
						transactionDataRow.createCell(11).setCellValue(t.getExRef());
						for (int i = 0; i < transactionDataRow.getLastCellNum(); i++) {
							transactionDataRow.getCell(i).setCellStyle(columnStyle);
						}
						lastRow = transactionDataRow;
					}
					if(lastRow != null) {
						for (int i = 0; i < lastRow.getLastCellNum(); i++) {
							lastRow.getCell(i).setCellStyle(lastRowStyle);
						}
					}
					if(transactionDataRow != null) {
						for (int i = 0; i < transactionDataRow.getLastCellNum(); i++) {
							sheet.autoSizeColumn(i);
						}
					}
				}	
			}
		}
	}

	private void buildStockAnnualReport(Map<String, Object> model, HSSFWorkbook workbook) {
		AnnualReportForm stockAnnualReportForm = (AnnualReportForm) model.get(EXCEL_DATA);

		// arbitrary define here
		int rowCount = 1, sheetWidth = 3;
		
		HSSFSheet sheet = workbook.createSheet("stock annual");
		HSSFCellStyle headerStyle = createHeaderStyle(workbook);
		HSSFCellStyle headerDataStyle = createHeaderDataStyle(workbook);
		HSSFCellStyle columnStyle = createColumnStyle(workbook);
		HSSFCellStyle lastRowStyle = createLastRowStyle(workbook);
		insertTitleHeader(workbook, sheet, sheetWidth, 1, "RS4 Stock Annual Report");

		HSSFRow headerSummaryRow = sheet.createRow(rowCount++);
		headerSummaryRow.setHeight(HEADER_HEIGHT);
		headerSummaryRow.createCell(0).setCellValue("Total Entitlement In");
		headerSummaryRow.createCell(1).setCellValue("Total Weight Out");
		headerSummaryRow.createCell(2);
		headerSummaryRow.createCell(3).setCellValue("Store Total");

		for (int i = 0; i < headerSummaryRow.getLastCellNum(); i++) {
			headerSummaryRow.getCell(i).setCellStyle(headerStyle);
		}

		HSSFRow dataSummaryRow = sheet.createRow(rowCount++);
		dataSummaryRow.setHeight(HEADER_HEIGHT);
		dataSummaryRow.createCell(0).setCellValue(stockAnnualReportForm.getTotalInDisplay());
		dataSummaryRow.createCell(1).setCellValue(stockAnnualReportForm.getTotalOutDisplay());
		dataSummaryRow.createCell(2);
		dataSummaryRow.createCell(3).setCellValue(stockAnnualReportForm.getStoreTotalDisplay());
		for (int i = 0; i < dataSummaryRow.getLastCellNum(); i++) {
			dataSummaryRow.getCell(i).setCellStyle(headerDataStyle);
		}

		HSSFRow headerDataRow = sheet.createRow(rowCount++);
		headerDataRow.setHeight(HEADER_HEIGHT);
		headerDataRow.createCell(0).setCellValue("Product");
		headerDataRow.createCell(1).setCellValue("Entitlement In");
		headerDataRow.createCell(2).setCellValue("Weight Out");
		headerDataRow.createCell(3).setCellValue("Store Total");
		for (int i = 0; i < headerDataRow.getLastCellNum(); i++) {
			headerDataRow.getCell(i).setCellStyle(headerStyle);
		}

		for (Map.Entry<String, AnnualReportStockForm> entry : stockAnnualReportForm.getMap().entrySet()) {
			String key = entry.getKey();
			AnnualReportStockForm val = entry.getValue();
			HSSFRow dataRow = sheet.createRow(rowCount++);
			dataRow.createCell(0).setCellValue(key);
			dataRow.createCell(1).setCellValue(val.getEntInDisplay());
			dataRow.createCell(2).setCellValue(val.getWeightOutDisplay());
			dataRow.createCell(3).setCellValue(val.getStoreTotalDisplay());
			
			for (int i = 0; i < dataRow.getLastCellNum(); i++) {
				if (rowCount == stockAnnualReportForm.getMap().size() + 4) {
					dataRow.getCell(i).setCellStyle(lastRowStyle);
				} else {
					dataRow.getCell(i).setCellStyle(columnStyle);
				}
			}
		}
		for (int i = 0; i < dataSummaryRow.getLastCellNum(); i++) {
			sheet.autoSizeColumn(i);
		}
	}

}
