package rs4.core.bills;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.util.CollectionUtils;

import rs4.core.bills.BillsCriteria.BillReportType;
import rs4.core.bills.form.StandardBillForm;
import rs4.core.bills.form.StorageBillForm;
import rs4.core.bills.form.StorageProductBillForm;
import rs4.core.search.TransactionType;
import rs4.core.stock.StockCriteria;
import rs4.core.stock.StockCriteria.StockGroupBy;
import rs4.core.stock.manager.StockManager;
import rs4.core.stock.manager.StockMap;
import rs4.database.dao.RSCustomerDao;
import rs4.database.dao.RSProductDao;
import rs4.database.dao.RSSettingsDao;
import rs4.database.dao.RSStockDao;
import rs4.database.dao.RSStockYearDao;
import rs4.database.dao.RSTransactionDao;
import rs4.database.dao.RSTransferDao;
import rs4.database.dao.model.RSDeduction;
import rs4.database.dao.model.RSStock;
import rs4.database.dao.model.RSStockYear;
import rs4.database.dao.model.RSTransaction;
import rs4.database.dao.model.RSTransfer;
import rs4.database.dao.model.RSUser;
import rs4.utils.DataUtils;
import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;
import rs4.utils.Logger;

public class BillsData {

	private RSCustomerDao rsCustomerDao;
	private RSProductDao rsProductDao;
	private RSStockDao rsStockDao;
	private RSStockYearDao rsStockYearDao;
	private RSTransferDao rsTransferDao;
	private RSTransactionDao rsTransactionDao;
	private RSSettingsDao rsSettingsDao;

	private BillsCriteria billsCriteria;
	private BillsResults billsResults;

	private static Map<Integer, BillsData> instances;
	private static Logger logger = Logger.getLogger(BillsData.class.getName());

	public static BillsData getInstance(RSUser user) {
		if (user == null) {
			logger.error("Requesting data object without user");
			return new BillsData();
		}
		if (instances == null) {
			instances = new HashMap<>();
		}
		if (!instances.containsKey(user.getId())) {
			instances.put(user.getId(), new BillsData());
		}
		return instances.get(user.getId());
	}

	public static void reloadInstance() {
		instances = new HashMap<>();
	}

	private BillsData() {
		rsCustomerDao = RSCustomerDao.getInstance();
		rsProductDao = RSProductDao.getInstance();
		rsTransferDao = RSTransferDao.getInstance();
		rsStockDao = RSStockDao.getInstance();
		rsStockYearDao = RSStockYearDao.getInstance();
		rsTransactionDao = RSTransactionDao.getInstance();
		rsSettingsDao = RSSettingsDao.getInstance();
		billsCriteria = new BillsCriteria();
		billsResults = new BillsResults();
		reloadCriteria();
	}

	private void reloadCriteria() {
		billsCriteria.setCustomers(rsCustomerDao.getRSCustomersFromStockYear(billsCriteria.getStockYearId()));
		billsCriteria.setProducts(rsProductDao.getRSProductsFromStockYear(billsCriteria.getStockYearId()));
		float storingRate = rsSettingsDao.getFloatSettingValueByKey(RSSettingsDao.STORING_RATE);
		billsCriteria.setStoringRate(storingRate);
	}

	public void generateBill(RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Search bills @ by @ with criteria @", actionId, DisplayUtils.logUser(user),
				billsCriteria.toString());
		billsResults.setReportType(billsCriteria.getReportType());
		billsResults.setReportDate(DateUtils.getDateTimeToday());
		if (billsCriteria.getReportType().equals(BillReportType.Standard)) {
			generateStandardBill(actionId);
		} else if (billsCriteria.getReportType().equals(BillReportType.Storage)) {
			generateStorageBill(actionId);
		}
		reloadCriteria();
	}

	public void reloadDates() {
		RSStockYear rsStockYear = DataUtils.getDefaultStockYear();
		billsCriteria.setDefaultDates(rsStockYear);
	}

	private void generateStandardBill(String actionId) {
		List<RSTransaction> rsTransactions = rsTransactionDao.getRSTransactionsByCriteria(billsCriteria);
		List<StandardBillForm> resultStandardBills = new ArrayList<StandardBillForm>();
		for (RSTransaction rsTransaction : rsTransactions) {
			if (rsTransaction.equalsType(TransactionType.IN)) {
				StandardBillForm billForm = billFormFromTransaction(rsTransaction);
				resultStandardBills.add(billForm);
			}
		}
		billsResults.setResultStandardBills(resultStandardBills);
		billsResults.setBillsSummary(getStandardBillSummary(resultStandardBills));
		billsResults.getBillsSummary().setProduct(billsCriteria.getProductCode());
		billsResults.getBillsSummary().setCustomer(billsCriteria.getCustomerCode());
		logger.info("Search standard bills @ returned @ results", actionId,
				DisplayUtils.logList(billsResults.getResultStandardBills()));
	}

	private BillsSummary getStandardBillSummary(List<StandardBillForm> standardBills) {
		BillsSummary summary = new BillsSummary();
		Float totalNetWt = 0.0F;
		Float totalCost = 0.0F;
		Float avgMoisture = 0.0F;
		Float avgAdmix = 0.0F;
		Float avgTemperature = 0.0F;
		int avgMoistureCount = 0;
		int avgAdmixCount = 0;
		int avgTemperatureCount = 0;
		for (StandardBillForm standardBill : standardBills) {
			totalCost += standardBill.getTotalCost();
			RSTransaction rsTransaction = standardBill.getTransaction();
			totalNetWt += rsTransaction.getNetWeight();
			if (rsTransaction.getMoisture() > 0) {
				avgMoisture += rsTransaction.getMoisture();
				avgMoistureCount++;
			}
			if (rsTransaction.getAdmix() > 0) {
				avgAdmix += rsTransaction.getAdmix();
				avgAdmixCount++;
			}
			if (rsTransaction.getTemperature() > 0) {
				avgTemperature += rsTransaction.getTemperature();
				avgTemperatureCount++;
			}
		}
		if (avgMoisture > 0) {
			avgMoisture /= avgMoistureCount;
		}
		if (avgAdmix > 0) {
			avgAdmix /= avgAdmixCount;
		}
		if (avgTemperature > 0) {
			avgTemperature /= avgTemperatureCount;
		}
		summary.setAvgMoisture(avgMoisture);
		summary.setAvgAdmix(avgAdmix);
		summary.setAvgTemperature(avgTemperature);
		summary.setTotalNetWt(totalNetWt);
		summary.setTotalCost(totalCost);
		return summary;
	}

	private void generateStorageBill(String searchId) {
		StockCriteria stockCriteria = new StockCriteria();
		stockCriteria.setCustomerCode(billsCriteria.getCustomerCode());
		stockCriteria.setProductCode(billsCriteria.getProductCode());
		stockCriteria.setGroupBy(StockGroupBy.Product.toString());
		List<RSStock> rsStocks = rsStockDao.getRSStocksByCriteria(stockCriteria);
		List<StorageProductBillForm> resultStorageProductBills = new ArrayList<>();
		billsResults.setResultStorageProductBills(resultStorageProductBills);
		List<Date> dateList = generateDateList();
		if (!canGenerateStorageReport(rsStocks, dateList)) {
			return;
		}
		float summaryTotalCost = 0.0F;
		float summaryTotalDayTonnage = 0.0F;
		float storingRate = billsCriteria.getStoringRate();
		String productCode = billsCriteria.getProductCode();
		for (RSStock rsStock : rsStocks) {
			StorageProductBillForm storageProductBillForm = storageBillFromStock(rsStock, storingRate, dateList);
			List<StorageBillForm> storageBills = storageProductBillForm.getResultStorageBills();
			if (!CollectionUtils.isEmpty(storageBills)) {
				for (int i = 0; i < storageBills.size(); i++) {
					if (i == storageBills.size() - 1) {
						summaryTotalDayTonnage += storageBills.get(i).getDayTonnage();
						summaryTotalCost += storageBills.get(i).getDayCost();
					}
				}
			}
			resultStorageProductBills.add(storageProductBillForm);
		}
		billsCriteria.setProductCode(productCode);
		billsResults.setResultStorageProductBills(resultStorageProductBills);
		billsResults.setBillsSummary(new BillsSummary());
		billsResults.getBillsSummary().setCustomer(billsCriteria.getCustomerCode());
		billsResults.getBillsSummary().setFromDateDisplay(billsCriteria.getDateFrom());
		billsResults.getBillsSummary().setToDateDisplay(billsCriteria.getDateTo());
		billsResults.getBillsSummary().setTotalDays(dateList.size());
		billsResults.getBillsSummary().setTotalDayTonnage(summaryTotalDayTonnage);
		billsResults.getBillsSummary().setStoringRateDisplay(DisplayUtils.formatCurrencyDecimals(storingRate, true, 3));
		billsResults.getBillsSummary().setTotalCost(summaryTotalCost);
		logger.info("Search storage bills @ returned @ results", searchId,
				DisplayUtils.logList(resultStorageProductBills));
	}

	private StorageProductBillForm storageBillFromStock(RSStock rsStock, float storingRate, List<Date> dates) {
		String dateFromCriteria = new String(billsCriteria.getDateFrom());
		RSStockYear rsStockYear = rsStockYearDao.getRSStockYearById(rsStock.getStockYearId());
		String dateFromStockYear = DateUtils.formatDate(rsStockYear.getDateFrom());
		billsCriteria.setDateFrom(dateFromStockYear);
		List<RSTransfer> rsTransfers = rsTransferDao.getRSTransfersByStockIdAndDateRange(rsStock.getId(),
				dateFromStockYear, billsCriteria.getDateTo());
		billsCriteria.setProductCode(rsStock.getProductCode());
		List<RSTransaction> rsTransactions = rsTransactionDao.getRSTransactionsByCriteria(billsCriteria);
		LinkedList<StorageBillForm> resultStorageBills = generateStorageBill(rsStock, dates, rsTransfers,
				rsTransactions);
		resultStorageBills = cutResultStorageBills(resultStorageBills, dateFromCriteria);
		float totalDayTonnage = calculateTotalDayTonnage(resultStorageBills);
		float totalCost = calculateTotalCost(storingRate, resultStorageBills);
		billsCriteria.setDateFrom(dateFromCriteria);
		StorageProductBillForm storageProductBillForm = new StorageProductBillForm();
		storageProductBillForm.setProduct(billsCriteria.getProductCode());
		storageProductBillForm.setTotalCost(totalCost);
		storageProductBillForm.setTotalDayTonnage(totalDayTonnage);
		storageProductBillForm.setResultStorageBills(resultStorageBills);
		return storageProductBillForm;
	}

	private LinkedList<StorageBillForm> generateStorageBill(RSStock rsStock, List<Date> dates,
			List<RSTransfer> rsTransfers, List<RSTransaction> rsTransactions) {
		LinkedList<StorageBillForm> resultStorageBills = new LinkedList<>();
		Map<String, Float> entWtByDateMap = new HashMap<>();
		for (RSTransaction rsTransaction : rsTransactions) {
			float entWt = DataUtils.getStockEntWt(rsTransaction);
			if (rsTransaction.equalsType(TransactionType.OUT)) {
				entWt *= -1;
			}
			String dateKey = DateUtils.formatDate(rsTransaction.getTransactionDate());
			if (entWtByDateMap.containsKey(dateKey)) {
				entWtByDateMap.put(dateKey, entWtByDateMap.get(dateKey) + entWt);
			} else {
				entWtByDateMap.put(dateKey, entWt);
			}
		}
		for (RSTransfer rsTransfer : rsTransfers) {
			float entWt = rsTransfer.getTransferValue();
			if (rsStock.getId() == rsTransfer.getFromStockId()) {
				entWt *= -1;
			}
			String dateKey = DateUtils.formatDate(rsTransfer.getTransferDate());
			if (entWtByDateMap.containsKey(dateKey)) {
				entWtByDateMap.put(dateKey, entWtByDateMap.get(dateKey) + entWt);
			} else {
				entWtByDateMap.put(dateKey, entWt);
			}
		}
		int dayCount = dates.size();
		rsStock = calculateDynamicStock(rsStock, rsTransactions, rsTransfers);
		float entWtStock = rsStock.getTotalValue();
		float lastEntWtChange = 0.0F;
		for (int i = 0; i < dates.size(); i++) {
			Date date = dates.get(i);
			float entWtChange = 0.0F;
			String dateKey = DateUtils.formatDate(date);
			if (entWtByDateMap.containsKey(dateKey)) {
				entWtChange = entWtByDateMap.get(dateKey);
			}
			if (entWtChange >= 0) {
				entWtStock -= lastEntWtChange;
				lastEntWtChange = entWtChange;
			} else {
				entWtStock -= entWtChange;
				lastEntWtChange = 0.0F;
			}
			StorageBillForm billForm = new StorageBillForm();
			billForm.setDayCount(dayCount);
			billForm.setDate(date);
			billForm.setEntWtStock(entWtStock);
			billForm.setEntWtChange(entWtChange);
			resultStorageBills.addFirst(billForm);
			dayCount--;
		}
		return resultStorageBills;
	}

	private StandardBillForm billFormFromTransaction(RSTransaction rsTransaction) {
		StandardBillForm billForm = new StandardBillForm();
		billForm.setTransaction(rsTransaction);
		RSDeduction rsDeduction = DataUtils.getRSDeductionFromTransaction(rsTransaction);
		float intakeCost = rsDeduction.getIntakeCost() * (rsTransaction.getNetWeight() / 1000);
		float dryingCost = rsDeduction.getDryingCost() * (rsTransaction.getNetWeight() / 1000);
		billForm.setIntakeCost(intakeCost);
		billForm.setDryingCost(dryingCost);
		billForm.setTotalCost(intakeCost + dryingCost);
		return billForm;
	}

	private boolean canGenerateStorageReport(List<RSStock> rsStocks, List<Date> dateList) {
		if (CollectionUtils.isEmpty(rsStocks)) {
			logger.info("No stock records for customer @ and product @", billsCriteria.getCustomerCode(),
					billsCriteria.getProductCode());
			return false;
		}
		if (CollectionUtils.isEmpty(dateList)) {
			logger.info("Cannot generate date list for storage report");
			return false;
		}
		return true;
	}

	private List<Date> generateDateList() {
		try {
			Date dateFrom = DateUtils.completeDateFrom(billsCriteria.getDateFrom());
			Date dateTo = DateUtils.completeDateTo(billsCriteria.getDateTo());
			if (dateFrom.after(dateTo)) {
				return new ArrayList<Date>();
			}
			List<Date> dateList = DateUtils.generateDateList(dateFrom, dateTo);
			dateList = DataUtils.revertCollection(dateList);
			return dateList;
		} catch (Exception e) {
			return new ArrayList<Date>();
		}
	}

	private LinkedList<StorageBillForm> cutResultStorageBills(LinkedList<StorageBillForm> resultStorageBills,
			String dateFromCriteria) {
		LinkedList<StorageBillForm> cuteResults = new LinkedList<>();
		try {
			for (StorageBillForm storageBillForm : resultStorageBills) {
				Date dateFrom = DateUtils.parseDate(dateFromCriteria);
				if (storageBillForm.getDate().compareTo(dateFrom) >= 0) {
					cuteResults.add(storageBillForm);
				}
			}
		} catch (Exception e) {
			logger.error("Problem cutting storage bill date");
		}
		return cuteResults;
	}

	private float calculateTotalCost(float storingRate, LinkedList<StorageBillForm> resultStorageBills) {
		float totalCost = 0.0F;
		for (int i = 0; i < resultStorageBills.size(); i++) {
			StorageBillForm billForm = resultStorageBills.get(i);
			if (billForm.getEntWtStock() > 0) {
				totalCost += billForm.getEntWtStock() * storingRate / 1000;
			}
			billForm.setDayCost(totalCost);
		}
		return totalCost;
	}

	private float calculateTotalDayTonnage(LinkedList<StorageBillForm> resultStorageBills) {
		float totalDayTonnage = 0.0F;
		for (int i = 0; i < resultStorageBills.size(); i++) {
			StorageBillForm billForm = resultStorageBills.get(i);
			if (billForm.getEntWtStock() > 0) {
				totalDayTonnage += billForm.getEntWtStock();
			}
			billForm.setDayTonnage(totalDayTonnage);
		}
		return totalDayTonnage;
	}

	private RSStock calculateDynamicStock(RSStock rsStock, List<RSTransaction> rsTransactions,
			List<RSTransfer> rsTransfers) {
		StockManager stockManager = new StockManager();
		StockMap stockMap = stockManager.calculateStock(rsTransactions, rsTransfers);
		return stockMap.getStock(rsStock.getId());
	}

	public BillsCriteria getBillsCriteria() {
		return billsCriteria;
	}

	public void setBillsCriteria(BillsCriteria billsCriteria) {
		this.billsCriteria = billsCriteria;
	}

	public BillsResults getBillsResults() {
		return billsResults;
	}

	public void setBillsResults(BillsResults billsResults) {
		this.billsResults = billsResults;
	}

}
