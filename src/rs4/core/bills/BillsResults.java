package rs4.core.bills;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import rs4.core.bills.BillsCriteria.BillReportType;
import rs4.core.bills.form.StandardBillForm;
import rs4.core.bills.form.StorageProductBillForm;

public class BillsResults implements Serializable {

	private static final long serialVersionUID = 3596772318134070144L;

	private List<StandardBillForm> resultStandardBills;
	private List<StorageProductBillForm> resultStorageProductBills;
	private String reportDate;
	private BillReportType reportType;
	private BillsSummary billsSummary;

	public BillsResults() {
		resultStandardBills = new ArrayList<StandardBillForm>();
		billsSummary = new BillsSummary();
	}

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public BillReportType getReportType() {
		return reportType;
	}

	public void setReportType(BillReportType reportType) {
		this.reportType = reportType;
	}

	public BillsSummary getBillsSummary() {
		return billsSummary;
	}

	public void setBillsSummary(BillsSummary billsSummary) {
		this.billsSummary = billsSummary;
	}

	public List<StandardBillForm> getResultStandardBills() {
		return resultStandardBills;
	}

	public void setResultStandardBills(List<StandardBillForm> resultStandardBills) {
		this.resultStandardBills = resultStandardBills;
	}

	public List<StorageProductBillForm> getResultStorageProductBills() {
		return resultStorageProductBills;
	}

	public void setResultStorageProductBills(List<StorageProductBillForm> resultStorageProductBills) {
		this.resultStorageProductBills = resultStorageProductBills;
	}

}
