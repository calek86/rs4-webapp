package rs4.core.bills;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import rs4.core.bills.form.StandardBillForm;
import rs4.core.bills.form.StorageProductBillForm;
import rs4.core.excel.AbstractExcelBuilder;
import rs4.core.excel.BillsExcelBuilder;
import rs4.database.dao.model.RSUser;
import rs4.utils.DataUtils;

@Controller
@SessionAttributes("billsForm")
public class BillsController {

	@RequestMapping(value = "/SearchBills", params = "Generate", method = RequestMethod.POST)
	public ModelAndView searchBills(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		BillsData billsData = BillsData.getInstance(DataUtils.requestUser(request));
		billsData.getBillsCriteria().setReportType(request.getParameter("billsCriteria.reportType"));
		billsData.getBillsCriteria().setTransactionsNo(request.getParameter("billsCriteria.transactionsNo"));
		billsData.getBillsCriteria().setDateFrom(request.getParameter("billsCriteria.dateFrom"));
		billsData.getBillsCriteria().setDateTo(request.getParameter("billsCriteria.dateTo"));
		billsData.getBillsCriteria().setCustomerCode(request.getParameter("billsCriteria.customerCode"));
		billsData.getBillsCriteria().setProductCode(request.getParameter("billsCriteria.products"));
		billsData.getBillsCriteria().setStoringRate(request.getParameter("billsCriteria.storingRate"));
		billsData.generateBill(user);
		return new ModelAndView("bills", "command", billsData);
	}

	@RequestMapping(value = "/SearchBills/standardExcel", method = RequestMethod.GET)
	public ModelAndView downloadStandardBillsExcel(HttpServletRequest request) {
		BillsData billsData = BillsData.getInstance(DataUtils.requestUser(request));
		List<StandardBillForm> transactionList = billsData.getBillsResults().getResultStandardBills();
		ModelAndView model = new ModelAndView("billsExcelView");
		model.addObject(AbstractExcelBuilder.EXCEL_TYPE, BillsExcelBuilder.EXCEL_TYPE_BILLS_STANDARD);
		model.addObject(AbstractExcelBuilder.EXCEL_DATA, transactionList);
		model.addObject(AbstractExcelBuilder.EXCEL_FILENAME, "billStandard" + AbstractExcelBuilder.getFileNameDatePrefix());
		model.addObject(AbstractExcelBuilder.EXCEL_SUMMARY_DATA, billsData.getBillsResults().getBillsSummary());
		return model;
	}

	@RequestMapping(value = "/SearchBills/storageExcel", method = RequestMethod.GET)
	public ModelAndView downloadStorageBillsExcel(HttpServletRequest request) {
		BillsData billsData = BillsData.getInstance(DataUtils.requestUser(request));
		List<StorageProductBillForm> billList = billsData.getBillsResults().getResultStorageProductBills();
		ModelAndView model = new ModelAndView("billsExcelView");
		model.addObject(AbstractExcelBuilder.EXCEL_TYPE, BillsExcelBuilder.EXCEL_TYPE_BILLS_STORAGE);
		model.addObject(AbstractExcelBuilder.EXCEL_DATA, billList);
		model.addObject(AbstractExcelBuilder.EXCEL_FILENAME, "billStorage" + AbstractExcelBuilder.getFileNameDatePrefix());
		model.addObject(AbstractExcelBuilder.EXCEL_SUMMARY_DATA, billsData.getBillsResults().getBillsSummary());
		return model;
	}

}
