package rs4.core.bills;

import java.io.Serializable;

import rs4.utils.DisplayUtils;

public class BillsSummary implements Serializable {

	private static final long serialVersionUID = 7577593683506121491L;

	private String product;
	private String customer;

	private int totalDays;

	private float totalNetWt;
	private float totalDayTonnage;
	private float totalCost;
	private float storingRate;

	private String totalNetWtDisplay;
	private String totalDayTonnageDisplay;
	private String totalCostDisplay;
	private String storingRateDisplay;

	private float avgMoisture;
	private float avgAdmix;
	private float avgTemperature;

	private String avgMoistureDisplay;
	private String avgAdmixDisplay;
	private String avgTemperatureDisplay;

	private String fromDateDisplay;
	private String toDateDisplay;
	private String rateTonneDayDisplay;

	public float getTotalNetWt() {
		return totalNetWt;
	}

	public void setTotalNetWt(float totalNetWt) {
		setTotalNetWtDisplay(DisplayUtils.formatWeight(totalNetWt, true));
		this.totalNetWt = totalNetWt;
	}

	public float getAvgMoisture() {
		return avgMoisture;
	}

	public void setAvgMoisture(float avgMoisture) {
		setAvgMoistureDisplay(DisplayUtils.formatPercentage(avgMoisture, true));
		this.avgMoisture = avgMoisture;
	}

	public float getAvgAdmix() {
		return avgAdmix;
	}

	public void setAvgAdmix(float avgAdmix) {
		setAvgAdmixDisplay(DisplayUtils.formatPercentage(avgAdmix, true));
		this.avgAdmix = avgAdmix;
	}

	public float getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(float totalCost) {
		setTotalCostDisplay(DisplayUtils.formatCurrency(totalCost, true));
		this.totalCost = totalCost;
	}

	public float getAvgTemperature() {
		return avgTemperature;
	}

	public void setAvgTemperature(float avgTemperature) {
		this.avgTemperature = avgTemperature;
		setAvgTemperatureDisplay(DisplayUtils.formatTemperature(avgTemperature, true));
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getTotalNetWtDisplay() {
		return totalNetWtDisplay;
	}

	public void setTotalNetWtDisplay(String totalNetWtDisplay) {
		this.totalNetWtDisplay = totalNetWtDisplay;
	}

	public String getAvgMoistureDisplay() {
		return avgMoistureDisplay;
	}

	public void setAvgMoistureDisplay(String avgMoistureDisplay) {
		this.avgMoistureDisplay = avgMoistureDisplay;
	}

	public String getAvgAdmixDisplay() {
		return avgAdmixDisplay;
	}

	public void setAvgAdmixDisplay(String avgAdmixDisplay) {
		this.avgAdmixDisplay = avgAdmixDisplay;
	}

	public String getAvgTemperatureDisplay() {
		return avgTemperatureDisplay;
	}

	public void setAvgTemperatureDisplay(String avgTemperatureDisplay) {
		this.avgTemperatureDisplay = avgTemperatureDisplay;
	}

	public float getStoringRate() {
		return storingRate;
	}

	public void setStoringRate(float storingRate) {
		this.storingRate = storingRate;
	}

	public String getTotalCostDisplay() {
		return totalCostDisplay;
	}

	public void setTotalCostDisplay(String totalCostDisplay) {
		this.totalCostDisplay = totalCostDisplay;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getStoringRateDisplay() {
		return storingRateDisplay;
	}

	public void setStoringRateDisplay(String storingRateDisplay) {
		this.storingRateDisplay = storingRateDisplay;
	}

	public int getTotalDays() {
		return totalDays;
	}

	public void setTotalDays(int totalDays) {
		this.totalDays = totalDays;
	}

	public float getTotalDayTonnage() {
		return totalDayTonnage;
	}

	public void setTotalDayTonnage(float totalDayTonnage) {
		setTotalDayTonnageDisplay(DisplayUtils.formatDayTonnage(totalDayTonnage, true));
		this.totalDayTonnage = totalDayTonnage;
	}

	public String getFromDateDisplay() {
		return fromDateDisplay;
	}

	public void setFromDateDisplay(String fromDateDisplay) {
		this.fromDateDisplay = fromDateDisplay;
	}

	public String getToDateDisplay() {
		return toDateDisplay;
	}

	public void setToDateDisplay(String toDateDisplay) {
		this.toDateDisplay = toDateDisplay;
	}

	public String getTotalDayTonnageDisplay() {
		return totalDayTonnageDisplay;
	}

	public void setTotalDayTonnageDisplay(String totalDayTonnageDisplay) {
		this.totalDayTonnageDisplay = totalDayTonnageDisplay;
	}

	public String getRateTonneDayDisplay() {
		return rateTonneDayDisplay;
	}

	public void setRateTonneDayDisplay(String rateTonneDayDisplay) {
		this.rateTonneDayDisplay = rateTonneDayDisplay;
	}

}
