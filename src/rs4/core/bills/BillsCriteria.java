package rs4.core.bills;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rs4.core.search.TransactionType;
import rs4.database.dao.model.RSCustomer;
import rs4.database.dao.model.RSProduct;
import rs4.database.dao.model.RSStockYear;
import rs4.utils.DataUtils;
import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;

public class BillsCriteria {

	private int stockYearId;

	private BillReportType reportType;
	private String[] transactionTypes;
	private String transactionsNo;
	private String customerCode;
	private String productCode;
	private String dateFrom;
	private String dateTo;
	private float storingRate;
	private String storingRateDisplay;

	private List<BillReportType> reportTypes;
	private List<RSCustomer> customers;
	private List<RSProduct> products;

	public enum BillReportType {
		Standard, Storage
	}

	public BillsCriteria() {
		RSStockYear rsStockYear = DataUtils.getDefaultStockYear();
		setStockYearId(rsStockYear.getId());
		setReportType(BillReportType.Standard);
		setTransactionTypes(new String[] { TransactionType.IN.toString(), TransactionType.OUT.toString() });
		setProductCode(DisplayUtils.KEYWORD_ALL);
		setDefaultDates(rsStockYear);
		setReportTypes();
	}

	private void setReportTypes() {
		reportTypes = new ArrayList<BillReportType>();
		for (BillReportType type : BillReportType.values()) {
			reportTypes.add(type);
		}
	}

	public void setDefaultDates(RSStockYear rsStockYear) {
		Date stockDateFrom = rsStockYear.getDateFrom();
		setDateFrom(DateUtils.formatDate(stockDateFrom));
		Date stockDateTo = rsStockYear.getDateTo();
		Date dateToday = new Date();
		if (dateToday.after(stockDateTo)) {
			setDateTo(DateUtils.formatDate(stockDateTo));
		} else {
			setDateTo(DateUtils.getDateToday());
		}
	}

	public List<RSCustomer> getCustomers() {
		return customers;
	}

	public List<RSProduct> getProducts() {
		return products;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customer) {
		this.customerCode = customer;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String product) {
		this.productCode = product;
	}

	public int getStockYearId() {
		return stockYearId;
	}

	public void setStockYearId(int stockYearId) {
		this.stockYearId = stockYearId;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getTransactionsNo() {
		return transactionsNo;
	}

	public void setTransactionsNo(String transactionsNo) {
		this.transactionsNo = transactionsNo;
	}

	public BillReportType getReportType() {
		return reportType;
	}

	public void setReportType(BillReportType reportType) {
		this.reportType = reportType;
	}

	public void setReportType(String reportType) {
		if (reportType.equals(BillReportType.Standard.toString())) {
			this.reportType = BillReportType.Standard;
		}
		if (reportType.equals(BillReportType.Storage.toString())) {
			this.reportType = BillReportType.Storage;
		}
	}

	public void setCustomers(List<RSCustomer> customers) {
		this.customers = customers;
	}

	public void setProducts(List<RSProduct> products) {
		this.products = products;
	}

	public List<BillReportType> getReportTypes() {
		return reportTypes;
	}

	public void setReportTypes(List<BillReportType> reportTypes) {
		this.reportTypes = reportTypes;
	}

	public String[] getTransactionTypes() {
		return transactionTypes;
	}

	public void setTransactionTypes(String[] transactionTypes) {
		this.transactionTypes = transactionTypes;
	}

	public float getStoringRate() {
		return storingRate;
	}

	public void setStoringRate(float storingRate) {
		setStoringRateDisplay(DisplayUtils.formatCurrencyDecimals(storingRate, false, 3));
		this.storingRate = storingRate;
	}

	public void setStoringRate(String storingRate) {
		this.storingRate = Float.parseFloat(storingRate);
		setStoringRateDisplay(storingRate);
	}

	public String getStoringRateDisplay() {
		return storingRateDisplay;
	}

	public void setStoringRateDisplay(String storingRateDisplay) {
		this.storingRateDisplay = storingRateDisplay;
	}

	@Override
	public String toString() {
		return DisplayUtils.buildToSting(Integer.toString(stockYearId), reportType.toString(), transactionsNo,
				customerCode, productCode, dateFrom, dateTo, storingRateDisplay);
	}

}
