package rs4.core.bills.form;

import java.util.List;

import rs4.utils.DisplayUtils;

public class StorageProductBillForm {

	private String product;
	private float totalCost;
	private float totalDayTonnage;
	private List<StorageBillForm> resultStorageBills;

	private String totalCostDisplay;
	private String totalDayTonnageDisplay;
	
	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public List<StorageBillForm> getResultStorageBills() {
		return resultStorageBills;
	}

	public void setResultStorageBills(List<StorageBillForm> resultStorageBills) {
		this.resultStorageBills = resultStorageBills;
	}

	public float getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(float totalCost) {
		setTotalCostDisplay(DisplayUtils.formatCurrency(totalCost, true));
		this.totalCost = totalCost;
	}

	public float getTotalDayTonnage() {
		return totalDayTonnage;
	}

	public void setTotalDayTonnage(float totalDayTonnage) {
		setTotalDayTonnageDisplay(DisplayUtils.formatDayTonnage(totalDayTonnage, true));
		this.totalDayTonnage = totalDayTonnage;
	}

	public String getTotalCostDisplay() {
		return totalCostDisplay;
	}

	public void setTotalCostDisplay(String totalCostDisplay) {
		this.totalCostDisplay = totalCostDisplay;
	}

	public String getTotalDayTonnageDisplay() {
		return totalDayTonnageDisplay;
	}

	public void setTotalDayTonnageDisplay(String totalDayTonnageDisplay) {
		this.totalDayTonnageDisplay = totalDayTonnageDisplay;
	}

}
