package rs4.core.bills.form;

import java.io.Serializable;

import rs4.database.dao.model.RSTransaction;
import rs4.utils.DisplayUtils;

public class StandardBillForm implements Serializable {

	private static final long serialVersionUID = 6013441365856045379L;

	private float intakeCost;
	private float dryingCost;
	private float totalCost;
	
	private String intakeCostDisplay;
	private String dryingCostDisplay;
	private String totalCostDisplay;
	
	private RSTransaction transaction;

	public StandardBillForm() {
	}

	public RSTransaction getTransaction() {
		return transaction;
	}

	public void setTransaction(RSTransaction transaction) {
		this.transaction = transaction;
	}

	public float getIntakeCost() {
		return intakeCost;
	}

	public void setIntakeCost(float intakeCost) {
		this.intakeCost = intakeCost;
		setIntakeCostDisplay(DisplayUtils.formatCurrencyZero(intakeCost, true, true));
	}

	public float getDryingCost() {
		return dryingCost;
	}

	public void setDryingCost(float dryingCost) {
		this.dryingCost = dryingCost;
		setDryingCostDisplay(DisplayUtils.formatCurrencyZero(dryingCost, true, true));
	}

	public float getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(float totalCost) {
		this.totalCost = totalCost;
		setTotalCostDisplay(DisplayUtils.formatCurrencyZero(totalCost, true, true));
	}

	public String getIntakeCostDisplay() {
		return intakeCostDisplay;
	}

	public void setIntakeCostDisplay(String intakeCostDisplay) {
		this.intakeCostDisplay = intakeCostDisplay;
	}

	public String getDryingCostDisplay() {
		return dryingCostDisplay;
	}

	public void setDryingCostDisplay(String dryingCostDisplay) {
		this.dryingCostDisplay = dryingCostDisplay;
	}

	public String getTotalCostDisplay() {
		return totalCostDisplay;
	}

	public void setTotalCostDisplay(String totalCostDisplay) {
		this.totalCostDisplay = totalCostDisplay;
	}

}
