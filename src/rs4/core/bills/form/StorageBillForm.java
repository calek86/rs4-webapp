package rs4.core.bills.form;

import java.util.Date;

import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;

public class StorageBillForm {

	private int dayCount;
	private Date date;
	private float entWtStock;
	private float entWtChange;
	private float dayTonnage;
	private float dayCost;
	
	private String dayCountDisplay;
	private String dateDisplay;
	private String entWtChangeDisplay;
	private String entWtStockDisplay;
	private String dayTonnageDisplay;
	private String dayCostDisplay;
	
	public int getDayCount() {
		return dayCount;
	}
	public void setDayCount(int dayCount) {
		setDayCountDisplay(Integer.toString(dayCount));
		this.dayCount = dayCount;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		setDateDisplay(DateUtils.formatDate(date));
		this.date = date;
	}
	public float getEntWtChange() {
		return entWtChange;
	}
	public void setEntWtChange(float entWtChange) {
		setEntWtChangeDisplay(DisplayUtils.formatWeight(entWtChange, true));
		this.entWtChange = entWtChange;
	}
	public float getDayTonnage() {
		return dayTonnage;
	}
	public void setDayTonnage(float dayTonnage) {
		setDayTonnageDisplay(DisplayUtils.formatDayTonnage(dayTonnage, true));
		this.dayTonnage = dayTonnage;
	}
	public float getDayCost() {
		return dayCost;
	}
	public void setDayCost(float dayCost) {
		setDayCostDisplay(DisplayUtils.formatCurrency(dayCost, true));
		this.dayCost = dayCost;
	}
	public float getEntWtStock() {
		return entWtStock;
	}
	public void setEntWtStock(float entWtStock) {
		setEntWtStockDisplay(DisplayUtils.formatWeightZero(entWtStock, true, true));
		this.entWtStock = entWtStock;
	}
	public String getEntWtStockDisplay() {
		return entWtStockDisplay;
	}
	public void setEntWtStockDisplay(String entWtStockDisplay) {
		this.entWtStockDisplay = entWtStockDisplay;
	}
	public String getDayCountDisplay() {
		return dayCountDisplay;
	}
	public void setDayCountDisplay(String dayCountDisplay) {
		this.dayCountDisplay = dayCountDisplay;
	}
	public String getDateDisplay() {
		return dateDisplay;
	}
	public void setDateDisplay(String dateDisplay) {
		this.dateDisplay = dateDisplay;
	}
	public String getEntWtChangeDisplay() {
		return entWtChangeDisplay;
	}
	public void setEntWtChangeDisplay(String entWtChangeDisplay) {
		this.entWtChangeDisplay = entWtChangeDisplay;
	}
	public String getDayTonnageDisplay() {
		return dayTonnageDisplay;
	}
	public void setDayTonnageDisplay(String dayTonnageDisplay) {
		this.dayTonnageDisplay = dayTonnageDisplay;
	}
	public String getDayCostDisplay() {
		return dayCostDisplay;
	}
	public void setDayCostDisplay(String dayCostDisplay) {
		this.dayCostDisplay = dayCostDisplay;
	}
	
}
