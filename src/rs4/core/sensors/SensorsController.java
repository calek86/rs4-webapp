package rs4.core.sensors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import rs4.database.dao.model.RSUser;
import rs4.utils.DataUtils;

@Controller
@SessionAttributes
public class SensorsController {

	@RequestMapping(value = "/SearchSensors", params = "Search", method = RequestMethod.POST)
	public ModelAndView searchSensors(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		SensorsData sensorsData = SensorsData.getInstance(user);
		sensorsData.getSensorsCriteria().setDateFrom(request.getParameter("sensorsCriteria.dateFrom"));
		sensorsData.getSensorsCriteria().setDateTo(request.getParameter("sensorsCriteria.dateTo"));
		sensorsData.getSensorsCriteria().setDeviceId(request.getParameter("sensorsCriteria.deviceId"));
		sensorsData.searchSensors(user);
		return new ModelAndView("sensors", "command", sensorsData);
	}
	
}