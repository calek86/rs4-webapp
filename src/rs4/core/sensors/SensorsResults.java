package rs4.core.sensors;

import java.util.List;

import com.calkasoftware.rs5.plugin.rs4.model.Sample;

import rs4.core.login.AccessLevel;

public class SensorsResults {

	private String accessLevel;
	private List<Sample> resultSamples;
	private String errorMessage;

	public SensorsResults() {
		accessLevel = AccessLevel.READ.toString();
		setErrorMessage("0");
	}

	public String getAccessLevel() {
		return accessLevel;
	}

	public void setAccessLevel(String accessLevel) {
		this.accessLevel = accessLevel;
	}

	public List<Sample> getResultSamples() {
		return resultSamples;
	}

	public void setResultSamples(List<Sample> resultSamples) {
		this.resultSamples = resultSamples;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
}
