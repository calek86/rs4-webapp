package rs4.core.sensors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.calkasoftware.rs5.plugin.rs4.SensorDao;
import com.calkasoftware.rs5.plugin.rs4.model.Sample;

import rs4.database.dao.model.RSUser;
import rs4.utils.DataUtils;
import rs4.utils.DisplayUtils;
import rs4.utils.Logger;

public class SensorsData {

	private SensorDao sensorDao;

	private SensorsCriteria sensorsCriteria;
	private SensorsResults sensorsResults;

	private static Map<Integer, SensorsData> instances;
	private static Logger logger = Logger.getLogger(SensorsData.class.getName());

	public static final SensorsData getInstance(RSUser user) {
		if (user == null) {
			logger.error("Requesting data object without user");
			return new SensorsData();
		}
		if (instances == null) {
			instances = new HashMap<>();
		}
		if (!instances.containsKey(user.getId())) {
			instances.put(user.getId(), new SensorsData());
		}
		return instances.get(user.getId());
	}

	public static void reloadInstance() {
		instances = new HashMap<>();
	}

	private SensorsData() {
		sensorDao = SensorDao.getInstance();
		sensorsCriteria = new SensorsCriteria();
		sensorsResults = new SensorsResults();
		reloadCriteria();
		reloadDeviceId();
	}

	public void searchSensors(RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Search sensors @ by @ with criteria @", actionId, DisplayUtils.logUser(user),
				sensorsCriteria.toString());
		List<Sample> rsSamples = sensorDao.getSamplesByCriteria(sensorsCriteria);
		sensorsResults.setAccessLevel(DataUtils.requestAccess(user).toString());
		sensorsResults.setErrorMessage("0");
		sensorsResults.setResultSamples(rsSamples);
		reloadCriteria();
		logger.info("Search @ returned @ samples", actionId, DisplayUtils.logList(rsSamples));
	}

	public void reloadDeviceId() {
		if (sensorsCriteria.getSensors() != null && sensorsCriteria.getSensors().size() > 0) {
			sensorsCriteria.setDeviceId(sensorsCriteria.getSensors().get(0).getDeviceId());
		}
	}

	// TODO: Change DataUtils.ACCOUNT_ID to user.getRS5ClientId();
	public void reloadCriteria() {
		sensorsCriteria.setSensors(sensorDao.getSensorsByAccountId(DataUtils.ACCOUNT_ID));
	}

	public void reloadDates() {
		sensorsCriteria.setDefaultDates();
	}

	public SensorsCriteria getSensorsCriteria() {
		return sensorsCriteria;
	}

	public void setTransferCriteria(SensorsCriteria sensorsCriteria) {
		this.sensorsCriteria = sensorsCriteria;
	}

	public SensorsResults getSensorsResults() {
		return sensorsResults;
	}

	public void setSensorsResults(SensorsResults sensorsResults) {
		this.sensorsResults = sensorsResults;
	}

}
