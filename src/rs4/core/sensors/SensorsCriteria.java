package rs4.core.sensors;

import java.util.List;

import com.calkasoftware.rs5.plugin.rs4.model.Sensor;

import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;

public class SensorsCriteria {

	private String deviceId;
	private String dateFrom;
	private String dateTo;

	private List<Sensor> sensors;
	
	public SensorsCriteria() {
		setDefaultDates();
	}

	public void setDefaultDates() {
		setDateFrom(DateUtils.getDateWeekAgo());
		setDateTo(DateUtils.getDateToday());	
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public List<Sensor> getSensors() {
		return sensors;
	}

	public void setSensors(List<Sensor> sensors) {
		this.sensors = sensors;
	}

	@Override
	public String toString() {
		return DisplayUtils.buildToSting(deviceId, dateFrom, dateTo);
	}

}
