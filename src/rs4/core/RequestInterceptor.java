package rs4.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import rs4.database.dao.RSUserDao;
import rs4.database.dao.model.RSUser;
import rs4.utils.CookieUtils;
import rs4.utils.DataUtils;
import rs4.utils.Logger;

public class RequestInterceptor extends HandlerInterceptorAdapter {

	static private RSUserDao rsUserDao = RSUserDao.getInstance();;
	static Logger logger = Logger.getLogger(RequestInterceptor.class.getName());

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String requestUri = request.getRequestURI();
		if (checkRequest(requestUri, "login") || checkRequest(requestUri, "DoLogin")) {
			return true;
		}
		String userToken = CookieUtils.extractUserToken(request);
		RSUser user = rsUserDao.getRSUserByToken(userToken);
		if (DataUtils.validateToken(user, userToken)) {
			request.setAttribute(DataUtils.REQUEST_USER_ATTRIBUTE, user);
			return true;
		}
		if (requestUri.contains("style.css") || requestUri.contains("header1200.png")) {
			return true;
		}
		return false;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		if (modelAndView != null) {
			addHelloUser(request, modelAndView);
			selectMenuTab(request, modelAndView);
		}
	}

	private void addHelloUser(HttpServletRequest request, ModelAndView modelAndView) {
		RSUser user = DataUtils.requestUser(request);
		if (user != null) {
			modelAndView.addObject("firstName", user.getFirstName());
			modelAndView.addObject("lastName", user.getLastName());
		} else {
			modelAndView.addObject("firstName", new String());
			modelAndView.addObject("lastName", new String());
		}
	}

	private void selectMenuTab(HttpServletRequest request, ModelAndView modelAndView) {
		String requestUri = request.getRequestURI();
		if (StringUtils.isEmpty(requestUri)) {
			modelAndView.addObject("activeTab", 1);
			return;
		}
		if (checkRequest(requestUri, "search")) {
			modelAndView.addObject("activeTab", 1);
			return;
		}
		if (checkRequest(requestUri, "login")) {
			modelAndView.addObject("activeTab", 1);
			return;
		}
		if (checkRequest(requestUri, "DeleteTransaction")) {
			modelAndView.addObject("activeTab", 1);
			return;
		}
		if (checkRequest(requestUri, "beans")) {
			modelAndView.addObject("activeTab", 2);
			return;
		}
		if (checkRequest(requestUri, "SearchBeans")) {
			modelAndView.addObject("activeTab", 2);
			return;
		}
		if (checkRequest(requestUri, "EditBeansSample")) {
			modelAndView.addObject("activeTab", 2);
			return;
		}
		if (checkRequest(requestUri, "stock")) {
			modelAndView.addObject("activeTab", 3);
			return;
		}
		if (checkRequest(requestUri, "SearchStock")) {
			modelAndView.addObject("activeTab", 3);
			return;
		}
		if (checkRequest(requestUri, "bills")) {
			modelAndView.addObject("activeTab", 4);
			return;
		}
		if (checkRequest(requestUri, "SearchBills")) {
			modelAndView.addObject("activeTab", 4);
			return;
		}
		if (checkRequest(requestUri, "transfer")) {
			modelAndView.addObject("activeTab", 5);
			return;
		}
		if (checkRequest(requestUri, "TransferStock")) {
			modelAndView.addObject("activeTab", 5);
			return;
		}
		if (checkRequest(requestUri, "settings")) {
			modelAndView.addObject("activeTab", 6);
			return;
		}
		if (checkRequest(requestUri, "SettingsDeleteMoistureDeduction")) {
			modelAndView.addObject("activeTab", 6);
			return;
		}
		if (checkRequest(requestUri, "SettingsSaveDefaultYear")) {
			modelAndView.addObject("activeTab", 6);
			return;
		}
		if (checkRequest(requestUri, "SettingsAddDeduction")) {
			modelAndView.addObject("activeTab", 6);
			return;
		}
		if (checkRequest(requestUri, "DeleteOneMoistureDeduction")) {
			modelAndView.addObject("activeTab", 6);
			return;
		}
		if (checkRequest(requestUri, "EditBillRates")) {
			modelAndView.addObject("activeTab", 6);
			return;
		}
		if (checkRequest(requestUri, "SettingsAssignProduct")) {
			modelAndView.addObject("activeTab", 6);
			return;
		}
		if (checkRequest(requestUri, "SettingsUnassignProduct")) {
			modelAndView.addObject("activeTab", 6);
			return;
		}
		if (checkRequest(requestUri, "SettingsCreateCustomer")) {
			modelAndView.addObject("activeTab", 6);
			return;
		}
		if (checkRequest(requestUri, "sensors")) {
			modelAndView.addObject("activeTab", 7);
			return;
		}
		if (checkRequest(requestUri, "SearchSensors")) {
			modelAndView.addObject("activeTab", 7);
			return;
		}
	}

	private boolean checkRequest(final String requestUri, final String uri) {
		if (requestUri.contains(uri)) {
			return true;
		}
		return false;
	}

}
