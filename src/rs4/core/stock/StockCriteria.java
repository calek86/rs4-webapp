package rs4.core.stock;

import java.util.ArrayList;
import java.util.List;

import rs4.database.dao.model.RSCustomer;
import rs4.database.dao.model.RSProduct;
import rs4.database.dao.model.RSStockYear;
import rs4.utils.DataUtils;
import rs4.utils.DisplayUtils;

public class StockCriteria {

	private int stockYearId;

	private StockReportType reportType;
	private String customerCode;
	private String productCode;
	private StockGroupBy groupBy;

	private List<StockReportType> reportTypes;
	private List<RSCustomer> customers;
	private List<RSProduct> products;
	private List<StockGroupBy> groupByList;

	public enum StockReportType {
		Control, Detailed, Annual
	}

	public enum StockGroupBy {
		Customer, Product
	}

	public StockCriteria() {
		RSStockYear rsStockYear = DataUtils.getDefaultStockYear();
		setStockYearId(rsStockYear.getId());
		setReportType(StockReportType.Control.name());
		setCustomerCode(DisplayUtils.KEYWORD_ALL);
		setProductCode(DisplayUtils.KEYWORD_ALL);
		setGroupBy(StockGroupBy.Customer.toString());
		setReportTypes();
		setGroupByList();
	}

	public void setCustomers(List<RSCustomer> customers) {
		this.customers = customers;
	}

	public void setProducts(List<RSProduct> products) {
		this.products = products;
	}

	private void setReportTypes() {
		reportTypes = new ArrayList<StockReportType>();
		for (StockReportType s : StockReportType.values()) {
			reportTypes.add(s);
		}
	}

	private void setGroupByList() {
		groupByList = new ArrayList<StockGroupBy>();
		for (StockGroupBy s : StockGroupBy.values()) {
			groupByList.add(s);
		}
	}

	public List<RSCustomer> getCustomers() {
		return customers;
	}

	public List<RSProduct> getProducts() {
		return products;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public StockGroupBy getGroupBy() {
		return groupBy;
	}

	public void setGroupBy(String groupBy) {
		if (groupBy.equals(StockGroupBy.Customer.name())) {
			this.groupBy = StockGroupBy.Customer;
		}
		if (groupBy.equals(StockGroupBy.Product.name())) {
			this.groupBy = StockGroupBy.Product;
		}
	}

	public void setCustomerCode(String customer) {
		this.customerCode = customer;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String product) {
		this.productCode = product;
	}

	public int getStockYearId() {
		return stockYearId;
	}

	public void setStockYearId(int stockYearId) {
		this.stockYearId = stockYearId;
	}

	public StockReportType getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		if (reportType.equals(StockReportType.Control.name())) {
			this.reportType = StockReportType.Control;
		}
		if (reportType.equals(StockReportType.Detailed.name())) {
			this.reportType = StockReportType.Detailed;
		}
		if (reportType.equals(StockReportType.Annual.name())) {
			this.reportType = StockReportType.Annual;
		}
	}

	public List<StockReportType> getReportTypes() {
		return reportTypes;
	}

	public List<StockGroupBy> getGroupByList() {
		return groupByList;
	}

	public void setGroupByList(List<StockGroupBy> orderByList) {
		this.groupByList = orderByList;
	}

	public void setReportTypes(List<StockReportType> reportTypes) {
		this.reportTypes = reportTypes;
	}

	@Override
	public String toString() {
		return DisplayUtils.buildToSting(Integer.toString(stockYearId), reportType.toString(), customerCode,
				productCode, groupBy.toString());
	}
}
