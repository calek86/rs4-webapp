package rs4.core.stock.manager;

import java.util.List;

import org.springframework.stereotype.Service;

import rs4.database.dao.RSStockDao;
import rs4.database.dao.RSTransactionDao;
import rs4.database.dao.RSTransferDao;
import rs4.database.dao.model.RSStock;
import rs4.database.dao.model.RSTransaction;
import rs4.database.dao.model.RSTransfer;
import rs4.utils.Logger;

@Service("StockManager")
public class StockManager {

	private RSStockDao rsStockDao;
	private RSTransferDao rsTransferDao;
	private RSTransactionDao rsTransactionDao;
	private static final int ACCEPTABLE_CALCULATION_TIME = 5; // in seconds
	private static Logger logger = Logger.getLogger(StockManager.class.getName());

	public StockManager() {
		rsStockDao = RSStockDao.getInstance();
		rsTransferDao = RSTransferDao.getInstance();
		rsTransactionDao = RSTransactionDao.getInstance();
	}

	public synchronized void manageStock() {
		try {
			long startTime = System.currentTimeMillis();
			final List<RSTransfer> transfers = rsTransferDao.getRSTransfers();
			final List<RSTransaction> transactions = rsTransactionDao.getRSTransactions();
			final StockMap stockMap = calculateStock(transactions, transfers);
			updateStockDatabase(stockMap);
			monitorCalculationTime(startTime);
		} catch (Exception e) {
			logger.error("Unknown problem in stock manager");
			e.printStackTrace();
		}
	}

	public StockMap calculateStock(List<RSTransaction> transactions, List<RSTransfer> transfers) {
		final List<RSStock> rsStock = rsStockDao.getRSStocks();
		cleanExistingStock(rsStock);
		StockMap stockMap = new StockMap();
		stockMap.putStock(rsStock);
		stockMap.putTransactions(transactions);
		stockMap.putTransfers(transfers);
		return stockMap;
	}

	private void cleanExistingStock(List<RSStock> existingStock) {
		for (RSStock rsStock : existingStock) {
			rsStock.setTotalValue(0.0F);
		}
	}

	private void updateStockDatabase(StockMap stockMap) {
		for (RSStock rsStock : stockMap.getStockList()) {
			rsStockDao.insertOrUpdate(rsStock);
		}
	}

	private void monitorCalculationTime(long startTime) {
		long calculationTime = System.currentTimeMillis() - startTime;
		if (calculationTime > ACCEPTABLE_CALCULATION_TIME * 1000) {
			logger.warn("Managing whole stock took @ millis!", calculationTime);
		}
	}

}
