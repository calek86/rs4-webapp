package rs4.core.stock.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rs4.core.search.TransactionType;
import rs4.database.dao.model.RSStock;
import rs4.database.dao.model.RSTransaction;
import rs4.database.dao.model.RSTransfer;
import rs4.utils.DataUtils;

public class StockMap {

	/**
	 * Holds 3D stock map using coordinates: stockYearId, customerId, productId
	 */
	private Map<Integer, Map<Integer, Map<Integer, RSStock>>> stockMap = new HashMap<>();

	public void putTransactions(List<RSTransaction> transactions) {
		for (RSTransaction rsTransaction : transactions) {
			putTransaction(rsTransaction);
		}
	}

	public void putTransfers(List<RSTransfer> transfers) {
		for (RSTransfer rsTransfer : transfers) {
			putTransfer(rsTransfer);
		}
	}

	public void putTransaction(RSTransaction rsTransaction) {
		if (!isStockTransaction(rsTransaction)) {
			return;
		}
		int stockYearId = rsTransaction.getStockYearId();
		int customerId = rsTransaction.getCustomerId();
		int productId = rsTransaction.getProductId();
		float transactionEntWt = getStockEntWt(rsTransaction);
		if (containsStock(stockYearId, customerId, productId)) {
			RSStock rsStock = getStock(stockYearId, customerId, productId);
			rsStock.setTotalValue(rsStock.getTotalValue() + transactionEntWt);
			putStock(rsStock);
		} else {
			RSStock rsStock = RSStock.fromRSTransaction(rsTransaction);
			rsStock.setTotalValue(transactionEntWt);
			putStock(rsStock);
		}
	}

	public void putTransfer(RSTransfer rsTransfer) {
		if (containsStock(rsTransfer.getFromStockId()) && containsStock(rsTransfer.getToStockId())) {
			RSStock fromStock = getStock(rsTransfer.getFromStockId());
			RSStock toStock = getStock(rsTransfer.getToStockId());
			float transferEntWt = rsTransfer.getTransferValue();
			fromStock.setTotalValue(fromStock.getTotalValue() - transferEntWt);
			toStock.setTotalValue(toStock.getTotalValue() + transferEntWt);
		}
	}

	public void putStock(RSStock rsStock) {
		if (!stockMap.containsKey(rsStock.getStockYearId())) {
			Map<Integer, Map<Integer, RSStock>> customerProductMap = new HashMap<>();
			Map<Integer, RSStock> productMap = new HashMap<>();
			productMap.put(rsStock.getProductId(), rsStock);
			customerProductMap.put(rsStock.getCustomerId(), productMap);
			stockMap.put(rsStock.getStockYearId(), customerProductMap);
		} else {
			Map<Integer, Map<Integer, RSStock>> customerProductMap = stockMap.get(rsStock.getStockYearId());
			if (!customerProductMap.containsKey(rsStock.getCustomerId())) {
				Map<Integer, RSStock> productMap = new HashMap<>();
				productMap.put(rsStock.getProductId(), rsStock);
				customerProductMap.put(rsStock.getCustomerId(), productMap);
			} else {
				Map<Integer, RSStock> productMap = customerProductMap.get(rsStock.getCustomerId());
				productMap.put(rsStock.getProductId(), rsStock);
			}
		}
	}

	public void putStock(List<RSStock> stock) {
		for (RSStock rsStock : stock) {
			putStock(rsStock);
		}
	}

	public Boolean containsStock(Integer stockYearId, Integer customerId, Integer productId) {
		if (stockMap.containsKey(stockYearId)) {
			Map<Integer, Map<Integer, RSStock>> customerProductMap = stockMap.get(stockYearId);
			if (customerProductMap != null && customerProductMap.containsKey(customerId)) {
				Map<Integer, RSStock> productMap = customerProductMap.get(customerId);
				if (productMap != null && productMap.containsKey(productId)) {
					return true;
				}
			}

		}
		return false;
	}

	public Boolean containsStock(Integer stockId) {
		for (Map<Integer, Map<Integer, RSStock>> customerProductMap : stockMap.values()) {
			if (customerProductMap != null && customerProductMap.values() != null) {
				for (Map<Integer, RSStock> productMap : customerProductMap.values()) {
					if (productMap != null && productMap.values() != null) {
						for (RSStock rsStock : productMap.values()) {
							if (stockId.equals(rsStock.getId())) {
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

	public RSStock getStock(Integer stockYearId, Integer customerId, Integer productId) {
		if (stockMap.containsKey(stockYearId)) {
			Map<Integer, Map<Integer, RSStock>> customerProductMap = stockMap.get(stockYearId);
			if (customerProductMap != null && customerProductMap.containsKey(customerId)) {
				if (customerProductMap.containsKey(customerId)) {
					Map<Integer, RSStock> productMap = customerProductMap.get(customerId);
					if (productMap != null && productMap.containsKey(productId)) {
						return productMap.get(productId);
					}
				}
			}
		}
		return new RSStock();
	}

	public RSStock getStock(Integer stockId) {
		for (Map<Integer, Map<Integer, RSStock>> customerProductMap : stockMap.values()) {
			if (customerProductMap != null && customerProductMap.values() != null) {
				for (Map<Integer, RSStock> productMap : customerProductMap.values()) {
					if (productMap != null && productMap.values() != null) {
						for (RSStock rsStock : productMap.values()) {
							if (stockId.equals(rsStock.getId())) {
								return rsStock;
							}
						}
					}
				}
			}
		}
		return new RSStock();
	}

	public List<RSStock> getStockList() {
		List<RSStock> stockList = new ArrayList<>();
		for (Map<Integer, Map<Integer, RSStock>> customerProductMap : stockMap.values()) {
			if (customerProductMap != null && customerProductMap.values() != null) {
				for (Map<Integer, RSStock> productMap : customerProductMap.values()) {
					if (productMap != null && productMap.values() != null) {
						for (RSStock rsStock : productMap.values()) {
							stockList.add(rsStock);
						}
					}
				}
			}
		}
		return stockList;
	}

	private boolean isStockTransaction(RSTransaction rsTransaction) {
		if (rsTransaction.equalsType(TransactionType.IN) || rsTransaction.equalsType(TransactionType.OUT)) {
			return true;
		}
		return false;
	}

	private float getStockEntWt(RSTransaction rsTransaction) {
		float transactionEntWt = DataUtils.getStockEntWt(rsTransaction);
		if (rsTransaction.equalsType(TransactionType.OUT)) {
			transactionEntWt *= -1;
		}
		return transactionEntWt;
	}

}
