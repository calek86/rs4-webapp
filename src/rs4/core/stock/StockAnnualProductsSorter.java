package rs4.core.stock;

import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class StockAnnualProductsSorter {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <K extends Comparable, V> LinkedHashMap<K, V> sortByKeys(LinkedHashMap<K, V> map) {
		List<K> keys = new LinkedList<K>(map.keySet());
		Collections.sort(keys, (Comparator<? super K>) new Comparator<String>() {
			@Override
			public int compare(String first, String second) {
				Collator collator = Collator.getInstance(Locale.getDefault());
				return collator.compare(first, second);
			}
		});
		LinkedHashMap<K, V> sortedMap = new LinkedHashMap<K, V>();
		for (K key : keys) {
			sortedMap.put(key, map.get(key));
		}
		return sortedMap;
	}

}
