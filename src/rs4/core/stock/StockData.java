package rs4.core.stock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.util.CollectionUtils;

import rs4.core.search.SearchCriteria;
import rs4.core.search.SearchData;
import rs4.core.search.SearchResultsSummary;
import rs4.core.search.TransactionType;
import rs4.core.search.form.TransactionForm;
import rs4.core.stock.StockCriteria.StockGroupBy;
import rs4.core.stock.StockCriteria.StockReportType;
import rs4.core.stock.form.AnnualReportForm;
import rs4.core.stock.form.AnnualReportStockForm;
import rs4.core.stock.form.DetailedReportForm;
import rs4.database.dao.RSCustomerDao;
import rs4.database.dao.RSProductDao;
import rs4.database.dao.RSStockDao;
import rs4.database.dao.RSTransactionDao;
import rs4.database.dao.RSTransferDao;
import rs4.database.dao.model.RSStock;
import rs4.database.dao.model.RSTransaction;
import rs4.database.dao.model.RSTransfer;
import rs4.database.dao.model.RSUser;
import rs4.utils.DataUtils;
import rs4.utils.DateUtils;
import rs4.utils.DisplayUtils;
import rs4.utils.Logger;

public class StockData {

	private RSStockDao rsStockDao;
	private RSCustomerDao rsCustomerDao;
	private RSProductDao rsProductDao;
	private RSTransferDao rsTransferDao;
	private RSTransactionDao rsTransactionDao;

	private StockCriteria stockCriteria;
	private StockResults stockResults;

	private static Map<Integer, StockData> instances;
	private static Logger logger = Logger.getLogger(StockData.class.getName());

	public static final StockData getInstance(RSUser user) {
		if (user == null) {
			logger.error("Requesting data object without user");
			return new StockData();
		}
		if (instances == null) {
			instances = new HashMap<>();
		}
		if (!instances.containsKey(user.getId())) {
			instances.put(user.getId(), new StockData());
		}
		return instances.get(user.getId());
	}

	public static void reloadInstance() {
		instances = new HashMap<>();
	}

	private StockData() {
		rsStockDao = RSStockDao.getInstance();
		rsCustomerDao = RSCustomerDao.getInstance();
		rsProductDao = RSProductDao.getInstance();
		rsTransferDao = RSTransferDao.getInstance();
		rsTransactionDao = RSTransactionDao.getInstance();
		stockCriteria = new StockCriteria();
		stockResults = new StockResults();
		reloadCriteria();
	}

	private void reloadCriteria() {
		stockCriteria.setCustomers(rsCustomerDao.getRSCustomersFromStockYear(stockCriteria.getStockYearId()));
		stockCriteria.setProducts(rsProductDao.getRSProductsFromStockYear(stockCriteria.getStockYearId()));
	}

	public void searchStock(RSUser user) {
		stockResults.setReportType(stockCriteria.getReportType());
		stockResults.setReportDate(DateUtils.getDateTimeToday());
		if (stockCriteria.getReportType().equals(StockReportType.Control)) {
			generateControlReport(stockCriteria.getGroupBy(), user);
		} else if (stockCriteria.getReportType().equals(StockReportType.Detailed)) {
			generateDetailedReport(stockCriteria.getGroupBy(), user);
		} else if (stockCriteria.getReportType().equals(StockReportType.Annual)) {
			generateAnnualReport(user);
		}
		if (!CollectionUtils.isEmpty(stockResults.getResultStockRecords())) {
			stockResults.setStockYear(stockResults.getResultStockRecords().get(0).getStockYearDescription());
		}
		reloadCriteria();
	}

	private void generateAnnualReport(RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Search stock annual @ by @ with criteria @", actionId, DisplayUtils.logUser(user),
				stockCriteria.toString());
		List<RSTransaction> rsTransactions = rsTransactionDao
				.getRSTransactionsByStockYearId(stockCriteria.getStockYearId());
		LinkedHashMap<String, AnnualReportStockForm> map = new LinkedHashMap<String, AnnualReportStockForm>();
		for (RSTransaction rsTransaction : rsTransactions) {
			if (rsTransaction.equalsType(TransactionType.IN)) {
				float entWt = DataUtils.getStockEntWt(rsTransaction);
				if (map.containsKey(rsTransaction.getProduct())) {
					AnnualReportStockForm val = map.get(rsTransaction.getProduct());
					val.setEntIn(val.getEntIn() + entWt);
					map.put(rsTransaction.getProduct(), val);
				} else {
					AnnualReportStockForm val = new AnnualReportStockForm();
					val.setEntIn(entWt);
					map.put(rsTransaction.getProduct(), val);
				}
			}
			if (rsTransaction.equalsType(TransactionType.OUT)) {
				float entWt = DataUtils.getStockEntWt(rsTransaction);
				if (map.containsKey(rsTransaction.getProduct())) {
					AnnualReportStockForm val = map.get(rsTransaction.getProduct());
					val.setWeightOut(val.getWeightOut() - entWt);
					map.put(rsTransaction.getProduct(), val);
				} else {
					AnnualReportStockForm val = new AnnualReportStockForm();
					val.setWeightOut(-1 * entWt);
					map.put(rsTransaction.getProduct(), val);
				}
			}
		}
		StockAnnualProductsSorter s = new StockAnnualProductsSorter();
		map = s.sortByKeys(map);

		float totalIn = 0.0f;
		float totalOut = 0.0f;
		for (Entry<String, AnnualReportStockForm> entry : map.entrySet()) {
			AnnualReportStockForm value = entry.getValue();
			value.setStoreTotal(value.getEntIn() + value.getWeightOut());
			entry.setValue(value);
			totalIn += value.getEntIn();
			totalOut += value.getWeightOut();
		}

		AnnualReportForm form = new AnnualReportForm();
		form.setMap(map);
		form.setTotalIn(totalIn);
		form.setTotalOut(totalOut);
		form.setStoreTotal(totalIn + totalOut);
		stockResults.setStockAnnualReportForm(form);
		logger.info("Search @ returned results", actionId);
	}

	private void generateControlReport(StockGroupBy stockGroupBy, RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Search stock control @ by @ with criteria @", actionId, DisplayUtils.logUser(user),
				stockCriteria.toString());
		List<RSStock> rsStockRecords = rsStockDao.getRSStocksByCriteria(stockCriteria);
		if (CollectionUtils.isEmpty(rsStockRecords)) {
			return;
		}
		if (stockGroupBy.equals(StockGroupBy.Customer)) {
			rsStockRecords = pullDennesOnTop(rsStockRecords);
		}
		SearchResultsSummary summary = new SearchResultsSummary();
		summary.setTotalEntWt(DisplayUtils.formatWeightZero(getStockTotalValue(rsStockRecords), true, true));
		rsStockRecords = insertSumTotals(rsStockRecords, stockGroupBy);
		stockResults.setResultsSummary(summary);
		stockResults.setResultStockRecords(rsStockRecords);
		logger.info("Search @ returned @ results", actionId, DisplayUtils.logList(rsStockRecords));
	}

	private void generateDetailedReport(StockGroupBy stockGroupBy, RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Search stock detailed @ by @ with criteria @", actionId, DisplayUtils.logUser(user),
				stockCriteria.toString());
		ArrayList<DetailedReportForm> stockFormList = new ArrayList<DetailedReportForm>();
		List<RSStock> rsStockRecords = rsStockDao.getRSStocksByCriteria(stockCriteria);
		List<RSTransaction> allTransactions = new ArrayList<RSTransaction>();
		for (RSStock rsStock : rsStockRecords) {
			DetailedReportForm stockForm = new DetailedReportForm();
			stockForm.setCustomer(rsStock.getCustomerCode());
			stockForm.setProduct(rsStock.getProductCode());
			stockForm.setTotalValue(DisplayUtils.formatWeightZero(rsStock.getTotalValue(), true, true));
			stockForm.setId(rsStock.getId());
			SearchCriteria searchCriteria = new SearchCriteria();
			searchCriteria.setStockYearId(stockCriteria.getStockYearId());
			searchCriteria.setProductCode(rsStock.getProductCode());
			searchCriteria.setCustomerCode(rsStock.getCustomerCode());
			searchCriteria.setDateFrom(new String());
			searchCriteria.setDateTo(new String());
			List<RSTransaction> transactions = rsTransactionDao.getRSTransactionsByCriteria(searchCriteria);
			transactions = filterStockTransactions(transactions);
			List<RSTransfer> transfers = rsTransferDao.getRSTransfersByStockId(rsStock.getId());
			for (RSTransfer rsTransfer : transfers) {
				TransactionForm transferForm = new TransactionForm();
				transferForm.setTicketId(rsTransfer.getTicketId());
				if (rsStock.getId() == rsTransfer.getToStockId()) {
					transferForm
							.setTransactionType(DataUtils.TRANSFER_TICKET_SUFFIX + " " + TransactionType.IN.toString());
					RSStock fromStock = rsStockDao.getRSStockById(rsTransfer.getFromStockId());
					if (fromStock != null) {
						transferForm.setDestination("from " + fromStock.getCustomerCode());
					}
					transferForm.setEntWeight(rsTransfer.getTransferValueDisplay());
					transferForm.setEntWeightBeans(rsTransfer.getTransferValueDisplay());
				} else if (rsStock.getId() == rsTransfer.getFromStockId()) {
					transferForm.setTransactionType(
							DataUtils.TRANSFER_TICKET_SUFFIX + " " + TransactionType.OUT.toString());
					RSStock toStock = rsStockDao.getRSStockById(rsTransfer.getToStockId());
					if (toStock != null) {
						transferForm.setDestination("to " + toStock.getCustomerCode());
					}
					transferForm.setEntWeight("-" + rsTransfer.getTransferValueDisplay());
					transferForm.setEntWeightBeans("-" + rsTransfer.getTransferValueDisplay());
				}
				transferForm.setTransactionDate(rsTransfer.getTransferDateDisplay());
				stockForm.getTransactions().add(transferForm);
			}
			for (RSTransaction transaction : transactions) {
				TransactionForm transactionForm = transaction.toTransactionForm(true);
				stockForm.getTransactions().add(transactionForm);
			}
			Collections.sort(stockForm.getTransactions());
			stockFormList.add(stockForm);
			allTransactions.addAll(transactions);
		}
		SearchResultsSummary summary = SearchData.getSearchResultsSummary(allTransactions);
		summary.setTotalEntWt(DisplayUtils.formatWeightZero(getStockTotalValue(rsStockRecords), true, true));
		stockResults.setResultsSummary(summary);
		stockResults.setStockFormList(stockFormList);
		logger.info("Search @ returned @ results", actionId, DisplayUtils.logList(allTransactions));
	}

	private float getStockTotalValue(List<RSStock> rsStockRecords) {
		float totalValue = 0.0F;
		for (RSStock rsStock : rsStockRecords) {
			totalValue += rsStock.getTotalValue();
		}
		return totalValue;
	}

	private List<RSTransaction> filterStockTransactions(List<RSTransaction> transactions) {
		List<RSTransaction> stockTransactions = new ArrayList<RSTransaction>();
		for (RSTransaction rsTransaction : transactions) {
			if (rsTransaction.equalsType(TransactionType.IN) || rsTransaction.equalsType(TransactionType.OUT)) {
				stockTransactions.add(rsTransaction);
			}
		}
		return stockTransactions;
	}

	private List<RSStock> insertSumTotals(List<RSStock> rsStockRecords, StockGroupBy stockGroupBy) {
		float sumEntWt = 0.0F;
		int lastId = -1;
		RSStock sumSeparator = null;
		List<RSStock> rsStockRecordsSeparated = new ArrayList<RSStock>();
		for (int i = 0; i < rsStockRecords.size(); i++) {
			int idToCheck = rsStockRecords.get(i).getCustomerId();
			if (stockGroupBy.equals(StockGroupBy.Product)) {
				idToCheck = rsStockRecords.get(i).getProductId();
			}
			if (i > 0 && lastId != idToCheck) {
				sumSeparator = new RSStock();
				sumSeparator.setId(-1);
				sumSeparator.setTotalValue(sumEntWt);
				rsStockRecordsSeparated.add(sumSeparator);
				sumEntWt = 0.0F;
			}
			sumEntWt += rsStockRecords.get(i).getTotalValue();
			rsStockRecordsSeparated.add(rsStockRecords.get(i));
			lastId = idToCheck;
		}
		sumSeparator = new RSStock();
		sumSeparator.setId(-1);
		sumSeparator.setTotalValue(sumEntWt);
		rsStockRecordsSeparated.add(sumSeparator);
		sumEntWt = 0.0F;
		return rsStockRecordsSeparated;
	}

	private List<RSStock> pullDennesOnTop(List<RSStock> rsStockRecords) {
		LinkedList<RSStock> rsStockRecordsSorted = new LinkedList<RSStock>();
		for (Iterator<RSStock> iterator = rsStockRecords.iterator(); iterator.hasNext();) {
			RSStock rsStock = (RSStock) iterator.next();
			if (rsStock.isDenne()) {
				rsStockRecordsSorted.addLast(rsStock);
				iterator.remove();
			}
		}
		rsStockRecordsSorted.addAll(rsStockRecords);
		return rsStockRecordsSorted;
	}

	public StockCriteria getStockCriteria() {
		return stockCriteria;
	}

	public void setStockCriteria(StockCriteria stockCriteria) {
		this.stockCriteria = stockCriteria;
	}

	public StockResults getStockResults() {
		return stockResults;
	}

	public void setStockResults(StockResults stockResults) {
		this.stockResults = stockResults;
	}

}
