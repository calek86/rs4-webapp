package rs4.core.stock;

import java.util.ArrayList;
import java.util.List;

import rs4.core.search.SearchResultsSummary;
import rs4.core.stock.StockCriteria.StockReportType;
import rs4.core.stock.form.AnnualReportForm;
import rs4.core.stock.form.DetailedReportForm;
import rs4.database.dao.model.RSStock;

public class StockResults {

	private StockReportType reportType;
	private String reportDate;
	private String stockYear;
	private List<RSStock> resultStockRecords;
	private ArrayList<DetailedReportForm> stockFormList;
	private AnnualReportForm stockAnnualReportForm;
	private SearchResultsSummary resultsSummary;

	public StockResults() {
		reportType = StockReportType.Control;
	}

	public StockReportType getReportType() {
		return reportType;
	}

	public void setReportType(StockReportType reportType) {
		this.reportType = reportType;
	}

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public List<RSStock> getResultStockRecords() {
		return resultStockRecords;
	}

	public void setResultStockRecords(List<RSStock> resultStockRecords) {
		this.resultStockRecords = resultStockRecords;
	}

	public ArrayList<DetailedReportForm> getStockFormList() {
		return stockFormList;
	}

	public void setStockFormList(ArrayList<DetailedReportForm> stockFormList) {
		this.stockFormList = stockFormList;
	}

	public SearchResultsSummary getResultsSummary() {
		return resultsSummary;
	}

	public void setResultsSummary(SearchResultsSummary resultsSummary) {
		this.resultsSummary = resultsSummary;
	}

	public AnnualReportForm getStockAnnualReportForm() {
		return stockAnnualReportForm;
	}

	public void setStockAnnualReportForm(AnnualReportForm stockAnnualReportForm) {
		this.stockAnnualReportForm = stockAnnualReportForm;
	}

	public String getStockYear() {
		return stockYear;
	}

	public void setStockYear(String stockYear) {
		this.stockYear = stockYear;
	}

}
