package rs4.core.stock.form;

import java.io.Serializable;
import java.util.LinkedHashMap;

import rs4.utils.DisplayUtils;

public class AnnualReportForm implements Serializable {

	private static final long serialVersionUID = 1L;

	LinkedHashMap<String, AnnualReportStockForm> map = new LinkedHashMap<String, AnnualReportStockForm>();
	float totalIn;
	float totalOut;
	float storeTotal;
	String totalInDisplay;
	String totalOutDisplay;
	String storeTotalDisplay;
	
	public LinkedHashMap<String, AnnualReportStockForm> getMap() {
		return map;
	}

	public void setMap(LinkedHashMap<String, AnnualReportStockForm> map) {
		this.map = map;
	}

	public float getTotalIn() {
		return totalIn;
	}

	public void setTotalIn(float totalIn) {
		setTotalInDisplay(DisplayUtils.formatWeight(totalIn, true));
		this.totalIn = totalIn;
	}

	public float getTotalOut() {
		return totalOut;
	}

	public void setTotalOut(float totalOut) {
		setTotalOutDisplay(DisplayUtils.formatWeight(totalOut, true));
		this.totalOut = totalOut;
	}

	public float getStoreTotal() {
		return storeTotal;
	}

	public void setStoreTotal(float storeTotal) {
		setStoreTotalDisplay(DisplayUtils.formatWeight(storeTotal, true));
		this.storeTotal = storeTotal;
	}

	public String getTotalInDisplay() {
		return totalInDisplay;
	}

	public void setTotalInDisplay(String totalInDisplay) {
		this.totalInDisplay = totalInDisplay;
	}

	public String getTotalOutDisplay() {
		return totalOutDisplay;
	}

	public void setTotalOutDisplay(String totalOutDisplay) {
		this.totalOutDisplay = totalOutDisplay;
	}

	public String getStoreTotalDisplay() {
		return storeTotalDisplay;
	}

	public void setStoreTotalDisplay(String storeTotalDisplay) {
		this.storeTotalDisplay = storeTotalDisplay;
	}

}
