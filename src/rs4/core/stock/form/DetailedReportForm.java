package rs4.core.stock.form;

import java.util.ArrayList;

import rs4.core.search.form.TransactionForm;

public class DetailedReportForm {

	private ArrayList<TransactionForm> transactions;
	private String customer;
	private String product;
	private String totalValue;
	private int id;

	public DetailedReportForm() {
		transactions = new ArrayList<TransactionForm>();
	}

	public ArrayList<TransactionForm> getTransactions() {
		return transactions;
	}

	public void setTransactions(ArrayList<TransactionForm> transactions) {
		this.transactions = transactions;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(String totalValue) {
		this.totalValue = totalValue;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
