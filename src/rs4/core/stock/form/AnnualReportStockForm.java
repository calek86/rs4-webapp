package rs4.core.stock.form;

import rs4.utils.DisplayUtils;

public class AnnualReportStockForm {

	private float entIn = 0.0F;
	private float weightOut = 0.0F;
	private float storeTotal = 0.0F;

	private String entInDisplay;
	private String weightOutDisplay;
	private String storeTotalDisplay;
	
	public float getEntIn() {
		return entIn;
	}

	public void setEntIn(float entIn) {
		setEntInDisplay(DisplayUtils.formatWeight(entIn, true));
		this.entIn = entIn;
	}

	public float getWeightOut() {
		return weightOut;
	}

	public void setWeightOut(float weightOut) {
		setWeightOutDisplay(DisplayUtils.formatWeight(weightOut, true));
		this.weightOut = weightOut;
	}

	public float getStoreTotal() {
		return storeTotal;
	}

	public void setStoreTotal(float storeTotal) {
		setStoreTotalDisplay(DisplayUtils.formatWeight(storeTotal, true));
		this.storeTotal = storeTotal;
	}

	public String getEntInDisplay() {
		return entInDisplay;
	}

	public void setEntInDisplay(String entInDisplay) {
		this.entInDisplay = entInDisplay;
	}

	public String getWeightOutDisplay() {
		return weightOutDisplay;
	}

	public void setWeightOutDisplay(String weightOutDisplay) {
		this.weightOutDisplay = weightOutDisplay;
	}

	public String getStoreTotalDisplay() {
		return storeTotalDisplay;
	}

	public void setStoreTotalDisplay(String storeTotalDisplay) {
		this.storeTotalDisplay = storeTotalDisplay;
	}

}