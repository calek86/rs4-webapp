package rs4.core.stock;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import rs4.core.excel.AbstractExcelBuilder;
import rs4.core.excel.StockExcelBuilder;
import rs4.core.stock.form.AnnualReportForm;
import rs4.core.stock.form.DetailedReportForm;
import rs4.database.dao.model.RSStock;
import rs4.database.dao.model.RSUser;
import rs4.utils.DataUtils;

@Controller
@SessionAttributes
public class StockController {

	@RequestMapping(value = "/SearchStock", method = RequestMethod.POST)
	public ModelAndView searchStock(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		StockData stockData = StockData.getInstance(user);
		stockData.getStockCriteria().setReportType(request.getParameter("stockCriteria.reportType"));
		stockData.getStockCriteria().setGroupBy(request.getParameter("stockCriteria.groupBy"));
		stockData.getStockCriteria().setCustomerCode(request.getParameter("stockCriteria.customers"));
		stockData.getStockCriteria().setProductCode(request.getParameter("stockCriteria.products"));
		stockData.searchStock(user);
		return new ModelAndView("stock", "command", stockData);
	}

	@RequestMapping(value = "/SearchStock/controlExcel", method = RequestMethod.GET)
	public ModelAndView downloadStockControlExcel(HttpServletRequest request) {
		StockData stockData = StockData.getInstance(DataUtils.requestUser(request));
		List<RSStock> stockList = stockData.getStockResults().getResultStockRecords();
		ModelAndView model = new ModelAndView("stockExcelView");
		model.addObject(AbstractExcelBuilder.EXCEL_TYPE, StockExcelBuilder.EXCEL_TYPE_STOCK_CONTROL);
		model.addObject(AbstractExcelBuilder.EXCEL_DATA, stockList);
		model.addObject(AbstractExcelBuilder.EXCEL_FILENAME, "stockControl" + AbstractExcelBuilder.getFileNameDatePrefix());
		model.addObject(AbstractExcelBuilder.EXCEL_SUMMARY_DATA, stockData.getStockResults().getResultsSummary());
		return model;
	}

	@RequestMapping(value = "SearchStock/detailedExcel", method = RequestMethod.GET)
	public ModelAndView downloadStockDetailedExcel(HttpServletRequest request) {
		StockData stockData = StockData.getInstance(DataUtils.requestUser(request));
		List<DetailedReportForm> stockFormList = stockData.getStockResults().getStockFormList();
		ModelAndView model = new ModelAndView("stockExcelView");
		model.addObject(AbstractExcelBuilder.EXCEL_TYPE, StockExcelBuilder.EXCEL_TYPE_STOCK_DETAILED);
		model.addObject(AbstractExcelBuilder.EXCEL_DATA, stockFormList);
		model.addObject(AbstractExcelBuilder.EXCEL_FILENAME, "stockDetailed" + AbstractExcelBuilder.getFileNameDatePrefix());
		model.addObject(AbstractExcelBuilder.EXCEL_SUMMARY_DATA, stockData.getStockResults().getResultsSummary());
		return model;
	}

	@RequestMapping(value = "SearchStock/annualExcel", method = RequestMethod.GET)
	public ModelAndView downloadStockAnnualExcel(HttpServletRequest request) {
		StockData stockData = StockData.getInstance(DataUtils.requestUser(request));
		AnnualReportForm stockAnnualForm = stockData.getStockResults().getStockAnnualReportForm();
		ModelAndView model = new ModelAndView("stockExcelView");
		model.addObject(AbstractExcelBuilder.EXCEL_TYPE, StockExcelBuilder.EXCEL_TYPE_STOCK_ANNUAL);
		model.addObject(AbstractExcelBuilder.EXCEL_DATA, stockAnnualForm);
		model.addObject(AbstractExcelBuilder.EXCEL_FILENAME, "stockAnnual" + AbstractExcelBuilder.getFileNameDatePrefix());
		return model;
	}

}