package rs4.core.settings;

import java.util.List;

import rs4.core.login.AccessLevel;
import rs4.database.dao.model.RSDeductionGroup;

public class SettingsResults {

	private String accessLevel;
	private String errorMessage;
	private String resultMessage;
	private List<RSDeductionGroup> resultDeductionGroups;

	public SettingsResults() {
		setErrorMessage("0");
		setResultMessage("0");
		accessLevel = AccessLevel.READ.toString();
	}

	public String getAccessLevel() {
		return accessLevel;
	}

	public void setAccessLevel(String accessLevel) {
		this.accessLevel = accessLevel;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
		this.resultMessage="0";
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
		this.errorMessage="0";
	}

	public List<RSDeductionGroup> getResultDeductionGroups() {
		return resultDeductionGroups;
	}

	public void setResultDeductionGroups(List<RSDeductionGroup> resultDeductionGroups) {
		this.resultDeductionGroups = resultDeductionGroups;
	}
	
}
