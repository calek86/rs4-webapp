package rs4.core.settings;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import rs4.core.settings.form.DefaultBillRatesForm;
import rs4.database.dao.model.RSUser;
import rs4.utils.DataUtils;

@Controller
@SessionAttributes
public class SettingsController {

	@RequestMapping(value = "/SettingsSaveDefaultYear", method = RequestMethod.POST)
	public ModelAndView saveDefaultYear(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		SettingsData settingsData = SettingsData.getInstance(user);
		settingsData.getSettingsCriteria()
				.setStockYearId(Integer.parseInt(request.getParameter("settingsCriteria.stockYears")));
		settingsData.saveDefaultYear(user);
		return new ModelAndView("settings", "command", settingsData);
	}

	@RequestMapping(value = "/EditBillRates", method = RequestMethod.POST)
	public ModelAndView editBillRatesSaveButton(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		SettingsData settingsData = SettingsData.getInstance(user);
		DefaultBillRatesForm editBillRatesForm = settingsData.getSettingsCriteria().getBillRates();
		editBillRatesForm.setStoringRate(request.getParameter("settingsCriteria.billRates.storingRate"));
		settingsData.updateBillRates(user);
		return new ModelAndView("settings", "command", settingsData);
	}
	
	@RequestMapping(value = "/SettingsAddDeduction", method = RequestMethod.POST)
	public ModelAndView addMoistureDeduction(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		SettingsData settingsData = SettingsData.getInstance(user);
		settingsData.getSettingsCriteria().setDeductionGroupCode(request.getParameter("settingsCriteria.deductionGroups"));
		settingsData.getSettingsCriteria().setMoistureFrom(request.getParameter("settingsCriteria.moistureFrom"));
		settingsData.getSettingsCriteria().setMoistureTo(request.getParameter("settingsCriteria.moistureTo"));
		settingsData.getSettingsCriteria().setMoistureDeduction(request.getParameter("settingsCriteria.moistureDeduction"));
		settingsData.getSettingsCriteria().setIntakeCost(request.getParameter("settingsCriteria.intakeCost"));
		settingsData.getSettingsCriteria().setDryingCost(request.getParameter("settingsCriteria.dryingCost"));
		settingsData.addDeduction(user);
		return new ModelAndView("settings", "command", settingsData);
	}

	@RequestMapping(value = "/DeleteOneMoistureDeduction", method = RequestMethod.POST)
	public ModelAndView deleteOneMoistureDeduction(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		SettingsData settingsData = SettingsData.getInstance(user);
		int idToDelete = Integer.valueOf(request.getParameter("idToDelete"));
		settingsData.getSettingsCriteria().setIdOfMoistureDeductionToDelete(idToDelete);
		settingsData.deleteMoistureDeductionById(user);
		return new ModelAndView("settings", "command", settingsData);
	}

	@RequestMapping(value = "/SettingsAssignProduct", method = RequestMethod.POST)
	public ModelAndView assignProduct(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		SettingsData settingsData = SettingsData.getInstance(user);
		settingsData.getSettingsCriteria().setProductWithoutDeductionCode(request.getParameter("settingsCriteria.productsWithoutDeductions"));
		settingsData.getSettingsCriteria().setDeductionGroupCode(request.getParameter("settingsCriteria.deductionGroups"));
		settingsData.assignProductToDeductionGroup(user);
		return new ModelAndView("settings", "command", settingsData);
	}

	@RequestMapping(value = "/SettingsUnassignProduct", method = RequestMethod.POST)
	public ModelAndView unassignProduct(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		SettingsData settingsData = SettingsData.getInstance(user);
		int deductionGroupCount = Integer.valueOf(request.getParameter("deductionGroupCount"));
		int deductionGroupIdProductIsUnssignedFrom = Integer.valueOf(request.getParameter("deductionGroupId"));
		String deductionGroupAccessParam = "settingsResults.resultDeductionGroups["+deductionGroupCount+"].products";
		settingsData.getSettingsCriteria().setProductToUnassignCode(request.getParameter(deductionGroupAccessParam));
		settingsData.getSettingsCriteria().setDeductionGroupIdProductIsUnssignedFrom(deductionGroupIdProductIsUnssignedFrom);
		settingsData.unassignProductFromDeductionGroup(user);
		return new ModelAndView("settings", "command", settingsData);
	}
	
	@RequestMapping(value = "/SettingsCreateCustomer", method = RequestMethod.POST)
	public ModelAndView createNewCustomer(HttpServletRequest request) {
		RSUser user = DataUtils.requestUser(request);
		SettingsData settingsData = SettingsData.getInstance(user);
		settingsData.getSettingsCriteria().setNewCustomerName(request.getParameter("settingsCriteria.newCustomerName"));
		settingsData.getSettingsCriteria().setNewCustomerAddress(request.getParameter("settingsCriteria.newCustomerAddress"));
		settingsData.getSettingsCriteria().setNewCustomerDescription(request.getParameter("settingsCriteria.newCustomerDescription"));
		settingsData.createCustomer(user);
		return new ModelAndView("settings", "command", settingsData);
	}

}