package rs4.core.settings;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import rs4.core.beans.BeansData;
import rs4.core.bills.BillsData;
import rs4.core.search.SearchData;
import rs4.core.search.TransactionType;
import rs4.core.settings.form.DefaultBillRatesForm;
import rs4.core.stock.StockData;
import rs4.core.transfer.TransferData;
import rs4.database.dao.RSCustomerDao;
import rs4.database.dao.RSDeductionDao;
import rs4.database.dao.RSDeductionGroupDao;
import rs4.database.dao.RSDeductionProductDao;
import rs4.database.dao.RSProductDao;
import rs4.database.dao.RSSettingsDao;
import rs4.database.dao.RSStockYearDao;
import rs4.database.dao.RSTransactionDao;
import rs4.database.dao.model.RSCustomer;
import rs4.database.dao.model.RSDeduction;
import rs4.database.dao.model.RSDeductionGroup;
import rs4.database.dao.model.RSDeductionProduct;
import rs4.database.dao.model.RSProduct;
import rs4.database.dao.model.RSStockYear;
import rs4.database.dao.model.RSTransaction;
import rs4.database.dao.model.RSUser;
import rs4.utils.DataUtils;
import rs4.utils.DisplayUtils;
import rs4.utils.Logger;

public class SettingsData {

	private RSSettingsDao rsSettingsDao;
	private RSProductDao rsProductDao;
	private RSCustomerDao rsCustomerDao;
	private RSDeductionGroupDao rsDeductionGroupDao;
	private RSDeductionProductDao rsDeductionProductDao;
	private RSStockYearDao rsStockYearDao;
	private RSTransactionDao rsTransactionDao;
	private RSDeductionDao rsDeductionDao;

	private SettingsCriteria settingsCriteria;
	private SettingsResults settingsResults;

	private static Map<Integer, SettingsData> instances;
	private static Logger logger = Logger.getLogger(SettingsData.class.getName());

	public static final SettingsData getInstance(RSUser user) {
		if (user == null) {
			logger.error("Requesting data object without user");
			return new SettingsData();
		}
		if (instances == null) {
			instances = new HashMap<>();
		}
		if (!instances.containsKey(user.getId())) {
			instances.put(user.getId(), new SettingsData());
		}
		return instances.get(user.getId());
	}

	private SettingsData() {
		rsSettingsDao = RSSettingsDao.getInstance();
		rsProductDao = RSProductDao.getInstance();
		rsCustomerDao = RSCustomerDao.getInstance();
		rsDeductionGroupDao = RSDeductionGroupDao.getInstance();
		rsDeductionProductDao = RSDeductionProductDao.getInstance();
		rsStockYearDao = RSStockYearDao.getInstance();
		rsDeductionDao = RSDeductionDao.getInstance();
		rsTransactionDao = RSTransactionDao.getInstance();
		settingsCriteria = new SettingsCriteria();
		settingsCriteria.setStockYears(rsStockYearDao.getRSStockYears());
		List<RSDeductionGroup> allDeductionGroups = rsDeductionGroupDao.getRSDeductionGroups();
		settingsCriteria.setDeductionGroups(allDeductionGroups);
		settingsCriteria.setProductsWithoutDeductions(new ArrayList<RSProduct>());
		if (!CollectionUtils.isEmpty(settingsCriteria.getDeductionGroups())) {
			settingsCriteria.setDeductionGroupCode(settingsCriteria.getDeductionGroups().get(0).getCode());
		}
		settingsResults = new SettingsResults();
	}

	private void reloadCriteria() {
		settingsCriteria.setMoistureFrom(new String());
		settingsCriteria.setMoistureTo(new String());
		settingsCriteria.setMoistureDeduction(new String());
		settingsCriteria.setIntakeCost(new String());
		settingsCriteria.setDryingCost(new String());
		settingsCriteria.setNewCustomerName(new String());
		settingsCriteria.setNewCustomerAddress(new String());
		settingsCriteria.setNewCustomerDescription(new String());
	}

	public void searchSettings(RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Search settings @ by @ with criteria @", actionId, DisplayUtils.logUser(user),
				settingsCriteria.toString());
		settingsResults.setResultMessage("0");
		settingsResults.setAccessLevel(DataUtils.requestAccess(user).toString());
		reloadBillRates(actionId);
		reloadDeductionGroups(actionId);
		reloadProductDeductions();
	}

	public void saveDefaultYear(RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Update year @ by @ with criteria @", actionId, DisplayUtils.logUser(user),
				settingsCriteria.toString());
		settingsResults.setAccessLevel(DataUtils.requestAccess(user).toString());
		rsSettingsDao.updateIntSettingValueByKey(RSSettingsDao.DEFAULT_STOCK_YEAR_ID,
				settingsCriteria.getStockYearId());
		RSStockYear rsStockYear = DataUtils.getDefaultStockYear();
		settingsCriteria.setStockYearId(rsStockYear.getId());
		settingsCriteria.setStockYearDescription(rsStockYear.getDescription());
		BeansData.reloadInstance();
		BillsData.reloadInstance();
		SearchData.reloadInstance();
		StockData.reloadInstance();
		TransferData.reloadInstance();
		settingsResults.setResultMessage("Succesfully changed default stock year");
	}

	public void updateBillRates(RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Update bills @ by @ with criteria @", actionId, DisplayUtils.logUser(user),
				settingsCriteria.toString());
		settingsResults.setAccessLevel(DataUtils.requestAccess(user).toString());
		String storingRate = settingsCriteria.getBillRates().getStoringRate();
		Float storingRateFloat = 0.0F;
		try {
			storingRateFloat = Float.parseFloat(storingRate);
			if (storingRateFloat < 0) {
				settingsResults.setErrorMessage("Please insert a positive number in \"Storing Rate\" field");
				return;
			}
		} catch (NumberFormatException e) {
			settingsResults.setErrorMessage("Please insert correct number in \"Storing Rate\" field");
			return;
		}
		String storingRateString = DisplayUtils.formatCurrencyDecimals(storingRateFloat.floatValue(), false, 3);
		rsSettingsDao.updateStringSettingValueByKey(RSSettingsDao.STORING_RATE, storingRateString);
		BillsData.reloadInstance();
		reloadBillRates(actionId);
		settingsResults.setResultMessage("Succesfully updated bill rates");
	}

	public void addDeduction(RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Add moisture @ by @ with criteria @", actionId, DisplayUtils.logUser(user),
				settingsCriteria.toString());
		settingsResults.setAccessLevel(DataUtils.requestAccess(user).toString());
		RSDeduction rsDeduction = extractDeduction();
		if (rsDeduction.getId() < 0) {
			return;
		}
		RSDeductionGroup deductionGroup = extractDeductionGroup();
		rsDeduction.setGroupId(deductionGroup.getId());
		rsDeductionDao.insert(rsDeduction);
		updateTransactionsForDeductionGroup(deductionGroup, user);
		settingsResults.setResultMessage("Moisture deduction saved successfully");
		reloadCriteria();
		reloadDeductionGroups(actionId);
		reloadProductDeductions();
	}

	public void deleteMoistureDeductionById(RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Delete moisture @ by @ with criteria @", actionId, DisplayUtils.logUser(user),
				settingsCriteria.toString());
		settingsResults.setAccessLevel(DataUtils.requestAccess(user).toString());
		int deductionId = settingsCriteria.getIdOfMoistureDeductionToDelete();
		RSDeduction rsDeduction = rsDeductionDao.getRSDeductionById(deductionId);
		RSDeductionGroup deductionGroup = rsDeductionGroupDao.getRSDeductionGroupById(rsDeduction.getGroupId());
		rsDeductionDao.deleteMoistureDeductionById(deductionId);
		updateTransactionsForDeductionGroup(deductionGroup, user);
		settingsResults.setResultMessage("Moisture deduction deleted successfully");
		reloadDeductionGroups(actionId);
		reloadProductDeductions();
	}

	public void assignProductToDeductionGroup(RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Assign product @ by @ with criteria @", actionId, DisplayUtils.logUser(user),
				settingsCriteria.toString());
		settingsResults.setAccessLevel(DataUtils.requestAccess(user).toString());
		RSDeductionGroup deductionGroup = rsDeductionGroupDao
				.getRSDeductionGroupByCode(settingsCriteria.getDeductionGroupCode());
		RSProduct productWithoutDeduction = extractProductWithoutDeduction();
		if (!(deductionGroup.getId() > 0 && productWithoutDeduction.getId() > 0)) {
			logger.error("Assign product to deduction @ unknown problem", actionId);
			settingsResults.setErrorMessage("Problem assigning product to deduction group");
			return;
		}
		RSDeductionProduct rsDeductionProduct = new RSDeductionProduct();
		rsDeductionProduct.setGroupId(deductionGroup.getId());
		rsDeductionProduct.setProductId(productWithoutDeduction.getId());
		rsDeductionProductDao.insert(rsDeductionProduct);
		updateTransactionsForProductId(productWithoutDeduction.getId(), user);
		logger.info("Assign deductions @ successful", actionId);
		settingsResults.setResultMessage("Product successfully assigned with deduction group");
		reloadDeductionGroups(actionId);
		reloadProductDeductions();
	}

	public void unassignProductFromDeductionGroup(RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Unassign product @ by @ with criteria @", actionId, DisplayUtils.logUser(user),
				settingsCriteria.toString());
		settingsResults.setAccessLevel(DataUtils.requestAccess(user).toString());
		int groupId = settingsCriteria.getDeductionGroupIdProductIsUnssignedFrom();
		String productCode = settingsCriteria.getProductToUnassignCode();
		RSProduct rsProduct = rsProductDao.getRSProductByCode(productCode);
		rsDeductionProductDao.deleteRSDeductionProductByGroupIdAndProductId(groupId, rsProduct.getId());
		updateTransactionsForProductId(rsProduct.getId(), user);
		logger.info("Unassign deductions @ successful", actionId);
		settingsResults.setResultMessage("Product successfully unassigned from deduction group");
		reloadDeductionGroups(actionId);
		reloadProductDeductions();
	}

	public void createCustomer(RSUser user) {
		String actionId = DisplayUtils.getActionId();
		logger.info("Create customer @ by @ with criteria @", actionId, DisplayUtils.logUser(user),
				settingsCriteria.toString());
		settingsResults.setAccessLevel(DataUtils.requestAccess(user).toString());
		RSCustomer rsCustomer = extractCustomer();
		if (rsCustomer.getId() < 0) {
			reloadCriteria();
			return;
		}
		rsCustomerDao.insert(rsCustomer);
		settingsResults.setResultMessage("New customer created successfully");
		reloadCriteria();
		reloadDeductionGroups(actionId);
		reloadProductDeductions();
	}

	private int updateTransactionsForProductId(int productId, RSUser user) {
		List<RSTransaction> transactions = rsTransactionDao.getRSTransactionsByProductId(productId);
		int count = 0;
		for (RSTransaction rsTransaction : transactions) {
			DataUtils.putStockEntWt(rsTransaction);
			rsTransactionDao.update(rsTransaction);
			count++;
		}
		logger.info("Updated @ transactions after product @ assignment to deduction group", count, productId);
		return count;
	}

	private void updateTransactionsForDeductionGroup(RSDeductionGroup deductionGroup, RSUser user) {
		List<RSDeductionProduct> rsDeductionProducts = rsDeductionProductDao
				.getRSDeductionProductsByGroupId(deductionGroup.getId());
		int count = 0;
		for (RSDeductionProduct rsDeductionProduct : rsDeductionProducts) {
			count += updateTransactionsForProductId(rsDeductionProduct.getProductId(), user);
		}
		logger.info("Updated @ transactions after deduction change for deduction group @", count,
				deductionGroup.getCode());
	}

	private RSDeduction extractDeduction() {
		Float moistureFromFloat = extractFloat(settingsCriteria.getMoistureFrom());
		if (moistureFromFloat < 0) {
			return invalidDeduction("Moisture From");
		}
		Float moistureToFloat = extractFloat(settingsCriteria.getMoistureTo());
		if (moistureToFloat < 0) {
			return invalidDeduction("Moisture To");
		}
		if (moistureFromFloat > moistureToFloat) {
			settingsResults.setErrorMessage(
					"Value in \"Moisture From\" field cannot be greater than in \"Moisture To\" field");
			return new RSDeduction(-1);
		}
		Float moistureDeductionFloat = extractFloat(settingsCriteria.getMoistureDeduction());
		if (moistureDeductionFloat < 0) {
			return invalidDeduction("Moisture Deduction");
		}
		Float intakeCostFloat = extractFloat(settingsCriteria.getIntakeCost());
		if (intakeCostFloat < 0) {
			return invalidDeduction("Intake Cost");
		}
		Float dryingCostFloat = extractFloat(settingsCriteria.getDryingCost());
		if (dryingCostFloat < 0) {
			return invalidDeduction("Drying Cost");
		}
		RSDeduction rsDeduction = new RSDeduction();
		rsDeduction.setMoistureFrom(moistureFromFloat);
		rsDeduction.setMoistureTo(moistureToFloat);
		rsDeduction.setMoistureDeduction(moistureDeductionFloat);
		rsDeduction.setIntakeCost(intakeCostFloat);
		rsDeduction.setDryingCost(dryingCostFloat);
		return rsDeduction;
	}

	private RSCustomer extractCustomer() {
		String customerName = settingsCriteria.getNewCustomerName();
		if (StringUtils.isEmpty(customerName)) {
			settingsResults.setErrorMessage("Customer name cannot be empty");
			return invalidCustomer(customerName);
		}
		if (customerExists(customerName)) {
			settingsResults.setErrorMessage("Customer with name \"" + customerName + "\" already exists");
			return invalidCustomer(customerName);
		}
		RSCustomer rsCustomer = new RSCustomer();
		rsCustomer.setCode(customerName);
		rsCustomer.setWeighmanId(DataUtils.generateUUID());
		rsCustomer.setAddress(settingsCriteria.getNewCustomerAddress());
		rsCustomer.setDescription(settingsCriteria.getNewCustomerDescription());
		return rsCustomer;
	}

	private boolean customerExists(final String code) {
		RSCustomer rsCustomer = rsCustomerDao.getRSCustomerByCode(code);
		return rsCustomer != null;
	}

	private Float extractFloat(String stringFloat) {
		try {
			return Float.parseFloat(stringFloat);
		} catch (NumberFormatException e) {
			return -1.0F;
		}
	}

	private RSDeduction invalidDeduction(String wrongField) {
		settingsResults.setErrorMessage("Please insert a positive number in a \"" + wrongField + "\" field");
		return new RSDeduction(-1);
	}

	private RSCustomer invalidCustomer(String wrongName) {
		return new RSCustomer(-1);
	}

	private RSDeductionGroup extractDeductionGroup() {
		for (RSDeductionGroup deductionGroup : settingsCriteria.getDeductionGroups()) {
			if (settingsCriteria.getDeductionGroupCode().equals(deductionGroup.getCode())) {
				return deductionGroup;
			}
		}
		return new RSDeductionGroup();
	}

	private RSProduct extractProductWithoutDeduction() {
		for (RSProduct product : settingsCriteria.getProductsWithoutDeductions()) {
			if (settingsCriteria.getProductWithoutDeductionCode().equals(product.getCode())) {
				return product;
			}
		}
		return new RSProduct();
	}

	private void reloadBillRates(String actionId) {
		DefaultBillRatesForm billRates = new DefaultBillRatesForm();
		float storingRate = rsSettingsDao.getFloatSettingValueByKey(RSSettingsDao.STORING_RATE);
		billRates.setStoringRate(DisplayUtils.formatCurrencyDecimals(storingRate, false, 3));
		settingsCriteria.setBillRates(billRates);
		logger.info("Reload bills @ returned results", actionId);
	}

	private void reloadProductDeductions() {
		List<RSProduct> allProducts = rsProductDao.getRSProductsFromTransactionTypes(TransactionType.IN,
				TransactionType.OUT);
		List<RSDeductionProduct> deductionProducts = rsDeductionProductDao.getRSDeductionProducts();
		List<RSProduct> productsWithoutDeductions = new ArrayList<>();
		for (RSProduct rsProduct : allProducts) {
			boolean productHasDeduction = false;
			for (RSDeductionProduct rsDeductionProduct : deductionProducts) {
				if (rsProduct.getId() == rsDeductionProduct.getProductId()) {
					productHasDeduction = true;
					break;
				}
			}
			if (!productHasDeduction) {
				productsWithoutDeductions.add(rsProduct);
			}
		}
		List<RSDeductionGroup> allDeductionGroups = rsDeductionGroupDao.getRSDeductionGroups();
		settingsCriteria.setDeductionGroups(allDeductionGroups);
		settingsCriteria.setProductsWithoutDeductions(productsWithoutDeductions);
	}

	private void reloadDeductionGroups(String actionId) {
		List<RSDeductionGroup> rsDeductionGroups = rsDeductionGroupDao.getRSDeductionGroups();
		for (RSDeductionGroup deductionGroup : rsDeductionGroups) {
			List<RSDeduction> deductions = rsDeductionDao.getRSDeductionByGroupId(deductionGroup.getId());
			List<RSDeductionProduct> deductionProducts = rsDeductionProductDao
					.getRSDeductionProductsByGroupId(deductionGroup.getId());
			List<Integer> productIds = new ArrayList<Integer>();
			for (RSDeductionProduct deductionProduct : deductionProducts) {
				productIds.add(deductionProduct.getProductId());
			}
			List<RSProduct> products = rsProductDao.getRSProductsByIds(productIds);
			Collections.sort(deductions);
			deductionGroup.setDeductions(deductions);
			deductionGroup.setProducts(products);
		}
		settingsResults.setResultDeductionGroups(rsDeductionGroups);
		logger.info("Reload deductions groups @ returned @ results", actionId, DisplayUtils.logList(rsDeductionGroups));
	}

	public SettingsCriteria getSettingsCriteria() {
		return settingsCriteria;
	}

	public void setSettingsCriteria(SettingsCriteria settingsCriteria) {
		this.settingsCriteria = settingsCriteria;
	}

	public SettingsResults getSettingsResults() {
		return settingsResults;
	}

	public void setSettingsResults(SettingsResults settingsResults) {
		this.settingsResults = settingsResults;
	}

}
