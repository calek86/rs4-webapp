package rs4.core.settings.form;

import java.io.Serializable;

public class DefaultBillRatesForm implements Serializable {

	private static final long serialVersionUID = 4834458593223270052L;

	private String storingRate;

	public String getStoringRate() {
		return storingRate;
	}

	public void setStoringRate(String storingRate) {
		this.storingRate = storingRate;
	}

}
