package rs4.core.settings;

import java.util.List;

import rs4.core.settings.form.DefaultBillRatesForm;
import rs4.database.dao.model.RSDeductionGroup;
import rs4.database.dao.model.RSProduct;
import rs4.database.dao.model.RSStockYear;
import rs4.utils.DataUtils;
import rs4.utils.DisplayUtils;

public class SettingsCriteria {

	private int stockYearId;
	private String stockYearDescription;
	private List<RSStockYear> stockYears;
	private String productToUnassignCode;
	private int deductionGroupIdProductIsUnssignedFrom;
	private String productWithoutDeductionCode;
	private List<RSProduct> productsWithoutDeductions;
	private String deductionGroupCode;
	private List<RSDeductionGroup> deductionGroups;
	private String moistureFrom;
	private String moistureTo;
	private String moistureDeduction;
	private String intakeCost;
	private String dryingCost;
	private int idOfMoistureDeductionToDelete;
	private DefaultBillRatesForm billRates;
	private String newCustomerName;
	private String newCustomerAddress;
	private String newCustomerDescription;

	public SettingsCriteria() {
		RSStockYear rsStockYear = DataUtils.getDefaultStockYear();
		setStockYearId(rsStockYear.getId());
		setStockYearDescription(rsStockYear.getDescription());
		setMoistureFrom(new String());
		setMoistureTo(new String());
		setMoistureDeduction(new String());
		setIntakeCost(new String());
		setDryingCost(new String());
		setNewCustomerName(new String());
		setNewCustomerAddress(new String());
		setNewCustomerDescription(new String());
	}

	public List<RSStockYear> getStockYears() {
		return stockYears;
	}

	public void setStockYears(List<RSStockYear> stockYears) {
		this.stockYears = stockYears;
	}

	public int getStockYearId() {
		return stockYearId;
	}

	public void setStockYearId(int stockYearId) {
		this.stockYearId = stockYearId;
	}

	public String getStockYearDescription() {
		return stockYearDescription;
	}

	public void setStockYearDescription(String stockYearDescription) {
		this.stockYearDescription = stockYearDescription;
	}

	public String getMoistureFrom() {
		return moistureFrom;
	}

	public void setMoistureFrom(String moistureFrom) {
		this.moistureFrom = moistureFrom;
	}

	public String getMoistureTo() {
		return moistureTo;
	}

	public void setMoistureTo(String moistureTo) {
		this.moistureTo = moistureTo;
	}

	public String getMoistureDeduction() {
		return moistureDeduction;
	}

	public void setMoistureDeduction(String moistureDeduction) {
		this.moistureDeduction = moistureDeduction;
	}

	public String getIntakeCost() {
		return intakeCost;
	}

	public void setIntakeCost(String intakeCost) {
		this.intakeCost = intakeCost;
	}

	public String getDryingCost() {
		return dryingCost;
	}

	public void setDryingCost(String dryingCost) {
		this.dryingCost = dryingCost;
	}

	public int getIdOfMoistureDeductionToDelete() {
		return idOfMoistureDeductionToDelete;
	}

	public void setIdOfMoistureDeductionToDelete(int idOfMoistureDeductionToDelete) {
		this.idOfMoistureDeductionToDelete = idOfMoistureDeductionToDelete;
	}

	public DefaultBillRatesForm getBillRates() {
		return billRates;
	}

	public void setBillRates(DefaultBillRatesForm billRates) {
		this.billRates = billRates;
	}

	public String getProductToUnassignCode() {
		return productToUnassignCode;
	}

	public void setProductToUnassignCode(String productToUnassignCode) {
		this.productToUnassignCode = productToUnassignCode;
	}

	public int getDeductionGroupIdProductIsUnssignedFrom() {
		return deductionGroupIdProductIsUnssignedFrom;
	}

	public void setDeductionGroupIdProductIsUnssignedFrom(int deductionGroupIdProductIsUnssignedFrom) {
		this.deductionGroupIdProductIsUnssignedFrom = deductionGroupIdProductIsUnssignedFrom;
	}

	public String getProductWithoutDeductionCode() {
		return productWithoutDeductionCode;
	}

	public void setProductWithoutDeductionCode(String productWithoutDeductionCode) {
		this.productWithoutDeductionCode = productWithoutDeductionCode;
	}

	public String getDeductionGroupCode() {
		return deductionGroupCode;
	}

	public void setDeductionGroupCode(String deductionGroupCode) {
		this.deductionGroupCode = deductionGroupCode;
	}

	public List<RSDeductionGroup> getDeductionGroups() {
		return deductionGroups;
	}

	public void setDeductionGroups(List<RSDeductionGroup> deductionGroups) {
		this.deductionGroups = deductionGroups;
	}

	public List<RSProduct> getProductsWithoutDeductions() {
		return productsWithoutDeductions;
	}

	public void setProductsWithoutDeductions(List<RSProduct> productsWithoutDeductions) {
		this.productsWithoutDeductions = productsWithoutDeductions;
	}

	public String getNewCustomerName() {
		return newCustomerName;
	}

	public void setNewCustomerName(String newCustomerName) {
		this.newCustomerName = newCustomerName;
	}

	public String getNewCustomerAddress() {
		return newCustomerAddress;
	}

	public void setNewCustomerAddress(String newCustomerAddress) {
		this.newCustomerAddress = newCustomerAddress;
	}

	public String getNewCustomerDescription() {
		return newCustomerDescription;
	}

	public void setNewCustomerDescription(String newCustomerDescription) {
		this.newCustomerDescription = newCustomerDescription;
	}

	@Override
	public String toString() {
		return DisplayUtils.buildToSting(Integer.toString(stockYearId), moistureFrom, moistureTo, moistureDeduction,
				intakeCost, dryingCost, Integer.toString(idOfMoistureDeductionToDelete), productWithoutDeductionCode,
				deductionGroupCode, productToUnassignCode, Integer.toString(deductionGroupIdProductIsUnssignedFrom));
	}
}
