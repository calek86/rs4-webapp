package rs4.utils;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import rs4.core.search.SearchData;
import rs4.database.dao.model.RSUser;

public class DisplayUtils {

	public static final String KEYWORD_ALL = "All";

	private static final String DECIMAL_PLACEHOLDER = "$";
	private static final String VALUE_PLACEHOLDER = "#";
	private static final String UNIT_PLACEHOLDER = "@";

	private static final String WEIGHT_VALUE_FORMAT = "%,.$f";
	private static final String DAYTONNAGE_VALUE_FORMAT = "%,.$f";
	private static final String PERCENTAGE_VALUE_FORMAT = "%.$f";
	private static final String TEMPERATURE_VALUE_FORMAT = "%.$f";
	private static final String CURRENCY_VALUE_FORMAT = "%,.$f";
	private static final String SPECIFIC_WEIGHT_VALUE_FORMAT = "%.$f";
	private static final String PLAIN_NUMBER_VALUE_FORMAT = "%.$f";
	private static final String VOLTAGE_VALUE_FORMAT = "%.$f";
	
	private static final String WEIGHT_DISPLAY_FORMAT = "#@";
	private static final String DAYTONNAGE_DISPLAY_FORMAT = "#@";
	private static final String PERCENTAGE_DISPLAY_FORMAT = "#@";
	private static final String TEMPERATURE_DISPLAY_FORMAT = "#@";
	private static final String CURRENCY_DISPLAY_FORMAT = "@#";
	private static final String SPECIFIC_WEIGHT_DISPLAY_FORMAT = "#@";
	private static final String VOLTAGE_DISPLAY_FORMAT = "#@";
	
	private static final String WEIGHT_UNIT_FORMAT = "";
	private static final String DAYTONNAGE_UNIT_FORMAT = "dt";
	private static final String PERCENTAGE_UNIT_FORMAT = "%";
	private static final String TEMPERATURE_UNIT_FORMAT = "�C";
	private static final String CURRENCY_UNIT_FORMAT = "�";
	private static final String SPECIFIC_WEIGHT_UNIT_FORMAT = "kg/hl";
	private static final String VOLTAGE_UNIT_FORMAT = "V";
	
	private static final String UNKNOWN_USER_TEXT = "unknown user";
	private static final String NULL_COLLECTION_TEXT = "empty";

	static Logger logger = Logger.getLogger(SearchData.class.getName());

	private DisplayUtils() {
	}

	public static String formatWeight(final float weight, boolean showUnit) {
		return formatNumber(weight/1000, 3, showUnit, false, WEIGHT_VALUE_FORMAT, WEIGHT_DISPLAY_FORMAT, WEIGHT_UNIT_FORMAT);
	}

	public static String formatWeightZero(final float weight, boolean showUnit, boolean showZero) {
		return formatNumber(weight/1000, 3, showUnit, showZero, WEIGHT_VALUE_FORMAT, WEIGHT_DISPLAY_FORMAT,
				WEIGHT_UNIT_FORMAT);
	}

	public static String formatSpecificWeight(final float specificWight, boolean showUnit) {
		return formatNumber(specificWight, 1, showUnit, false, SPECIFIC_WEIGHT_VALUE_FORMAT, SPECIFIC_WEIGHT_DISPLAY_FORMAT,
				SPECIFIC_WEIGHT_UNIT_FORMAT);
	}	
	
	public static String formatDayTonnage(final float dayTonnage, boolean showUnit) {
		return formatNumber(dayTonnage/1000, 3, showUnit, true, DAYTONNAGE_VALUE_FORMAT, DAYTONNAGE_DISPLAY_FORMAT,
				DAYTONNAGE_UNIT_FORMAT);
	}

	public static String formatPercentage(final float percent, boolean showUnit) {
		return formatNumber(percent, 2, showUnit, false, PERCENTAGE_VALUE_FORMAT, PERCENTAGE_DISPLAY_FORMAT,
				PERCENTAGE_UNIT_FORMAT);
	}

	public static String formatPercentageZero(final float percent, boolean showUnit, boolean showZero) {
		return formatNumber(percent, 2, showUnit, showZero, PERCENTAGE_VALUE_FORMAT, PERCENTAGE_DISPLAY_FORMAT,
				PERCENTAGE_UNIT_FORMAT);
	}

	public static String formatPercentageDecimalsZero(final float percent, int decimals, boolean showUnit, boolean showZero) {
		return formatNumber(percent, decimals, showUnit, showZero, PERCENTAGE_VALUE_FORMAT, PERCENTAGE_DISPLAY_FORMAT,
				PERCENTAGE_UNIT_FORMAT);
	}
	
	public static String formatTemperature(final float temperature, boolean showUnit) {
		return formatNumber(temperature, 1, showUnit, false, TEMPERATURE_VALUE_FORMAT, TEMPERATURE_DISPLAY_FORMAT,
				TEMPERATURE_UNIT_FORMAT);
	}

	public static String formatTemperatureZero(final float temperature, boolean showUnit, boolean showZero) {
		return formatNumber(temperature, 1, showUnit, showZero, TEMPERATURE_VALUE_FORMAT, TEMPERATURE_DISPLAY_FORMAT,
				TEMPERATURE_UNIT_FORMAT);
	}
	
	public static String formatCurrency(final float money, boolean showUnit) {
		return formatNumber(money, 2, showUnit, true, CURRENCY_VALUE_FORMAT, CURRENCY_DISPLAY_FORMAT,
				CURRENCY_UNIT_FORMAT);
	}

	public static String formatCurrencyZero(final float money, boolean showUnit, boolean showZero) {
		return formatNumber(money, 2, showUnit, showZero, CURRENCY_VALUE_FORMAT, CURRENCY_DISPLAY_FORMAT,
				CURRENCY_UNIT_FORMAT);
	}
	
	public static String formatCurrencyDecimals(final float money, boolean showUnit, int decimals) {
		return formatNumber(money, decimals, showUnit, true, CURRENCY_VALUE_FORMAT, CURRENCY_DISPLAY_FORMAT,
				CURRENCY_UNIT_FORMAT);
	}

	public static String formatVoltage(final float money, boolean showUnit) {
		return formatNumber(money, 2, showUnit, true, VOLTAGE_VALUE_FORMAT, VOLTAGE_DISPLAY_FORMAT,
				VOLTAGE_UNIT_FORMAT);
	}

	public static String formatVoltageZero(final float money, boolean showUnit, boolean showZero) {
		return formatNumber(money, 2, showUnit, showZero, VOLTAGE_VALUE_FORMAT, VOLTAGE_DISPLAY_FORMAT,
				VOLTAGE_UNIT_FORMAT);
	}
	
	public static String formatVoltageDecimals(final float money, boolean showUnit, int decimals) {
		return formatNumber(money, decimals, showUnit, true, VOLTAGE_VALUE_FORMAT, VOLTAGE_DISPLAY_FORMAT,
				VOLTAGE_UNIT_FORMAT);
	}
	
	public static String formatPlainNumber(final float number, int decimals) {
		return formatNumber(number, decimals, false, true, PLAIN_NUMBER_VALUE_FORMAT, new String(), new String());		
	}
	
	public static String formatTextNumber(final String text) {
		if (text.equalsIgnoreCase("0.0")) {
			return new String();
		}
		return text;
	}

	public static String logUser(RSUser user) {
		if (user == null) {
			return UNKNOWN_USER_TEXT;
		}
		if (!StringUtils.isEmpty(user.getFirstName())) {
			return user.getFirstName();
		}
		if (!StringUtils.isEmpty(user.getToken())) {
			return user.getToken();
		}
		if (!StringUtils.isEmpty(user.getEmail())) {
			return user.getEmail();
		}
		return Integer.toString(user.getId());
	}

	public static <T> String logList(List<T> collection) {
		if (collection == null) {
			return NULL_COLLECTION_TEXT;
		}
		return Integer.toString(collection.size());
	}

	public static String getActionId() {
		return RandomStringUtils.randomAlphabetic(10).toUpperCase();
	}

	public static String buildToSting(String... params) {
		StringBuilder builder = new StringBuilder();
		if(params == null) {
			builder.toString();
		}
		for (int i = 0; i < params.length; i++) {
			if(i == 0) {
				builder.append("[");
			}
			if(!StringUtils.isEmpty(params[i])) {
				builder.append(params[i]);	
			}
			if(i == params.length - 1) {
				builder.append("]");
			}
			else {
				builder.append(",");
			}
		}
		return builder.toString();
	}

	private static String formatNumber(final float number, final int decimals, boolean showUnit, boolean showZero,
			String valueFormat, String displayFormat, String unitFormat) {
		if (number == 0.0 && !showZero) {
			return new String();
		}
		String percentReplacement = "PERCENT";
		String numberString = valueFormat.replace(DECIMAL_PLACEHOLDER, Integer.toString(decimals));
		if (showUnit) {
			numberString = displayFormat.replace(VALUE_PLACEHOLDER, numberString);
			unitFormat = unitFormat.replace(PERCENTAGE_UNIT_FORMAT, percentReplacement);
			numberString = numberString.replace(UNIT_PLACEHOLDER, unitFormat);
		}
		numberString = String.format(numberString, number);
		return numberString.replace(percentReplacement, PERCENTAGE_UNIT_FORMAT);
	}

}
