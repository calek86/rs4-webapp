package rs4.utils;

public class Logger {

	private org.apache.log4j.Logger log4jLogger;
	
	private Logger() {
	}
	
	public static Logger getLogger(String className) {
		Logger logger = new Logger();
		logger.setLog4jLogger(org.apache.log4j.Logger.getLogger(className));
		return logger;
	}

	public void info(final String message, Object... params) {
		log4jLogger.info(formatMessage(message, params));
	}

	public void warn(final String message, Object... params) {
		log4jLogger.warn(formatMessage(message, params));
	}
	
	public void error(final String message, Object... params) {
		log4jLogger.error(formatMessage(message, params));
	}
	
	private String formatMessage(final String message, Object... params) {
		String outputMessage = message;
		for (int i = 0; i < params.length; i++) {
			outputMessage = outputMessage.replaceFirst("@", params[i].toString());
		}
		return outputMessage;
	}
	
	private void setLog4jLogger(org.apache.log4j.Logger log4jLogger) {
		this.log4jLogger = log4jLogger;
	}


}
