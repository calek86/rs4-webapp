package rs4.utils;

import java.awt.Desktop;
import java.net.URI;

public class EmailUtils {

	//private static final String RS4_USER_TOKEN_COOKIE = "rs4userToken";
	private static Logger logger = Logger.getLogger(DataUtils.class.getName());
	
	public static boolean sendEmail(String subject, String attachment) {
		try {
			Desktop desktop;
			if (Desktop.isDesktopSupported() && (desktop = Desktop.getDesktop()).isSupported(Desktop.Action.MAIL)) {
				//maybe use JMAPI???
				String uri = (new StringBuilder())
						.append("mailto:")
						.append("")
						.append("?subject=")
						.append(subject)
						.append("&attachment=")
						.append(attachment)
						.toString();
				URI mailto = new URI(uri);
				desktop.mail(mailto);
				return true;
			} else {
			  return false;
			}
		} catch (Exception e) {
			logger.error("Error sending email with subject: @ and attachemnt: @", subject, attachment);
			e.printStackTrace();
			return false;
		}
	}
	
}
