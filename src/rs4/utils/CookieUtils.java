package rs4.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookieUtils {

	private static final String RS4_USER_TOKEN_COOKIE = "rs4userToken";
	private static final int COOKIE_MAX_AGE = 60*60*24; //24 hours 

	public static String extractUserToken(HttpServletRequest request) {
		if(request == null || request.getCookies() == null) {
			return new String();
		}
		for (Cookie cookie : request.getCookies()) {
			if (cookie.getName().equalsIgnoreCase(RS4_USER_TOKEN_COOKIE)) {
				return cookie.getValue();
			}
		}
		return new String();
	}

	public static void saveUserToken(HttpServletResponse response, final String userToken) {
		Cookie userTokenCookie = new Cookie(RS4_USER_TOKEN_COOKIE, userToken);
		userTokenCookie.setMaxAge(COOKIE_MAX_AGE);
		response.addCookie(userTokenCookie);
	}

	public static void deleteUserToken(HttpServletResponse response) {
		Cookie userTokenCookie = new Cookie(RS4_USER_TOKEN_COOKIE, "0");
		userTokenCookie.setMaxAge(0);
		response.addCookie(userTokenCookie);
	}
	
}
