package rs4.utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Days;

import rs4.core.search.SearchData;

public class DateUtils {

	private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy h:mm:ss a");
	private static SimpleDateFormat wmDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static SimpleDateFormat backupDateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");

	static Logger logger = Logger.getLogger(SearchData.class.getName());

	private DateUtils() {
	}

	public static String formatDate(Date date) {
		return dateFormat.format(date);
	}

	public static String formatBackupDate(Date date) {
		return backupDateFormat.format(date);
	}
	
	public static String formatDate(Timestamp timestamp) {
		return dateFormat.format(timestamp);
	}

	public static String formatDateTime(Date date) {
		return dateTimeFormat.format(date);
	}

	public static String formatDateTime(Timestamp timestamp) {
		return dateTimeFormat.format(timestamp);
	}

	public static Date parseDate(final String dateString) throws Exception {
		Date date = new Date();
		try {
			date = dateFormat.parse(dateString);
		} catch (Exception e) {
			logger.error("Problem parsing string date @", dateString);
			throw e;
		}
		return date;
	}

	public static Date parseDateTime(final String dateTimeString) throws Exception {
		Date dateTime = new Date();
		try {
			dateTime = dateTimeFormat.parse(dateTimeString);
		} catch (Exception e) {
			logger.error("Problem parsing string date time @", dateTimeString);
			throw e;
		}
		return dateTime;
	}

	public static Date parseWMDateTimeToDate(final String dateTimeString) throws Exception {
		Date dateTime = new Date();
		try {
			dateTime = wmDateTimeFormat.parse(dateTimeString);
		} catch (Exception e) {
			logger.error("Problem parsing Weighman string date time @ to date", dateTimeString);
			throw e;
		}
		return dateTime;
	}

	public static Timestamp parseWMDateTimeToTimestamp(final String dateTimeString) throws Exception {
		Timestamp timestamp = new Timestamp(0);
		try {
			timestamp = new Timestamp(wmDateTimeFormat.parse(dateTimeString).getTime());
		} catch (Exception e) {
			logger.error("Problem parsing Weighman string date time @ to timestamp", dateTimeString);
			throw e;
		}
		return timestamp;
	}
	
	public static Timestamp parseDateToTimestamp(final String dateString) throws Exception {
		Timestamp timestamp = new Timestamp(0);
		try {
			timestamp = new Timestamp(dateFormat.parse(dateString).getTime());
		} catch (Exception e) {
			logger.error("Problem parsing string date @ to timestamp", dateString);
			throw e;
		}
		return timestamp;
	}

	public static Timestamp parseDateTimeToTimestamp(final String dateTimeString) throws Exception {
		Timestamp timestamp = new Timestamp(0);
		try {
			timestamp = new Timestamp(dateTimeFormat.parse(dateTimeString).getTime());
		} catch (Exception e) {
			logger.error("Problem parsing string date time @ to timestamp", dateTimeString);
			throw e;
		}
		return timestamp;
	}

	public static Date completeDateFrom(final String dateFrom) throws Exception {
		return parseDateTime(dateFrom + " 12:00:00 AM");
	}

	public static Date completeDateTo(final String dateFrom) throws Exception {
		return parseDateTime(dateFrom + " 11:59:59 PM");
	}

	public static String getDateToday() {
		Calendar cal = Calendar.getInstance();
		return formatDate(cal.getTime());
	}

	public static String getDateTimeToday() {
		Calendar cal = Calendar.getInstance();
		return formatDateTime(cal.getTime());
	}

	public static String getDateYesterday() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		return formatDate(cal.getTime());
	}

	public static String getDateWeekAgo() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -7);
		return formatDate(cal.getTime());
	}
	
	public static int daysBetween(Date fromDate, Date toDate, boolean inclusive) {
		DateTime fromDateJoda = new DateTime(fromDate);
		DateTime toDateJoda = new DateTime(toDate);
		int daysBetween = Days.daysBetween(fromDateJoda, toDateJoda).getDays();
		daysBetween = Math.abs(daysBetween);
		if(inclusive) {
			daysBetween++;
		}
		return daysBetween;
	}
	
	public static Timestamp getDateFromTimestamp(final String dateFrom) {
		Date date = new Date();
		try {
			date = completeDateFrom(dateFrom);
		} catch (Exception e) {
			logger.error("Cannot read starting date @", dateFrom);
		}
		return new Timestamp(date.getTime());
	}

	public static Timestamp getDateToTimestamp(String dateTo) {
		Date date = new Date();
		try {
			date = completeDateTo(dateTo);
		} catch (Exception e) {
			logger.error("Cannot read ending date @", dateTo);
		}
		return new Timestamp(date.getTime());
	}

	public static List<Date> generateDateList(Date dateFrom, Date dateTo) {
		List<Date> dateList = new ArrayList<Date>();
		int daysBetween = daysBetween(dateFrom, dateTo, true);
		if(daysBetween <= 0) {
			return dateList;
		}
		DateTime dateTime = new DateTime(dateFrom);
		for (int i = 0; i < daysBetween; i++) {
			dateList.add(dateTime.toDate());
			dateTime = dateTime.plusDays(1);
		}
		return dateList;
	}
}
