package rs4.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import rs4.core.login.AccessLevel;
import rs4.core.search.TransactionType;
import rs4.database.dao.RSDeductionDao;
import rs4.database.dao.RSDeductionGroupDao;
import rs4.database.dao.RSDeductionProductDao;
import rs4.database.dao.RSSettingsDao;
import rs4.database.dao.RSStockYearDao;
import rs4.database.dao.RSTransactionDao;
import rs4.database.dao.RSTransactionTrashDao;
import rs4.database.dao.RSTransferDao;
import rs4.database.dao.model.RSDeduction;
import rs4.database.dao.model.RSDeductionGroup;
import rs4.database.dao.model.RSStockYear;
import rs4.database.dao.model.RSTransaction;
import rs4.database.dao.model.RSTransfer;
import rs4.database.dao.model.RSUser;

public class DataUtils {

	public static final String ACCOUNT_ID = "190fd2ab-688a-11ea-890a-0ac7078c25ec";
	
	public static final String REQUEST_USER_ATTRIBUTE = "user";
	public static final String TRANSACTION_TICKET_SUFFIX = "TN";
	public static final String TRANSFER_TICKET_SUFFIX = "TF";
	public static final String[] CONTAMINANTS_LIST = { "None, Infestation", "Hazardous Impurities", "Contamininants", "Abnormal Smell"};

	private static RSStockYearDao rsStockYearDao = RSStockYearDao.getInstance();
	private static RSSettingsDao rsSettingsDao = RSSettingsDao.getInstance();
	private static RSTransferDao rsTransferDao = RSTransferDao.getInstance();
	private static RSTransactionDao rsTransactionDao = RSTransactionDao.getInstance();
	private static RSTransactionTrashDao rsTransactionTrashDao = RSTransactionTrashDao.getInstance();
	private static RSDeductionDao rsMoistureDeductionDao = RSDeductionDao.getInstance();
	private static RSDeductionProductDao rsDeductionProductDao = RSDeductionProductDao.getInstance();
	private static RSDeductionGroupDao rsDeductionGroupDao = RSDeductionGroupDao.getInstance();

	private static float minMoistureDeduction = rsSettingsDao
			.getFloatSettingValueByKey(RSSettingsDao.MIN_MOISTURE_DEDUCTION);
	private static float minIntakeCost = rsSettingsDao.getFloatSettingValueByKey(RSSettingsDao.MIN_INTAKE_COST);
	private static float minDryingCost = rsSettingsDao.getFloatSettingValueByKey(RSSettingsDao.MIN_DRYING_COST);
	private static float minAcceptableAdmix = rsSettingsDao
			.getFloatSettingValueByKey(RSSettingsDao.MIN_ACCEPTABLE_ADMIX);
	private static String beansProductRegex = rsSettingsDao
			.getStringSettingValueByKey(RSSettingsDao.BEANS_PRODUCT_REGEX);

	private static Logger logger = Logger.getLogger(DataUtils.class.getName());

	public static float getStockEntWt(RSTransaction rsTransaction) {
		if (DataUtils.isBeans(rsTransaction.getProduct())) {
			return rsTransaction.getEntWeightBeans();
		}
		return rsTransaction.getEntWeight();
	}

	public static float getStockPercentLoss(RSTransaction rsTransaction) {
		if (DataUtils.isBeans(rsTransaction.getProduct())) {
			return rsTransaction.getPercentLossBeans();
		}
		return rsTransaction.getPercentLoss();
	}

	public static void putStockEntWt(RSTransaction rsTransaction) {
		if (rsTransaction == null || rsTransaction.getNetWeight() <= 0.0
				|| StringUtils.isEmpty(rsTransaction.getProduct())) {
			return;
		}
		float deduction = 0.0F;
		float entWt = rsTransaction.getNetWeight();
		float deductionBeans = deduction;
		float entWtBeans = entWt;
		if (rsTransaction.equalsType(TransactionType.IN)) {
			deduction = getTotalDeductionFromTransaction(rsTransaction);
			entWt = calcEntWeight(rsTransaction.getNetWeight(), deduction);
			deductionBeans = deduction;
			entWtBeans = entWt;

			// FIX 06-06-2017 from Jon: "for BeansIn use Ent.Wt (smaller one) to
			// use in Stock"
			// if (isBeans(rsTransaction.getProduct())) {
			// deductionBeans =
			// getTotalBeansDeductionFromTransaction(rsTransaction);
			// if (deductionBeans <= 0.0) {
			// deductionBeans = deduction;
			// }
			// entWtBeans = calcEntWeight(rsTransaction.getNetWeight(),
			// deductionBeans);
			// }
		}
		rsTransaction.setEntWeight(Math.round(entWt));
		rsTransaction.setEntWeightBeans(Math.round(entWtBeans));
		rsTransaction.setPercentLoss(deduction * 100);
		rsTransaction.setPercentLossBeans(deductionBeans * 100);
	}

	public static String translateWMTransactionType(String type) {
		switch (type) {
		case "ProductIn":
			return TransactionType.IN.toString();
		case "ProductOut":
			return TransactionType.OUT.toString();
		case "WeighOnly":
			return TransactionType.WO.toString();
		default:
			return new String();
		}
	}

	public static RSDeductionGroup detectDeductionGroupFromProductCode(String productCode) {
		List<RSDeductionGroup> deductionGroups = rsDeductionGroupDao.getRSDeductionGroups();
		for (RSDeductionGroup rsDeductionGroup : deductionGroups) {
			String[] regex = rsDeductionGroup.getRegex().split(";");
			for (String exp : regex) {
				if (StringUtils.isNotEmpty(productCode) && productCode.toLowerCase().matches(exp)) {
					return rsDeductionGroup;
				}
			}
		}
		return new RSDeductionGroup();
	}

	// Transactions: 15400TN0, 15400TN1, 15400TN2, ...
	public static String generateNextTransactionTicketId() {
		String lastTicketId = new String();
		RSTransaction rsTransaction = rsTransactionDao.getRSTransactionLast();
		if (rsTransaction != null) {
			lastTicketId = rsTransaction.getTicketId();
		}
		if (lastTicketId.contains(TRANSACTION_TICKET_SUFFIX)) {
			int tHowMany = Integer
					.parseInt(lastTicketId.substring(lastTicketId.indexOf(TRANSACTION_TICKET_SUFFIX) + 2));
			tHowMany += 1;
			return lastTicketId.substring(0, lastTicketId.indexOf(TRANSACTION_TICKET_SUFFIX) + 2)
					+ Integer.toString(tHowMany);
		}
		return lastTicketId + TRANSACTION_TICKET_SUFFIX + "0";
	}

	// Transfers: 2015TF0, 2015TF1, 2015TF2, ...
	public static String generateTransferTicketId(RSStockYear toStockYear) {
		RSTransfer rsTransfer = rsTransferDao.getLastRSTransferFromStockYear(toStockYear.getId());
		if (rsTransfer != null && !StringUtils.isEmpty(rsTransfer.getTicketId())
				&& rsTransfer.getTicketId().contains(TRANSFER_TICKET_SUFFIX)) {
			return generateNextTransferTicketId(rsTransfer, toStockYear);
		}
		return generateFirstTransferTicketId(toStockYear);
	}

	private static String generateFirstTransferTicketId(RSStockYear rsStockYear) {
		return rsStockYear.getDescription() + TRANSFER_TICKET_SUFFIX + "0";
	}

	private static String generateNextTransferTicketId(RSTransfer rsTransfer, RSStockYear rsStockYear) {
		String lastTransferTicketId = rsTransfer.getTicketId();
		int lastTransferNo = Integer
				.parseInt(lastTransferTicketId.substring(lastTransferTicketId.indexOf(TRANSFER_TICKET_SUFFIX) + 2));
		return rsStockYear.getDescription() + TRANSFER_TICKET_SUFFIX + (++lastTransferNo);
	}

	public static String generateUUID() {
		String uuid = UUID.randomUUID().toString();
		StringBuilder rs4Uuid = new StringBuilder(uuid);
		rs4Uuid.setCharAt(0, 'R');
		rs4Uuid.setCharAt(1, 'S');
		rs4Uuid.setCharAt(2, '4');
		return rs4Uuid.toString();
	}

	public static AccessLevel requestAccess(final RSUser user) {
		if (user != null && !StringUtils.isEmpty(user.getAccess())) {
			return AccessLevel.fromString(user.getAccess().toUpperCase());
		} else {
			return AccessLevel.READ;
		}
	}

	public static AccessLevel requestAccess(final HttpServletRequest request) {
		RSUser user = requestUser(request);
		return requestAccess(user);
	}

	public static RSUser requestUser(final HttpServletRequest request) {
		if (request.getAttribute(REQUEST_USER_ATTRIBUTE) != null) {
			return (RSUser) request.getAttribute(REQUEST_USER_ATTRIBUTE);
		}
		return null;
	}

	public static boolean validateToken(final RSUser user, final String userToken) {
		if (StringUtils.isEmpty(userToken)) {
			return false;
		}
		if (user == null || StringUtils.isEmpty(user.getToken()) || !userToken.equals(user.getToken())) {
			return false;
		}
		return true;
	}

	public static RSStockYear getDefaultStockYear() {
		int id = rsSettingsDao.getIntSettingValueByKey(RSSettingsDao.DEFAULT_STOCK_YEAR_ID);
		return rsStockYearDao.getRSStockYearById(id);
	}

	public static boolean isBeans(String productCode) {
		if (StringUtils.isEmpty(productCode) || !productCode.toLowerCase().matches(beansProductRegex)) {
			return false;
		}
		return true;
	}

	public static <T> List<T> revertCollection(List<T> collection) {
		LinkedList<T> revertedCollection = new LinkedList<T>();
		if (CollectionUtils.isEmpty(collection)) {
			return revertedCollection;
		}
		for (T element : collection) {
			revertedCollection.addFirst(element);
		}
		return revertedCollection;
	}

	public static boolean wasTransactionDeleted(String ticketId) {
		if (rsTransactionTrashDao.getRSTransactionTrashByTicketId(ticketId) != null) {
			return true;
		}
		return false;
	}

	public static RSDeduction getRSDeductionFromTransaction(RSTransaction rsTransaction) {
		int productId = rsTransaction.getProductId();
		float moisture = rsTransaction.getMoisture();
		int groupId = rsDeductionProductDao.getRSDeductionGroupIdByProductId(productId);
		if (groupId < 0) {
			return getStandardDeduction();
		}
		List<RSDeduction> productMoistureDeductions = rsMoistureDeductionDao.getRSDeductionByGroupId(groupId);
		if (CollectionUtils.isEmpty(productMoistureDeductions)) {
			return getStandardDeduction();
		}
		RSDeduction firstDeduction = productMoistureDeductions.get(0);
		if (moisture < firstDeduction.getMoistureFrom()) {
			return getStandardDeduction();
		}
		RSDeduction lastDeduction = productMoistureDeductions.get(productMoistureDeductions.size() - 1);
		if (moisture > lastDeduction.getMoistureTo()) {
			return lastDeduction;
		}
		for (RSDeduction rsMoistureDeduction : productMoistureDeductions) {
			if ((moisture >= rsMoistureDeduction.getMoistureFrom())
					&& (moisture <= rsMoistureDeduction.getMoistureTo())) {
				return rsMoistureDeduction;
			}
		}
		return getStandardDeduction();
	}

	public static float getTotalBeansDeductionFromTransaction(RSTransaction rsTransaction) {
		float screening = getScreening(rsTransaction);
		float holed = getHoled(rsTransaction);
		float blind = getBlind(rsTransaction);
		float stains = getStains(rsTransaction);
		return getBeansDeductionFromParams(screening, holed, blind, stains);
	}

	public static float getScreening(RSTransaction rsTransaction) {
		try {
			if (StringUtils.isEmpty(rsTransaction.getScreen2())) {
				return 0.0F;
			}
			return Float.parseFloat(rsTransaction.getScreen2());
		} catch (Exception e) {
			logger.error("Problem parsing screening: @ for transaction @", rsTransaction.getScreen2(),
					rsTransaction.getTicketId());
		}
		return 0.0F;
	}

	public static float getHoled(RSTransaction rsTransaction) {
		try {
			if (StringUtils.isEmpty(rsTransaction.getHoled())) {
				return 0.0F;
			}
			return Float.parseFloat(rsTransaction.getHoled());
		} catch (Exception e) {
			logger.error("Problem parsing holed: @ for transaction @", rsTransaction.getHoled(),
					rsTransaction.getTicketId());
			return 0.0F;
		}
	}

	public static float getBlind(RSTransaction rsTransaction) {
		try {
			if (StringUtils.isEmpty(rsTransaction.getBlind())) {
				return 0.0F;
			}
			return Float.parseFloat(rsTransaction.getBlind());
		} catch (Exception e) {
			logger.error("Problem parsing blind: @ for transaction @", rsTransaction.getBlind(),
					rsTransaction.getTicketId());
			return 0.0F;
		}
	}

	public static float getStains(RSTransaction rsTransaction) {
		try {
			if (StringUtils.isEmpty(rsTransaction.getStains())) {
				return 0.0F;
			}
			return Float.parseFloat(rsTransaction.getStains());
		} catch (Exception e) {
			logger.error("Problem parsing stains: @ for transaction @", rsTransaction.getStains(),
					rsTransaction.getTicketId());
			return 0.0F;
		}
	}

	private static float getTotalDeductionFromTransaction(RSTransaction rsTransaction) {
		RSDeduction rsDeduction = getRSDeductionFromTransaction(rsTransaction);
		float moistureDeduction = rsDeduction.getMoistureDeduction();
		float admixDeduction = getAdmixDeductionFromTransaction(rsTransaction);
		return (moistureDeduction + admixDeduction) / 100;
	}

	private static RSDeduction getStandardDeduction() {
		RSDeduction rsDeduction = new RSDeduction();
		rsDeduction.setId(-1);
		rsDeduction.setMoistureDeduction(minMoistureDeduction);
		rsDeduction.setIntakeCost(minIntakeCost);
		rsDeduction.setDryingCost(minDryingCost);
		return rsDeduction;
	}

	private static float getAdmixDeductionFromTransaction(RSTransaction rsTransaction) {
		if (rsTransaction.getAdmix() < minAcceptableAdmix) {
			return 0.0F;
		}
		return rsTransaction.getAdmix() - minAcceptableAdmix;
	}

	private static float getBeansDeductionFromParams(float screen, float holed, float blind, float stains) {
		return (screen + holed + blind + stains) / 100;
	}

	private static float calcEntWeight(float netWeight, float deduction) {
		return netWeight * (1.0F - deduction);
	}

}
