<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:insertAttribute name="title" ignore="true" /></title>
<link rel="stylesheet" href="resources/style.css" type="text/css"></link>
<script language="javascript" type="text/javascript">
	function positionRecords() {
		var menuHeight = document.getElementById("menu").offsetHeight;
		var headerCriteriaHeight = document.getElementById("headerCriteria").offsetHeight;
		var headerRecordsHeight = document.getElementById("headerRecords").offsetHeight;
		var chartHeight = 0;
		if(document.getElementById("recordsChart") != null) {
			chartHeight = document.getElementById("recordsChart").offsetHeight;
		}
		var recordsTop = menuHeight + headerCriteriaHeight
				+ headerRecordsHeight + chartHeight;
		var recordsDiv = document.getElementById("recordsDiv");
		recordsDiv.style.top = recordsTop + "px";
		recordsDiv.style.display = "inline";
		console.log("recordsTop: " + recordsTop + "px");
	}
</script>
</head>
<body onLoad="positionRecords();">
	<div>
		<div id="menu">
			<tiles:insertAttribute name="menu" />
		</div>
		<div id="body">
			<tiles:insertAttribute name="body" />
		</div>
	</div>
</body>
</html>
