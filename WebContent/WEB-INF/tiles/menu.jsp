<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="resources/style.css" type="text/css"></link>
<title></title>
</head>
<body>
	<div id="header"
		style="position: absolute; left: 0px; top: 10; width: 1000px; z-index: 1;">
		<img src="resources/images/header1200.png" />
	</div>
	<div id="rs_"
		style="position: absolute; left: 85px; top: 15px; z-index: 2;">
		<font color="white" style="font-size: 19px"><c:if
				test="${not empty firstName and not empty lastName}">
						Hello ${firstName} ${lastName}
					</c:if></font>
	</div>
	<jsp:include page="../version.jsp" />
	<div class="menuDiv">
		<c:set var="activeColor" value="#00000" />
		<c:set var="inactiveColor" value="#1B8EE0" />
		<table class="menuTable">
			<tr>
				<td onClick="document.location.href='search';"
					bgcolor='<c:choose><c:when test="${empty activeTab or activeTab == 1}">${activeColor}</c:when><c:otherwise>${inactiveColor}</c:otherwise></c:choose>'>
					<a href="search" class="linkWithoutBackgroundColor">Search</a>
				</td>
				<td onClick="document.location.href='beans';"
					bgcolor='<c:choose><c:when test="${activeTab == 2}">${activeColor}</c:when><c:otherwise>${inactiveColor}</c:otherwise></c:choose>'>
					<a href="beans" class="linkWithoutBackgroundColor">Beans</a>
				</td>
				<td onClick="document.location.href='stock';"
					bgcolor='<c:choose><c:when test="${activeTab == 3}">${activeColor}</c:when><c:otherwise>${inactiveColor}</c:otherwise></c:choose>'>
					<a href="stock" class="linkWithoutBackgroundColor">Stock</a>
				</td>
				<td onClick="document.location.href='bills';"
					bgcolor='<c:choose><c:when test="${activeTab == 4}">${activeColor}</c:when><c:otherwise>${inactiveColor}</c:otherwise></c:choose>'>
					<a href="bills" class="linkWithoutBackgroundColor">Bills</a>
				</td>
				<td onClick="document.location.href='transfer';"
					bgcolor='<c:choose><c:when test="${activeTab == 5}">${activeColor}</c:when><c:otherwise>${inactiveColor}</c:otherwise></c:choose>'>
					<a href="transfer" class="linkWithoutBackgroundColor">Transfer</a>
				</td>
				<td onClick="document.location.href='sensors';"
					bgcolor='<c:choose><c:when test="${activeTab == 7}">${activeColor}</c:when><c:otherwise>${inactiveColor}</c:otherwise></c:choose>'>
					<a href="sensors" class="linkWithoutBackgroundColor">Sensors</a>
				</td>
				<td onClick="document.location.href='settings';"
					bgcolor='<c:choose><c:when test="${activeTab == 6}">${activeColor}</c:when><c:otherwise>${inactiveColor}</c:otherwise></c:choose>'>
					<a href="settings" class="linkWithoutBackgroundColor">Settings</a>
				</td>
				<td
					bgcolor='<c:choose><c:when test="${activeTab == 8}">${activeColor}</c:when><c:otherwise>${inactiveColor}</c:otherwise></c:choose>'>
					<form:form action="DoLogout" method="POST">
						<input type="submit" value="Logout" class="logoutButton" />
					</form:form>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>