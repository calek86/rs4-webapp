<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<div id="headerCriteria">
	<c:if test="${command.settingsResults.accessLevel=='WRITE'}">
		<table class="rs4table header" style="width: 100%">
			<tr>
				<th colspan="2">Default Stock Year</th>
				<th colspan="2">Default Storing Rate (&pound;/tonne)</th>
				<th>Products Without Deductions</th>
				<th>Deduction Groups</th>
				<th></th>
			</tr>
			<tr>
				<form:form action="SettingsSaveDefaultYear" method="POST">
					<td align="center" colspan="2">
						<form:select path="settingsCriteria.stockYears" size="1"
							multiple="false" style="width:49%;">
							<option
								value="<c:out value="${command.settingsCriteria.stockYearId}"/>">
								<c:out value="${command.settingsCriteria.stockYearDescription}" />
							</option>
							<c:forEach var="stockYear"
								items="${command.settingsCriteria.stockYears}">
								<option value="<c:out value="${stockYear.id}"/>">
									<c:out value="${stockYear.description}" />
								</option>
							</c:forEach>
						</form:select>
						<input type="submit" value="Set Year" style="width: 49%;" />
					</td>
				</form:form>
				<form:form action="EditBillRates" method="POST">
					<td class="valuetd" colspan="2">
						<form:input style="width:49%" type="text" path="settingsCriteria.billRates.storingRate" id="storingRate" />
						<input type="submit" value="Save Rate" name="Save" style="width: 46%" id="Save" />
					</td>
				</form:form>
					<form:form action="SettingsAssignProduct" method="POST">
						<td align="center" colspan="3">
							<form:select
								path="settingsCriteria.productsWithoutDeductions" size="1"
								multiple="false" style="width:33%">
								<c:forEach var="product"
									items="${command.settingsCriteria.productsWithoutDeductions}">
									<option value="<c:out value="${product.code}"/>">
										<c:out value="${product.code}" />
									</option>
								</c:forEach>
							</form:select>
							<form:select
								path="settingsCriteria.deductionGroups" size="1"
								multiple="false" style="width:32%">
								<c:forEach var="group"
									items="${command.settingsCriteria.deductionGroups}">
									<option value="<c:out value="${group.code}"/>">
										<c:out value="${group.code}" />
									</option>
								</c:forEach>
							</form:select>
							<input type="submit" value="Assign Product" style="width: 33%" 
								<c:if test="${empty command.settingsCriteria.deductionGroups 
								|| empty command.settingsCriteria.productsWithoutDeductions}">disabled</c:if>/>
						</td>
					</form:form>
			</tr>
		</table>
		<c:if test="${command.settingsResults.errorMessage!='0'}">
			<center>
				<div style="background-color: red; width: 100%; height: 40px">
					<font color="white"> <b><br> <c:out
								value="${command.settingsResults.errorMessage}" /> <br> <br></b>
					</font>
				</div>
			</center>
		</c:if>
		<c:if test="${command.settingsResults.resultMessage!='0'}">
			<center>
				<div style="background-color: green; width: 100%; height: 40px">
					<font color="white"> <b><br> <c:out
								value="${command.settingsResults.resultMessage}" /> <br> <br></b>
					</font>
				</div>
			</center>
		</c:if>
		<table class="rs4table header" style="width: 100%">
			<tr>
				<th>Deduction Group</th>
				<th>Moisture From (%)</th>
				<th>Moisture To (%)</th>
				<th>Moisture Deduction (%)</th>
				<th>Intake Cost (&pound;/tonne)</th>
				<th>Drying Cost (&pound;/tonne)</th>
				<th></th>
			</tr>
			<tr>
				<form:form action="SettingsAddDeduction" method="POST">
					<td align="center"><form:select
							path="settingsCriteria.deductionGroups" size="1" multiple="false"
							style="width:98%">
							<c:forEach var="group"
								items="${command.settingsCriteria.deductionGroups}">
								<option value="<c:out value="${group.code}"/>">
									<c:out value="${group.code}" />
								</option>
							</c:forEach>
						</form:select></td>
					<td><form:input path="settingsCriteria.moistureFrom"
							style="width:94%" type="text" id="moistureFrom" /></td>
					<td><form:input path="settingsCriteria.moistureTo"
							style="width:94%" type="text" id="moistureTo" /></td>
					<td><form:input path="settingsCriteria.moistureDeduction"
							style="width:94%" type="text" id="moistureDeduction" /></td>
					<td><form:input path="settingsCriteria.intakeCost"
							style="width:94%" type="text" id="intakeCost" /></td>
					<td><form:input path="settingsCriteria.dryingCost"
							style="width:94%" type="text" id="dryingCost" /></td>
					<td><input type="submit" value="Add Deduction"
						style="width: 99%" /></td>
				</form:form>
			</tr>
		</table>
		<table class="rs4table header" style="width: 100%">
			<tr>
				<th>New Customer Name</th>
				<th colspan="2">Address</th>
				<th colspan="3">Description</th>				
				<th></th>
			</tr>
			<tr>
				<form:form action="SettingsCreateCustomer" method="POST">
					<td><form:input path="settingsCriteria.newCustomerName"
							style="width:94%" type="text" id="newCustomerName" /></td>
					<td colspan="2"><form:input path="settingsCriteria.newCustomerAddress"
							style="width:96%" type="text" id="newCustomerAddress" /></td>
					<td colspan="3"><form:input path="settingsCriteria.newCustomerDescription"
							style="width:98%" type="text" id="newCustomerDescription" /></td>
					<td><input type="submit" value="Create Customer"
						style="width: 99%" /></td>
				</form:form>
			</tr>
		</table>		
	</c:if>
</div>
<div><jsp:include page="reports/settings/deductions.jsp" /></div>
