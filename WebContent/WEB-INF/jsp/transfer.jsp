<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<div id="headerCriteria">
	<c:if test="${command.transferResults.accessLevel=='WRITE'}">
		<form:form action="TransferStock" method="POST">
			<table class="rs4table header" style="width: 100%">
				<tr>
					<th>Date</th>
					<th style="width: 300px">From Stock</th>
					<th>To Customer</th>
					<th style="width: 100px">To Year</th>
					<th style="width: 150px">Transfer Value (kg)</th>
					<th style="width: 150px">Reference No</th>
					<th style="width: 150px"></th>
				</tr>
				<tr>
					<td align="center"><form:input
							path="transferCriteria.transferDate" style="width:94%"
							type="text" /></td>
					<td><form:select path="transferCriteria.fromStockList"
							size="1" multiple="false" style="width:98%;">
							<c:forEach var="stock"
								items="${command.transferCriteria.fromStockList}">
								<option value="<c:out value="${stock.id}"/>">
									<c:out value="${stock.stockYearDescription}" /> -
									<c:out value="${stock.customerCode}" /> -
									<c:out value="${stock.productCode}" /> 
									(<c:out value="${stock.totalValueDisplay}" />) 
								</option>
							</c:forEach>
						</form:select></td>
					<td><form:select path="transferCriteria.toCustomerList" size="1"
							multiple="false" style="width:98%;">
							<c:forEach var="customer"
								items="${command.transferCriteria.toCustomerList}">
								<option value="<c:out value="${customer.id}"/>">
									<c:out value="${customer.code}" />
								</option>
							</c:forEach>
						</form:select></td>
					<td align="center">
						<form:select path="transferCriteria.stockYears" size="1"
							multiple="false" style="width:98%;">
							<option
								value="<c:out value="${command.transferCriteria.toStockYearId}"/>">
								<c:out value="${command.transferCriteria.toStockYearDescription}" />
							</option>
							<c:forEach var="stockYear"
								items="${command.transferCriteria.stockYears}">
								<option value="<c:out value="${stockYear.id}"/>">
									<c:out value="${stockYear.description}" />
								</option>
							</c:forEach>
						</form:select>
					</td>						
					<td><form:input path="transferCriteria.transferValue"
							type="text" style="width:68%" id="transferValue" value="0" /><form:checkbox
							path="transferCriteria.transferEverything"
							id="transferEverything" value="1" />All</td>
					<td align="center"><form:input
							path="transferCriteria.referenceNumber" style="width:94%"
							type="text" /></td>
					<td><input type="submit" value="Transfer" style="width: 98%;" />
					</td>
				</tr>
			</table>
		</form:form>
	</c:if>
	<c:if test="${command.transferResults.errorMessage!='0'}">
		<center>
			<div style="background-color: red; width: 100%; height: 40px">
				<font color="white"> <b><br> <c:out
							value="${command.transferResults.errorMessage}" /> <br> <br></b>
				</font>
			</div>
		</center>
	</c:if>
</div>
<div><jsp:include page="reports/transfer/list.jsp" /></div>
