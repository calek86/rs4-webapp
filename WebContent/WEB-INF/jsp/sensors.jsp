<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<div id="headerCriteria">
	<form:form action="SearchSensors" method="POST">
		<table class="rs4table header" style="width: 100%">
			<tr>
				<th>Date From</th>
				<th>Date To</th>
				<th>Sensor</th>
				<th></th>
			</tr>
			<tr>
				<td align="center"><form:input path="sensorsCriteria.dateFrom"
						style="width:94%" type="text" /></td>
				<td align="center"><form:input path="sensorsCriteria.dateTo"
						style="width:94%" type="text" /></td>
				<td align="center"><form:select path="sensorsCriteria.deviceId"
						size="1" multiple="false" style="width:98%">
						<c:forEach var="sensor" items="${command.sensorsCriteria.sensors}">
							<option value="<c:out value="${sensor.deviceId}"/>"
								<c:if test="${command.sensorsCriteria.deviceId == sensor.deviceId}">selected="selected"</c:if>>
								<c:out value="${sensor.name}" />
							</option>
						</c:forEach>
					</form:select></td>
				<td align="center"><input type="submit" name="Search"
					value="Search" style="width: 98%;" /></td>
			</tr>
		</table>
	</form:form>
</div>
<div id="records"><jsp:include
		page="reports/sensors/chartlist.jsp" /></div>

