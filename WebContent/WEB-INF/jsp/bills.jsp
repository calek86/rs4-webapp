<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<div id="headerCriteria">
	<form:form action="SearchBills" method="POST">
		<table class="rs4table header" style="width: 100%">
			<tr>
				<th>Bill Type</th>
				<th>Date From</th>
				<th>Date To</th>
				<th>Customer</th>
				<th>Product</th>
				<th>Transaction No</th>
				<th>Storing Rate (&pound;/tonne)</th>
				<th></th>
			</tr>
			<tr>
				<td align="center"><form:select path="billsCriteria.reportType"
						style="width:98%">
						<c:forEach var="rt" items="${command.billsCriteria.reportTypes}">
							<option value="${rt}"
								<c:if test="${command.billsCriteria.reportType == rt}">selected="selected"</c:if>>
								${rt}</option>
						</c:forEach>
					</form:select></td>
				<td align="center"><form:input path="billsCriteria.dateFrom"
						style="width:94%" type="text" /></td>
				<td align="center"><form:input path="billsCriteria.dateTo"
						style="width:94%" type="text" /></td>
				<td align="center"><form:select
						path="billsCriteria.customerCode" size="1" multiple="false"
						style="width:98%">
						<c:forEach var="customer"
							items="${command.billsCriteria.customers}">
							<option value="${customer.code}"
								<c:if test="${command.billsCriteria.customerCode == customer.code}">selected="selected"</c:if>>
								${customer.code}</option>
						</c:forEach>
					</form:select></td>
				<td align="center"><form:select path="billsCriteria.products"
						size="1" multiple="false" style="width:98%">
						<c:if test="${command.billsCriteria.productCode=='All'}">
							<option
								value="<c:out value="${command.billsCriteria.productCode}"/>"><c:out
									value="${command.billsCriteria.productCode}" /></option>
						</c:if>
						<c:if test="${command.billsCriteria.productCode!='All'}">
							<option
								value="<c:out value="${command.billsCriteria.productCode}"/>"><c:out
									value="${command.billsCriteria.productCode}" /></option>
							<option value="All">All</option>
						</c:if>
						<c:forEach var="product" items="${command.billsCriteria.products}">
							<option value="<c:out value="${product.code}"/>">
								<c:out value="${product.code}" />
							</option>
						</c:forEach>
					</form:select></td>
				<td align="center"><form:input
						path="billsCriteria.transactionsNo" type="text" style="width:94%"
						id="transactionsNo" /></td>
				<td align="center"><form:input path="billsCriteria.storingRate"
						type="text" style="width:94%" id="storingRate"
						value="${command.billsCriteria.storingRateDisplay}" /></td>
				<td align="center"><input type="submit" name="Generate"
					value="Generate" style="width: 98%;" /></td>
					
			</tr>
		</table>
	</form:form>
</div>
<c:if test="${command.billsResults.reportType=='Standard'}">
	<div><jsp:include page="reports/bills/standard.jsp" /></div>
</c:if>
<c:if test="${command.billsResults.reportType=='Storage'}">
	<div><jsp:include page="reports/bills/storage.jsp" /></div>
</c:if>
