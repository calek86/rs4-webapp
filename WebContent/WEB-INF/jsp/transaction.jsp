<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<form:form modelAttribute="transactionForm" method="post">
	<table class="rs4table header" border="1">
		<tr>
			<th style="width: 230px;">Customer</th>
			<th style="width: 230px;">Haulier</th>
			<th style="width: 230px;">Product</th>
			<th style="width: 230px;">Destination</th>
			<th style="width: 230px;">Source</th>			
		</tr>
		<tr>
			<td class="valuetd"><form:select path="customer" size="1"
					multiple="false" style="width:98%;">
					<c:forEach var="cust" items="${transactionForm.customers}">
						<c:set var="custIdCode" value="${cust.id}|${cust.code}" />
						<option value="${custIdCode}"
							<c:if test="${transactionForm.customer == cust.code}">selected="selected"</c:if>>
							<c:out value="${cust.code}" />
						</option>
					</c:forEach>
				</form:select> <br> <form:errors class="errortd" path="customer" /></td>
			<td class="valuetd"><form:select path="haulier" size="1"
					multiple="false" style="width:98%;">
					<c:forEach var="ha" items="${transactionForm.hauliers}">
						<c:set var="haulierIdCode" value="${ha.id}|${ha.code}" />
						<option value="${haulierIdCode}"
							<c:if test="${transactionForm.haulier == ha.code}">selected="selected"</c:if>>
							<c:out value="${ha.code}" />
						</option>
					</c:forEach>
				</form:select> <br> <form:errors class="errortd" path="haulier" /></td>
			<td class="valuetd"><form:select path="product" size="1"
					multiple="false" style="width:98%;">
					<c:forEach var="pr" items="${transactionForm.products}">
						<c:set var="productIdCode" value="${pr.id}|${pr.code}" />
						<option value="${productIdCode}"
							<c:if test="${transactionForm.product == pr.code}">selected="selected"</c:if>>
							<c:out value="${pr.code}" />
						</option>
					</c:forEach>
				</form:select> <br> <form:errors class="errortd" path="product" /></td>
			<td class="valuetd"><form:select path="destination" size="1"
					multiple="false" style="width:98%;">
					<c:forEach var="dest" items="${transactionForm.destinations}">
						<c:set var="destIdCode" value="${dest.id}|${dest.code}" />
						<option value="${destIdCode}"
							<c:if test="${transactionForm.destination == dest.code}">selected="selected"</c:if>>
							<c:out value="${dest.code}" />
						</option>
					</c:forEach>
				</form:select> <br> <form:errors class="errortd" path="destination" /></td>
			<td class="valuetd"><form:select path="source" size="1"
					multiple="false" style="width:98%;">
					<c:forEach var="sour" items="${transactionForm.sources}">
						<c:set var="sourIdCode" value="${sour.id}|${sour.code}" />
						<option value="${sourIdCode}"
							<c:if test="${transactionForm.source == sour.code}">selected="selected"</c:if>>
							<c:out value="${sour.code}" />
						</option>
					</c:forEach>
				</form:select> <br> <form:errors class="errortd" path="source" /></td>
		</tr>
		<tr>
			<th>Stock Year &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Type</th>
			<th>Date</th>
			<th>Reg.No.</th>
			<th>1st Wt. (kg)</th>
			<th>2nd Wt. (kg)</th>
		</tr>
		<tr>
			<td class="valuetd">
			<form:select path="stockYearWithId" size="1"
					multiple="false" style="width:49%;">
					<c:forEach var="st" items="${transactionForm.stockYears}">
						id=${st.description} curr=${transactionForm.currentStockYear}
							<c:choose>
							<c:when
								test="${st.description == transactionForm.currentStockYear}">
								<option value="${st.id}|${st.description}" selected="selected">
									<c:out value="${st.description}" />
								</option>
							</c:when>
							<c:otherwise>
								<option value="${st.id}|${st.description}">
									<c:out value="${st.description}" />
								</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</form:select>
				<form:select path="transactionType"
					style="width:49%;">
					<option value="WO"
						<c:if test="${transactionForm.transactionType eq 'WO'}">selected="selected"</c:if>>WO</option>
					<option value="IN"
						<c:if test="${transactionForm.transactionType eq 'IN'}">selected="selected"</c:if>>IN</option>
					<option value="OUT"
						<c:if test="${transactionForm.transactionType eq 'OUT'}">selected="selected"</c:if>>OUT</option>
				</form:select>
				<br> <form:errors class="errortd" path="stockYearWithId" />
				</td>
			<td class="valuetd"><form:input path="transactionDate"
					type="text" style="width:98%;" /> <br> <form:errors
					class="errortd" path="transactionDate" /></td>
			<td class="valuetd"><form:input path="registration"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="registration" /></td>
			<td class="valuetd"><form:input path="firstWeight"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="firstWeight" /></td>
			<td class="valuetd"><form:input path="secondWeight"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="secondWeight" /></td>
		</tr>
		<tr>
			<th>Moisture (%)</th>
			<th>Admix (%)</th>
			<th>Temp. (�C)</th>
			<th>Screen1</th>
			<th>Screen2</th>
		</tr>
		<tr>
			<td class="valuetd"><form:input path="moisture"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="moisture" /></td>
			<td class="valuetd"><form:input path="admix" style="width:98%;" /></td>
			<td class="valuetd"><form:input path="temperature"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="temperature" /></td>
			<td class="valuetd"><form:input path="screen1"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="screen1" /></td>
			<td class="valuetd"><form:input path="screen2"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="screen2" /></td>
		</tr>
		<tr>
			<th>SpWt</th>
			<th>OrderNo</th>
			<th>ExRef</th>
			<th>Contaminants</th>
			<th>Inspected</th>
		</tr>
		<tr>
			<td class="valuetd"><form:input path="spWt" style="width:98%;" /><br>
				<form:errors class="errortd" path="spWt" /></td>
			<td class="valuetd"><form:input path="orderNo"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="orderNo" /></td>
			<td class="valuetd"><form:input path="exRef" style="width:98%;" /><br>
				<form:errors class="errortd" path="exRef" /></td>
			<td class="valuetd"><form:select path="n2CP"
					style="width:98%;">
					<option value="None"
						<c:if test="${transactionForm.n2CP eq 'None'}">selected="selected"</c:if>>None</option>
					<option value="Infestation"
						<c:if test="${transactionForm.n2CP eq 'Infestation'}">selected="selected"</c:if>>Infestation</option>
					<option value="Hazardous Impurities"
						<c:if test="${transactionForm.n2CP eq 'Hazardous Impurities'}">selected="selected"</c:if>>Hazardous Impurities</option>
					<option value="Contaminants"
						<c:if test="${transactionForm.n2CP eq 'Contaminants'}">selected="selected"</c:if>>Contaminants</option>
					<option value="Abnormal Smell"
						<c:if test="${transactionForm.n2CP eq 'Abnormal Smell'}">selected="selected"</c:if>>Abnormal Smell</option>
				</form:select><br> <form:errors class="errortd" path="n2CP" /></td>				
			<td class="valuetd"><form:select path="inspected"
					style="width:98%;">
					<option value="NULL"
						<c:if test="${empty transactionForm.inspected}">selected="selected"</c:if>>NOT
						SELECTED</option>
					<option value="Y"
						<c:if test="${transactionForm.inspected eq 'Y'}">selected="selected"</c:if>>YES</option>
					<option value="N"
						<c:if test="${transactionForm.inspected eq 'N'}">selected="selected"</c:if>>NO</option>

				</form:select><br> <form:errors class="errortd" path="inspected" /></td>
		</tr>
		<tr>
			<th>Hagberg</th>
			<th>1st PrevLoad</th>
			<th>2nd PrevLoad</th>
			<th>3rd PrevLoad</th>
			<th>TrailerNo</th>
		</tr>
		<tr>
			<td class="valuetd"><form:input path="hagberg"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="hagberg" /></td>
			<td class="valuetd"><form:input path="firstPrevLoad"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="firstPrevLoad" /></td>
			<td class="valuetd"><form:input path="secondPrevLoad"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="secondPrevLoad" /></td>
			<td class="valuetd"><form:input path="thirdPrevLoad"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="thirdPrevLoad" /></td>
			<td class="valuetd"><form:input path="trailerNo"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="trailerNo" /></td>
		</tr>
		<tr>
			<th>ACCSNo</th>
			<th>Holed</th>
			<th>Blind</th>
			<th>Stains</th>
			<th>Comments</th>
		</tr>
		<tr>
			<td class="valuetd"><form:input path="aCCSNo" style="width:98%;" /><br>
				<form:errors class="errortd" path="aCCSNo" /></td>
			<td class="valuetd"><form:input path="holed" style="width:98%;" /><br>
				<form:errors class="errortd" path="holed" /></td>
			<td class="valuetd"><form:input path="blind" style="width:98%;" /><br>
				<form:errors class="errortd" path="blind" /></td>
			<td class="valuetd"><form:input path="stains" style="width:98%;" /><br>
				<form:errors class="errortd" path="stains" /></td>
			<td class="valuetd"><form:input path="comments"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="comments" /></td>
		</tr>
		<tr>
			<th style="text-align: left"><form:button value="Cancel"
					name="Cancel" id="Cancel" class="capsButton">Cancel</form:button></th>
			<th></th>
			<th></th>
			<th></th>
			<th style="text-align: right"><form:button value="Save"
					name="Save" id="Save" class="capsButton" style="width:150px">
					<c:choose>
						<c:when test="${empty transactionForm.id}">
								Save
							</c:when>
						<c:otherwise>
								Update 				
							</c:otherwise>
					</c:choose>
				</form:button> <c:choose>
					<c:when test="${not empty transactionForm.id}">
						<form:button value="Delete" name="Delete" id="Delete"
							class="capsButton" style="width:80px; color:red;">
									Delete
								</form:button>
					</c:when>
				</c:choose></th>
		</tr>
	</table>
</form:form>
