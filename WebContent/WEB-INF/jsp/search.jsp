<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<div id="headerCriteria">
	<form:form action="SearchTransactions" method="POST">
		<table class="rs4table header" style="width: 100%">
			<tr>
				<th>Report Type</th>
				<th>Transaction Type</th>
				<th>Date From</th>
				<th>Customer</th>
				<th>Haulier</th>
				<th></th>
			</tr>
			<tr>
				<td align="center"><form:select
						path="searchCriteria.reportType" style="width:98%">
						<c:forEach var="rt" items="${command.searchCriteria.reportTypes}">
							<option value="${rt}"
								<c:if test="${command.searchCriteria.reportType == rt}">selected="selected"</c:if>>
								${rt}</option>
						</c:forEach>
					</form:select></td>
				<td><form:select path="searchCriteria.transactionTypes"
						size="1" multiple="false" style="width:98%;">
						<c:forEach var="type"
							items="${command.searchCriteria.transactionTypes}">
							<option value="${type}"
								<c:if test="${command.searchCriteria.transactionType == type}">selected="selected"</c:if>>
								${type}</option>
						</c:forEach>
					</form:select></td>
				<td align="center"><form:input path="searchCriteria.dateFrom"
						style="width:96%" type="text" /></td>
				<td align="center"><form:select
						path="searchCriteria.customerCode" size="1" multiple="false"
						style="width:98%">
						<c:if test="${command.searchCriteria.customerCode=='All'}">
							<option
								value="<c:out value="${command.searchCriteria.customerCode}"/>"><c:out
									value="${command.searchCriteria.customerCode}" /></option>
						</c:if>
						<c:if test="${command.searchCriteria.customerCode!='All'}">
							<option
								value="<c:out value="${command.searchCriteria.customerCode}"/>"><c:out
									value="${command.searchCriteria.customerCode}" /></option>
							<option value="All">All</option>
						</c:if>
						<c:forEach var="customer"
							items="${command.searchCriteria.customers}">
							<option value="<c:out value="${customer.code}"/>">
								<c:out value="${customer.code}" />
							</option>
						</c:forEach>
					</form:select></td>
				<td align="center"><form:select path="searchCriteria.hauliers"
						size="1" multiple="false" style="width:98%">
						<c:if test="${command.searchCriteria.haulierCode=='All'}">
							<option
								value="<c:out value="${command.searchCriteria.haulierCode}"/>"><c:out
									value="${command.searchCriteria.haulierCode}" /></option>
						</c:if>
						<c:if test="${command.searchCriteria.haulierCode!='All'}">
							<option
								value="<c:out value="${command.searchCriteria.haulierCode}"/>"><c:out
									value="${command.searchCriteria.haulierCode}" /></option>
							<option value="All">All</option>
						</c:if>
						<c:forEach var="haulier"
							items="${command.searchCriteria.hauliers}">
							<option value="<c:out value="${haulier.code}"/>">
								<c:out value="${haulier.code}" />
							</option>
						</c:forEach>
					</form:select></td>
				<td align="center" valign="middle"><input type="submit"
					id="search" name="Search" value="Search" style="width: 98%" /></td>
			</tr>
			<tr>
				<th>Transaction No.  Registration</th>
				<th>Source</th>
				<th>Date To</th>
				<th>Product</th>
				<th>Destination</th>
				<th></th>
			</tr>
			<tr>
				<td align="center"><form:input
						path="searchCriteria.transactionsNo" type="text" style="width:47%"
						id="transactionsNo" /> <form:input
						path="searchCriteria.registration" type="text" style="width:47%"
						id="registration" /></td>
				<td align="center"><form:select path="searchCriteria.sources"
						size="1" multiple="false" style="width:98%">
						<c:if test="${command.searchCriteria.sourceCode=='All'}">
							<option
								value="<c:out value="${command.searchCriteria.sourceCode}"/>"><c:out
									value="${command.searchCriteria.sourceCode}" /></option>
						</c:if>
						<c:if test="${command.searchCriteria.sourceCode!='All'}">
							<option
								value="<c:out value="${command.searchCriteria.sourceCode}"/>"><c:out
									value="${command.searchCriteria.sourceCode}" /></option>
							<option value="All">All</option>
						</c:if>
						<c:forEach var="source" items="${command.searchCriteria.sources}">
							<option value="<c:out value="${source.code}"/>">
								<c:out value="${source.code}" />
							</option>
						</c:forEach>
					</form:select></td>						
				<td align="center"><form:input path="searchCriteria.dateTo"
						style="width:96%" type="text" /></td>
				<td align="center"><form:select path="searchCriteria.products"
						size="1" multiple="false" style="width:98%">
						<c:if test="${command.searchCriteria.productCode=='All'}">
							<option
								value="<c:out value="${command.searchCriteria.productCode}"/>"><c:out
									value="${command.searchCriteria.productCode}" /></option>
						</c:if>
						<c:if test="${command.searchCriteria.productCode!='All'}">
							<option
								value="<c:out value="${command.searchCriteria.productCode}"/>"><c:out
									value="${command.searchCriteria.productCode}" /></option>
							<option value="All">All</option>
						</c:if>
						<c:forEach var="product"
							items="${command.searchCriteria.products}">
							<option value="<c:out value="${product.code}"/>">
								<c:out value="${product.code}" />
							</option>
						</c:forEach>
					</form:select></td>
				<td align="center"><form:select
						path="searchCriteria.destinations" size="1" multiple="false"
						style="width:98%">
						<c:if test="${command.searchCriteria.destinationCode=='All'}">
							<option
								value="<c:out value="${command.searchCriteria.destinationCode}"/>"><c:out
									value="${command.searchCriteria.destinationCode}" /></option>
						</c:if>
						<c:if test="${command.searchCriteria.destinationCode!='All'}">
							<option
								value="<c:out value="${command.searchCriteria.destinationCode}"/>"><c:out
									value="${command.searchCriteria.destinationCode}" /></option>
							<option value="All">All</option>
						</c:if>
						<c:forEach var="destination"
							items="${command.searchCriteria.destinations}">
							<option value="<c:out value="${destination.code}"/>">
								<c:out value="${destination.code}" />
							</option>
						</c:forEach>
					</form:select></td>
				<td><c:if test="${command.searchResults.accessLevel=='WRITE'}">
						<input type="submit" name="Add" value="Add Transaction"
							style="width: 98%" />
					</c:if></td>
			</tr>
		</table>
	</form:form>
	<c:if test="${not empty addingTransactionsOK}">
		<center>
			<div style="background-color: green; width: 100%; height: 40px">
				<font color="white"> <b><br> <c:out
							value="${addingTransactionsOK}" /> <br> <br></b>
				</font>
			</div>
		</center>
	</c:if>
	<c:if test="${not empty editTransactionsOK}">
		<center>
			<div style="background-color: green; width: 100%; height: 40px">
				<font color="white"> <b><br> <c:out
							value="${editTransactionsOK}" /> <br> <br></b>
				</font>
			</div>
		</center>
	</c:if>
	<c:if test="${not empty deleteTransactionsOK}">
		<center>
			<div style="background-color: green; width: 100%; height: 40px">
				<font color="white"> <b><br> <c:out
							value="${deleteTransactionsOK}" /> <br> <br></b>
				</font>
			</div>
		</center>
	</c:if>
</div>
<c:if test="${command.searchResults.reportType=='Simple'}">
	<div id="records"><jsp:include page="reports/search/simple.jsp" /></div>
</c:if>
<c:if test="${command.searchResults.reportType=='Detailed'}">
	<div id="records"><jsp:include page="reports/search/detailed.jsp" /></div>
</c:if>
