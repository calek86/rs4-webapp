<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<div id="header"
	style="position: absolute; left: 0px; top: 10; width: 1000px; z-index: 1;">
	<img src="resources/images/header1200.png" />
</div>
<div id="rs_"
	style="position: absolute; left: 85px; top: 15px; z-index: 2;">
	<font color="white" style="font-size: 20px">[RS.4]</font>
</div>
<jsp:include page="../version.jsp" />
<form:form action="DoLogin" method="POST">
	<table class="rs4table login"
		style="position: absolute; left: 300px; top: 28px; z-index: 2; width: 700px;">
		<tr>
			<th>Username</th>
			<th>Password</th>
			<th></th>
		</tr>
		<tr>
			<th><form:input path="loginCriteria.userName" style="width:96%"
					type="text" id="username" /></th>
			<th><form:input path="loginCriteria.password" style="width:96%"
					type="password" id="password" /></th>
			<th><input type="submit" value="Login" style="width: 70%;" /></th>
		</tr>
	</table>
</form:form>
<c:if test="${command.errorMessage!='0'}">
	<div
		style="text-align: center; background-color: red; width: 1200px; position: absolute; left: 0px; top: 105px; z-index: 2;">
		<font color="white"><b><br>${command.errorMessage}<br>
				<br></b></font>
	</div>
</c:if>
