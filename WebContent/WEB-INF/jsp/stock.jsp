<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<div id="headerCriteria">
	<form:form action="SearchStock" method="POST">
		<table class="rs4table header" style="width: 100%">
			<tr>
				<th>Report Type</th>
				<th>Customer</th>
				<th>Product</th>
				<th>Group By</th>
				<th></th>
			</tr>
			<tr>
				<td align="center"><form:select id="reportTypeSelect"
						path="stockCriteria.reportType" style="width:98%">
						<c:forEach var="rt" items="${command.stockCriteria.reportTypes}">
							<option value="${rt}"
								<c:if test="${command.stockCriteria.reportType == rt}">selected="selected"</c:if>>
								${rt}</option>
						</c:forEach>
					</form:select></td>
				<td align="center"><form:select id='customersSelect'
						path="stockCriteria.customers" size="1" multiple="false"
						style="width:98%;">
						<c:if test="${command.stockCriteria.customerCode=='All'}">
							<option
								value="<c:out value="${command.stockCriteria.customerCode}"/>"><c:out
									value="${command.stockCriteria.customerCode}" /></option>
						</c:if>
						<c:if test="${command.stockCriteria.customerCode!='All'}">
							<option
								value="<c:out value="${command.stockCriteria.customerCode}"/>"><c:out
									value="${command.stockCriteria.customerCode}" /></option>
							<option value="All">All</option>
						</c:if>
						<c:forEach var="customer"
							items="${command.stockCriteria.customers}">
							<option value="<c:out value="${customer.code}"/>">
								<c:out value="${customer.code}" />
							</option>
						</c:forEach>
					</form:select></td>
				<td align="center"><form:select id='productsSelect'
						path="stockCriteria.products" size="1" multiple="false"
						style="width:98%">
						<c:if test="${command.stockCriteria.productCode=='All'}">
							<option
								value="<c:out value="${command.stockCriteria.productCode}"/>"><c:out
									value="${command.stockCriteria.productCode}" /></option>
						</c:if>
						<c:if test="${command.stockCriteria.productCode!='All'}">
							<option
								value="<c:out value="${command.stockCriteria.productCode}"/>"><c:out
									value="${command.stockCriteria.productCode}" /></option>
							<option value="All">All</option>
						</c:if>
						<c:forEach var="product" items="${command.stockCriteria.products}">
							<option value="<c:out value="${product.code}"/>">
								<c:out value="${product.code}" />
							</option>
						</c:forEach>
					</form:select></td>
				<td align="center"><form:select id='groupBySelect'
						path="stockCriteria.groupBy" size="1" multiple="false"
						style="width:98%">
						<c:forEach var="grouping"
							items="${command.stockCriteria.groupByList}">
							<option value="${grouping}"
								<c:if test="${command.stockCriteria.groupBy == grouping}">selected="selected"</c:if>>
								${grouping}</option>
						</c:forEach>
					</form:select></td>
				<td><input type="submit" value="Search" style="width: 98%;" />
				</td>
			</tr>
		</table>
	</form:form>
</div>
<c:if test="${command.stockResults.reportType=='Control'}">
	<div><jsp:include page="reports/stock/control.jsp" /></div>
</c:if>
<c:if test="${command.stockResults.reportType=='Detailed'}">
	<div><jsp:include page="reports/stock/detailed.jsp" /></div>
</c:if>
<c:if test="${command.stockResults.reportType=='Annual'}">
	<div><jsp:include page="reports/stock/annual.jsp" /></div>
</c:if>
