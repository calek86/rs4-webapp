<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<form:form modelAttribute="beansSampleForm" method="post">
	<table class="rs4table header" border="1">
		<tr>
			<th>Stock Year</th>
			<th>Date</th>
			<th>Customer</th>
			<th>Product</th>
			<th>Tonnage (kg)</th>
			<th>Moisture (%)</th>			
		</tr>
		<tr>
			<td class="valuetd"><form:select path="stockYearWithId" size="1"
					multiple="false" style="width:98%;">
					<c:forEach var="st" items="${beansSampleForm.stockYears}">
						id=${st.description} curr=${beansSampleForm.currentStockYear}
							<c:choose>
							<c:when
								test="${st.description == beansSampleForm.currentStockYear}">
								<option value="${st.id}|${st.description}" selected="selected">
									<c:out value="${st.description}" />
								</option>
							</c:when>
							<c:otherwise>
								<option value="${st.id}|${st.description}">
									<c:out value="${st.description}" />
								</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</form:select> <br> <form:errors class="errortd" path="stockYearWithId" /></td>
			<td class="valuetd"><form:input path="sampleDate"
					type="text" style="width:98%;" /> <br> <form:errors
					class="errortd" path="sampleDate" /></td>				
			<td class="valuetd"><form:select path="customer" size="1"
					multiple="false" style="width:98%;">
					<c:forEach var="cust" items="${beansSampleForm.customers}">
						<c:set var="custIdCode" value="${cust.id}|${cust.code}" />
						<option value="${custIdCode}"
							<c:if test="${beansSampleForm.customer == cust.code}">selected="selected"</c:if>>
							<c:out value="${cust.code}" />
						</option>
					</c:forEach>
				</form:select> <br> <form:errors class="errortd" path="customer" /></td>
			<td class="valuetd"><form:select path="product" size="1"
					multiple="false" style="width:98%;">
					<c:forEach var="pr" items="${beansSampleForm.products}">
						<c:set var="productIdCode" value="${pr.id}|${pr.code}" />
						<option value="${productIdCode}"
							<c:if test="${beansSampleForm.product == pr.code}">selected="selected"</c:if>>
							<c:out value="${pr.code}" />
						</option>
					</c:forEach>
				</form:select> <br> <form:errors class="errortd" path="product" /></td>
			<td class="valuetd"><form:input path="tonnage"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="tonnage" /></td>		
			<td class="valuetd"><form:input path="moisture"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="moisture" /></td>				
		</tr>
		<tr>
			<th>Admix (%)</th>
			<th>Temp. (�C)</th>
			<th>Screen1</th>
			<th>Screen2</th>
			<th>ReferenceNo</th>
			<th>KgHl</th>			
		</tr>
		<tr>
			<td class="valuetd"><form:input path="admix" style="width:98%;" /></td>
			<td class="valuetd"><form:input path="temperature"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="temperature" /></td>
			<td class="valuetd"><form:input path="screen1"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="screen1" /></td>
			<td class="valuetd"><form:input path="screen2"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="screen2" /></td>
			<td class="valuetd"><form:input path="referenceNo"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="referenceNo" /></td>
			<td class="valuetd"><form:input path="kgHl" style="width:98%;" /><br>
				<form:errors class="errortd" path="kgHl" /></td>
		</tr>
		<tr>
		</tr>
		<tr>
		</tr>
		<tr>
			<th>Processable</th>
			<th>Holed</th>
			<th>Blind</th>
			<th>Stains</th>
			<th colspan="2">Comments</th>
		</tr>
		<tr>
			<td class="valuetd"><form:select path="processable"
					style="width:98%;">
					<option value="NULL"
						<c:if test="${empty beansSampleForm.processable}">selected="selected"</c:if>>NOT
						SELECTED</option>
					<option value="Y"
						<c:if test="${beansSampleForm.processable eq 'Y'}">selected="selected"</c:if>>YES</option>
					<option value="N"
						<c:if test="${beansSampleForm.processable eq 'N'}">selected="selected"</c:if>>NO</option>
				</form:select><br> <form:errors class="errortd" path="processable" /></td>		
			<td class="valuetd"><form:input path="holed" style="width:98%;" /><br>
				<form:errors class="errortd" path="holed" /></td>
			<td class="valuetd"><form:input path="blind" style="width:98%;" /><br>
				<form:errors class="errortd" path="blind" /></td>
			<td class="valuetd"><form:input path="stains" style="width:98%;" /><br>
				<form:errors class="errortd" path="stains" /></td>
			<td class="valuetd" colspan="2"><form:input path="comments"
					style="width:98%;" /><br> <form:errors class="errortd"
					path="comments" /></td>
		</tr>
		<tr>
			<th style="text-align: left"><form:button value="Cancel"
					name="Cancel" id="Cancel" class="capsButton">Cancel</form:button></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th style="text-align: right"><form:button value="Save"
					name="Save" id="Save" class="capsButton" style="width:100px">
					<c:choose>
						<c:when test="${empty beansSampleForm.id}">
								Save
							</c:when>
						<c:otherwise>
								Update 				
							</c:otherwise>
					</c:choose>
				</form:button> <c:choose>
					<c:when test="${not empty beansSampleForm.id}">
						<form:button value="Delete" name="Delete" id="Delete"
							class="capsButton" style="width:80px; color:red;">
									Delete
								</form:button>
					</c:when>
				</c:choose></th>
		</tr>
	</table>
</form:form>
