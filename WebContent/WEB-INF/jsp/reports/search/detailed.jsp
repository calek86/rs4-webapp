<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<c:if test="${empty command.searchResults.resultTransactions}">
	<div align="center" id="noRecordsDiv">
		<font color="white"><b><br>No records in the system
				for specified search criteria.<br> <br></b> </font>
	</div>
</c:if>
<c:if test="${!empty command.searchResults.resultTransactions}">
	<div id="headerRecords"><jsp:include page="header.jsp" />
		<table class="rs4table" border="1" style="width: 1200px;">
			<tr>
				<th style="width: 100px" style="text-align: center"><a
					href="SearchTransactions/detailedExcel"
					style="background-color: #1B8EE0;"><img
						src="resources/images/save-16.png" style="vertical-align: middle;"></a>
					No.</th>
				<th>Type</th>
				<th style="width: 140px">Date</th>
				<th>Customer</th>
				<th>Haulier</th>
				<th>Product</th>
				<th>Destination</th>
				<th>Source</th>
				<th>1st Wt.</th>
				<th>2nd Wt.</th>
				<th>Net.Wt.</th>
				<th>Ent.Wt.</th>
				<!-- <th style="width: 120px">Ent.Wt.Beans</th> -->
			</tr>
			<tr>
				<th>StockYear</th>
				<th>Reg.No.</th>
				<th>% Loss</th>
				<th>% Loss Beans</th>
				<th>Moisture</th>
				<th>Admix</th>
				<th>SpWt</th>
				<th>Temp.</th>
				<th>Screen1</th>
				<th>Screen2</th>
				<th>OrderNo</th>
				<th>ExRef</th>
			</tr>
			<tr>
				<th>Contaminants</th>
				<th>Hagberg</th>
				<th>1st PrevLoad</th>
				<th>2nd PrevLoad</th>
				<th>3rd PrevLoad</th>
				<th>TrailerNo</th>
				<th>Inspected</th>
				<th>ACCSNo</th>
				<th>Holed</th>
				<th>Blind</th>
				<th>Stains</th>
				<th>Comments</th>
			</tr>
		</table>
	</div>
	<div id="loadingDiv">
		<div align="center">
			<font color="grey"><b><br>Loading...<br>
				<br></b></font>
		</div>
	</div>
	<div id="recordsDiv">
		<table class="rs4table records">
			<c:forEach items="${command.searchResults.resultTransactions}"
				var="transaction">
				<tr>
					<td style="text-align: center; width: 100px"><c:if
							test="${command.searchResults.accessLevel=='WRITE'}">
							<form:form action="EditTransaction" method="POST">
								<input type="hidden" name="idToEdit" value="${transaction.id}">
								<input type="submit" value="" id="edit"
									style="background-color: #FAFAFA; background-image: url('resources/images/edit-16.png'); float: left; margin-left: 5px; margin-right: -20px; border: solid 0px #000000; width: 16px; height: 16px; cursor: pointer; opacity: 1.0;" />
							</form:form>
						</c:if> ${transaction.ticketId}</td>
					<td>${transaction.transactionType}</td>
					<td style="width: 140px">${transaction.transactionDateDisplay}</td>
					<td>${transaction.customer}</td>
					<td>${transaction.haulier}</td>
					<td>${transaction.product}</td>
					<td>${transaction.destination}</td>
					<td>${transaction.source}</td>
					<td>${transaction.firstWeightDisplay}</td>
					<td>${transaction.secondWeightDisplay}</td>
					<td>${transaction.netWeightDisplay}</td>
					<td>${transaction.entWeightDisplay}</td>
					<!-- <td style="width: 120px">${transaction.entWeightBeansDisplay}</td> -->
				</tr>
				<tr>
					<td>${transaction.stockYear}</td>
					<td>${transaction.registration}</td>
					<td>${transaction.percentLossDisplay}</td>
					<td>${transaction.percentLossBeansDisplay}</td>
					<td>${transaction.moistureDisplay}</td>
					<td>${transaction.admixDisplay}</td>
					<td>${transaction.spWtDisplay}</td>
					<td>${transaction.temperatureDisplay}</td>
					<td>${transaction.screen1}</td>
					<td>${transaction.screen2}</td>
					<td>${transaction.orderNo}</td>
					<td>${transaction.exRef}</td>
				</tr>
				<tr>
					<td>${transaction.n2CP}</td>
					<td>${transaction.hagberg}</td>
					<td>${transaction.firstPrevLoad}</td>
					<td>${transaction.secondPrevLoad}</td>
					<td>${transaction.thirdPrevLoad}</td>
					<td>${transaction.trailerNo}</td>
					<td>${transaction.inspected}</td>
					<td>${transaction.aCCSNo}</td>
					<td>${transaction.holed}</td>
					<td>${transaction.blind}</td>
					<td>${transaction.stains}</td>
					<td>${transaction.comments}</td>
				</tr>
				<tr>
					<td colspan="12" style="background-color: #1B8EE0; height: 0px"></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</c:if>
