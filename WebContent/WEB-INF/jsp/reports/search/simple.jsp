<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<c:if test="${empty command.searchResults.resultTransactions}">
	<div align="center" id="noRecordsDiv">
		<font color="white"><b><br>No records in the system
				for specified search criteria.<br> <br></b> </font>
	</div>
</c:if>
<c:if test="${!empty command.searchResults.resultTransactions}">
	<div id="headerRecords"><jsp:include page="header.jsp" />
		<table class="rs4table" border="1">
			<tr>
				<th style="width: 80px;"><a
					href="SearchTransactions/simpleExcel"
					style="background-color: #1B8EE0;"><img
						src="resources/images/save-16.png" style="vertical-align: middle;"></a>
					No.</th>
				<th style="width: 40px;">Type</th>
				<th style="width: 140px;">Date</th>
				<th style="width: 100px;">Customer</th>
				<th style="width: 100px;">Haulier</th>
				<th style="width: 100px;">Product</th>
				<th style="width: 80px;">Destination</th>
				<th style="width: 70px;">Reg.No.</th>
				<th style="width: 64px;">Net.Wt.</th>
				<th style="width: 70px;">Ent.Wt.</th>
				<th style="width: 60px;">% Loss</th>
				<th style="width: 50px;">Moisture</th>
				<th style="width: 50px;">Admix</th>
				<th style="width: 50px;">Temp.</th>
				<th style="width: 60px;">Sp.Wt.</th>
			</tr>
		</table>
	</div>
	<div id="loadingDiv">
		<div align="center">
			<font color="grey"><b><br>Loading...<br><br></b></font>
		</div>
	</div>
	<div id="recordsDiv">
		<table class="rs4table records">
			<c:forEach items="${command.searchResults.resultTransactions}"
				var="transaction">
				<tr>
					<td style="text-align: center; padding-left: 20px; width: 60px;"><c:if
							test="${command.searchResults.accessLevel=='WRITE'}">
							<form:form action="EditTransaction" method="POST">
								<input type="hidden" name="idToEdit" value="${transaction.id}">
								<input type="submit" value="" id="edit"
									style="float: left; margin-left: -15px; background-color: #FAFAFA; background-image: url('resources/images/edit-16.png'); border: solid 0px #000000; width: 16px; height: 16px; cursor: pointer; opacity: 1.0;" />
							</form:form>
						</c:if> ${transaction.ticketId}</td>
					<td style="width: 40px;">${transaction.transactionType}</td>
					<td style="width: 140px;">${transaction.transactionDateDisplay}</td>
					<td style="width: 100px;">${transaction.customer}</td>
					<td style="width: 100px;">${transaction.haulier}</td>
					<td style="width: 100px;">${transaction.product}</td>
					<td style="width: 80px;">${transaction.destination}</td>
					<td style="width: 70px;">${transaction.registration}</td>
					<td style="width: 64px;">${transaction.netWeightDisplay}</td>
					<td style="width: 70px;">${transaction.entWeightDisplay}</td>
					<td style="width: 60px;">${transaction.percentLossDisplay}</td>
					<td style="width: 50px;">${transaction.moistureDisplay}</td>
					<td style="width: 50px;">${transaction.admixDisplay}</td>
					<td style="width: 50px;">${transaction.temperatureDisplay}</td>
					<td style="width: 60px;">${transaction.spWtDisplay}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</c:if>
