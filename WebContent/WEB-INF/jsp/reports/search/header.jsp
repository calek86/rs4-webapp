<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<table class="rs4table header" border="1">
	<tr>
		<th>Report Date</th>
		<th>Total Net.Wt.</th>
		<th>Total Ent.Wt.</th>
		<th>Avg. Moisture</th>
		<th>Avg. Admix</th>
		<th>Avg. Temperature</th>
		<th>Avg. Sp.Wt.</th>
	</tr>
	<tr>
		<td>${command.searchResults.reportDate}</td>
		<td>${command.searchResults.resultsSummary.totalNetWt}</td>
		<td>${command.searchResults.resultsSummary.totalEntWt}</td>
		<td>${command.searchResults.resultsSummary.avgMoisture}</td>
		<td>${command.searchResults.resultsSummary.avgAdmix}</td>
		<td>${command.searchResults.resultsSummary.avgTemperature}</td>
		<td>${command.searchResults.resultsSummary.avgSpWt}</td>
	</tr>
</table>
