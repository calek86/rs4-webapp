<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<c:if test="${!empty command.settingsResults.resultDeductionGroups}">
	<div id="headerRecords"></div>
	<div id="loadingDiv">
		<div align="center">
			<font color="grey"><b><br>Loading...<br> <br></b></font>
		</div>
	</div>
	<div id="recordsDiv">
		<table class="rs4table records">
		<c:set var="groupCount" value="0"/>
			<c:forEach items="${command.settingsResults.resultDeductionGroups}"
				var="group">
				<tr>
					<th style="width: 60px;">${group.id}</th>
					<c:if test="${empty command.settingsResults.resultDeductionGroups[groupCount].products}">
						<th>${group.code}</th>
						<th colspan="4">${group.description}</th>
					</c:if>
					<c:if test="${not empty command.settingsResults.resultDeductionGroups[groupCount].products}">
						<th>${group.code}</th>
						<th>${group.description}</th>
						<th style="text-align:right">Products:</th>
						<form:form action="SettingsUnassignProduct" method="POST">
							<th>
								<c:set var="cmd1Str" value="settingsResults.resultDeductionGroups["/>
								<c:set var="cmd2Str" value="].products"/>
								<form:select path="${cmd1Str}${groupCount}${cmd2Str}" size="1"
		 								multiple="false" style="width:99%">
		 								<c:forEach var="product"
		 									items="${command.settingsResults.resultDeductionGroups[groupCount].products}">
		 									<option value="<c:out value="${product.code}"/>">
		 										<c:out value="${product.code}" />
											</option>
										</c:forEach>
								</form:select>
							</th>
							<th>
								<input type="hidden" name="deductionGroupCount" value="${groupCount}">
								<input type="hidden" name="deductionGroupId" value="${group.id}">
								<input type="submit" value="Unassign Product" style="width: 99%" />
							</th>
						</form:form>
					</c:if>
				</tr>
				<th>No.</th>
				<th>Moisture From</th>
				<th>Moisture To</th>
				<th>Moisture Deduction</th>
				<th>Intake Cost</th>
				<th>Drying Cost</th>
				<c:forEach items="${group.deductions}" var="deduction">
					<tr>
						<td style="text-align: center; padding-left: 20px; width: 40px"><c:if
								test="${command.settingsResults.accessLevel == 'WRITE'}">
								<form:form action="DeleteOneMoistureDeduction" method="POST">
									<input type="hidden" name="idToDelete" value="${deduction.id}">
									<input type="submit" value="" id="bi"
										style="float: left; margin-left: -15px; background-color: white; background-image: url('resources/images/delete-16.png'); border: solid 0px #000000; width: 16px; height: 16px; cursor: pointer;" />
								</form:form>
							</c:if> ${deduction.id}</td>
						<td>${deduction.moistureFromDisplay}</td>
						<td>${deduction.moistureToDisplay}</td>
						<td>${deduction.moistureDeductionDisplay}</td>
						<td>${deduction.intakeCostDisplay}</td>
						<td>${deduction.dryingCostDisplay}</td>
					</tr>
				</c:forEach>
				<c:set var="groupCount" value="${count + 1}"/>
			</c:forEach>
		</table>
	</div>
</c:if>
