<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<table class="rs4table header" border="1">
	<tr>
		<th>Bill Date</th>
		<th>Customer</th>
		<th>Date From</th>
		<th>Date To</th>
	</tr>
	<tr>
		<td>${command.billsResults.reportDate}</td>
		<td>${command.billsResults.billsSummary.customer}</td>
		<td>${command.billsResults.billsSummary.fromDateDisplay}</td>
		<td>${command.billsResults.billsSummary.toDateDisplay}</td>
	</tr>
	<tr>
		<th><a href="SearchBills/storageExcel"
			style="background-color: #1B8EE0; float: left; margin-left: 5px; margin-right: -20px; vertical-align: middle;"><img
				src="resources/images/save-16.png"></a>Total Days</th>
		<th>Total Day Tonnage</th>
		<th>Tonne/Day Rate</th>
		<th>Total Cost</th>
	</tr>
	<tr>
		<td>${command.billsResults.billsSummary.totalDays}</td>
		<td>${command.billsResults.billsSummary.totalDayTonnageDisplay}</td>
		<td>${command.billsResults.billsSummary.storingRateDisplay}</td>
		<td>${command.billsResults.billsSummary.totalCostDisplay}</td>
	</tr>
</table>