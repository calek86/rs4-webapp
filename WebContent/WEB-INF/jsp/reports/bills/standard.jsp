<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:if test="${empty command.billsResults.resultStandardBills}">
	<div align="center" id="noRecordsDiv">
		<font color="white"><b><br>No records in the system
				for specified search criteria.<br> <br></b> </font>
	</div>
</c:if>
<c:if test="${!empty command.billsResults.resultStandardBills}">
	<div id="headerRecords"><jsp:include page="standard_header.jsp" />
		<table class="rs4table" border="1">
			<tr>
				<th style="width: 80px;"><a href="SearchBills/standardExcel"
					style="background-color: #1B8EE0;"><img
						src="resources/images/save-16.png" style="vertical-align: middle;"></a>
					No.</th>
				<th style="width: 140px;">Date</th>
				<th style="width: 100px;">Customer</th>
				<th style="width: 100px;">Product</th>
				<th style="width: 80px;">Net.Wt.</th>
				<th>Moisture</th>
				<th>Admix</th>
				<th>Temperature</th>
				<th>Intake Cost</th>
				<th>Drying Cost</th>
				<th>Total Cost</th>
			</tr>
		</table>
	</div>
	<div id="loadingDiv">
		<div align="center">
			<font color="grey"><b><br>Loading...<br>
				<br></b></font>
		</div>
	</div>
	<div id="recordsDiv">
		<table class="rs4table records">
			<c:forEach items="${command.billsResults.resultStandardBills}"
				var="transactionForBill">
				<tr>
					<td style="width: 80px;">${transactionForBill.transaction.ticketId}</td>
					<td style="width: 140px;">${transactionForBill.transaction.transactionDateDisplay}</td>
					<td style="width: 100px;">${transactionForBill.transaction.customer}</td>
					<td style="width: 100px;">${transactionForBill.transaction.product}</td>
					<td style="width: 80px;">${transactionForBill.transaction.netWeightDisplay}</td>
					<td>${transactionForBill.transaction.moistureDisplay}</td>
					<td>${transactionForBill.transaction.admixDisplay}</td>
					<td>${transactionForBill.transaction.temperatureDisplay}</td>
					<td>${transactionForBill.intakeCostDisplay}</td>
					<td>${transactionForBill.dryingCostDisplay}</td>
					<td>${transactionForBill.totalCostDisplay}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</c:if>
