<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:if test="${empty command.billsResults.resultStorageProductBills}">
	<div align="center" id="noRecordsDiv">
		<font color="white"><b><br>No records in the system
				for specified search criteria.<br> <br></b> </font>
	</div>
</c:if>
<c:if test="${!empty command.billsResults.resultStorageProductBills}">
	<div id="headerRecords"><jsp:include page="storage_header.jsp" /></div>
	<div id="loadingDiv">
		<div align="center">
			<font color="grey"><b><br>Loading...<br> <br></b></font>
		</div>
	</div>
	<div id="recordsDiv">
		<table class="rs4table records" border="1">
			<c:forEach items="${command.billsResults.resultStorageProductBills}"
				var="storageProductBill" varStatus="status">
				<tr>
					<th style="width: 60px;">No.</th>
					<th>Date</th>
					<th>Ent.Wt. Change</th>
					<th>Product: ${storageProductBill.product}</th>
					<th>Total DT: ${storageProductBill.totalDayTonnageDisplay}</th>
					<th>Total Cost: ${storageProductBill.totalCostDisplay}</th>
				</tr>
				<c:forEach items="${storageProductBill.resultStorageBills}"
					var="storageBill">
					<tr>
						<td>${storageBill.dayCountDisplay}</td>
						<td>${storageBill.dateDisplay}</td>
						<td>${storageBill.entWtChangeDisplay}</td>
						<td>${storageBill.entWtStockDisplay}</td>
						<td>${storageBill.dayTonnageDisplay}</td>
						<td>${storageBill.dayCostDisplay}</td>
					</tr>
				</c:forEach>
			</c:forEach>
		</table>
	</div>
</c:if>
