<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<table class="rs4table header" border="1">
	<tr>
		<th style="width:200px">Bill Date</th>
		<th>Customer</th>
		<th>Product</th>
		<th>Avg. Moisture</th>
		<th>Avg. Admix</th>
		<th>Avg. Temperature</th>
		<th>Total Net.Wt.</th>
		<th>Total Cost</th>
	</tr>
	<tr>
		<td>${command.billsResults.reportDate}</td>
		<td>${command.billsResults.billsSummary.customer}</td>
		<td>${command.billsResults.billsSummary.product}</td>
		<td>${command.billsResults.billsSummary.avgMoistureDisplay}</td>
		<td>${command.billsResults.billsSummary.avgAdmixDisplay}</td>
		<td>${command.billsResults.billsSummary.avgTemperatureDisplay}</td>
		<td>${command.billsResults.billsSummary.totalNetWtDisplay}</td>
		<td>${command.billsResults.billsSummary.totalCostDisplay}</td>
	</tr>
</table>