<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<table class="rs4table header" border="1">
	<tr>
		<th>Report Date</th>
		<th>Avg. Moisture</th>
		<th>Avg. Admix</th>
		<th>Avg. Temperature</th>
		<th>Avg. Sp.Wt.</th>
		<th>Total Stock</th>
	</tr>
	<tr>
		<td>${command.stockResults.reportDate}</td>
		<td>${command.stockResults.resultsSummary.avgMoisture}</td>
		<td>${command.stockResults.resultsSummary.avgAdmix}</td>
		<td>${command.stockResults.resultsSummary.avgTemperature}</td>
		<td>${command.stockResults.resultsSummary.avgSpWt}</td>
		<td>${command.stockResults.resultsSummary.totalEntWt}</td>
	</tr>
</table>
