<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:if test="${empty command.stockResults.stockAnnualReportForm.map}">
	<div align="center" id="noRecordsDiv">
		<font color="white"><b><br>No records in the system
				for specified search criteria.<br> <br></b> </font>
	</div>
</c:if>
<c:if test="${!empty command.stockResults.stockAnnualReportForm.map}">
	<div id="headerRecords"><jsp:include page="annual_header.jsp" />
		<table class="rs4table" border="1">
			<tr>
				<th><a href="SearchStock/annualExcel"
					style="background-color: #1B8EE0; float: left; margin-left: 5px; margin-right: -20px;">
						<img src="resources/images/save-16.png">
				</a>Product</th>
				<th>Entitlement In</th>
				<th>Weight Out</th>
				<th>Store Total</th>
			</tr>
		</table>
	</div>
	<div id="loadingDiv">
		<div align="center">
			<font color="grey"><b><br>Loading...<br>
				<br></b></font>
		</div>
	</div>
	<div id="recordsDiv">
		<table class="rs4table records">
			<c:forEach items="${command.stockResults.stockAnnualReportForm.map}"
				var="product">
				<tr>
					<td>${product.key}</td>
					<td>${product.value.entInDisplay}</td>
					<td>${product.value.weightOutDisplay}</td>
					<td>${product.value.storeTotalDisplay}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</c:if>
