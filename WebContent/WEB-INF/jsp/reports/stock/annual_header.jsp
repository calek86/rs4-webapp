<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<table class="rs4table header" border="1">
	<tr>
		<th>Total Entitlement In</th>
		<th>Total Weight Out</th>
		<th>Store Total</th>
	</tr>
	<tr>
		<td>${command.stockResults.stockAnnualReportForm.totalInDisplay}</td>
		<td>${command.stockResults.stockAnnualReportForm.totalOutDisplay}</td>
		<td>${command.stockResults.stockAnnualReportForm.storeTotalDisplay}</td>
	</tr>
</table>