<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:if test="${empty command.stockResults.stockFormList}">
	<div align="center" id="noRecordsDiv">
		<font color="white"><b><br>No records in the system
				for specified search criteria.<br> <br></b> </font>
	</div>
</c:if>
<c:if test="${!empty command.stockResults.stockFormList}">
	<div id="headerRecords"><jsp:include page="detailed_header.jsp" />
		<table class="rs4table" border="1">
			<tr>
				<th style="width: 45px;"><a href="SearchStock/detailedExcel"
					style="background-color: #1B8EE0; float: left; margin-left: 5px;">
						<img src="resources/images/save-16.png">
				</a></th>
				<th>Customer</th>
				<th>Product</th>
				<th>Total</th>
			</tr>
		</table>
	</div>
	<div id="loadingDiv">
		<div align="center">
			<font color="grey"><b><br>Loading...<br> <br></b></font>
		</div>
	</div>
	<div id="recordsDiv">
		<table class="rs4table records">
			<c:forEach items="${command.stockResults.stockFormList}"
				var="stockRecord">
				<c:if test="${stockRecord.id>0}">
					<tr>
						<th style="width: 45px;"></th>
						<th>${stockRecord.customer}</th>
						<th>${stockRecord.product}</th>
						<th>${stockRecord.totalValue}</th>
					</tr>
					<c:if test="${not empty stockRecord.transactions}">
						<tr>
							<td colspan="4" style="padding: 1px 0px 0px 0px;">
								<table class="rs4table withMargins">
									<tr>
										<th style="width: 50px;">No.</th>
										<th style="width: 35px;">Type</th>
										<th style="width: 100px;">Date</th>
										<th style="width: 60px;">Haulier</th>
										<th style="width: 60px;">Destination</th>
										<th style="width: 60px;">Reg.No.</th>
										<th style="width: 60px;">Net.Wt.</th>
										<th style="width: 60px;">Ent.Wt.</th>
										<th style="width: 60px;">1st Weight</th>
										<th style="width: 60px;">2nd Weight</th>
										<th style="width: 60px;">Order No.</th>
										<th style="width: 60px;">Ex Ref.</th>
									</tr>
									<c:forEach items="${stockRecord.transactions}"
										var="transaction">
										<tr>
											<td>${transaction.ticketId}</td>
											<td>${transaction.transactionType}</td>
											<td>${transaction.transactionDate}</td>
											<td>${transaction.haulier}</td>
											<td>${transaction.destination}</td>
											<td>${transaction.registration}</td>
											<td>${transaction.netWeight}</td>
											<c:if
												test="${transaction.entWeight==transaction.entWeightBeans}">
												<td>${transaction.entWeight}</td>
											</c:if>
											<c:if
												test="${transaction.entWeight!=transaction.entWeightBeans}">
												<td>${transaction.entWeightBeans}</td>
											</c:if>
											<td>${transaction.firstWeight}</td>
											<td>${transaction.secondWeight}</td>
											<td>${transaction.orderNo}</td>
											<td>${transaction.exRef}</td>
										</tr>
									</c:forEach>
								</table>
							</td>
						</tr>
					</c:if>
				</c:if>
			</c:forEach>
		</table>
	</div>
</c:if>
