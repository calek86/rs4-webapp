<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<c:if test="${empty command.stockResults.resultStockRecords}">
	<div align="center" id="noRecordsDiv">
		<font color="white"><b><br>No records in the system
				for specified search criteria.<br> <br></b> </font>
	</div>
</c:if>
<c:if test="${!empty command.stockResults.resultStockRecords}">
	<div id="headerRecords"><jsp:include page="control_header.jsp" />
		<table class="rs4table" border="1">
			<tr>
				<th><a href="SearchStock/controlExcel"
					style="background-color: #1B8EE0; float: left; margin-left: 5px; margin-right: -20px;">
						<img src="resources/images/save-16.png">
				</a>Customer</th>
				<th>Product</th>
				<th>Total</th>
			</tr>
		</table>
	</div>
	<div id="loadingDiv">
		<div align="center">
			<font color="grey"><b><br>Loading...<br> <br></b></font>
		</div>
	</div>
	<div id="recordsDiv">
		<table class="rs4table records">
			<c:forEach items="${command.stockResults.resultStockRecords}"
				var="stockRecord">
				<c:if test="${stockRecord.id>0}">
					<tr>
						<td>${stockRecord.customerCode}</td>
						<td>${stockRecord.productCode}</td>
						<td>${stockRecord.totalValueDisplay}</td>
					</tr>
				</c:if>
				<c:if test="${stockRecord.id==-1}">
					<tr>
						<td></td>
						<td></td>
						<td>${stockRecord.totalValueDisplay}</td>
					</tr>
					<tr>
						<td colspan="3" style="background-color: #1B8EE0; height: 0px"></td>
					</tr>
				</c:if>
			</c:forEach>
		</table>
	</div>
</c:if>
