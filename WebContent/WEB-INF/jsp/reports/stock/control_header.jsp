<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<table class="rs4table header" border="1">
	<tr>
		<th>Report Date</th>
		<th>Stock Year</th>
		<th>Total Stock</th>
	</tr>
	<tr>
		<td>${command.stockResults.reportDate}</td>
		<td>${command.stockResults.stockYear}</td>
		<td>${command.stockResults.resultsSummary.totalEntWt}</td>
	</tr>
</table>
