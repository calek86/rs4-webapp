<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<c:if test="${empty command.sensorsResults.resultSamples}">
	<div align="center" id="noRecordsDiv">
		<font color="white"><b><br>No sensors registered in
				the system yet.<br> <br></b> </font>
	</div>
</c:if>
<c:if test="${!empty command.sensorsResults.resultSamples}">
	<div id="recordsChart"
		style="position: absolute; left: 0px; width: 1200px; z-index: 1;">
		<table class="rs4table records">
			<tr>
				<th style="text-align: left;">Temperature [�C]</th>
				<th style="text-align: center;">Showing
					${fn:length(command.sensorsResults.resultSamples)} samples</th>
				<th style="text-align: right;">Humidity [%]</th>
			</tr>
		</table>
		<img src="resources/images/sensors-chart-1200.png" />
		<c:forEach items="${command.sensorsResults.resultSamples}"
			var="sample" varStatus="sensorloop">
			<div id="temppoint"
				style="
				position: absolute; 
				background-color: red; 
				left: ${32+sensorloop.index*(1136/fn:length(command.sensorsResults.resultSamples))}px; 
				top: ${226-(sample.temperature*(25/5))}px; 
				width: ${1136/fn:length(command.sensorsResults.resultSamples)}px; 
				height: 2px;"
				title="${sample.temperatureDisplay} : ${sample.publishedDisplay} : ${sample.id}"></div>
			<div id="humidpoint"
				style="
				position: absolute; 
				background-color: darkgreen; 
				left: ${32+sensorloop.index*(1136/fn:length(command.sensorsResults.resultSamples))}px; 
				top: ${276-(sample.humidity*(25/10))}px; 
				width: ${1136/fn:length(command.sensorsResults.resultSamples)}px; 
				height: 2px;"
				title="${sample.humidityDisplay} : ${sample.publishedDisplay} : ${sample.id}"></div>
		</c:forEach>
	</div>
	<div id="headerRecords" style="position: absolute; top: 407px">
		<table class="rs4table" border="1">
			<tr>
				<th style="width: 250px;">Sample Id</th>
				<th style="width: 180px;">Date</th>
				<th>Temperature</th>
				<th>Humidity</th>
				<th>Signal Strength</th>
				<th>Voltage</th>
			</tr>
		</table>
	</div>
	<div id="loadingDiv" style="position: absolute; top: 433px">
		<div align="center">
			<font color="grey"><b><br>Loading...<br> <br></b></font>
		</div>
	</div>
	<div id="recordsDiv">
		<table class="rs4table records">
			<c:forEach items="${command.sensorsResults.resultSamples}"
				var="sample">
				<tr>
					<td style="width: 250px;">${sample.id}</td>
					<td style="width: 180px;">${sample.publishedDisplay}</td>
					<td>${sample.temperatureDisplay}</td>
					<td>${sample.humidityDisplay}</td>
					<td>${sample.signalStrengthDisplay}</td>
					<td>${sample.voltageDisplay}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</c:if>
