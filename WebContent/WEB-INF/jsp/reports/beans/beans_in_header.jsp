<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<table class="rs4table header" border="1">
	<tr>
		<th colspan="2">Report Date</th>
		<th>Total Net.Wt.</th>
		<th>Total Ent.Wt.</th>
		<th>Avg. % Loss</th>
		<th>Avg. Moisture</th>
		<th>Avg. Admix</th>
	</tr>
	<tr>
		<td colspan="2">${command.beansResults.reportDate}</td>
		<td>${command.beansResults.beansInResultsSummary.searchResultSummary.totalNetWt}</td>
		<td>${command.beansResults.beansInResultsSummary.searchResultSummary.totalEntWt}</td>
		<td>${command.beansResults.beansInResultsSummary.searchResultSummary.avgPercentLoss}</td>
		<td>${command.beansResults.beansInResultsSummary.searchResultSummary.avgMoisture}</td>
		<td>${command.beansResults.beansInResultsSummary.searchResultSummary.avgAdmix}</td>
	</tr>
	<tr>
		<th>Avg. Screen</th>
		<th>Avg. Holed</th>
		<th>Avg. Blind</th>
		<th>Avg. Stains</th>
		<th>Avg. Deduction</th>
		<th>Avg. Feed</th>
		<th>Avg. Head</th>
	</tr>
	<tr>
		<td>${command.beansResults.beansInResultsSummary.avgScreen}</td>
		<td>${command.beansResults.beansInResultsSummary.avgHoled}</td>
		<td>${command.beansResults.beansInResultsSummary.avgBlind}</td>
		<td>${command.beansResults.beansInResultsSummary.avgStains}</td>
		<td>${command.beansResults.beansInResultsSummary.avgDeduction}</td>
		<td>${command.beansResults.beansInResultsSummary.avgFeed}</td>
		<td>${command.beansResults.beansInResultsSummary.avgHead}</td>
	</tr>
</table>
