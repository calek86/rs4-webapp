<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<c:if test="${empty command.beansResults.resultBeansSamples}">
	<div align="center" id="noRecordsDiv">
		<font color="white"><b><br>No beans samples in the system.<br> <br></b> </font>
	</div>
</c:if>
<c:if test="${!empty command.beansResults.resultBeansSamples}">
	<div id="headerRecords">
		<table class="rs4table" border="1">
			<tr>
				<th style="width: 50px;">No.</th>
				<th style="width: 140px;">Date</th>
				<th style="width: 100px;">Customer</th>
				<th style="width: 80px;">Product</th>
				<th style="width: 50px;">Tonnage</th>
				<th style="width: 80px;">Moisture</th>
				<th>Admix</th>
				<th>Temp.</th>
				<th>KgHl</th>
				<th>Process.</th>
				<th>RefNo.</th>
				<th>Screen1</th>
				<th>Screen2</th>
				<th>Holed</th>
				<th>Blind</th>
				<th>Stains</th>
				<th style="width: 100px;">Comments</th>				
			</tr>
		</table>
	</div>
	<div id="loadingDiv">
		<div align="center">
			<font color="grey"><b><br>Loading...<br> <br></b></font>
		</div>
	</div>
	<div id="recordsDiv">
		<table class="rs4table records">
			<c:forEach items="${command.beansResults.resultBeansSamples}"
				var="beansSample">
				<tr>
					<td style="text-align: center; padding-left: 20px; width: 30px;"><c:if
							test="${command.beansResults.accessLevel=='WRITE'}">
							<form:form action="EditBeansSample" method="POST">
								<input type="hidden" name="idToEdit" value="${beansSample.id}">
								<input type="submit" value="" id="edit"
									style="float: left; margin-left: -15px; background-color: #FAFAFA; background-image: url('resources/images/edit-16.png'); border: solid 0px #000000; width: 16px; height: 16px; cursor: pointer; opacity: 1.0;" />
							</form:form>
						</c:if> ${beansSample.id}</td>				
					<td style="width: 140px;">${beansSample.sampleDateDisplay}</td>
					<td style="width: 100px;">${beansSample.customer}</td>
					<td style="width: 80px;">${beansSample.product}</td>
					<td style="width: 50px;">${beansSample.tonnageDisplay}</td>
					<td style="width: 80px;">${beansSample.moistureDisplay}</td>
					<td>${beansSample.admixDisplay}</td>
					<td>${beansSample.temperatureDisplay}</td>
					<td>${beansSample.kgHl}</td>
					<td>${beansSample.processable}</td>
					<td>${beansSample.referenceNo}</td>
					<td>${beansSample.screen1}</td>
					<td>${beansSample.screen2}</td>
					<td>${beansSample.holed}</td>
					<td>${beansSample.blind}</td>
					<td>${beansSample.stains}</td>
					<td style="width: 100px;">${beansSample.comments}</td>					
				</tr>
			</c:forEach>
		</table>
	</div>
</c:if>
