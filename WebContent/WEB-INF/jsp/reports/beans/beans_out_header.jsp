<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<table class="rs4table header" border="1">
	<tr>
		<th>Report Date</th>
		<th>From Date</th>
		<th>To Date</th>
		<th>Customer Name</th>
		<th>Total Net Weight</th>
	</tr>
	<tr>
		<td>${command.beansResults.reportDate}</td>
		<td>${command.beansResults.beansOutResultsSummary.fromDate}</td>
		<td>${command.beansResults.beansOutResultsSummary.toDate}</td>
		<td>${command.beansResults.beansOutResultsSummary.customer}</td>
		<td>${command.beansResults.beansOutResultsSummary.totalNetWeight}</td>
	</tr>
</table>
