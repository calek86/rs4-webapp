<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<c:if test="${empty command.beansResults.resultBeansOutTransactions}">
	<div align="center" id="noRecordsDiv">
		<font color="white"><b><br>No records in the system
				for specified search criteria.<br> <br></b> </font>
	</div>
</c:if>
<c:if test="${!empty command.beansResults.resultBeansOutTransactions}">
	<div id="headerRecords"><jsp:include page="beans_out_header.jsp" />
		<table class="rs4table" border="1">
			<tr>
				<th rowspan="2" style="width: 80px;"><a href="SearchBeans/beansOutExcel"
					style="background-color: #1B8EE0;"><img
						src="resources/images/save-16.png" style="vertical-align: middle"></a>&nbsp;No.</th>
				<th style="width: 140px;">Date</th>
				<th>Customer</th>
				<th>Product</th>				
				<th>Tare</th>
				<th>Gross</th>
				<th>Net.Wt.</th>
				<th>Container Tare</th>
				<th>Contract</th>				
			</tr>
			<tr>
				<th>Inspected</th>
				<th>Destination</th>				
				<th>Haulier</th>				
				<th>Registration</th>
				<th>Seal</th>
				<th>Container</th>
				<th>Release</th>
				<th>LPL</th>
			</tr>
		</table>
	</div>
	<div id="loadingDiv">
		<div align="center">
			<font color="grey"><b><br>Loading...<br> <br></b></font>
		</div>
	</div>
	<div id="recordsDiv">
		<table class="rs4table records">
			<c:forEach items="${command.beansResults.resultBeansOutTransactions}"
				var="beansOutTransaction">
				<tr>
					<td rowspan="2" style="width: 80px;">${beansOutTransaction.transaction.ticketId}</td>
					<td style="width: 140px;">${beansOutTransaction.transaction.transactionDateDisplay}</td>
					<td>${beansOutTransaction.transaction.customer}</td>
					<td>${beansOutTransaction.transaction.product}</td>					
					<td>${beansOutTransaction.transaction.firstWeightDisplay}</td>
					<td>${beansOutTransaction.transaction.secondWeightDisplay}</td>
					<td>${beansOutTransaction.transaction.netWeightDisplay}</td>
					<td>${beansOutTransaction.containerTareDisplay}</td>
					<td>${beansOutTransaction.transaction.exRef}</td>					
				</tr>
				<tr>
					<td>${beansOutTransaction.transaction.inspected}</td>
					<td>${beansOutTransaction.transaction.destination}</td>
					<td>${beansOutTransaction.transaction.haulier}</td>
					<td>${beansOutTransaction.transaction.registration}</td>
					<td>${beansOutTransaction.transaction.trailerNo}</td>
					<td>${beansOutTransaction.transaction.aCCSNo}</td>
					<td>${beansOutTransaction.transaction.orderNo}</td>
					<td>${beansOutTransaction.transaction.comments}</td>
				</tr>
				<tr>
					<td colspan="9" style="background-color: #1B8EE0; height: 0px"></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</c:if>
