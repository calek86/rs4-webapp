<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<c:if test="${empty command.beansResults.resultBeansInTransactions}">
	<div align="center" id="noRecordsDiv">
		<font color="white"><b><br>No records in the system
				for specified search criteria.<br> <br></b> </font>
	</div>
</c:if>
<c:if test="${!empty command.beansResults.resultBeansInTransactions}">
	<div id="headerRecords"><jsp:include page="beans_in_header.jsp" />
		<table class="rs4table" border="1">
			<tr>
				<th style="width: 60px;"><a href="SearchBeans/beansInExcel"
					style="background-color: #1B8EE0;"><img
						src="resources/images/save-16.png" style="vertical-align: middle"></a>
					No.</th>
				<th style="width: 140px;">Date</th>
				<th style="width: 100px;">Customer</th>
				<th style="width: 80px;">Product</th>
				<th style="width: 40px;">Dest.</th>
				<th style="width: 70px;">Haulier</th>
				<th style="width: 70px;">Net.Wt.</th>
				<th style="width: 70px;">Ent.Wt.</th>
				<th>% Loss</th>
				<th>Moisture</th>
				<th>Admix</th>
				<th>Screen</th>
				<th>Holed</th>
				<th>Blind</th>
				<th>Stains</th>
				<th>Ded.</th>
				<th>Feed</th>
				<th>Head</th>
			</tr>
		</table>
	</div>
	<div id="loadingDiv">
		<div align="center">
			<font color="grey"><b><br>Loading...<br> <br></b></font>
		</div>
	</div>
	<div id="recordsDiv">
		<table class="rs4table records">
			<c:forEach items="${command.beansResults.resultBeansInTransactions}"
				var="beansInTransaction">
				<tr>
					<td style="width: 60px;">${beansInTransaction.transaction.ticketId}</td>
					<td style="width: 140px;">${beansInTransaction.transaction.transactionDateDisplay}</td>
					<td style="width: 100px;">${beansInTransaction.transaction.customer}</td>
					<td style="width: 80px;">${beansInTransaction.transaction.product}</td>
					<td style="width: 40px;">${beansInTransaction.transaction.destination}</td>
					<td style="width: 70px;">${beansInTransaction.transaction.haulier}</td>
					<td style="width: 70px;">${beansInTransaction.transaction.netWeightDisplay}</td>
					<td style="width: 70px;">${beansInTransaction.transaction.entWeightDisplay}</td>
					<td>${beansInTransaction.transaction.percentLossDisplay}</td>
					<td>${beansInTransaction.transaction.moistureDisplay}</td>
					<td>${beansInTransaction.transaction.admixDisplay}</td>
					<td>${beansInTransaction.transaction.screen2}</td>
					<td>${beansInTransaction.transaction.holed}</td>
					<td>${beansInTransaction.transaction.blind}</td>
					<td>${beansInTransaction.transaction.stains}</td>
					<td>${beansInTransaction.deductionDisplay}</td>
					<td>${beansInTransaction.feedDisplay}</td>
					<td>${beansInTransaction.headDisplay}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</c:if>
