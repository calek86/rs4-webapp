<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<c:if test="${empty command.transferResults.resultTransfers}">
	<div align="center" id="noRecordsDiv">
		<font color="white"><b><br>No transfers in the system
				yet.<br>
			<br></b> </font>
	</div>
</c:if>
<c:if test="${!empty command.transferResults.resultTransfers}">
	<div id="headerRecords">
		<table class="rs4table" border="1">
			<tr>
				<th style="width: 80px;"><a href="TransferStock/transferList"
					style="background-color: #1B8EE0;"><img
						src="resources/images/save-16.png" style="vertical-align: middle;"></a>
					No.</th>
				<th>Transfer Date</th>
				<th>From Stock</th>
				<th>To Stock</th>
				<th>Reference Number</th>
				<th>Transfer Value</th>
			</tr>
		</table>
	</div>
	<div id="loadingDiv">
		<div align="center">
			<font color="grey"><b><br>Loading...<br> <br></b></font>
		</div>
	</div>
	<div id="recordsDiv">
		<table class="rs4table records">
			<c:forEach items="${command.transferResults.resultTransfers}"
				var="transfer">
				<tr>
					<td style="text-align: center; padding-left: 20px; width: 60px"><c:if
							test="${command.transferResults.accessLevel=='WRITE'}">
							<form:form action="DeleteOneTransfer" method="POST">
								<input type="hidden" name="idToDelete" value="${transfer.id}">
								<input type="submit" value="" id="bi"
									style="float: left; margin-left: -15px; background-color: white; background-image: url('resources/images/delete-16.png'); border: solid 0px #000000; width: 16px; height: 16px; cursor: pointer;" />
							</form:form>
						</c:if> ${transfer.ticketId}</td>
					<td>${transfer.transferDateDisplay}</td>
					<c:forEach var="stock"
						items="${command.transferCriteria.fromStockList}">
						<c:if test="${stock.id==transfer.fromStockId}">
							<td>${stock.stockYearDescription} - ${stock.customerCode} - ${stock.productCode}</td>
						</c:if>
					</c:forEach>
					<c:forEach var="stock"
						items="${command.transferCriteria.toStockList}">
						<c:if test="${stock.id==transfer.toStockId}">
							<td>${stock.stockYearDescription} - ${stock.customerCode} - ${stock.productCode}</td>
						</c:if>
					</c:forEach>
					<td>${transfer.referenceNumber}</td>
					<td>${transfer.transferValueDisplay}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</c:if>
