<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<div id="headerCriteria">
	<form:form action="SearchBeans" method="POST">
		<table class="rs4table header" style="width: 100%">
			<tr>
				<th>Report Type</th>
				<th>Date From</th>
				<th>Customer</th>
				<th>Destination</th>
				<th></th>
			</tr>
			<tr>
				<td align="center"><form:select path="beansCriteria.reportType"
						style="width:98%">
						<c:forEach var="rt" items="${command.beansCriteria.reportTypes}">
							<option value="${rt}"
								<c:if test="${command.beansCriteria.reportType == rt}">selected="selected"</c:if>>
								${rt}</option>
						</c:forEach>
					</form:select></td>
				<td align="center"><form:input path="beansCriteria.dateFrom"
						style="width:94%" type="text" /></td>
				<td align="center"><form:select path="beansCriteria.customers"
						size="1" multiple="false" style="width:98%;">
						<c:if test="${command.beansCriteria.customerCode=='All'}">
							<option
								value="<c:out value="${command.beansCriteria.customerCode}"/>"><c:out
									value="${command.beansCriteria.customerCode}" /></option>
						</c:if>
						<c:if test="${command.beansCriteria.customerCode!='All'}">
							<option
								value="<c:out value="${command.beansCriteria.customerCode}"/>"><c:out
									value="${command.beansCriteria.customerCode}" /></option>
							<option value="All">All</option>
						</c:if>
						<c:forEach var="customer"
							items="${command.beansCriteria.customers}">
							<option value="<c:out value="${customer.code}"/>">
								<c:out value="${customer.code}" />
							</option>
						</c:forEach>
					</form:select></td>
				<td align="center"><form:select
						path="beansCriteria.destinations" size="1" multiple="false"
						style="width:98%">
						<c:if test="${command.beansCriteria.destinationCode=='All'}">
							<option
								value="<c:out value="${command.beansCriteria.destinationCode}"/>"><c:out
									value="${command.beansCriteria.destinationCode}" /></option>
						</c:if>
						<c:if test="${command.beansCriteria.destinationCode!='All'}">
							<option
								value="<c:out value="${command.beansCriteria.destinationCode}"/>"><c:out
									value="${command.beansCriteria.destinationCode}" /></option>
							<option value="All">All</option>
						</c:if>
						<c:forEach var="destination"
							items="${command.beansCriteria.destinations}">
							<option value="<c:out value="${destination.code}"/>">
								<c:out value="${destination.code}" />
							</option>
						</c:forEach>
					</form:select></td>
				<td><input type="submit" value="Search" name="Search"
					id="search" style="width: 98%;" /></td>
			</tr>
			<tr>
				<th>Transaction/Sample No.</th>
				<th>Date To</th>
				<th>Product</th>
				<th>Haulier</th>
				<th></th>
			</tr>
			<tr>
				<td align="center"><form:input path="beansCriteria.transactionsNo" type="text" style="width:98%"
						id="transactionsNo" /></td>
				<td align="center"><form:input path="beansCriteria.dateTo"
						style="width:94%" type="text" /></td>
				<td align="center"><form:select path="beansCriteria.products"
						size="1" multiple="false" style="width:98%">
						<c:if test="${command.beansCriteria.productCode=='All'}">
							<option
								value="<c:out value="${command.beansCriteria.productCode}"/>"><c:out
									value="${command.beansCriteria.productCode}" /></option>
						</c:if>
						<c:if test="${command.beansCriteria.productCode!='All'}">
							<option
								value="<c:out value="${command.beansCriteria.productCode}"/>"><c:out
									value="${command.beansCriteria.productCode}" /></option>
							<option value="All">All</option>
						</c:if>
						<c:forEach var="product" items="${command.beansCriteria.products}">
							<option value="<c:out value="${product.code}"/>">
								<c:out value="${product.code}" />
							</option>
						</c:forEach>
					</form:select></td>
				<td align="center"><form:select path="beansCriteria.hauliers"
						size="1" multiple="false" style="width:98%">
						<c:if test="${command.beansCriteria.haulierCode=='All'}">
							<option
								value="<c:out value="${command.beansCriteria.haulierCode}"/>"><c:out
									value="${command.beansCriteria.haulierCode}" /></option>
						</c:if>
						<c:if test="${command.beansCriteria.haulierCode!='All'}">
							<option
								value="<c:out value="${command.beansCriteria.haulierCode}"/>"><c:out
									value="${command.beansCriteria.haulierCode}" /></option>
							<option value="All">All</option>
						</c:if>
						<c:forEach var="haulier" items="${command.beansCriteria.hauliers}">
							<option value="<c:out value="${haulier.code}"/>">
								<c:out value="${haulier.code}" />
							</option>
						</c:forEach>
					</form:select></td>
				<td><c:if test="${command.beansResults.accessLevel=='WRITE'}">
						<input type="submit" value="Add Sample" name="Add"
							style="width: 98%;" />
					</c:if></td>
			</tr>
		</table>
	</form:form>
</div>
<div>
	<c:if
		test="${command.beansResults.reportType=='BeansIn' || command.beansResults.reportType=='BeansOut'}">
		<c:if test="${command.beansResults.transactionType=='IN'}">
			<jsp:include page="reports/beans/beans_in.jsp" />
		</c:if>
		<c:if test="${command.beansResults.transactionType=='OUT'}">
			<jsp:include page="reports/beans/beans_out.jsp" />
		</c:if>
	</c:if>
	<c:if test="${command.beansResults.reportType=='BeansSample'}">
		<jsp:include page="reports/beans/beans_sample.jsp" />
	</c:if>
</div>




